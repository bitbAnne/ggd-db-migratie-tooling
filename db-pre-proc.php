<?php

// Parse woonverbanden to woonverbanden hist
// proces telefoonnummer fields to extract relatie

include_once("constants.php");
include_once("appconfig.php");

$showProgress = true;
$forceStore = false;
$cmdLine = false;

if (isset($argv)) {
    $cmdLine = true;
    if (count($argv) > 1) {
        foreach ($argv as $arg) {
            if (strtolower($arg) == '--noprogress') {
                $showProgress = false;
            }
            if (strtolower($arg) == '--forcestore') {
                $forceStore = true;
            }
        }
    }
}

$time = trackTime();
$maxrecords = -1; // 1000; // TODO remove after test // -1;  


if ($cmdLine) {
    $prefilledWV = preprocWoonverbanden();
    
    $prefilledTR = preprocTelRelaties();
    
    // If neither woonverbanden hist nor tel relations prefilled,
    // then not anonymized, so proces DossierInfoData and DossierInterventieData
    if ($forceStore || (!$prefilledWV && !$prefilledTR)) {
        // Only use once per new DB dump to check rtf file sizes
        storeDossierInfoDataRTF($time, $maxrecords);
        trackTime($time);
        
        // Only use once per new DB dump to check rtf file sizes
        storeDossierInterventieDataRTF($time, $maxrecords);
        trackTime($time);
    }
    
}

function preprocWoonverbanden() {
    global $DB, $logger;
    $prefilled = false;
    $sqlCheck = "SELECT count(*) as cnt 
                 FROM woonverbanden_hist 
                 WHERE NOT iswvadres = 1";
    $cnt = $DB->get_record($sqlCheck);
    if ($cnt ==false) {
        $logger->error("woonverbanden_hist bestaat niet. Aanmaken historische woonverbanden gestopt!");
        $prefilled = false;
        
    } elseif ($cnt->cnt > 0) {
        $logger->error("woonverbanden_hist bevat al diagitem records. Aanmaken historische woonverbanden gestopt!");
        $prefilled = true;
        
    } else {
        $statusfilters = array("(pg.status = 0 OR pg.status = 5)", "NOT (pg.status = 0 OR pg.status = 5)");
        $turn = 0;
        foreach ($statusfilters as $statusfilter) {
            $sql = "SELECT dosi.casnummer, dd.chardata, dosi.datum as datum, di.bdsnummer, wv.woonverbandid, wv.periodevan, wv.periodetot, 
                           iif(wv.flags = 2 OR wv.flags = 3 OR wv.flags = 130 OR wv.flags = 162, 1, 0) as geheim 
                    FROM woonverbanden wv 
                    JOIN DossierItems dosi ON (wv.casnummer = dosi.Casnummer)
                    JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
                    LEFT JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
                    JOIN persoonsgegevens pg ON (pg.casnummer = dosi.casnummer)
                    WHERE dd.diagitemid in (1641, 1643, 1644, 1645, 1646)
                    AND {$statusfilter}
                    ORDER BY dosi.casnummer desc, dosi.Datum desc";
            
            $turn++;
            $statusText = "Process address data [{$turn}]";
            $verbanden = $DB->get_records($sql);
            if ($verbanden !== false) {
                $cnt = 0;
                $total = count($verbanden);
                $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
                
                $curcasnummer = $curregistryDate = $periodevan = $adres = $geheim = null;
                $histadressen = array();
                $adrcnt = 0;
                foreach ($verbanden as $verband) {
                    $casnummer = $verband->casnummer;
                    if ($casnummer  != $curcasnummer) {
                        // push last adres to addressen stack for the casnummer
                        if ($curregistryDate !== null) {
                            $adres[WV_FLD_PERVAN] = $periodevan;
                            $histadressen[$adrcnt] = $adres;
                        }
                        // Process the adressen
                        if (count($histadressen) > 0) {
                            procHistAddresses($histadressen, $curcasnummer, $woonverbandId);
                        }
                        
                        // reset and repeat
                        $curcasnummer = $casnummer;
                        $woonverbandId = $verband->woonverbandid;
                        $geheim = $verband->geheim;
                        $histadressen = array();
                        $adrcnt = 0;
                        $curregistryDate = null;
                        $periodevan = $verband->periodevan;
                        $periodetot = $verband->periodetot;
                    }
                    
                    $registryDate = $verband->datum;
                    if ($registryDate !== $curregistryDate) {
                        if ($curregistryDate !== null) {
                            $adres[WV_FLD_PERVAN] = $periodevan;
                            $histadressen[$adrcnt] = $adres;
                            $adrcnt++;
                        }
                        if ($adres == null) {
                            $adres = array(WV_BDS_PLAATS => null, WV_BDS_STRAAT => null, WV_BDS_HUISNUMMER => null, WV_BDS_TOEVOEGING => null, 
                                           WV_BDS_POSTCODE => null, WV_FLD_PERTOT => $registryDate, WV_FLD_GEHEIM => $geheim);
                            
                        } else {
                            // next address: clear toevoeging and date
                            $adres[WV_BDS_TOEVOEGING] = null;
                            $adres[WV_FLD_PERTOT] = $registryDate;
                        } 
                        $curregistryDate = $registryDate;
                    }
                    if ($periodevan > $registryDate) {
                        $periodevan = $registryDate;
                    }
                    
                    $bdsnummer = $verband->bdsnummer;
                    $adres[$bdsnummer] = $verband->chardata;
                    
                    $cnt++;
                    show_status($cnt, $total, $statusText, $mcheck); //
                }
                
                // push last adres to addressen stack for the casnummer
                if ($curregistryDate !== null) {
                    $adres[WV_FLD_PERVAN] = $periodevan;
                    $histadressen[$adrcnt] = $adres;
                }
                if (count($histadressen) > 0) {
                    procHistAddresses($histadressen, $curcasnummer, $woonverbandId);
                }
            }
        }
    }
    return $prefilled;
}

function procHistAddresses($histadressen, $casnummer, $woonverbandId) {
    global $DB, $logger;
    $insertSql = "INSERT INTO woonverbanden_hist
                  (casnummer, woonverbandid, straatnaam, huisnummer, huisnummertoev, woonplaats, postcode, periodevan, periodetot, indicatiegeheim, iswvadres)
                  VALUES(:casnummer, :woonverbandid, :straatnaam, :huisnummer, :huisnummertoev, :woonplaats, :postcode, :periodevan, :periodetot, :indicatiegeheim, 0)";
    $updateSql = "UPDATE woonverbanden_hist
                  SET periodevan = :periodevan 
                  WHERE woonverbandid = :woonverbandid AND iswvadres = 1";
    // Process the adressen
    $wvperiodevan = null;
    foreach ($histadressen as $el => $histadres) {
        if (isset($histadressen[$el + 1])) {
            $histadres[WV_FLD_PERVAN] = $histadressen[$el + 1][WV_FLD_PERTOT];
        }
        if ($wvperiodevan == null || $wvperiodevan < $histadres[WV_FLD_PERTOT]) {
            $wvperiodevan = $histadres[WV_FLD_PERTOT];
        }
        $paramsInsert = array('casnummer' => $casnummer, 'woonverbandid' => $woonverbandId,
            'straatnaam' => $histadres[WV_BDS_STRAAT], 'huisnummer' => $histadres[WV_BDS_HUISNUMMER], 'huisnummertoev' => $histadres[WV_BDS_TOEVOEGING],
            'woonplaats' => $histadres[WV_BDS_PLAATS], 'postcode' => $histadres[WV_BDS_POSTCODE],
            'periodevan' => $histadres['periodevan'], 'periodetot' => $histadres[WV_FLD_PERTOT], 'indicatiegeheim' => $histadres[WV_FLD_GEHEIM]);
        $inserted = $DB->execute($insertSql, $paramsInsert);
        if (!$inserted) {
            $error = $DB->getLastError();
            $logger->error("Insert woonverband hist FAILED Error: " . print_r($error, true) . " Params: " . print_r($paramsInsert, true));
        }
    }
    if ($wvperiodevan != null) {
        $paramsUpdate = array('woonverbandid' => $woonverbandId, 'periodevan' => $wvperiodevan);
        $updated = $DB->execute($updateSql, $paramsUpdate);
        if (!$updated) {
            $error = $DB->getLastError();
            $logger->error("Update woonverband hist FAILED Error: " . print_r($error, true) . " Params: " . print_r($paramsUpdate, true));
        }
    }
    
}

function preprocTelRelaties() {
    global $DB, $logger;
    $sqlCheck = "SELECT count(*) as cnt FROM persoonsgegevens WHERE (NOT tel_relatie IS NULL) AND (NOT tel_relatie = '') OR 
                 (NOT tel_relatie2 IS NULL) AND (NOT tel_relatie2 = '') OR 
                 (NOT tel_relatie3 IS NULL) AND (NOT tel_relatie3 = '')";
    $cnt = $DB->get_record($sqlCheck);
    $prefilled = false;
    if ($cnt ==false) {
        $logger->error("persoonsgegevens met tel_relatie bestaan niet. Vullen telefoon relaties gestopt!");
        $prefilled = false;
        
    } elseif ($cnt->cnt > 0) {
        $logger->error("persoonsgegevens bevat al records met tel_relatie*. Vullen telefoon relaties gestopt!");
        $prefilled = true;
    } else {
        
        $sql = "SELECT casnummer, telefoon, telefoon2, telefoon3 FROM persoonsgegevens";
        $sqlUpdate = "UPDATE persoonsgegevens SET telefoon = :tel, telefoon2 = :tel2, telefoon3 = :tel3, tel_relatie = :tel_relatie, tel_relatie2 = :tel_relatie2, tel_relatie3 = :tel_relatie3
                      WHERE casnummer = :casnummer";
        $perstels = $DB->get_records($sql);
        if ($perstels !== false) {
            $statusText = "Parsing telefoon en relaties";
            $total = count($perstels);
            $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
            $cnt = 0;
            
            foreach ($perstels as $telefoonnummers) {
                $casnr = $telefoonnummers->casnummer;
                $tel = $telefoonnummers->telefoon;
                $tel2 = $telefoonnummers->telefoon2;
                $tel3 = $telefoonnummers->telefoon3;
                
                $tel_new = $tel2_new = $tel3_new = $tel_relatie = $tel_relatie2 = $tel_relatie3 = "";
                if (!empty($tel)) {
                    $parsedtelefoon = str_replace(PHONE_REPLACE_SPECIAL_CHARS, '', $tel);
                    list($tel_relatie, $tel_new) = parseRelationFromTel($parsedtelefoon);
                    if (!validPhoneNumber($tel_new)) {
                        $tel_new = "";
                    }
                }
                if (!empty($tel2)) {
                    $parsedtelefoon = str_replace(PHONE_REPLACE_SPECIAL_CHARS, '', $tel2);
                    list($tel_relatie2, $tel2_new) = parseRelationFromTel($parsedtelefoon);
                    if (!validPhoneNumber($tel2_new)) {
                        $tel2_new = "";
                    }
                }
                if (!empty($tel3)) {
                    $parsedtelefoon = str_replace(PHONE_REPLACE_SPECIAL_CHARS, '', $tel3);
                    list($tel_relatie3, $tel3_new) = parseRelationFromTel($parsedtelefoon);
                    if (!validPhoneNumber($tel3_new)) {
                        $tel3_new = "";
                    }
                }
                $params = array('casnummer' => $casnr, 'tel' => $tel_new, 'tel2' => $tel2_new, 'tel3' => $tel3_new, 
                                'tel_relatie' => $tel_relatie, 'tel_relatie2' => $tel_relatie2, 'tel_relatie3' => $tel_relatie3);
                $updated = $DB->execute($sqlUpdate, $params);
                if (!$updated) {
                    $error = $DB->getLastError();
                    $logger->error("Update persoonsgegevens telefoon/relatie FAILED Error: " . print_r($error, true) . " Params: " . print_r($params, true));
                }
                $cnt++;
                show_status($cnt, $total, $statusText, $mcheck); //
                
            }
        }
    }
    return $prefilled;
}