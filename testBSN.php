<?php

include_once("constants.php");
include_once("appconfig.php");

$startBSN =  10000000;
$eindBSN  = 999999999;

$validBSNs = array();

for ($bsn = $startBSN; $bsn <= $eindBSN; $bsn++) {
    $validBSN = isValidBSN($bsn);
    if ($validBSN) {
        $validBSNs[] = $bsn;
        $logger->debug("Valid BSN: [{$bsn}]");
    }
}

$logger->debug(print_r($validBSNs, true));

