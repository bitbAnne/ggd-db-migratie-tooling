<?php 

namespace nl\finalist\db;

/*
 * This class definition contains the code to access a database using PDO style
 * 
 */

/*
 * Class to access MS SQL DB layer using PDO
 * 
 */
class MSSqlDatabaseManager {
  private $dbname = '';
  private $username = '';
  private $password = '';
  private $hostname = '';
  private $dbport;
  
  private $dsn = '';
  
  private $dbh;
  
  private $error = array();
  
  private $options = array(
      \PDO::ATTR_PERSISTENT => true, 
      \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
      \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
  );
  
  /*
   * Create MysqlDatabaseManager using the give parameters
   * 
   * @param $dbname
   * @param $username
   * @param $password
   * @param $hostname [localhost]
   * @param $dbport [3306]
   * @param $options
   */
  public function __construct($dbname, $username = null, $password = null, $hostname = 'localhost', $dbport = -1, $options = null) {
    $this->dbname = $dbname;
    if ($username != null) {
        $this->username = $username;
        if ($password != null) {
            $this->password = $password;
        }
    }
    $this->hostname = $hostname;
    $this->dbport = $dbport;
    if ($dbport == -1) {
        $this->dsn = "sqlsrv:Server=(local);Database=$this->dbname";
        
    } else {
        $this->dsn = "sqlsrv:Server=(local),{$this->dbport};Database=$this->dbname";
        
    }
    
    if (!empty($options)) {
      $this->options = $options;
    }
    
    try {
        // $this->dbh = new \PDO($this->dsn, $this->username, $this->password, $this->options);
        if ($username == null) {
            $this->dbh = new \PDO($this->dsn);
            
        } else {
            $this->dbh = new \PDO($this->dsn, $this->username, $this->password);
            
        }
      
    } catch (\Exception $e) {
      $this->error = $e;
      
    }
  }
  
  /*
   * Get record
  * Returns object 
  *
  * @param $query The SQL query to execute
  * @param $params Array of variables to bind without the leading ':'
  * @param $fetch_style Type of return value [objects]
  *
  * @return Object (or other type when defined), false if none found
  *
  */
  public function get_record($sql, $params = array(), $fetch_style = \PDO::FETCH_OBJ) {
    $obj = false;
    $result = $this->get_records($sql, $params, $fetch_style);
    if ($result !== false) {
      if (is_array($result) && count($result) == 1) {
        $obj = reset($result);
        
      } else {
        throw new \Exception("Single record requested, multiple records found. Query: '{$sql}'");
        
      } 
    }
    return $obj;
  }
  
  
  /*
   * Get records
   * Returns (list of) objects (default)
   * 
   * @param $query The SQL query to execute
   * @param $params Array of variables to bind without the leading ':'
   * @param $fetch_style Type of return value [objects]
   * 
   * @return List of objects (or other type when defined), false if empty or on failure
   * 
   */
  public function get_records($sql, $params = array(), $fetch_style = \PDO::FETCH_OBJ) {
    $result = false;
    $pstmt = $this->dbh->prepare($sql);
    $this->bindParamsByValue($pstmt, $params);
    $executed = $pstmt->execute();
    $result = $pstmt->fetchAll($fetch_style);
    if (is_array($result) && count($result) == 0) {
      $result = false;
    }
    return $result;
  }
  
  /*
   * Bind parameters by valye
   * 
   * @param $pstmt The prepared statement to bind te parameters to
   * @param $params The array of parameters that will be bound to the prepared statement
   * 
   * @return $pstmt The prepared statement wiht the params bound to it
   */
  private function bindParamsByValue($pstmt, $params, $supportBlob = false) {
    if (isset($pstmt) && isset($params) && is_array($params)) {
      foreach ($params as $placeholder => $param) {
        $type = \PDO::PARAM_STR;
        if ($supportBlob && startsWith($placeholder, 'blob')) {
            $type = \PDO::PARAM_LOB;
            
        } else {
            if (is_int($param)) {
                $type = \PDO::PARAM_INT;
                
            } else if (is_bool($param)) {
                $type = \PDO::PARAM_BOOL;
                
            } else if (is_null($param)) {
                $type = \PDO::PARAM_NULL;
                
            }
        }
        $placeholder = ':' . $placeholder;
        $pstmt->bindValue($placeholder, $param, $type);
      }
    }
    return $pstmt; 
  }
  
  /*
   * Execute query
   * Returns (list of) objects
   * 
   * @param $query The SQL query to execute
   * @param $params Array of variables to bind without the leading ':'
   * 
   * @return $executed If query is executed
   * 
   */
  public function execute($sql, $params = array(), $supportBlob = false, $forceMSSQL8bit = false) {
    $executed = false;
    try {
      $pstmt = $this->dbh->prepare($sql);
      $this->bindParamsByValue($pstmt, $params, $supportBlob);
      if ($forceMSSQL8bit) {
          $pstmt->setAttribute(\PDO::SQLSRV_ATTR_ENCODING, \PDO::SQLSRV_ENCODING_SYSTEM);
      }
      $executed = $pstmt->execute();
      if (!$executed) {
          $this->error = $pstmt->errorInfo();
          
      }
      
    } catch (\PDOException $pdoe) {
      throw new \PDOException("Failed to execute statement: '{$sql}' with parameters '" . print_r($params, true). "': " . $pdoe->getMessage());
      
    } catch (\Exception $e) {
      throw new \Exception("Failed to execute statement: '{$sql}' with parameters '" . print_r($params, true). "': " . $e->getMessage());
      
    }
    return $executed;
  }
  
  /*
   * Execute Insert query
  * Returns (list of) objects
  *
  * @param $query The SQL insert query to execute
  * @param $params Array of variables to bind without the leading ':'
  *
  * @return $executed Insert was succesful
  *
  */
  public function insert($sql, $params = array()) {
    $id = false;
    $executed = $this->execute($sql, $params);
    if ($executed) {
      $id = $this->dbh->lastInsertId();
    }
    return $id;
  }
  
  
  /*
  * Execute query
  * Returns (list of) objects
  *
  * @param $query The SQL query to execute
  *
  * @return $result The result set False if none
  *
  */
  public function query($query) {
    return $this->dbh->query($query);
  }
  
  /*
   * Count number of records in table of tables
   * 
   * @param $tables comma seperated list of table(s)
   * @param $where optional where clause to use, to limit records or link tables
   * 
   * @return $count The counted number of records or -1 if failed
   */
  public function count($tables, $where = "") {
    $countquery = "SELECT COUNT(*) FROM {$tables}";
    if (!empty($where)) {
      $countquery .= " WHERE {$where}";
    }
    $count = $this->countQuery($countquery);
    return $count; 
  }
  
  /*
   * Count number of records in query result
  *
  * @param $query The count query to execute
  *
  * @return $count The counted number of records or -1 if failed
  */
  public function countQuery($countquery) {
    $count = -1;
    $result = $this->query($countquery);
    if ($result !== false) {
      $count = $result->fetchColumn();
    }
    return $count;
  }
  
  public function getLastError() {
      return $this->error;
  }
  
}


/*
 * Initialize DB using the given parameters
 * Returns global variable $DB which can be used to execute queries using PDO
 *
 * @param $dbname
 * @param $username
 * @param $password
 * @param $hostname [localhost]
 * @param $dbport [3306]
 * @param $options
 *
 */
function initDB($dbname, $username, $password, $hostname = 'localhost', $dbport = -1, $options = null) {
    $lclDB = new MSSqlDatabaseManager($dbname, $username, $password, $hostname, $dbport, $options);
    return $lclDB;
}


