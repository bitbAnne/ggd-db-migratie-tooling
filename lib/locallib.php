<?php

// Define global variable $logger as reference to log4php framework
include ('log4php/Logger.php');
Logger::configure($APPCFG->dirroot . '/ggd-db-tooling-log4php-config.xml');
$logger = Logger::getLogger("mainLogger");

function doReDirUrl($constructfullurl = false) {
    $redirurl = getReDirUrl($constructfullurl);
    header('Location: ' . $redirurl);
    flush();
    exit(0);
}

/*
 * Construct a url, adding / where needed
 *
 * @param $urlparts Array of url parts to construct the url from
 *
 * @return $url The constructed url
 */
function constructUrl($urlparts) {
    $url = "";
    if (isset($urlparts) && is_array($urlparts)) {
        foreach ($urlparts as $urlpart) {
            if (! empty($url) && ! startsWith($urlpart, "/") && ! endsWith($url, "/")) {
                $url .= "/";
            }
            $url .= $urlpart;
        }
    }
    return $url;
}

/*
 * Check if string starts with specific character
 *
 * @param $haystack The string to search in
 * @param $needle The string to search for
 *
 * @return $needle empty or found
 */
function startsWith($haystack, $needle) {
    return $needle === "" || strpos($haystack, $needle) === 0;
}

/*
 * Check if string ends with specific character
 *
 * @param $haystack The string to search in
 * @param $needle The string to search for
 *
 * @return $needle empty or found
 */
function endsWith($haystack, $needle) {
    return $needle === "" || substr($haystack, - strlen($needle)) === $needle;
}

/*
 * Retrieve optional paramters
 * Check if name is in POST, GET or REQUEST if so use it
 * If not use the default value
 *
 * @param $paramnem The name of the optional parameter
 * @param $defaultvalue The default value to use
 *
 * @return $returnvalue The value from a parameter or if not found, the default value
 *
 */
function optional_param($paramname, $defaultvalue) {
    $returnvalue = $defaultvalue;
    if (isset($_POST[$paramname]) && ($_POST[$paramname] !== '')) {
        $returnvalue = $_POST[$paramname];
    } elseif (isset($_GET[$paramname]) && ($_GET[$paramname] !== '')) {
        $returnvalue = $_GET[$paramname];
    } elseif (isset($_REQUEST[$paramname]) && ($_REQUEST[$paramname] !== '')) {
        $returnvalue = $_REQUEST[$paramname];
    }
    return $returnvalue;
}

/*
 * Return csv file to be downloaded
 *
 * @param $filename The name of the CSV file that will be downloaded
 * @param $header The header of the CSV file
 * @param $cvslines The lines of the CSV file
 *
 * @return There is no returning from this function (die(0))
 */
function streamCSV($filename, $header, $cvslines) {
    $csvoutput = null;

    // output headers so that the file is downloaded rather than displayed
    header("Content-Type: text/csv; charset=utf-8");
    header("Content-Disposition: attachment; filename={$filename}");

    // create a file pointer connected to the output stream
    $csvoutput = fopen('php://output', 'w');

    $csvcolumns = count($cvslines[0]);

    $csvheader = array_fill(0, $csvcolumns, "");
    $csvheader[0] = $header;
    fputcsv($csvoutput, $csvheader, ";");

    foreach ($cvslines as $csvline) {
        fputcsv($csvoutput, $csvline, ";");
    }
    die(0);
}

function isValidBSN($bsnr) {
    $isValid = false;
    $res = 0;
    $verm = strlen($bsnr);
    if ($verm >= 8 && $verm <=9) {
        for ($i = 0; $i < strlen($bsnr); $i++, $verm--) {
            if ($verm == 1) {
                $verm = -1;
            }
            $res += substr($bsnr, $i, 1) * $verm;
        }
        $isValid = (($res % 11) === 0);
    }
    return $isValid;
}

/**
 Validate an email address.
 Provide email address (raw input)
 Returns true if the email address has the email
 address format and the domain exists.
 */
function validEmail($email, $checkReverseDNS = true) {
    global $logger;
    $isValid = true;
    $atIndex = strrpos($email, "@");
    if (is_bool($atIndex) && !$atIndex)
    {
        $isValid = false;
    }
    else
    {
        $domain = substr($email, $atIndex+1);
        $local = substr($email, 0, $atIndex);
        $localLen = strlen($local);
        $domainLen = strlen($domain);
        if ($localLen < 1 || $localLen > 64)
        {
            // local part length exceeded
            $isValid = false;
        }
        else if ($domainLen < 1 || $domainLen > 255)
        {
            // domain part length exceeded
            $isValid = false;
        }
        else if ($local[0] == '.' || $local[$localLen-1] == '.')
        {
            // local part starts or ends with '.'
            $isValid = false;
        }
        else if (preg_match('/\\.\\./', $local))
        {
            // local part has two consecutive dots
            $isValid = false;
        }
        else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
        {
            // character not valid in domain part
            $isValid = false;
        }
        else if (preg_match('/\\.\\./', $domain))
        {
            // domain part has two consecutive dots
            $isValid = false;
        }
        else if
        (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
            str_replace("\\\\","",$local)))
        {
            // character not valid in local part unless
            // local part is quoted
            if (!preg_match('/^"(\\\\"|[^"])+"$/',
                str_replace("\\\\","",$local)))
            {
                $isValid = false;
            }
        }
        if ($isValid && $checkReverseDNS && !(checkdnsrr($domain,"MX") ||
            checkdnsrr($domain,"A")))
        {
            // domain not found in DNS
            $isValid = false;
        }
    }
    return $isValid;
}

function checkArchiefDocTypes() {
    global $DBC, $logger;

    $extensionTypes = array();
    $extensionTypes[''] = 0;
    $noext = 0;
    $filenames = $DBC->get_records("SELECT FileName FROM Archief");
    if ($filenames !== false) {
        $total = count($filenames);
        foreach ($filenames as $filename) {
            $fn = trim($filename->FileName);
            $fnlastbit = strtolower(strrchr($fn, '.'));
            if ($fnlastbit == null) {
                $extensionTypes['']++;

            } else {
                if (!array_key_exists ($fnlastbit, $extensionTypes)) {
                    $extensionTypes[$fnlastbit] = 1;

                } else {
                    $extensionTypes[$fnlastbit]++;
                }
            }
        }

        $logger->info("Archief tabel bevat {$total} records met de volgende bestands typen:");
        foreach ($extensionTypes as $key => $value) {
            if ($key === '') {
                $key = "GEEN";
            }
            $logger->info("{$key}; {$value}");
        }
    }
}

function checkDuplicateFilenames() {
    global $DBC, $logger;
    $sqlcnt = "SELECT * FROM (
               SELECT filename, count(filename) AS cnt FROM archief
               GROUP BY filename) cntfilename
               WHERE cnt > 1
               ORDER BY cnt desc";

    $duplicates = $DBC->get_records($sqlcnt);
    if ($duplicates !== false) {
        $logger->info("Filenames that exist more than once in Archief:");
        foreach ($duplicates as $duplicate) {
            $logger->info("#; {$duplicate->cnt}; Filename; {$duplicate->filename}");
        }
    }
}

function checkBSNs($fieldName = "bsn") {
    global $DB, $logger;
    $valid = 0;
    $invalid = 0;
    $sql = "SELECT count(*) FROM Persoonsgegevens WHERE {$fieldName} IS NULL OR {$fieldName} = ''";
    $cntnobsn = $DB->countQuery($sql);
    $sql = "SELECT casNummer, {$fieldName} FROM Persoonsgegevens WHERE NOT {$fieldName} IS NULL AND NOT {$fieldName} = ''";
    $bsns = $DB->get_records($sql);
    if ($bsns !== false) {
        $cntbsns = count($bsns);
        $logger->info("Proces {$cntbsns} records with field: {$fieldName}");
        foreach ($bsns as $bsn) {
            $casNr = $bsn->casNummer;
            $bsnNummer = $bsn->$fieldName;
            if (!isValidBSN($bsnNummer)) {
                $logger->info("Invalid {$fieldName}; {$bsnNummer}; Casnr; {$casNr}");
                $invalid++;
            } else {
                $valid++;

            }
        }
    }
    $logger->info("{$fieldName}'s Persoonsgegevens: Valid:  [{$valid}], Invalid: [{$invalid}]");
    if ($cntnobsn !== 0) {
        $logger->info("No {$fieldName}'s Persoonsgegevens: [{$cntnobsn}]");
    }
}

function shuffleEmail() {
    global $DB, $logger, $showProgress;
    $start = new DateTime();
    $invalidEml = $validEml = $noEml = $noNewEml = $validSpecChar = 0;
    $newEmails = array();
    $invalidEmls = array();
    $validEmlsWithSpecChar = array();
    $fieldInfo = array();
    $fieldInfo[0] = 'email';
    $fieldInfo[1] = 'email';
    // $fieldInfo[0] = 'email2';
    // $fieldInfo[1] = 'email2';

    $sql = "select casNummer, email, email2 from persoonsgegevens";
    $personen = $DB->get_records($sql);
    if ($personen  !== false) {
        $total = count($personen);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $done = 0;
        $logger->info("Processing {$total} records");
        foreach ($personen as $persoon) {
            $casnr = $persoon->casNummer;

            $oldFieldName = $fieldInfo[0];
            $newFieldName = $fieldInfo[1];

            $email = $persoon->$oldFieldName;
            $newEmail1 = $newEmail = "";
            if (!validEmail($email)) {
                if (empty($email)) {
                    $noEml++;
                } else {
                    $invalidEml++;
                    $invalidEmls[] = $email;
                }

            } else {
                $validEml++;

                $replacementEmail = generateAnonymousEmail($email);
                if (empty($replacementEmail)) {
                    $noNewEml++;

                } else {
                    $newEmails[] = $replacementEmail;
                    $updatesql = "UPDATE an_cross SET {$newFieldName} = :newEmail WHERE casNummer = :casNummer";
                    $update = $DB->execute($updatesql, array('newEmail' => $replacementEmail, 'casNummer' => $casnr));
                    if ($update) {
                        $logger->debug("Updated field {$newFieldName} record for casNummer: [{$casnr}] with an_email: [{$replacementEmail}]");

                    } else {
                        $logger->debug("Updated FAILED field {$newFieldName} record for casNummer: [{$casnr}] with an_email: [{$replacementEmail}]");

                    }
                }
            }
            $done++;
            if ($showProgress) {
                show_status($done, $total, "Shuffle Eamil", $mcheck);
            }
        }
        foreach ($invalidEmls as $invalidEmail) {
            $logger->debug("Invalid eml; {$invalidEmail} for casnr; {$casnr}; in field {$newFieldName}");
        }
        $logger->info("Processed valid: [{$validEml}], empty: [{$noEml}], invalid: [{$invalidEml}] email records New eml fail: [{$noNewEml}]");
    }
    $logger->info("Run time: " . displayTime($start));
}

function displayTime($start) {
    global $logger;
    $sinceStart = $start->diff(new DateTime());
    $displayTime = "";
    $displayTime .= $sinceStart->d.' days ';
    $displayTime .= $sinceStart->h.' hours ';
    $displayTime .= $sinceStart->i.' minutes ';
    $displayTime .= $sinceStart->s.' seconds ';
    return $displayTime;
}

function shuffleText($orgtext, $forceSpecCharReuse = false, $splitText = false) {
    global $logger;
    $shuffledText = "";

    $kommas = substr_count($orgtext, ",");

    $base = "";
    $match1 = $match2 = $match3 = $match4 = $match5 = array();

    $base = "";
    $text2Shuffle = $orgtext;
    preg_match_all(TXT_REGEXP_LOWERCASE, $text2Shuffle, $match1);
    preg_match_all(TXT_REGEXP_UPPERCASE, $text2Shuffle, $match2);
    preg_match_all(TXT_REGEXP_UTF8, $text2Shuffle, $match3);
    preg_match_all(TXT_REGEXP_DIGITS, $text2Shuffle, $match4);
    preg_match_all(TXT_REGEXP_SPECIALCHARS, $text2Shuffle, $match5);
    if (count($match1[0]) > 0) {
        $base .= shuffleMatch($match1[0], TXT_LOWERCASE);
    }
    if (count($match2[0]) > 0) {
        $base .= shuffleMatch($match2[0], TXT_UPPERCASE);
    }
    if (count($match3[0]) > 0) {
        $base .= shuffleMatch($match3[0], TXT_UTF8);

    }
    if (count($match4[0]) > 0) {
        $base .= shuffleMatch($match4[0], TXT_DIGITS);
    }
    $cntm5 = count($match5[0]);
    if ($cntm5 > 0) {
        if (!$forceSpecCharReuse) {
            $base .= shuffleMatch($match5[0], TXT_SPECIALCHARS);
        }
    }
    if ($splitText && $kommas > 0) {
        $baselen = strlen($base);
        $base = str_pad($base, ($baselen + $kommas), ",");
    }
    $shuffledText .= str_shuffle($base);
    if ($forceSpecCharReuse) {
        if ($cntm5 > 0) {
            $pos = 0;
            $newText = substr($shuffledText, $pos, 1);
            $pos++;
            foreach ($match5[0] as $match5) {
                $newText .= $match5;
                $newText .= substr($shuffledText, $pos, 1);
                $pos++;
            }
            $shuffledText = $newText . substr($shuffledText, $pos);
        }
    }
    return $shuffledText;
}



function shuffleMatch($match, $type) {
    global $logger;
    $shuffledText = $base = "";
    if ($type == TXT_LOWERCASE) {
        $base = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
    } else if ($type == TXT_UPPERCASE) {
        $base = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
    } else if ($type == TXT_UTF8) {
        $base = "äöüÄÖÜß";
    } else if ($type == TXT_DIGITS) {
        $base = "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";
    } else if ($type == TXT_SPECIALCHARS) {
        $base = "!#$%&*/-+?^~{}|]";
    }
    if (!empty($base)) {
        $shuffledText = str_shuffle($base);
        $matchLen = 0;
        foreach ($match as $matched) {
            $matchLen += strlen($matched);
        }
        $shuffledText = substr($shuffledText, 0, $matchLen);
    }
    return $shuffledText;
}

function generateAnonymousEmail($email) {
    global $logger;
    $replacementEmail = "";

    $atpos = strpos($email, "@");
    if ($atpos > 0) {

        $preat = substr($email, 0, $atpos);
        $postat = substr($email, $atpos + 1);
        $tdpos = strrchr($postat, ".");
        $postatpretd = str_replace($tdpos, "", $postat);
        if ($tdpos !== null) {

            $nonstdpre = preg_replace(EML_REGEXP_ALL, '', $preat);
            $nonstdpostatpretd = preg_replace(EML_REGEXP_ALL, '', $postatpretd);
            if (($nonstdpre !== $preat) || ($nonstdpostatpretd !== $postatpretd)) {
                if ($nonstdpre !== $preat) {
                    $logger->debug("Nonstd preAt: [{$preat}] [{$nonstdpre}] in email: [{$email}]");
                }
                if ($nonstdpostatpretd !== $postatpretd) {
                    $logger->debug("Nonstd postAt: [{$postatpretd}] [{$nonstdpostatpretd}] in email: [{$email}]");
                }
                // $validEmlsWithSpecChar[] = $email;
                // $validSpecChar++;
            }

            $newpreat = shuffleText($preat, true);
            $newpostatpretd = shuffleText($postatpretd, true);
            $newposttd = shuffleText(substr($tdpos, 1));
            $replacementEmail = "{$newpreat}@{$newpostatpretd}.{$newposttd}";
            if (!validEmail($replacementEmail, false)) {
                // $noNewEml ++;
                $chk = validEmail($replacementEmail, false);
            }
        }
    }
    return $replacementEmail;
}

function generateAnonymousPhn($phoneNumber) {
    global $logger;
    $replacementPhone = "";
    $preDigits = "";
    $postDigits = $phoneNumber;
    $phnLen = strlen($phoneNumber);
    if ($phnLen > 10) {
        $preDigits = substr($phoneNumber, 0, $phnLen - 10 + 1);
        $postDigits = substr($phoneNumber, - 10 + 1);
    }
    $nonstdphn = preg_replace(EML_REGEXP_ALL, '', $postDigits);
    $replacementPhone = shuffleText($nonstdphn, true);
    $replacementPhone = $preDigits . $replacementPhone;
    if (!validPhoneNumber($replacementPhone, false)) {
        // $noNewPhn ++;
        $chk = validEmail($replacementPhone, false);
    }
    return $replacementPhone;
}


function shufflePhonenumbers($updateANC = true, $mlcas20 = false) {
    global $DB, $logger, $showProgress;
    $start = new DateTime();
    $invalidPhn = $validPhn = $noPhn = $noNewPhn = 0;
    $newPhns = array();
    $invalidPhns = array();
    $lastChars = array();
    $invalidChars = array();
    $relations = array();

    $fieldInfo = array();
    $fieldInfo[0] = array('Telefoon', 'an_telefoon');
    $fieldInfo[1] = array('Telefoon2', 'an_telefoon2');
    $fieldInfo[2] = array('Telefoon3', 'an_telefoon3');
    
    if ($mlcas20) {
        $fieldInfo[3] = array('Telefoon4', 'an_telefoon4');
        $fieldInfo[4] = array('Telefoon5', 'an_telefoon5');
    }
    
    $sql = "select casNummer";
    foreach ($fieldInfo as $key => $value) {
        $fieldName = $value[0];
        $sql .= ", {$fieldName}";
    }
    $sql .= " from persoonsgegevens";
    // TelefoonOms, TelefoonOms2, TelefoonOms3, TelefoonOms4, TelefoonOms5
    $personen = $DB->get_records($sql);
    if ($personen  !== false) {
        $total = count($personen);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $done = 0;
        $logger->info("Processing {$total} records");
        foreach ($personen as $persoon) {
            $casnr = $persoon->casNummer;
            foreach ($fieldInfo as $key => $value) {
                $oldFieldName = $value[0]; // $fieldInfo[0];
                $newFieldName = $value[1]; // $fieldInfo[1];
            $tel = $persoon->$oldFieldName;
            $orgtel = $tel;

            // pre-process phonenumber
            if (!empty($tel)) {
                $tel = str_replace(PHONE_REPLACE_SPECIAL_CHARS, '', $tel);
                $orgtel = $tel;

                list($relation, $tel) = parseRelationFromTel($tel);

                if (!empty($tel)) {
                    $lastChar = substr($tel, -1);
                    if (!array_key_exists($lastChar, $lastChars)) {
                        $ord = ord($lastChar);
                        $lastChars[$lastChar] = 0;
                    }
                    $lastChars[$lastChar]++;
                }
            }

            if (!validPhoneNumber($tel)) {
                if (empty($tel)) {
                    $noPhn++;
                } else {
                    $invalidPhn++;
                    $invalidPhns[$casnr] = $orgtel;
                    $invalidChar = preg_replace(PHONE_REXEXP, '', $tel);
                    if ($invalidChar !== "") {
                        if (!array_key_exists($invalidChar, $invalidChars)) {
                            $invalidChars[$invalidChar] = 0;
                        }
                        $invalidChars[$invalidChar]++;
                    }
                    if (!empty($relation)) {
                        $logger->debug("Field {$newFieldName} record for casNummer: [{$casnr}] still invalid after removing relation info: [{$tel}] [{$orgtel}]");
                        $relation = "";
                        $tel = $orgtel;
                        if ($invalidChar !== "") {
                            $invalidChars[$invalidChar]--;
                        }
                        $invalidChar = preg_replace(PHONE_REXEXP, '', $tel);
                        if ($invalidChar !== "") {
                            if (!array_key_exists($invalidChar, $invalidChars)) {
                                $invalidChars[$invalidChar] = 0;
                            }
                            $invalidChars[$invalidChar]++;
                        }
                    }
                }

            } else {
                $validPhn++;
                if (!empty($relation)) {
                    if (!array_key_exists($relation, $relations)) {
                        $relations[$relation] = 0;
                    }
                    $relations[$relation]++;
                }

                $replacementPhn = generateAnonymousPhn($tel);
                if (empty($replacementPhn)) {
                    $noNewPhn++;

                } else {
                    $newPhns[] = $replacementPhn;
                    $update = false;
                    if ($updateANC) {
                        $updatesql = "UPDATE an_cross SET {$newFieldName} = :newPhn WHERE casNummer = :casNummer";
                        $update = $DB->execute($updatesql, array('newPhn' => $replacementPhn, 'casNummer' => $casnr));
                        if (!$update) {
                            $logger->debug("Updated FAILED field {$newFieldName} record for casNummer: [{$casnr}] with Phn: [{$replacementPhn}]");
                            
                        }
                    }
                    if (!$updateANC || $update) {
                        $logger->debug("Updated field {$newFieldName} record for casNummer: [{$casnr}] with Phn: [{$replacementPhn}]");
                    }
                    
                }
            }
            }
            
            $done++;
            if ($showProgress) {
                show_status($done, $total, "Shuffle Phonenumbers", $mcheck);
            }
        }
        foreach ($invalidPhns as $casnr => $invalidPhones) {
            $logger->debug("Invalid Phn; {$invalidPhones}; for casnr; {$casnr}; in field {$oldFieldName}");
        }
        foreach ($lastChars as $lastChar => $lastCharCnt) {
            $ord = ord($lastChar);
            $logger->debug("Last char; {$lastChar}; ord; {$ord}; cnt; {$lastCharCnt}");

        }
        $sorted = arsort($invalidChars);
        foreach ($invalidChars as $invalidChar => $invalidCharCnt) {
            if ($invalidCharCnt > 0) {
                if (strlen($invalidChar) == 1) {
                    $ord = ord($invalidChar);
                    $logger->debug("Invalid chars; {$invalidChar}; cnt; {$invalidCharCnt}; ord; {$ord}");
                } else {
                    $logger->debug("Invalid chars; {$invalidChar}; cnt; {$invalidCharCnt}");

                }
            }
        }
        foreach ($relations as $relationName => $relationCnt) {
            $logger->debug("Relatie; {$relationName}; cnt; {$relationCnt}; gevonden.");
        }
        $logger->info("Processed total: [{$total}] fields: " . count($fieldInfo) . "  valid: [{$validPhn}], empty: [{$noPhn}], invalid: [{$invalidPhn}] Phone records New Phn fail: [{$noNewPhn}]");
    }
    $logger->info("Run time: " . displayTime($start));
}

/*
 * Controleer of er relatie informatie in het telefoonnummer zit.
 *
 * Omdat sommige checks positief zijn gebaseerd op 1 letter,
 * moeten eerst de checks die hele worodne bevatten worden uitgevoerd
 * daarnaa pas de checks met enkele letters
 *
 */
function parseRelationFromTel($tel) {
    $relation = "";
    $ltel = strtolower($tel);

    $tel = str_replace(PHONE_REPLACE_ANDERERELATIE_FULL, '', $ltel);
    if ($tel != $ltel) {
        $relation = PHONE_RELATIE_ANDERS;

    } else {
        $tel = str_replace(PHONE_REPLACE_MOEDER_FULL, '', $ltel);
        if ($tel != $ltel) {
            $relation = PHONE_RELATIE_MOEDER;

        } else {
            $tel = str_replace(PHONE_REPLACE_VADER_FULL, '', $ltel);
            if ($tel != $ltel) {
                $relation = PHONE_RELATIE_VADER;

            } else {
                $tel = str_replace(PHONE_REPLACE_ANDERERELATIE_LTR, '', $ltel);
                if ($tel != $ltel) {
                    $relation = PHONE_RELATIE_ANDERS;

                } else {
                    $tel = str_replace(PHONE_REPLACE_MOEDER_LTR, '', $ltel);
                    if ($tel != $ltel) {
                        $relation = PHONE_RELATIE_MOEDER;

                    } else {
                        $tel = str_replace(PHONE_REPLACE_VADER_LTR, '', $ltel);
                        if ($tel != $ltel) {
                            $relation = PHONE_RELATIE_VADER;
                        }
                    }
                }
            }
        }
    }
    $tel = trim($tel);
    return array($relation, $tel);
}

function validPhoneNumber($phoneNumber) {
    $isValid = false;
    $invalidChar = preg_replace(PHONE_REXEXP, '', $phoneNumber);
    if ($invalidChar == '') {
        if (startsWith($phoneNumber, '+31')) {
            // check length 12
            $len = strlen($phoneNumber);
            $isValid = ($len == 12);

        } else if (!startsWith($phoneNumber, '+')) {
            // check length 10
            $len = strlen($phoneNumber);
            $isValid = ($len == 10);

        }
    }
    return $isValid;
}

function generateBSN($cntorgbsn) {
    $mcheck = (int)($cntorgbsn / (100 / SHOW_STATUS_MOD));
    $newbsnnrs = array();
    $startBSN =  10000000;
    $eindBSN  = 999999999;
    if ($cntorgbsn != null && $cntorgbsn > 0) {
        $start = rand($startBSN, $eindBSN);
        $cntBsn = 0;
        for ($newbsn = $start; $newbsn <= $eindBSN; $newbsn++) {
            if (isValidBSN($newbsn)) {
                $newbsnnrs[] = $newbsn;
                $cntBsn++;
                if ($cntBsn >= $cntorgbsn) {
                    break;
                }
                show_status($cntBsn, $cntorgbsn, "Generate BSN", $mcheck);
            }
        }
        if ($cntBsn < $cntorgbsn) {
            for ($newbsn = $start; $newbsn >= $startBSN; $newbsn--) {
                if (isValidBSN($newbsn)) {
                    $newbsnnrs[] = $newbsn;
                    $cntBsn++;
                    if ($cntBsn >= $cntorgbsn) {
                        break;
                    }
                    show_status($cntBsn, $cntorgbsn, "Generate BSN", $mcheck);
                }
            }
        }
    }
    return $newbsnnrs;
}

function generateAnummer($total, $min = 1, $max = -1) {
    global $DB, $logger;
    if ($max == -1) {
        $max = $total;
    }
    $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
    $imin = (int)$min;
    $imax = (int)$max;
    $factor = $max/$imax;
    $imin = $min / $factor;
    $anummers = array();
    $cnt = 0;
    while ($cnt < $total) {
        $basenum = rand($imin, $imax);
        $newnum = round($basenum * $factor);
        if (($newnum < $min) || ($newnum > $max)) {
            $logger->debug("{$newnum} does not meet min/max requirement");
            continue;
        }
        if (!in_array($newnum, $anummers)) {
            $anummers[] = $newnum;
            // $logger->debug("anummers[{$cnt}] Added {$newnum}");
            $cnt++;
            show_status($cnt, $total, "Generate New Anummers", $mcheck);
        }
    }
    return $anummers;
}


function rndDayOffset() {
    $offset =  rand(-15, 15);
    return $offset;
}


function getClonedShiftedDTasString($dateTimeObject, $diOffset, $addOffset = true) {
    $retDT = null;
    $newDateTimeObject = $dateTimeObject;
    if (!empty($dateTimeObject)) {
        $newDateTimeObject = clone $dateTimeObject;
        if ($addOffset) {
            $newDateTimeObject->add($diOffset);
        } else {
            $newDateTimeObject->sub($diOffset);
        }
        $retDT = $newDateTimeObject->format('Y-m-d H:i:s');
    }
    return $retDT;
}


function trackTime($startTime = -1, $interval = false) {
    global $logger;
    $time = null;
    if ($startTime == -1) {
        $time = time();
        $stime = date("H i s", $time);
        $logger->info("Start: {$stime} - DB: " . DB_DATABASENAAM . "/" . DB_ARCH_DATABASENAAM);
    } else {
        $endtime = time();
        $timediff = $endtime - $startTime;
        if ($timediff < 3600) {
            $tds = date("i\m\i\\n s\s\\e\c", $timediff);
        } else {
            $hour = floor($timediff / 3600);
            $min  = floor(($timediff - $hour * 3600) / 60);
            $sec = $timediff - $hour * 3600 - $min * 60;
            $tds = "{$hour}u {$min}min {$sec}sec";
        }

        if (!$interval) {
            $logger ->info("Total process time: {$tds} - DB: " . DB_DATABASENAAM . "/" . DB_ARCH_DATABASENAAM);
        } else {
            $logger ->info("Interval : {$tds}");
        }
    }
    return $time;
}

function checkAnumbers() {
    global $DB, $logger;
    $valid = 0;
    $invalid = 0;
    $sql = "SELECT count(*) FROM Persoonsgegevens WHERE anummer IS NULL OR anummer = ''";
    $cntnobsn = $DB->countQuery($sql);
    $sql = "SELECT casNummer, anummer FROM Persoonsgegevens WHERE NOT anummer IS NULL AND NOT anummer = ''";
    $anummers = $DB->get_records($sql);
    if ($anummers !== false) {
        $cntanummers = count($anummers);
        $logger->info("Anummer check. Proces {$cntanummers} records.");
        foreach ($anummers as $anummer) {
            $casNr = $anummer->casNummer;
            $anmr = $anummer->anummer;
            if (!is_numeric($anmr)) {
                $logger->info("Invalid anummer; {$anmr}; Casnr; {$casNr}");
                $invalid++;
            } else {
                $valid++;

            }
        }
    }
    $logger->info("Anummers Persoonsgegevens: Valid:  [{$valid}], Invalid: [{$invalid}]");
    if ($cntnobsn !== 0) {
        $logger->info("No Anummers Persoonsgegevens: [{$cntnobsn}]");
    }


}

function cleanAnCrossTable() {
    global $DB, $logger;
    $logger->info("Clear records from an_cross");
    $sql = "DELETE FROM an_cross";
    $deleted = $DB->execute($sql);
    if ($deleted) {
        $logger->info("Success");
    }
}

function fillAnCrossCasnummers() {
    global $DB, $logger;
    $logger->info("Fill records with casnummer from persoonsgegevens");
    $sql = "INSERT INTO an_cross (casnummer)
            (SELECT casnummer from persoonsgegevens)";
    $success = $DB->execute($sql);
    if ($success) {
        $logger->info("Success");
    }
}


function getGebLandCodesList() {
    global $DB, $logger;
    $gebLandcodes = array();
    $sql = "SELECT gblc, gblc1, gblc2, count(gblc) AS cnt FROM (
            SELECT concat('', GebLandcode) AS gblc,
            concat('', GebLandOuder1code) AS gblc1,
            concat('', GebLandOuder2code) AS gblc2
            FROM Persoonsgegevens) gblcs
            GROUP BY gblc, gblc1, gblc2";
    $records = $DB->get_records($sql);
    if ($records !== false) {
        foreach ($records as $record) {
            $gblc = $record->gblc;
            $gblc1 =$record->gblc1;
            $gblc2 = $record->gblc2;
            $cnt = intval($record->cnt);
            if ($cnt > 0) {
                $gebLandcodes[] = array($gblc, $gblc1, $gblc2, $cnt);
            }
        }
    }
    return $gebLandcodes;
}

function popGebLandCodesList(&$gebLandcodes) {
    $len = count($gebLandcodes);
    $rndel = rand(0, ($len - 1));
    $gblcel = $gebLandcodes[$rndel];
    $cnt = $gblcel[3];
    $cnt--;
    if ($cnt > 0) {
        $gebLandcodes[$rndel] = array($gblcel[0], $gblcel[1], $gblcel[2], $cnt);

    } else {
        //remove array element $rndel
        $removed = array_splice($gebLandcodes, $rndel, 1);
    }
    return $gblcel;
}

function getNationaliteitenList() {
    global $DB, $logger;
    $nationaliteiten = array();
    $sql = "SELECT nationaliteit, count(nationaliteit) AS cnt FROM
            (SELECT concat('', pg.nationaliteit) as nationaliteit FROM Persoonsgegevens pg) pgn
            GROUP BY nationaliteit";
    $records = $DB->get_records($sql);
    if ($records !== false) {
        foreach ($records as $record) {
            $nat = $record->nationaliteit;
            $cnt = intval($record->cnt);
            if ($cnt > 0) {
                $nationaliteiten[] = array($nat, $cnt);
            }
        }
    }
    return $nationaliteiten;
}

function popNationaliteitenList(&$nationaliteiten) {
    $len = count($nationaliteiten);
    $rndel = rand(0, ($len - 1));
    $natcel = $nationaliteiten[$rndel];
    $cnt = $natcel[1];
    $cnt--;
    if ($cnt > 0) {
        $nationaliteiten[$rndel] = array($natcel[0], $cnt);

    } else {
        //remove array element $rndel
        $removed = array_splice($nationaliteiten, $rndel, 1);
    }
    return $natcel;
}

function getNamenList($field = "", $shuffle = false) {
    global $DB, $logger;
    $mnms = array();
    $namen = array();
    $shufflednamen = array();
    $cntvnms = array();
    $sql = "SELECT casnummer, {$field} FROM persoonsgegevens
            WHERE (NOT {$field} IS NULL AND NOT {$field} = '')";
    $ns = $DB->get_records($sql);
    if ($ns !== false) {
        foreach ($ns as $nm) {
            $casnr = $nm->casnummer;
            $naam = utf8_encode($nm->$field); // converteer namen naar UTF8
            $splitnms = explode(" ", $naam); // split meerdere namen naar array
            $len = count($splitnms); // bepaald aantal namen in het veld
            if (!isset($cntnms[$len])) { // tel aantal keer dat dit aantal namen in 1 veld staan
                $cntnms[$len] = 0;
            }
            $cntnms[$len]++;
            foreach ($splitnms as $splitnm) {
                $splitnm = trim($splitnm); // per naam in het veld 
                if ($shuffle) {
                    $splitnm = strtolower($splitnm); // gebruik lowercase naam als key
                    if (!array_key_exists($splitnm, $shufflednamen)) { // als niet eerder al omgezet naar 'geshuffelde' variant
                        $shuffled = str_shuffle($splitnm); // shuffle de naam
                        $shuffled = ucfirst($shuffled); // maak het 1e teken weer een hoofdellter
                        $shufflednamen[$splitnm] = $shuffled; // voeg toe aan de lijst van reeds geshuffelde namen
                    }
                }
                if (!isset($namen[$splitnm])) { // tel het aantal keer dat deze naam voorkomt
                    $namen[$splitnm] = 0;
                }
                $namen[$splitnm]++;
            }
        }
        $ns = null;
        $lennamen = count($namen);
        $namenkeys = array_keys($namen);
        $mnms = array();
        while (true) { // ???
            arsort($cntnms);
            foreach ($cntnms as $cnt => $cntnm) {
                for ($i = 1; $i <= $cntnm; $i++) {
                    $newnm = "";
                    for ($j = 0; $j < $cnt; $j++) {
                        $rnd = rand(0, ($lennamen - 1));
                        $naam = $namenkeys[$rnd];
                        if ($shuffle) {
                            $naam = $shufflednamen[$naam];
                        }
                        $newnm .=  $naam . " ";
                    }
                    $mnms[] = utf8_encode(trim($newnm));
                }
            }
            break; // ???
        }
    }
    return $mnms;
}

function popGbavvList(&$gbavvList) {
    $len = count($gbavvList);
    $rndel = rand(0, ($len - 1));
    $gbavvel = $gbavvList[$rndel];
    $cnt = $gbavvel[1];
    $cnt--;
    if ($cnt > 0) {
        $gbavvList[$rndel] = array($gbavvel[0], $cnt);

    } else {
        //remove array element $rndel
        $removed = array_splice($gbavvList, $rndel, 1);
    }
    return $gbavvel[0];
}

function getVVList($field) {
    global $DB, $logger;
    $vvs = array();
    $sql = "SELECT {$field}, count({$field}) as cnt from persoonsgegevens
            WHERE (NOT {$field} IS NULL AND NOT {$field} = '')
            group by {$field}";
    $vvrecs = $DB->get_records($sql);
    if ($vvrecs !== false) {
        foreach ($vvrecs as $vvrec) {
            $vv = $vvrec->$field;
            $cnt= $vvrec->cnt;
            $vvs[] = array($vv, $cnt);
        }
    }

    $vvnull = $DB->count("persoonsgegevens", "{$field} IS NULL");
    if ($vvnull > 0) {
        $vvs[] = array('null', $vvnull);
    }
    $vvleeg = $DB->count("persoonsgegevens", "{$field} = ''");
    if ($vvleeg > 0) {
        $vvs[] = array('', $vvleeg);
    }
    return $vvs;
}

function popVVList(&$vvList) {
    $len = count($vvList);
    $rndel = rand(0, ($len - 1));
    $vvel = $vvList[$rndel];
    $cnt = $vvel[1];
    $cnt--;
    if ($cnt > 0) {
        $vvList[$rndel] = array($vvel[0], $cnt);

    } else {
        //remove array element $rndel
        $removed = array_splice($vvList, $rndel, 1);
    }
    return $vvel[0];
}




function getLeerjaarGroepList() {
    global $DB, $logger;
    $ljkgs = array();
    $sql = "SELECT CONCAT('', leerjaar) AS leerjaar, CONCAT('', klasgroep) as klasgroep, count(CONCAT('', klasgroep)) AS cnt FROM persoonsgegevens
            GROUP BY CONCAT('', leerjaar), CONCAT('', klasgroep)";
    $ljkgrecs = $DB->get_records($sql);
    if ($ljkgrecs !== false) {
        foreach ($ljkgrecs as $ljkgrec) {
            $leerjaar = $ljkgrec->leerjaar;
            $klasgroep = $ljkgrec->klasgroep;
            $ljkgcnt = $ljkgrec->cnt;
            $ljkgs[$leerjaar][$klasgroep] = $ljkgcnt;
        }
    }
    return $ljkgs;
}

function getLeerjaarklasgroep($leerjaarklasgoepList, $leerjaar) {
    global $logger;
    $klasgroep = "";
    if ($leerjaar == null) {
        $leerjaar = "";
    }
    $klasgroepen = $leerjaarklasgoepList[$leerjaar];
    if (!isset($klasgroepen) || !is_array($klasgroepen)) {
        $logger->debug("Nog klasgroep array found.");

    } else {
        $klasgroepkeys = array_keys($klasgroepen);
        $len = count($klasgroepkeys);
        $rndel = rand(0, $len - 1);
        $klasgroep = $klasgroepkeys[$rndel];
        if ($klasgroep === null) {
            $logger->error("Klassgroep : {$klasgroep} el: {$rndel} for leerjaar: {$leerjaar}");

        }
    }
    return $klasgroep;
}

function anonDossierDiagCharData($time, $maxrecords) {
    global $DB, $logger;
    $minmaxsql = "select min(dataId) as min, max(dataId) as max from dossierdiagdata";
    $mmdiagdata = $DB->get_record($minmaxsql);
    $minid = $mmdiagdata->min;
    $maxid = $mmdiagdata->max;
    $totalsql = "select count(*) as cnt from dossierdiagdata WHERE dl > " . DIAGDATA_CHARDATA_GREATER_SIZE;
    $totaldiagdata = $DB->get_record($totalsql);
    $total = $totaldiagdata->cnt;
    $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
    $cnt = 0;
    $idstep = 30000;
    $startid = $minid;
    $endId = $startid + $idstep;
    while (true) {
        $sql = "SELECT ddd.dataid, ddd.chardata
            FROM dossierdiagdata ddd
            WHERE ddd.dataid >= {$startid} AND ddd.dataid <= {$endId}
            AND ddd.dl > " . DIAGDATA_CHARDATA_GREATER_SIZE;
        $diagdata = $DB->get_records($sql);
        if ($diagdata !== false) {
            foreach ($diagdata as $data) {
                $id = $data->dataid;
                $chardata = $data->chardata;
                if (! empty($chardata)) {
                    $anonCharData = shuffleText($chardata, false, true);
                    $updatesql = "UPDATE dossierdiagdata
                                  SET CharData = :chardata
                                  WHERE dataid = :dataid";
                    $updated = $DB->execute($updatesql, array('dataid' => $id, 'chardata' => $anonCharData));
                    // $logger->debug("CharData: [{$chardata}], anon: [{$anonCharData}]");
                }
                $cnt++;
                show_status($cnt, $total, "CharData process", $mcheck);
                if ($maxrecords > 0 && $cnt > $maxrecords) {
                    $endId = ++$maxid;
                    break;
                }
            }
        }
        if ($endId > $maxid) {
            break;
        } else {
            $startid = $endId + 1;
            $endId = $startid + $idstep;
        }
    }
}

/*
 * Update diagdata datum velden
 */
function anonDossierDiagData($time, $maxrecords) {
    global $DB, $logger;
    $sql = "UPDATE ddd 
            SET ddd.data = CONVERT(varchar, DATEADD(day, anc.date_offset, CONVERT(datetime, convert(varchar(10), [data]))), 112) 
            FROM DossierDiagData ddd 
            LEFT JOIN DossierItems dosi ON (dosi.itemid = ddd.itemId) 
            LEFT JOIN an_cross anc ON (anc.casnummer = dosi.casnummer) 
            WHERE ddd.diagitemid IN 
                  (SELECT di.diagitemid as diagitemid FROM diagitems di
                   WHERE di.datasoort = 27)
                   AND ddd.[data] > 19010000
                   AND ddd.[data] < 22000000";
    $updated = $DB->execute($sql);
    if (!$updated) {
        $error = $DB->getLastError();
        $logger->error("UPDATE DossierDiagData for datasoort 27 FAILED!");
    }
}

/*
 * Anonimises the name values and gebplaats, leerjaar and persmemo for a person
 *
 * There may be issues with encoding as the database uses a weird type of encoding.
 * In these cases the values are left empty
 *
 */
function anonPersoonsGegevensBasevalues($time, $maxrecords = -1, $fixedListNamesOnly = false) {
    global $DB, $logger;
    $sql = "SELECT pg.casnummer, pg.voornamen, pg.geslacht, pg.geslachtsnaam, pg.achternaam, pg.naamouder1, pg.naamouder2,
            pg.gebplaats, pg.leerjaar, pg.persmemo, pg.schoolnummer, s.postcode
            FROM persoonsgegevens pg
            LEFT JOIN scholen s on (s.schoolnummer = pg.schoolnummer)";
    $persons = $DB->get_records($sql);
    if ($persons === false) {
        $error = $DB->getLastError();
        $logger->error("Get persoonsgegevens list FAILED Error: " . print_r($error, true));
    }
    $cntpersons = count($persons);
    $cntnewcas = 0;

    if ($maxrecords > 0) {
        $cntpersons = $maxrecords; // TODO remove after test
    }
    $mcheck = (int)($cntpersons / (100 / SHOW_STATUS_MOD));
    $voornamenList = getNamenList("voornamen", true);
    $lenvoornamenList = count($voornamenList);

    $roepnamenList = getNamenList("roepnaam", true);
    $lenroepnamenList = count($roepnamenList);

    $gbavvList = getVVList("gbavoorvoegsel");
    $vvList = getVVList("voorvoegsel");

    $leerjaarklasgoepList = getLeerjaarGroepList();

    $schoolnummerlist = getSchoolList();

    $cntfail = 0;

    foreach ($persons as $person) {
        $casnummer = $voornamen = $roepnaam = $gbavv = $vv= $geslachtsnaam = $achternaam = $naamouder1 = $naamouder2 = $gebplaats = $klasgroep = $persmemo = $schoolnummer = "";
        
        $casnummer = $person->casnummer;

        if ($fixedListNamesOnly) {
            if ($person->geslacht == 2) {
                $voornamenList = $roepnamenList = array('Anna', 'Emma', 'Eva', 'Evi', 'Fleur', 'Julia', 'Lauren', 'Liv', 'Lotte', 'Lynn', 'Mila', 'Noor', 'Nora', 'Olivia', 'Saar', 'Sara', 'Sophie', 'Tess', 'Yara');
                $lenvoornamenList = $lenroepnamenList = 19;
            } else {
                $voornamenList = $roepnamenList = array('Adam', 'Bram', 'Daan', 'Finn', 'James', 'Jesse', 'Julian', 'Levi', 'Liam', 'Lucas', 'Luuk', 'Max', 'Mees', 'Milan', 'Noah', 'Noud', 'Sam', 'Sem', 'Thijs', 'Thomas');
                $lenvoornamenList = $lenroepnamenList = 20;
            }
        }
        
        $rndel = rand(0, ($lenvoornamenList - 1));
        $voornamen = $voornamenList[$rndel];
        
        if (strlen($voornamen) > 200) {
            $voornamen = substr($voornamen, 0, 200);
        }

        $rndel = rand(0, ($lenroepnamenList - 1));
        $roepnaam = $roepnamenList[$rndel];
        if (strlen($roepnaam) > 20) {
            $roepnaam = substr($roepnaam, 0, 20);
        }
        
        $geslachtsnaam = $person->geslachtsnaam;
        if (!empty($geslachtsnaam)) {
            $geslachtsnaam = shuffleText($geslachtsnaam);
            $geslachtsnaam = utf8_encode($geslachtsnaam);
        }

        $achternaam = $person->achternaam;
        if (!empty($achternaam)) {
            $achternaam = shuffleText($achternaam);
            $achternaam = utf8_encode($achternaam);
        }

        $naamouder1 = $person->naamouder1;
        if (!empty($naamouder1)) {
            $naamouder1 = shuffleText($naamouder1);
            $naamouder1 = utf8_encode($naamouder1);
        }
        $naamouder2 = $person->naamouder2;
        if (!empty($naamouder2)) {
            $naamouder2 = shuffleText($naamouder2);
            $naamouder2 = utf8_encode($naamouder2);
        }

        $gebplaats = $person->gebplaats;
        if (!empty($gebplaats)) {
            $gebplaats = shuffleText($gebplaats);
            $gebplaats = utf8_encode($gebplaats);
        }

        $gbavv = popVVList($gbavvList);
        if ($gbavv == 'null') {
            $gbavv =  null;
        }

        $vv = popVVList($vvList);
        if ($vv == 'null') {
            $vv =  null;
        }
        $leerjaar = $person->leerjaar;
        $klasgroep = getLeerjaarklasgroep($leerjaarklasgoepList, $leerjaar);

        $persmemo = $person->persmemo;
        if (!empty($persmemo)) {
            $persmemo = utf8_encode(shuffleText($persmemo, false, true));
            if (strlen($persmemo) > 2000) {
                $persmemo = substr($persmemo, 0, (2000 - 1));
            }
        }

        $schoolnummer = $person->schoolnummer;
        if (!empty($schoolnummer)) {
            $postcode = $person->postcode;
            $pcoffset = 0;
            if (!empty($postcode)) {
                $pcoffset = (int)substr($postcode, 0, PC_PARSE_LEN);
            }
            $schoolnummer = getSchoolPC($schoolnummerlist, $pcoffset);
        }
        
        // store new fields
        $sql = "UPDATE persoonsgegevens SET
                voornamen = :voornamen,
                roepnaam = :roepnaam,
                gbavoorvoegsel = :gbavoorvoegsel,
                voorvoegsel = :voorvoegsel,
                geslachtsnaam = :geslachtsnaam,
                achternaam = :achternaam,
                naamouder1 = :naamouder1,
                naamouder2 = :naamouder2,
                gebplaats = :gebplaats,
                klasgroep = :klasgroep,
                persmemo = :persmemo,
                schoolnummer = :schoolnummer
                WHERE casnummer = :casnummer";
        $params = array('casnummer' => $casnummer,
            'voornamen' => $voornamen,
            'roepnaam' => $roepnaam,
            'gbavoorvoegsel' => $gbavv,
            'voorvoegsel' => $vv,
            'geslachtsnaam' => $geslachtsnaam,
            'achternaam' => $achternaam,
            'naamouder1' => $naamouder1,
            'naamouder2' => $naamouder2,
            'gebplaats' => $gebplaats,
            'klasgroep' => $klasgroep,
            'persmemo' => $persmemo,
            'schoolnummer' => $schoolnummer
        );
        $updated  = $DB->execute($sql, $params, false, true);
        if (!$updated) {
            $error = $DB->getLastError();
            $cntfail++;
            $logger->error("UPDATE persoonsgegevens record for casnummer {$casnummer} FAILED [{$cntfail}] Error: " . print_r($error, true) . print_r($params, true) . " - Using empty values. ");
            $voornamen = $roepnaam = 'Invalid-ACS-2-name';
            $params = array('casnummer' => $casnummer,
                'voornamen' => $voornamen,
                'roepnaam' => $roepnaam,
                'gbavoorvoegsel' => $gbavv,
                'voorvoegsel' => $vv,
                'geslachtsnaam' => $geslachtsnaam,
                'achternaam' => $achternaam,
                'naamouder1' => $naamouder1,
                'naamouder2' => $naamouder2,
                'gebplaats' => $gebplaats,
                'klasgroep' => $klasgroep,
                'persmemo' => $persmemo,
                'schoolnummer' => $schoolnummer
            );
            $updated = $DB->execute($sql, $params);
            if (!$updated) {
                $logger->error("UPDATE persoonsgegevens record for casnummer {$casnummer} with EMPTY 1st and call names FAILED." . var_dump($params));
                $params = array('casnummer' => $casnummer,
                     'voornamen' => '', 'roepnaam' => '', 'gbavoorvoegsel' => '', 'voorvoegsel' => '', 'geslachtsnaam' => '', 'achternaam' => '',
                     'naamouder1' => '', 'naamouder2' => '', 'gebplaats' => '', 'klasgroep' => '', 'persmemo' => '', 'schoolnummer' => 0);
                $updated = $DB->execute($sql, $params);
                if (!$updated) {
                    $logger->error("UPDATE persoonsgegevens record for casnummer {$casnummer} with EMPTY values FAILED." . var_dump($params));
                }
            }
        }
        $cntnewcas++;
        show_status($cntnewcas, $cntpersons, "Fill namen in persoons table", $mcheck);
        if ($cntnewcas >= $cntpersons) {
            break;
        }

    }
}

function fillCrossTable($time, $maxrecords = -1) {
    global $DB, $logger;
    if (!isset($time)) {
        $time = time();
    }
    $sql = "SELECT pg.casnummer, pg.email, pg.email2, pg.telefoon, pg.telefoon2, pg.telefoon3, pg.anummer,
            pg.aanmelddatum, pg.datumvertrek, pg.datumvestiging, pg.gebouder1, pg.gebouder2, pg.geboortedatum, pg.overlijdensdatum, pg.schoolmutatie, pg.uitzorg,
            pg.geblandcode, pg.geblandouder1code, pg.geblandouder2code,
            pg.nationaliteit
            FROM persoonsgegevens pg";
    $persons = $DB->get_records($sql);
    $cntpersons = count($persons);
    if ($maxrecords > 0) {
        $cntpersons = $maxrecords; // TODO remove after test
    }
    $mcheck = (int)($cntpersons / (100 / SHOW_STATUS_MOD));

    $newcasnummers = generateNewCasnummers($cntpersons);
    $cntnewcas = 0;

    $gebLandcodesList = getGebLandCodesList();

    $nationaliteitenList = getNationaliteitenList();

    trackTime($time, true);

    $min = $max = -1;
    $sql = "SELECT MIN(anummer) FROM persoonsgegevens";
    $result = $DB->get_record($sql);
    foreach ($result as $key => $value) {
        $min =  $value;
    }
    $sql = "SELECT MAX(anummer) FROM persoonsgegevens";
    $result = $DB->get_record($sql);
    foreach ($result as $key => $value) {
        $max =  $value;
    }
    $anummers_new = generateAnummer($cntpersons, $min, $max);

    foreach ($persons as $person) {
        // create new fields
        $casnummer = $person->casnummer;

        $email = $person->email ;
        $email2 = $person->email2;

        $telefoon = $person->telefoon;
        $telefoon2 = $person->telefoon2;
        $telefoon3 = $person->telefoon3;

        $anummer = $person->anummer;

        $aanmelddatum = $datumvertrek = $datumvestiging = $gebouder1 = $gebouder2 = $geboortedatum = $overlijdensdatum = $schoolmutatie = $uitzorg = null;
        $dtaanmelddatum = $dtdatumvertrek = $dtdatumvestiging = $dtgebouder1 = $dtgebouder2 = $dtgeboortedatum = $dtoverlijdensdatum = $dtschoolmutatie = $dtuitzorg = null;
        $geblandcode_new = $geblandouder1code_new = $geblandouder2code_new = null;

        $aanmelddatum = $person->aanmelddatum;
        $datumvertrek = $person->datumvertrek;
        $datumvestiging = $person->datumvestiging;
        $gebouder1 = $person->gebouder1;
        $gebouder2 = $person->gebouder2;
        $geboortedatum = $person->geboortedatum;
        $overlijdensdatum = $person->overlijdensdatum;
        $schoolmutatie = $person->schoolmutatie;
        $uitzorg = $person->uitzorg;

        $geblandcode       = $person->geblandcode;
        $geblandouder1code = $person->geblandouder1code;
        $geblandouder2code = $person->geblandouder2code;

        $nationaliteit = $person->nationaliteit;

        if (!empty($aanmelddatum)) {
            $dtaanmelddatum      = new DateTime($person->aanmelddatum);
        }
        if (!empty($datumvertrek)) {
            $dtdatumvertrek      = new DateTime($person->datumvertrek);
        }
        if (!empty($datumvestiging)) {
            $dtdatumvestiging    = new DateTime($person->datumvestiging);
        }
        if (!empty($gebouder1)) {
            $dtgebouder1         = new DateTime($person->gebouder1);
        }
        if (!empty($gebouder2)) {
            $dtgebouder2         = new DateTime($person->gebouder2);
        }
        if (!empty($geboortedatum)) {
            $dtgeboortedatum     = new DateTime($person->geboortedatum);
        }
        if (!empty($overlijdensdatum)) {
            $dtoverlijdensdatum  = new DateTime($person->overlijdensdatum);
        }
        if (!empty($schoolmutatie)) {
            $dtschoolmutatie     = new DateTime($person->schoolmutatie);
        }
        if (!empty($uitzorg)) {
            $dtuitzorg           = new DateTime($person->uitzorg);
        }

        $email_new = $email2_new = $telefoon_new = $telefoon2_new = $telefoon3_new = $tel_relatie = $tel_relatie2 = $tel_relatie3 = "";
        $aanmelddatum_new = $datumvertrek_new = $datumvestiging_new = $gebouder1_new = $gebouder2_new = $geboortedatum_new = $overlijdensdatum_new = $schoolmutatie_new = $uitzorg_new =  null;

        $casnummer_new = $newcasnummers[$cntnewcas];

        $anummer_new = null;
        if (!empty($anummer)) {
            $anummer_new = $anummers_new[$cntnewcas];;
        }

        if (empty($email)) {
            $email = "";
        } else if (validEmail($email, false)) {
            $email_new = generateAnonymousEmail($email);
        }

        if (empty($email2)) {
            $email2 = "";
        } else if (validEmail($email2, false)) {
            $email2_new = generateAnonymousEmail($email2);
        }

        if (empty($telefoon)) {
            $telefoon = "";
        } else {
            $parsedtelefoon = str_replace(PHONE_REPLACE_SPECIAL_CHARS, '', $telefoon);
            list($tel_relatie, $parsedtelefoon) = parseRelationFromTel($parsedtelefoon);
            if (validPhoneNumber($parsedtelefoon)) {
                $telefoon_new = generateAnonymousPhn($parsedtelefoon);
            }
        }
        if (empty($telefoon2)) {
            $telefoon2 = "";
        } else {
            $parsedtelefoon = str_replace(PHONE_REPLACE_SPECIAL_CHARS, '', $telefoon2);
            list($tel_relatie2, $parsedtelefoon) = parseRelationFromTel($parsedtelefoon);
            if (validPhoneNumber($parsedtelefoon)) {
                $telefoon2_new = generateAnonymousPhn($parsedtelefoon);
            }
        }
        if (empty($telefoon3)) {
            $telefoon3 = "";
        } else {
            $parsedtelefoon = str_replace(PHONE_REPLACE_SPECIAL_CHARS, '', $telefoon3);
            list($tel_relatie3, $parsedtelefoon) = parseRelationFromTel($parsedtelefoon);
            if (validPhoneNumber($parsedtelefoon)) {
                $telefoon3_new = generateAnonymousPhn($parsedtelefoon);
            }
        }

        $dateOffset = rndDayOffset();
        $addOffset = true;
        $sdateOffset = "P0D";
        if ($dateOffset < 0) {
            $addOffset = false;
            $repl = (-1 * $dateOffset);
            $sdateOffset = str_replace("0", $repl, $sdateOffset);
        } else {
            $sdateOffset = str_replace("0", $dateOffset, $sdateOffset);
        }
        $diOffset = new DateInterval($sdateOffset);

        $aanmelddatum_new = getClonedShiftedDTasString($dtaanmelddatum, $diOffset, $addOffset);
        $datumvertrek_new = getClonedShiftedDTasString($dtdatumvertrek, $diOffset, $addOffset);
        $datumvestiging_new = getClonedShiftedDTasString($dtdatumvestiging, $diOffset, $addOffset);
        $gebouder1_new = getClonedShiftedDTasString($dtgebouder1, $diOffset, $addOffset);
        $gebouder2_new = getClonedShiftedDTasString($dtgebouder2, $diOffset, $addOffset);
        $geboortedatum_new = getClonedShiftedDTasString($dtgeboortedatum, $diOffset, $addOffset);
        $overlijdensdatum_new = getClonedShiftedDTasString($dtoverlijdensdatum, $diOffset, $addOffset);
        $schoolmutatie_new = getClonedShiftedDTasString($dtschoolmutatie, $diOffset, $addOffset);
        $uitzorg_new = getClonedShiftedDTasString($dtuitzorg, $diOffset, $addOffset);

        if (isset($gebLandcodesList) && count($gebLandcodesList) > 0) {
            $rndgebLandcode = popGebLandCodesList($gebLandcodesList);
            $geblandcode_new = $rndgebLandcode[0];
            $geblandouder1code_new = $rndgebLandcode[1];
            $geblandouder2code_new = $rndgebLandcode[2];
            if ($geblandcode_new === '') {
                $geblandcode_new = null;
            }
            if ($geblandouder1code_new === '') {
                $geblandouder1code_new = null;
            }
            if ($geblandouder2code_new === '') {
                $geblandouder2code_new = null;
            }
        }
        if (isset($nationaliteitenList) && count($nationaliteitenList) > 0) {
            $rndnationaliteit = popNationaliteitenList($nationaliteitenList);
            $nationaliteit_new = $rndnationaliteit[0];
            if ($nationaliteit_new === '') {
                $nationaliteit_new = null;
            }
        }

        // store new fields
        $sql = "UPDATE an_cross SET
                casnummer_new = :casnummer_new,
                email = :email,
                email_new = :email_new,
                email2 = :email2,
                email2_new = :email2_new,
                telefoon = :telefoon,
                telefoon_new = :telefoon_new,
                telefoon2 = :telefoon2,
                telefoon2_new = :telefoon2_new,
                telefoon3 = :telefoon3,
                telefoon3_new = :telefoon3_new,
                tel_relatie = :tel_relatie,
                tel_relatie2 = :tel_relatie2,
                tel_relatie3 = :tel_relatie3,
                date_offset = :date_offset,
                anummer = :anummer,
                anummer_new = :anummer_new,
                aanmelddatum     = :aanmelddatum,
                datumvertrek     = :datumvertrek,
                datumvestiging   = :datumvestiging,
                gebouder1        = :gebouder1,
                gebouder2        = :gebouder2,
                geboortedatum    = :geboortedatum,
                overlijdensdatum = :overlijdensdatum,
                schoolmutatie    = :schoolmutatie,
                uitzorg          = :uitzorg,
                aanmelddatum_new     = :aanmelddatum_new,
                datumvertrek_new     = :datumvertrek_new,
                datumvestiging_new   = :datumvestiging_new,
                gebouder1_new        = :gebouder1_new,
                gebouder2_new        = :gebouder2_new,
                geboortedatum_new    = :geboortedatum_new,
                overlijdensdatum_new = :overlijdensdatum_new,
                schoolmutatie_new    = :schoolmutatie_new,
                uitzorg_new          = :uitzorg_new,
                geblandcode       = :geblandcode,
                geblandouder1code = :geblandouder1code,
                geblandouder2code = :geblandouder2code,
                geblandcode_new       = :geblandcode_new,
                geblandouder1code_new = :geblandouder1code_new,
                geblandouder2code_new = :geblandouder2code_new,
                nationaliteit =:nationaliteit,
                nationaliteit_new =:nationaliteit_new
                WHERE casnummer = :casnummer";
        $params = array('casnummer' => $casnummer,
            'casnummer_new' => $casnummer_new,
            'email' => $email,
            'email_new' => $email_new,
            'email2' => $email2,
            'email2_new' => $email2_new,
            'telefoon' => $telefoon,
            'telefoon_new' => $telefoon_new,
            'telefoon2' => $telefoon2,
            'telefoon2_new' => $telefoon2_new,
            'telefoon3' => $telefoon3,
            'telefoon3_new' => $telefoon3_new,
            'tel_relatie' => $tel_relatie,
            'tel_relatie2' => $tel_relatie2,
            'tel_relatie3' => $tel_relatie3,
            'date_offset' => $dateOffset,
            'anummer' => $anummer,
            'anummer_new' => $anummer_new,
            'aanmelddatum'     => $aanmelddatum,
            'datumvertrek'     => $datumvertrek,
            'datumvestiging'   => $datumvestiging,
            'gebouder1'        => $gebouder1,
            'gebouder2'        => $gebouder2,
            'geboortedatum'    => $geboortedatum,
            'overlijdensdatum' => $overlijdensdatum,
            'schoolmutatie'    => $schoolmutatie,
            'uitzorg'          => $uitzorg,
            'aanmelddatum_new'     => $aanmelddatum_new,
            'datumvertrek_new'     => $datumvertrek_new,
            'datumvestiging_new'   => $datumvestiging_new,
            'gebouder1_new'        => $gebouder1_new,
            'gebouder2_new'        => $gebouder2_new,
            'geboortedatum_new'    => $geboortedatum_new,
            'overlijdensdatum_new' => $overlijdensdatum_new,
            'schoolmutatie_new'    => $schoolmutatie_new,
            'uitzorg_new'          => $uitzorg_new,
            'geblandcode'       => $geblandcode,
            'geblandouder1code' => $geblandouder1code,
            'geblandouder2code' => $geblandouder2code,
            'geblandcode_new'       => $geblandcode_new,
            'geblandouder1code_new' => $geblandouder1code_new,
            'geblandouder2code_new' => $geblandouder2code_new,
            'nationaliteit' => $nationaliteit,
            'nationaliteit_new' => $nationaliteit_new
        );
        $inserted = $DB->execute($sql, $params);
        if (!$inserted) {
            $error = $DB->getLastError();
            $logger->error("UPDATE an_cross record for casnummer {$casnummer} FAILED." . print_r($error, true) . var_dump($params));
        }
        $cntnewcas++;
        show_status($cntnewcas, $cntpersons, "Fill email/phone/relation/anummer x table", $mcheck);
        if ($cntnewcas >= $cntpersons) {
            break;
        }
    }
}

function createBsnxTable() {
    global $DB, $logger;
    // Create bsn x table
    $logger->info("Clear records from an_cross_bsn");
    $sql = "DELETE FROM an_cross_bsn";
    $deleted = $DB->execute($sql);
    if ($deleted) {
        $logger->info("Success");
    }

    $orgbsnnmrs = array();
    $bsnFields = array('bsn', 'bsnouder1', 'bsnouder2');
    foreach ($bsnFields as $bsnField) {
        $sql = "SELECT pg.{$bsnField} as bsn FROM persoonsgegevens pg WHERE NOT pg.{$bsnField} IS NULL AND NOT pg.{$bsnField} = ''";
        $bsnrs = $DB->get_records($sql);
        foreach ($bsnrs as $bsn) {
            $value = $bsn->bsn;
            if (!array_key_exists($value, $orgbsnnmrs)) {
                $orgbsnnmrs[$value] = 0;
            }
            $orgbsnnmrs[$value]++;
        }
    }
    $cntorgbsn = count($orgbsnnmrs);
    $mcheck = (int)($cntorgbsn / (100 / SHOW_STATUS_MOD));
    $newbsnnrs = generateBSN($cntorgbsn);
    $cntnewbsnnrs = count($newbsnnrs);
    $cnt = 0;
    foreach ($orgbsnnmrs as $bsnnumber => $bsncnt) {
        $newbsn = $newbsnnrs[$cnt];
        $cnt++;
        show_status($cnt, $cntorgbsn, "Fill BSN x table", $mcheck);
        $updateSql = "INSERT INTO an_cross_bsn (bsn, bsn_new) VALUES(:bsn, :bsn_new)";
        $inserted = $DB->execute($updateSql, array('bsn' => $bsnnumber, 'bsn_new' => $newbsn));
        if (!$inserted) {
            $logger->error("INSERT INTO an_cross_bsn FAILED");
            break;
        }
        if ($cnt >= $cntnewbsnnrs) {
            break;
        }
    }
    $logger->info("{$cnt} records inserted into an_cross_bsn");
}

function generateNewCasnummers($total) {
    global $DB, $logger;
    $minmaxsql = "select min(casnummer) as min, max(casnummer) as max from persoonsgegevens";
    $mm = $DB->get_record($minmaxsql);
    $minid = $mm->min;
    $maxid = $mm->max;

    $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
    $casnummers = array();
    $cnt = 0;

    $skipnumbers = $skipped = array();
    $max = $maxid - $minid;
    $toskip = $max - $total;
    for ($i = 0; $i < $toskip; $i++) {
        $cnr = rand($minid, $maxid);
        $skipnumbers[] = $cnr;
    }
    asort($skipnumbers);

    for ($i = $minid; $i < $maxid; $i++) {
        if (!in_array($i, $skipnumbers)) {
            $casnummers[] = $i;
            $cnt++;
            show_status($cnt, $total, "Generate New Casnummers", $mcheck); //
            if ($cnt >= $total) {
                break;
            }
        } else {
            $skipped[] = $i;

        }
    }
    return $casnummers;
}

function anonDiagitems($time, $maxrecords = -1) {
    global $DB, $logger;
    if (!isset($time)) {
        $time = time();
    }
    $cnt = 0;
    if ($maxrecords > 0) {
        $total = $maxrecords;
    }
    $sql = "SELECT di.diagitemid, di.VanDatum, di.TotDatum, di.timestamp FROM diagitems di";
    $diagitems = $DB->get_records($sql);
    if ($diagitems !== false) {
        $total = count($diagitems);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        foreach ($diagitems as $diagitem) {
            // create new fields
            $dateOffset = rndDayOffset(); // You can not work your way back to a casnummer from diagitems, so use a random offset number
            $diagitemid = $diagitem->diagitemid;

            $vandatum = $totdatum = $timestamp = null;
            $vandatum_new = $totdatum_new = $teimstamp_new = null;

            $vandatum = $diagitem->VanDatum;
            $totdatum = $diagitem->TotDatum;
            $timestamp = $diagitem->timestamp;

            $addOffset = true;
            $sdateOffset = "P0D";
            if ($dateOffset < 0) {
                $addOffset = false;
                $repl = (-1 * $dateOffset);
                $sdateOffset = str_replace("0", $repl, $sdateOffset);
            } else {
                $sdateOffset = str_replace("0", $dateOffset, $sdateOffset);
            }
            $diOffset = new DateInterval($sdateOffset);

            if (!empty($vandatum)) {
                $dtvandatum = new DateTime($vandatum);
                $vandatum_new = getClonedShiftedDTasString($dtvandatum, $diOffset, $addOffset);
            }
            if (!empty($totdatum)) {
                $dttotdatum = new DateTime($totdatum);
                $totdatum_new = getClonedShiftedDTasString($dttotdatum, $diOffset, $addOffset);
            }
            if (!empty($timestamp)) {
                $dttimestamp = new DateTime($timestamp);
                $timestamp_new = getClonedShiftedDTasString($dttimestamp, $diOffset, $addOffset);
            }


            // store new fields
            $sql = "UPDATE diagitems SET
                    vandatum = :vandatum_new,
                    totdatum = :totdatum_new,
                    timestamp = :timestamp_new
                    WHERE diagitemid = :diagitemid";
            $params = array('diagitemid' => $diagitemid,
                'vandatum_new' => $vandatum_new,
                'totdatum_new' => $totdatum_new,
                'timestamp_new' => $timestamp_new
            );
            $updated = $DB->execute($sql, $params);
            if (!$updated) {
                $logger->error("UPDATE diagitems record for diagitemid {$diagitemid} FAILED." . var_dump($params));
            }
            $cnt++;
            show_status($cnt, $total, "Update diagitems table", $mcheck);
            if ($cnt > $total) {
                break;
            }
        }
    }
}

function anonDossieritems($time, $maxrecords = -1) {
    global $DB, $logger;
    if (!isset($time)) {
        $time = time();
    }
    $minmaxsql = "select min(itemId) as min, max(itemId) as max from DossierItems";
    $mmdi = $DB->get_record($minmaxsql);
    $minid = $mmdi->min;
    $maxid = $mmdi->max;
    $totalsql = "select count(*) as cnt from DossierItems";
    $totaldidata = $DB->get_record($totalsql);
    $total = $totaldidata->cnt;
    $cnt = 0;
    $idstep = 30000;
    $startid = $minid;
    $endId = $startid + $idstep;
    if ($maxrecords > 0) {
        $total = $maxrecords;
    }
    $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
    while (true) {
        $sql = "SELECT di.itemid, di.casnummer, di.datum, di.formdatum, di.timestamp, ac.date_offset
            FROM DossierItems di
            LEFT JOIN an_cross ac ON (ac.casnummer = di.casnummer)
            WHERE di.itemid >= {$startid} AND di.itemid <= {$endId}";
        $dossieritems = $DB->get_records($sql);
        foreach ($dossieritems as $dossieritem) {
            // create new fields
            $itemid = $dossieritem->itemid;
            $casnummer = $dossieritem->casnummer;
            $dateOffset = $dossieritem->date_offset;

            $datum = $formdatum = $timestamp = null;
            $dtdatum = $dtformdatum = $dttimestamp = null;
            $datum_new = $formdatum_new = $timestamp_new = null;

            $datum = $dossieritem->datum;
            $formdatum = $dossieritem->formdatum;
            $timestamp = $dossieritem->timestamp;

            $addOffset = true;
            $sdateOffset = "P0D";
            if ($dateOffset < 0) {
                $addOffset = false;
                $repl = (-1 * $dateOffset);
                $sdateOffset = str_replace("0", $repl, $sdateOffset);
            } else {
                $sdateOffset = str_replace("0", $dateOffset, $sdateOffset);
            }
            $diOffset = new DateInterval($sdateOffset);

            if (!empty($datum)) {
                $dtdatum = new DateTime($datum);
                $datum_new = getClonedShiftedDTasString($dtdatum, $diOffset, $addOffset);
            }
            if (!empty($formdatum)) {
                $dtformdatum = new DateTime($formdatum);
                $formdatum_new = getClonedShiftedDTasString($dtformdatum, $diOffset, $addOffset);
            }
            if (!empty($timestamp)) {
                $dttimestamp = new DateTime($timestamp);
                $timestamp_new = getClonedShiftedDTasString($dttimestamp, $diOffset, $addOffset);
            }

            // store new fields
            $sql = "UPDATE DossierItems SET
                    datum = :datum_new,
                    formdatum = :formdatum_new,
                    timestamp = :timestamp_new
                    WHERE itemid = :itemid";
            $params = array('itemid' => $itemid,
                'datum_new' => $datum_new,
                'formdatum_new' => $formdatum_new,
                'timestamp_new' => $timestamp_new
            );
            $updated = $DB->execute($sql, $params);
            if (!$updated) {
                $error = $DB->getLastError();
                $logger->error("UPDATE DossierItems record for itemid {$itemid} FAILED. Error: ". print_r($error, true) . " - " . print_r($params, true));
            }
            $cnt++;
            show_status($cnt, $total, "Update DossierItems table", $mcheck);
            if ($cnt >= $total) {
                break;
            }
        }
        if ($endId > $maxid) {
            break;
        } else {
            $startid = $endId + 1;
            $endId = $startid + $idstep;
        }

    }
}

/*
 * Anonimise Woonverbanden table
 * In combination with SQL statement
 *
 * it takes 5 minutes to complete, when using an index ;P
 *
 * Still TODO Casnummer - replace with x table
 *
 */
function anonWoonverbanden($time, $maxrecords) {
    global $DB, $logger;
    $cnt = 0;

    $sql = "SELECT wv.woonverbandid, wv.aantal, wv.Postcode, wv.Locatiebeschrijving, wv.NaamOpenbRuimte, wv.PeriodeVan, wv.PeriodeTot, wv.timestamp, wv.Woonplaatsnaam, wv.CodeNummerAanduid, wv.CodeVerblijfplaats, wv.Gemeentecode, ac.date_offset FROM Woonverbanden wv
            LEFT JOIN an_cross ac ON (ac.casnummer = wv.casnummer)";
    $woonverbanden = $DB->get_records($sql);
    if ($woonverbanden !== false) {
        $total = count($woonverbanden);
        $mcheck = (int)($total / (100 / 0.01));
        $snl = getStraatnaamList(array('straatnaam', 'straatnaam2', 'straatnaam3', 'straatnaam4'), "woonverbanden");
        $cntsnl = count($snl);
        list($hnl, $hntl) = getWVHuisnumList();
        $wplpcl = getWVWoonplaatsList();
        foreach ($woonverbanden as $woonverband) {
            $woonverbandID = $woonverband->woonverbandid;
            $postcode = $woonverband->Postcode;
            $pcoffset = -1;
            if (!empty($postcode)) {
                $pcoffset = (int)substr($postcode, 0, PC_PARSE_LEN);
            }

            $huisnummer = $huisnummer2 = $huisnummer3 = $huisnummer4 = $huisnummertoev = $huisnummertoev2 = $huisnummertoev3 = $huisnummertoev4 = "";
            $straatnaam = $straatnaam2 = $straatnaam3 = $straatnaam4 = "";
            $woonplaats = $woonplaats2 = $woonplaats3 = $woonplaats4 = "";
            $postcode = $postcode2 = $postcode3 = $postcode4 = "";
            $locatiebeschr_new = $naamopenbr_new = "";
            $periodeVan_new = $periodeTot_new = $timestamp_new = null;

            $locatiebeschr = $woonverband->Locatiebeschrijving;
            $naamopenbr = $woonverband->NaamOpenbRuimte;

            if (!empty($locatiebeschr)) {
                $locatiebeschr_new = utf8_encode(shuffleText($locatiebeschr));
            }
            if (!empty($naamopenbr)) {
                $naamopenbr_new = utf8_encode(shuffleText($naamopenbr));
            }

            $woonplaatsnaam = $codeNummerAanduid = $codeVerblijfplaats = $gemeentecode = "";
            $maxfieldid = (int)$woonverband->aantal;
            for ($i = 1; $i <= $maxfieldid; $i++) {
                list($hn, $hnt) = getWVHuisnumToev($hnl, $hntl);
                $rndsnl = rand(0, $cntsnl - 1);
                $straatnm = $snl[$rndsnl];

                $huisnummerfld = "huisnummer";
                if ($i >= 2) {
                    $huisnummerfld .= $i;
                }
                $huisnummertoefld = "huisnummertoev";
                if ($i >= 2) {
                    $huisnummertoefld .= $i;
                }
                $straatnmfld = "straatnaam";
                if ($i >= 2) {
                    $straatnmfld .= $i;
                }

                $$huisnummerfld = $hn;
                $$huisnummertoefld = $hnt;
                $$straatnmfld = $straatnm;

                if ($pcoffset != -1) {
                    list($wpl, $pc) = getWoonplaatsPC($wplpcl, $pcoffset);
                    $woonpltsfld = "woonplaats";
                    if ($i >= 2) {
                        $woonpltsfld .= $i;
                    } else {
                        if (!empty($woonverband->Woonplaatsnaam) && !empty($wpl[WPL_WOONPLAATSNAAM])) {
                            $woonplaatsnaam = $wpl[WPL_WOONPLAATSNAAM];
                        }
                        if (!empty($woonverband->CodeNummerAanduid)) {
                            $codeNummerAanduid = $wpl[WPL_CODENUMMERAANDUID];
                        }
                        if (!empty($woonverband->CodeVerblijfplaats)) {
                            $codeVerblijfplaats = $wpl[WPL_CODEVERBLIJFPLAATS];
                        }
                        if (!empty($woonverband->Gemeentecode)) {
                            $gemeentecode = $wpl[WPL_GEMEENTECODE];
                        }
                    }
                    $pcfld = "postcode";
                    if ($i >= 2) {
                        $pcfld .= $i;
                    }

                    $$woonpltsfld = $wpl[WPL_WOONPLAATS];
                    $$pcfld = $pc;
                }
            }

            $periodeVan = $woonverband->PeriodeVan;
            $periodeTot = $woonverband->PeriodeTot;
            $timestamp = $woonverband->timestamp;

            if (!empty($periodeVan) || !empty($periodeTot) ||!empty($timestamp)) {
                $dateOffset = $woonverband->date_offset;
                $addOffset = true;
                $sdateOffset = "P0D";
                if ($dateOffset < 0) {
                    $addOffset = false;
                    $repl = (-1 * $dateOffset);
                    $sdateOffset = str_replace("0", $repl, $sdateOffset);
                } else {
                    $sdateOffset = str_replace("0", $dateOffset, $sdateOffset);
                }
                $diOffset = new DateInterval($sdateOffset);

                if (!empty($periodeVan)) {
                    $dtperiodeVan = new DateTime($periodeVan);
                    $periodeVan_new = getClonedShiftedDTasString($dtperiodeVan, $diOffset, $addOffset);
                }
                if (!empty($periodeTot)) {
                    $dtperiodeTot = new DateTime($periodeTot);
                    $periodeTot_new = getClonedShiftedDTasString($dtperiodeTot, $diOffset, $addOffset);
                }
                if (!empty($timestamp)) {
                    $dttimestamp = new DateTime($timestamp);
                    $timestamp_new = getClonedShiftedDTasString($dttimestamp, $diOffset, $addOffset);
                }
            }

            $updatesql = "UPDATE Woonverbanden
                          SET Huisnummer = :huisnummer, Huisnummer2 = :huisnummer2, Huisnummer3 = :huisnummer3, Huisnummer4 = :huisnummer4,
                          HuisnummerToev = :huisnummerToev, HuisnummerToev2 = :huisnummerToev2, HuisnummerToev3 = :huisnummerToev3, HuisnummerToev4 = :huisnummerToev4,
                          Straatnaam = :straatnaam, Straatnaam2 = :straatnaam2, Straatnaam3 = :straatnaam3, Straatnaam4 = :straatnaam4,
                          Woonplaats = :woonplaats, Woonplaats2 = :woonplaats2, Woonplaats3 = :woonplaats3, Woonplaats4 = :woonplaats4,
                          Woonplaatsnaam = :woonplaatsnaam, CodeNummerAanduid = :codeNummerAanduid, CodeVerblijfplaats = :codeVerblijfplaats, Gemeentecode = :gemeentecode,
                          Postcode = :postcode, Postcode2 = :postcode2, Postcode3 = :postcode3, Postcode4 = :postcode4,
                          Locatiebeschrijving = :locatiebeschrijving, NaamOpenbRuimte = :naamOpenbRuimte,
                          PeriodeVan = :periodeVan, PeriodeTot = :periodeTot, timestamp = :timestamp
                          WHERE WoonverbandID = :woonverbandID ";
            $params = array('woonverbandID' => $woonverbandID,
                'huisnummer' => $huisnummer, 'huisnummer2' => $huisnummer2, 'huisnummer3' => $huisnummer3, 'huisnummer4' => $huisnummer4,
                'huisnummerToev' => $huisnummertoev, 'huisnummerToev2' => $huisnummertoev2, 'huisnummerToev3' => $huisnummertoev3, 'huisnummerToev4' => $huisnummertoev4,
                'straatnaam' => $straatnaam, 'straatnaam2' => $straatnaam2, 'straatnaam3' => $straatnaam3, 'straatnaam4' => $straatnaam4,
                'woonplaats' => $woonplaats, 'woonplaats2' => $woonplaats2, 'woonplaats3' => $woonplaats3, 'woonplaats4' => $woonplaats4,
                'woonplaatsnaam' => $woonplaatsnaam, 'codeNummerAanduid' => $codeNummerAanduid, 'codeVerblijfplaats' => $codeVerblijfplaats, 'gemeentecode' => $gemeentecode,
                'postcode' => $postcode, 'postcode2' => $postcode2, 'postcode3' => $postcode3, 'postcode4' => $postcode4,
                'locatiebeschrijving' => $locatiebeschr_new, 'naamOpenbRuimte' => $naamopenbr_new,
                'periodeVan' => $periodeVan_new, 'periodeTot' => $periodeTot_new, 'timestamp' => $timestamp_new
            );
            $updated = $DB->execute($updatesql, $params);
            if (!$updated) {
                $error = $DB->getLastError();
                $logger->error("UPDATE woonverbanden FAILED. Error: " . print_r($error, true));
            }
            $cnt++;
            show_status($cnt, $total, "Woonverbanden process", $mcheck);
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}

function getWVHuisnumList() {
    global $DB, $logger;
    $hnl = array();
    $hntl = array();
    $cnthn = 0;
    $cnthnv = 0;
    $sql = "SELECT Huisnummer, Huisnummer2, Huisnummer3, Huisnummer4, HuisnummerToev, HuisnummerToev2, HuisnummerToev3, HuisnummerToev4 FROM Woonverbanden";
    $huisnums = $DB->get_records($sql);
    if ($huisnums !== false) {
        $total = count($huisnums);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        foreach ($huisnums as $huisnum) {
            for ($i = 1; $i <=4; $i++) {
                $huisnummerfld = "Huisnummer";
                if ($i >= 2) {
                    $huisnummerfld .= $i;
                }
                $huisnummertoefld = "HuisnummerToev";
                if ($i >= 2) {
                    $huisnummertoefld .= $i;
                }
                $huisnummer = "" . $huisnum->$huisnummerfld;
                $huisnummertoe = "" . $huisnum->$huisnummertoefld;
                if (!empty($huisnummer) && !in_array($huisnummer, $hnl)) {
                    $hnl[$cnthn] = $huisnummer;
                    $cnthn++;
                }
                if (!empty($huisnummertoe) && !in_array($huisnummertoe, $hntl)) {
                    $hntl[$cnthnv] = $huisnummertoe;
                    $cnthnv++;
                }
            }
            $cnt++;
            show_status($cnt, $total, "Create huisnummer list", $mcheck);
        }
    }
    return array($hnl, $hntl);
}


function getWVHuisnumToev($hnl, $hntl) {
    $cnthn = count($hnl);
    $cnthnt = count($hntl);

    $rndhn = rand(0, $cnthn - 1);
    $rndhnt = rand(0, $cnthnt - 1);

    $huisnummer = $hnl[$rndhn];
    $huisnummertoev = $hntl[$rndhnt];

    return array($huisnummer, $huisnummertoev);
}

function getHuisnumList($fields = array(), $tablename = "") {
    global $DB, $logger;
    $hnl = array();
    $cnthn = 0;
    $lenflds = count($fields);
    $sql = "SELECT ";
    $cnt = 0;
    foreach ($fields as $field) {
        if ($cnt != 0) {
            $sql .= ", ";
        }
        $sql .= "$field";
        $cnt++;
    }
    $sql .= " FROM {$tablename}";
    $huisnums = $DB->get_records($sql);
    if ($huisnums !== false) {
        $total = count($huisnums);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        foreach ($huisnums as $huisnum) {
            for ($i = 1; $i <= $lenflds; $i++) {
                $huisnummerfld = "huisnummer";
                if ($i >= 2) {
                    $huisnummerfld .= $i;
                }
                $huisnummer = "" . $huisnum->$huisnummerfld;
                if (!empty($huisnummer) && !in_array($huisnummer, $hnl)) {
                    $hnl[$cnthn] = $huisnummer;
                    $cnthn++;
                }
            }
            $cnt++;
            show_status($cnt, $total, "Create huisnummer list", $mcheck);
        }
    }
    return $hnl;
}

function getStraatnaamList($fields = array(), $tablename = "") {
    global $DB, $logger;
    $snl = array();
    $lenflds = count($fields);
    if ($lenflds > 0 && !empty($tablename)) {
        $cntsn = 0;
        $sql = "SELECT ";
        $cnt = 0;
        foreach ($fields as $field) {
            if ($cnt != 0) {
                $sql .= ", ";
            }
            $sql .= "$field";
            $cnt++;
        }
        $sql .= " FROM {$tablename}";
        $straatnamen = $DB->get_records($sql);
        if ($straatnamen !== false) {
            $total = count($straatnamen);
            $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
            $cnt = 0;
            foreach ($straatnamen as $straatnm) {
                for ($i = 1; $i <=$lenflds; $i++) {
                    $straatnaamfld = $fields[0];
                    if ($i >= 2) {
                        $straatnaamfld .= $i;
                    }
                    $straatnaam = "" . $straatnm->$straatnaamfld;
                    if (!empty($straatnaam) && !in_array($straatnaam, $snl)) {
                        $snl[$cntsn] = $straatnaam;
                        $cntsn++;
                    }
                }
                $cnt++;
                show_status($cnt, $total, "Create straatnamen list", $mcheck);
            }
        }
    }
    return $snl;
}

function getWVWoonplaatsList() {
    global $DB, $logger;
    $pcs = array();
    $lastpc = "";
    $sql = "SELECT Woonplaats, Woonplaats2, Woonplaats3, Woonplaats4, Postcode, Postcode2, Postcode3, Postcode4, Woonplaatsnaam, CodeNummerAanduid, CodeVerblijfplaats, Gemeentecode FROM Woonverbanden";
    $wplpcs = $DB->get_records($sql);
    if ($wplpcs !== false) {
        $total = count($wplpcs);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        foreach ($wplpcs as $wplpc) {
            $woonplaatsnaam = $codeNummerAanduid = $codeVerblijfplaats = $gemeentecode = "";
            for ($i = 1; $i <=4; $i++) {
                $wplfld = "Woonplaats";
                if ($i >= 2) {
                    $wplfld .= $i;
                } else {
                    $woonplaatsnaam .= $wplpc->Woonplaatsnaam;
                    $codeNummerAanduid .= $wplpc->CodeNummerAanduid;
                    $codeVerblijfplaats .= $wplpc->CodeVerblijfplaats;
                    $gemeentecode .= $wplpc->Gemeentecode;
                }
                $pcfld = "Postcode";
                if ($i >= 2) {
                    $pcfld .= $i;
                }
                $woonplaats = $wplpc->$wplfld;
                if (!empty($woonplaats)) {
                    $postcode = $wplpc->$pcfld;
                    if (!empty($postcode)) {
                        $lastpc = $postcode;
                    } else {
                        $postcode = $lastpc;
                    }
                    $pcoffset = (int)substr($postcode, 0, PC_PARSE_LEN);
                    $pcsar[$pcoffset][$postcode] = array($woonplaats, $woonplaatsnaam, $codeNummerAanduid, $codeVerblijfplaats, $gemeentecode);
                }
            }
            $cnt++;
            show_status($cnt, $total, "Create WV woonplaats list", $mcheck);
        }
    }
    ksort($pcsar);
    return $pcsar;
}

function getWoonplaatsPC($pcsar, $pci = -1) {
    $wpl = $pc = "";
    if ($pci  == -1) {
        $pci = rand(0, PC_PARSE_MAX);
    }
    while (!isset($pcsar[$pci])) {
        $pci = rand(0, PC_PARSE_MAX);
    }
    $wpcars = $pcsar[$pci];
    $arkeys = array_keys($wpcars);
    $totwpcars = count($wpcars);
    $rnd = rand(0, $totwpcars - 1);

    $pc = $arkeys[$rnd];
    $wpl = $wpcars[$pc];
    return array($wpl, $pc);
}


function getWoonplaatsList($fields = array(), $tablename = "") {
    global $DB, $logger;
    $pcs = array();
    $lenflds = count($fields);
    $lastpc = "";
    $sql = "SELECT ";
    $cnt = 0;
    foreach ($fields as $field) {
        if ($cnt != 0) {
            $sql .= ", ";
        }
        $sql .= "$field";
        $cnt++;
    }
    $sql .= ", postcode FROM {$tablename}";
    $wplpcs = $DB->get_records($sql);
    if ($wplpcs !== false) {
        $total = count($wplpcs);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        foreach ($wplpcs as $wplpc) {
            for ($i = 1; $i <= $lenflds; $i++) {
                $wplfld = "woonplaats";
                if ($i >= 2) {
                    $wplfld .= $i;
                }
                $pcfld = "postcode";
                if ($i >= 2) {
                    $pcfld .= $i;
                }
                $woonplaats = $wplpc->$wplfld;
                if (!empty($woonplaats)) {
                    $postcode = $wplpc->$pcfld;
                    if (!empty($postcode)) {
                        $lastpc = $postcode;
                    } else {
                        $postcode = $lastpc;
                    }
                    $pcoffset = (int)substr($postcode, 0, PC_PARSE_LEN);
                    $pcsar[$pcoffset][$postcode] = array($woonplaats);
                }
            }
            $cnt++;
            show_status($cnt, $total, "Create woonplaats list", $mcheck);
        }
    }
    ksort($pcsar);
    return $pcsar;
}

function getRegioList() {
    global $DB, $logger;
    $regios = array();
    $sql = "SELECT regio, count(regio) AS cnt FROM bureaus
            GROUP BY regio";
    $records = $DB->get_records($sql);
    if ($records !== false) {
        foreach ($records as $record) {
            $reg = $record->regio;
            $cnt = intval($record->cnt);
            if ($cnt > 0) {
                $regios[] = array($reg, $cnt);
            }
        }
    }
    return $regios;
}

function popRegioList(&$regios) {
    $len = count($regios);
    $regcel = array(-1, -1);
    if ($len  > 0) {
        $rndel = rand(0, ($len - 1));
        $regcel = $regios[$rndel];
        $cnt = $regcel[1];
        $cnt--;
        if ($cnt > 0) {
            $regios[$rndel] = array($regcel[0], $cnt);

        } else {
            //remove array element $rndel
            $removed = array_splice($regios, $rndel, 1);
        }
    }
    return $regcel;
}

function getSchoolList() {
    global $DB, $logger;
    $pcs = array();
    $lastpc = "";
    $sql = "SELECT schoolnummer, postcode FROM scholen";
    $slpcs = $DB->get_records($sql);
    if ($slpcs !== false) {
        $pcsar = array();
        $total = count($slpcs);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        foreach ($slpcs as $slpc) {
            $schoolnummer = $slpc->schoolnummer;
            $postcode = $slpc->postcode;
            $pcoffset = (int)substr($postcode, 0, PC_PARSE_LEN);
            $slnmar = array();
            if (isset($pcsar[$pcoffset])) {
                $slnmar = $pcsar[$pcoffset];
                array_push($slnmar, $schoolnummer);
            } else {
                $slnmar = array($schoolnummer);
            }
            $pcsar[$pcoffset] = $slnmar;
            $cnt++;
            show_status($cnt, $total, "Create Schoolnummer list", $mcheck);
        }
    }
    ksort($pcsar);
    return $pcsar;
}

function getSchoolPC($pcsar, $pci = -1) {
    $sln = $pc = "";
    if ($pci  == -1) {
        $pci = rand(0, PC_PARSE_MAX);
    }
    while (!isset($pcsar[$pci])) {
        $pci = rand(0, PC_PARSE_MAX);
    }
    $slnpcars = $pcsar[$pci];
    $arvals = array_values($slnpcars);
    $totslncars = count($arvals);
    $rnd = rand(0, $totslncars - 1);

    $snl = $arvals[$rnd];
    return $snl;
}


/*
 * Anonymize Bureaus table - takes about 1sec ;)
 * Combine with SQL execution
 *
 */
function anonBureaus($time, $maxrecords) {
    global $DB, $logger;
    $cnt = 0;

    $sql = "SELECT bureau, locatie, brieflocatie, email, telefoon FROM Bureaus";
    $bureaus = $DB->get_records($sql);
    if ($bureaus !== false) {
        $total = count($bureaus);
        $regioList = getRegioList();
        $updatesql = "UPDATE Bureaus
                          SET regio = :regio,
                          email = :email,
                          telefoon = :telefoon,
                          locatie = :locatie,
                          brieflocatie = :brieflocatie
                          WHERE bureau = :bureau";

        foreach ($bureaus as $bureau) {
            $bureauId = $bureau->bureau;

            $replacementEmail = "";
            $email = $bureau->email;
            if (!empty($email)) {
                $replacementEmail = generateAnonymousEmail($email);
            }
            $replacementtelefoon = "";
            $telefoon = $bureau->telefoon;
            if (!empty($telefoon)) {
                $replacementtelefoon = generateAnonymousPhn($telefoon);
            }

            $locatie = $bureau->locatie;
            $locatie_new = shuffleText($locatie);
            $brieflocatie = $bureau->brieflocatie;
            $brieflocatie_new = shuffleText($brieflocatie);

            $regio = popRegioList($regioList);
            $regioId = $regio[0];

            $params = array('bureau' => $bureauId, 'regio' => $regioId, 'email' => $replacementEmail, 'telefoon' => $replacementtelefoon,
                'locatie' => $locatie_new, 'brieflocatie' => $brieflocatie_new
            );
            $updated = $DB->execute($updatesql, $params);
            $cnt++;
            show_status($cnt, $total, "Bureaus process");
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}

function anonBureauXRefAFAS($time, $maxrecords) {
    global $DB, $logger;
    $sql = "SELECT rowid, mbxa.Bureaunummer_mlcas AS bureau, Locaties_Afdeling_ID_AFAS as afas, Locaties_Topdesk_code as topdesk, Adres_AFAS, b.regio FROM mi_bureau_x_afas mbxa 
            LEFT JOIN Bureaus b ON (mbxa.Bureaunummer_mlcas = b.Bureau) 
            ORDER BY b.regio";
    $bureauxs = $DB->get_records($sql);
    if ($bureauxs !== false) {
        $updatesql = "UPDATE mi_bureau_x_afas
                      SET Bureaunummer_mlcas = :newBureau,
                      Locaties_Afdeling_ID_AFAS = :newAFAS,
                      Locaties_Topdesk_code = :newTopdesk,
                      Adres_AFAS = :newAdresAFAS
                      WHERE rowid = :rowid";

        $regioBureaus = getBureauXListPerRegio();
        foreach ($bureauxs as $bureau) {
            $newBureau = $newAFAS = $newTopdesk = $new_AdresAFAS = null;
            $rowid = $bureau->rowid;
            $regio = $bureau->regio;
            $topdesk = $bureau->topdesk;
            $adresAFAS = $bureau->Adres_AFAS;
            
            list($newBureau, $newAFAS, $newTopdesk) =   getNewBureauByRegio($regioBureaus[$regio]);
            /*if (!empty($topdesk)) {
                $new_topdesk = shuffleText($topdesk, true);
            }*/
            if (!empty($adresAFAS)) {
                $new_AdresAFAS = shuffleText($adresAFAS);
            }
            $updated = $DB->execute($updatesql, array('rowid' => $rowid, 'newBureau' => $newBureau, 'newAFAS' => $newAFAS, 'newTopdesk' => $newTopdesk, 'newAdresAFAS' => $new_AdresAFAS));
            if (!$updated) {
                $error = $DB->getLastError();
                $logger->error("UPDATE mi_bureau_x_afas for row {$rowid} FAILED!");
            }
        }
    }
}

function getBureauXListPerRegio() {
    global $DB, $logger;
    $sql = "SELECT mbxa.Bureaunummer_mlcas AS bureau, Locaties_Afdeling_ID_AFAS as afas, Locaties_Topdesk_code as topdesk_code, b.regio FROM mi_bureau_x_afas mbxa
            LEFT JOIN Bureaus b ON (mbxa.Bureaunummer_mlcas = b.Bureau)
            ORDER BY b.regio";
    $bureauxs = $DB->get_records($sql);
    $newBureaus = array();
    if ($bureauxs !== false) {
        foreach ($bureauxs as $bureau) {
            $regio = "" . $bureau->regio;
            $buro = "" . $bureau->bureau;
            $afas = $bureau->afas;
            $topdesk_code = $bureau->topdesk_code;
            if (empty($newBureaus[$regio])) {
                $newBureaus[$regio] = array();
            }
            $afasses = $newBureaus[$regio];
            $afasses[] = array($buro, $afas, $topdesk_code);
            $newBureaus[$regio] = $afasses;
        }
    }
    return $newBureaus;
}

function getNewBureauByRegio($bureaus) {
    $len = count($bureaus);
    $burcel = array(-1, -1);
    if ($len  > 0) {
        $rndel = rand(0, ($len - 1));
        $burcel = $bureaus[$rndel];
    }
    return $burcel;
}




/*
 * Create array with AGBCodes - takes about 15sec
 *
 */

function getAGBList($tablename = "medewerkers") {
    global $DB, $logger;
    $agbcl = array();
    $cnt = 0;
    $sql = "SELECT distinct(agbcode) as agbcode FROM {$tablename}";
    $agbcodes = $DB->get_records($sql);
    if ($agbcodes!== false) {
        $total = count($agbcodes);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        foreach ($agbcodes as $agbcode) {
            $code = $agbcode->agbcode;
            $agbcl[$cnt] = $code;
            $cnt++;
            show_status($cnt, $total, "Create AGBCode list", $mcheck);
        }
    }
    return $agbcl;
}

/*
 * Anonymise Medewerkers tabel - takes about 5sec
 */
function anonMedewerkers($time, $maxrecords) {
    global $DB, $logger;
    $sql = "SELECT medewerkerid, indienst, uitdienst,
                   naam, briefnaam, achternaam, tussenvoegsel, voorletters, voornaam,
                   telefoon, extern,
                   email
            FROM medewerkers";
    $medewerkers = $DB->get_records($sql);
    $cntmedewerkers = count($medewerkers);
    if ($maxrecords > 0) {
        $cntmedewerkers = $maxrecords; // TODO remove after test
    }
    $cnt = 0;
    $mcheck = (int)($cntmedewerkers / (100 / SHOW_STATUS_MOD));
    if ($medewerkers !== false) {
        $agblist = getDistinctList("agbcode", "medewerkers");
        $externlist = getDistinctList("extern", "medewerkers");
        $updatesql = "UPDATE medewerkers
                      SET agbcode = :agbcode,
                      indienst =:indienst, uitdienst = :uitdienst,
                      naam = :naam, briefnaam = :briefnaam, achternaam = :achternaam, tussenvoegsel = :tussenvoegsel, voorletters = :voorletters, voornaam = :voornaam,
                      telefoon = :telefoon, extern = :extern, email = :email
                      WHERE medewerkerid = :medewerkerid";

        foreach ($medewerkers as $medewerker) {
            $id = $medewerker->medewerkerid;
            $agbCode = getRndDistinctEl($agblist);

            $indienst = $medewerker->indienst;
            $uitdienst = $medewerker->uitdienst;
            $indienst_new = $uitdienst_new = null;
            if (!empty($indienst) || !empty($uitdienst)) {
                $dateOffset = rndDayOffset();
                $addOffset = true;
                $sdateOffset = "P0D";
                if ($dateOffset < 0) {
                    $addOffset = false;
                    $repl = (-1 * $dateOffset);
                    $sdateOffset = str_replace("0", $repl, $sdateOffset);
                } else {
                    $sdateOffset = str_replace("0", $dateOffset, $sdateOffset);
                }
                $diOffset = new DateInterval($sdateOffset);

                if (!empty($indienst)) {
                    $dtindienst = new DateTime($indienst);
                    $indienst_new = getClonedShiftedDTasString($dtindienst, $diOffset, $addOffset);
                }
                if (!empty($uitdienst)) {
                    $dtuitdienst = new DateTime($uitdienst);
                    $uitdienst_new = getClonedShiftedDTasString($dtuitdienst, $diOffset, $addOffset);
                }
            }
            $naam = $medewerker->naam;
            $briefnaam = $medewerker->briefnaam;
            $achternaam = $medewerker->achternaam;
            $tussenvoegsel = $medewerker->tussenvoegsel;
            $voorletters = $medewerker->voorletters;
            $voornaam = $medewerker->voornaam;

            if (!empty($naam)) {
                $naam = shuffleText($naam);
            }
            if (!empty($briefnaam)) {
                $briefnaam = shuffleText($briefnaam);
            }
            if (!empty($achternaam)) {
                $achternaam = shuffleText($achternaam);
            }
            if (!empty($tussenvoegsel)) {
                $tussenvoegsel = shuffleText($tussenvoegsel);
            }
            if (!empty($voorletters)) {
                $voorletters = shuffleText($voorletters);
            }
            if (!empty($voornaam)) {
                $voornaam = shuffleText($voornaam);
            }

            $telefoon = $medewerker->telefoon;
            if (!empty($telefoon)) {
                $telefoon = generateAnonymousPhn($telefoon);
            }

            $extern = $medewerker->extern;
            if (!empty($extern)) {
                $extern = popDistinctList($externlist);
            }

            $email = $medewerker->email;
            if (!empty($email)) {
                $email = generateAnonymousEmail($email);
            }

            $params = array('medewerkerid' => $id, 'agbcode' => $agbCode,
                'indienst' => $indienst_new, 'uitdienst' => $uitdienst_new,
                'naam' => $naam, 'briefnaam' => $briefnaam, 'achternaam' =>$achternaam, 'tussenvoegsel' =>$tussenvoegsel, 'voorletters' =>$voorletters, 'voornaam' =>$voornaam,
                'telefoon' => $telefoon, 'extern' => $extern, 'email' => $email
            );

            $updated = $DB->execute($updatesql, $params);
            $cnt++;
            show_status($cnt, $cntmedewerkers, "Medewerkers process");
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}

/*
 * Anonymise verwijzers tabel - takes about 1 sec
 */
function anonVerwijzers($time, $maxrecords) {
    global $DB, $logger;
    $sql = "SELECT verwijzerid,
                   agbcode,
                   naam, adresnaam, organisatie,
                   straatnaam, huisnummer,
                   postcode, woonplaats,
                   telefoon, extern
            FROM verwijzers";
    $verwijzers = $DB->get_records($sql);
    if ($verwijzers !== false) {
        $cntverwijzers = count($verwijzers);
        if ($maxrecords > 0) {
            $cntverwijzers = $maxrecords; // TODO remove after test
        }
        $cnt = 0;
        $mcheck = (int)($cntverwijzers / (100 / SHOW_STATUS_MOD));

        $agblist = getDistinctList("agbcode", "verwijzers");

        $snl = getStraatnaamList(array('straatnaam'), "verwijzers");
        $cntsnl = count($snl);

        $wplpcl = getWoonplaatsList(array('distinct(woonplaats) as woonplaats'), "verwijzers");

        $hnl = getHuisnumList(array('distinct(huisnummer) as huisnummer'), "verwijzers");
        $cnthn = count($hnl);

        $updatesql = "UPDATE verwijzers
                      SET agbcode = :agbcode,
                      naam = :naam, adresnaam = :adresnaam, straatnaam = :straatnaam, huisnummer = :huisnummer,
                      organisatie = :organisatie, postcode = :postcode, woonplaats = :woonplaats,
                      telefoon = :telefoon, extern = :extern
                      WHERE verwijzerid = :verwijzerid";

        foreach ($verwijzers as $verwijzer) {
            $id = $verwijzer->verwijzerid;

            $agbCode = $verwijzer->agbcode;

            $naam = $verwijzer->naam;
            $adresnaam = $verwijzer->adresnaam;
            $organisatie = $verwijzer->organisatie;

            $straatnaam = $verwijzer->straatnaam;
            $huisnummer = $verwijzer->huisnummer;

            $woonplaats = $verwijzer->woonplaats;
            $postcode = $verwijzer->postcode;

            $pcoffset = -1;
            if (!empty($postcode)) {
                $pcoffset = (int)substr($postcode, 0, PC_PARSE_LEN);
                list($wpl, $pc) = getWoonplaatsPC($wplpcl, $pcoffset);
                $woonplaats = $wpl[WPL_WOONPLAATS];
                $postcode = $pc;
            } else if (!empty($woonplaats)) {
                $woonplaats = null;
            }

            $telefoon = $verwijzer->telefoon;
            $extern = $verwijzer->extern;

            if (!empty($agbCode)) {
                $agbCode = getRndDistinctEl($agblist);
            }

            if (!empty($naam)) {
                $naam = shuffleText($naam);
            }
            if (!empty($adresnaam)) {
                $adresnaam = shuffleText($adresnaam);
            }
            if (!empty($organisatie)) {
                $organisatie = shuffleText($organisatie);
            }

            if (!empty($straatnaam)) {
                $rndsnl = rand(0, $cntsnl - 1);
                $straatnaam = $snl[$rndsnl];
            }
            if (!empty($huisnummer)) {
                $rndhn = rand(0, $cnthn - 1);
                $huisnummer = $hnl[$rndhn];
            }

            if (!empty($telefoon)) {
                $telefoon = generateAnonymousPhn($telefoon);
            }
            if (!empty($extern)) {
                $extern = generateAnonymousPhn($extern);
            }

            $params = array('verwijzerid' => $id, 'agbcode' => $agbCode,
                'naam' => $naam, 'adresnaam' => $adresnaam, 'straatnaam' =>$straatnaam, 'huisnummer' =>$huisnummer,
                'organisatie' =>$organisatie,
                'postcode' =>$postcode, 'woonplaats' =>$woonplaats,
                'telefoon' => $telefoon, 'extern' => $extern
            );

            $updated = $DB->execute($updatesql, $params);
            $cnt++;
            show_status($cnt, $cntverwijzers, "Verwijzers process");
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}

/*
 * Create array with AGBCodes - takes about 15sec
 *
 */

function getDistinctList($fieldname = "", $tablename = "") {
    global $DB, $logger;
    $adl = array();
    $cnt = 0;
    $sql = "SELECT distinct({$fieldname}) as value FROM {$tablename}";
    $result = $DB->get_records($sql);
    if ($result !== false) {
        $total = count($result);
        $mcheck = (int)($total / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        foreach ($result as $element) {
            $value = $element->value;
            if ($value !== null) {
                $adl[$cnt] = $value;
                $cnt++;
                show_status($cnt, $total, "Create distinct list for field {$fieldname} in {$tablename}", $mcheck);
            }
        }
    }
    return $adl;
}

function getRndDistinctEl($adl) {
    $len = count($adl);
    $rndel = rand(0, $len - 1);
    $value = $adl[$rndel];
    return $value;
}

/*
 * Get random element from a list of distinct items and remove it from the list
 * Only works for items that are unique (or should be) in a database.
 */
function popDistinctList(&$distinctList) {
    $distinctel = null;
    if (is_array($distinctList)) {
        $len = count($distinctList);
        if ($len > 0) {
            $rndel = rand(0, ($len - 1));
            $distinctel = $distinctList[$rndel];
            //remove array element $rndel
            $removed = array_splice($distinctList, $rndel, 1);
        }
    }
    return $distinctel;
}



/*
 * Anonymise plannings tabel - takes about 70 sec
 */
function anonPlanning($time, $maxrecords) {
    global $DB, $logger;
    $sql = "SELECT p.planid,
                   p.afspraakbijz, p.afspraakvacc, p.afspraakdatum, p.timestamp,
                   ac.date_offset FROM planning p
                                  LEFT JOIN an_cross ac on (ac.casnummer = p.casnummer)";
    $plannings = $DB->get_records($sql);
    if ($plannings!== false) {
        $cntplannings = count($plannings);
        if ($maxrecords > 0) {
            $cntplannings = $maxrecords; // TODO remove after test
        }
        $cnt = 0;
        $mcheck = (int)($cntplannings / (100 / SHOW_STATUS_MOD));

        $afsprbijzList = getDistinctList("afspraakbijz", "planning");
        $afsprvacclist = getDistinctList("afspraakvacc", "planning");

        $updatesql = "UPDATE planning
                      SET afspraakbijz = :afspraakbijz, afspraakvacc = :afspraakvacc,
                          afspraakdatum = :afspraakdatum, timestamp = :timestamp
                      WHERE planid = :planid";

        foreach ($plannings as $planning) {
            $id = $planning->planid;

            $afspraakbijz = $planning->afspraakbijz;
            $afspraakvacc = $planning->afspraakvacc;
            $afspraakdatum = $planning->afspraakdatum;
            $timestamp = $planning->timestamp;

            $afspraakdatum_new = $timestamp_new = null;

            if (!empty($afspraakbijz)) {
                $afspraakbijz = getRndDistinctEl($afsprbijzList);
            }
            if (!empty($afspraakvacc)) {
                $afspraakvacc = getRndDistinctEl($afsprvacclist);
            }

            $dtafspraakdatum = $dttimestamp = null;
            if (!empty($afspraakdatum) || !empty($timestamp) ) {
                $dateOffset = rndDayOffset();
                $dateOffset = $planning->date_offset;
                $addOffset = true;
                $sdateOffset = "P0D";
                if ($dateOffset < 0) {
                    $addOffset = false;
                    $repl = (-1 * $dateOffset);
                    $sdateOffset = str_replace("0", $repl, $sdateOffset);
                } else {
                    $sdateOffset = str_replace("0", $dateOffset, $sdateOffset);
                }
                $diOffset = new DateInterval($sdateOffset);

                if (!empty($afspraakdatum)) {
                    $dtafspraakdatum = new DateTime($afspraakdatum);
                    $afspraakdatum_new = getClonedShiftedDTasString($dtafspraakdatum, $diOffset, $addOffset);
                }
                if (!empty($timestamp) ) {
                    $dttimestamp = new DateTime($timestamp);
                    $timestamp_new = getClonedShiftedDTasString($dttimestamp, $diOffset, $addOffset);
                }

            }

            $params = array('planid' => $id,
                'afspraakbijz' => $afspraakbijz, 'afspraakvacc' => $afspraakvacc,
                'afspraakdatum' => $afspraakdatum_new, 'timestamp' => $timestamp_new
            );

            $updated = $DB->execute($updatesql, $params);
            if (!$updated) {
                $error = $DB->getLastError();
                $logger->error("UPDATE planning record for id {$id} FAILED Error: " . print_r($error, true));
            }
            $cnt++;
            show_status($cnt, $cntplannings, "Planning process");
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}

/*
 * Anonymise Afspraken tabel - takes about 10 sec
 */
function anonAfspraken($time, $maxrecords) {
    global $DB, $logger;
    $sql = "SELECT a.afspraakid,
            a.afspraakbijz, a.afspraakvacc,
            a.afspraakdatum, a.afspraaktijd, a.linktijd, a.timestamp,
            ac.date_offset
            FROM afspraken a
            LEFT JOIN an_cross ac on (ac.casnummer = a.casnummer)";
    $afspraken = $DB->get_records($sql);
    if ($afspraken !== false) {
        $cntafspraken = count($afspraken);
        if ($maxrecords > 0) {
            $cntafspraken = $maxrecords; // TODO remove after test
        }
        $cnt = 0;
        $mcheck = (int)($cntafspraken / (100 / SHOW_STATUS_MOD));

        $afsprbijzList = getDistinctList("afspraakbijz", "afspraken");
        $afsprvacclist = getDistinctList("afspraakvacc", "afspraken");

        $updatesql = "UPDATE afspraken
                      SET afspraakbijz = :afspraakbijz, afspraakvacc = :afspraakvacc,
                          afspraakdatum = :afspraakdatum, afspraaktijd = :afspraaktijd, linktijd = :linktijd, timestamp = :timestamp
                      WHERE afspraakid = :afspraakid";

        foreach ($afspraken as $afspraak) {
            $id = $afspraak->afspraakid;

            $afspraakbijz = $afspraak->afspraakbijz;
            $afspraakvacc = $afspraak->afspraakvacc;
            $afspraakdatum = $afspraak->afspraakdatum;
            $afspraaktijd = $afspraak->afspraaktijd;
            $linktijd = $afspraak->linktijd;
            $timestamp = $afspraak->timestamp;

            if (!empty($afspraakbijz)) {
                $afspraakbijz = getRndDistinctEl($afsprbijzList);
            }
            if (!empty($afspraakvacc)) {
                $afspraakvacc = getRndDistinctEl($afsprvacclist);
            }

            $dateOffset = $afspraak->date_offset;
            $addOffset = true;
            $sdateOffset = "P0D";
            if ($dateOffset < 0) {
                $addOffset = false;
                $repl = (-1 * $dateOffset);
                $sdateOffset = str_replace("0", $repl, $sdateOffset);
            } else {
                $sdateOffset = str_replace("0", $dateOffset, $sdateOffset);
            }
            $diOffset = new DateInterval($sdateOffset);

            $dtafspraakdatum = $dtafspraaktijd = $dtlinktijd = $dttimestamp = null;
            $afspraakdatum_new = $afspraaktijd_new = $linktijd_new = $timestamp_new = null;
            if (!empty($afspraakdatum)) {
                $dtafspraakdatum = new DateTime($afspraakdatum);
                $afspraakdatum_new = getClonedShiftedDTasString($dtafspraakdatum, $diOffset, $addOffset);
            }
            if (!empty($afspraaktijd)) {
                $dtafspraaktijd = new DateTime($afspraaktijd);
                $afspraaktijd_new = getClonedShiftedDTasString($dtafspraaktijd, $diOffset, $addOffset);
            }
            if (!empty($linktijd)) {
                $dtlinktijd = new DateTime($linktijd);
                $linktijd_new = getClonedShiftedDTasString($dtlinktijd, $diOffset, $addOffset);
            }
            if (!empty($timestamp)) {
                $dttimestamp = new DateTime($timestamp);
                $timestamp_new = getClonedShiftedDTasString($dttimestamp, $diOffset, $addOffset);
            }

            $params = array('afspraakid' => $id,
                'afspraakbijz' => $afspraakbijz, 'afspraakvacc' => $afspraakvacc,
                'afspraakdatum' => $afspraakdatum_new, 'afspraaktijd' => $afspraaktijd_new, 'linktijd' => $linktijd_new, 'timestamp' => $timestamp_new
            );

            $updated = $DB->execute($updatesql, $params);
            if (!$updated) {
                $error = $DB->getLastError();
                $logger->error("UPDATE afspraken record for id {$id} FAILED Error: " . print_r($error, true));
            }
            $cnt++;
            show_status($cnt, $cntafspraken, "Afspraken process");
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}


/*
 * Anonymise AfsprakenHist tabel - takes about 11min (1.1m records)
 */
function anonAfsprakenHist($time, $maxrecords) {
    global $DB, $logger;
    $minmaxsql = "select min(afspraakid) as min, max(afspraakid) as max, count(*) as total from afsprakenhist";
    $mmafsprakenhist = $DB->get_record($minmaxsql);
    $minid = $mmafsprakenhist->min;
    $maxid = $mmafsprakenhist->max;
    $total = $mmafsprakenhist->total;
    $cntafsprakenhist = $total;
    if ($maxrecords > 0) {
        $cntafsprakenhist = $maxrecords; // TODO remove after test
    }
    $mcheck = (int)($cntafsprakenhist / (100 / SHOW_STATUS_MOD));
    $cnt = 0;

    $afsprbijzList = getDistinctList("afspraakbijz", "afsprakenhist");
    $afsprvacclist = getDistinctList("afspraakvacc", "afsprakenhist");
    $afsprverzidlist = getDistinctList("verzekeraarid", "afsprakenhist");
    
    $idstep = ceil($maxid /3) - 10;
    $startid = $minid;
    $endId = $startid + $idstep;
    while (true) {
        $sql = "SELECT ah.afspraakid,
                ah.afspraakbijz, ah.afspraakvacc, 
                ah.afspraakdatum, ah.afspraaktijd, ah.linktijd,
                ac.date_offset
                FROM afsprakenhist ah
                LEFT JOIN an_cross ac on (ac.casnummer = ah.casnummer)
                WHERE ah.afspraakid >= :startid AND ah.afspraakid <= :endid";
        // ,    ah.verzekeraarid,ah.polisnummer,
        $params = array('startid' => $startid, 'endid' => $endId);
        $afsprakenhist = $DB->get_records($sql, $params);
        if ($afsprakenhist !== false) {
    
            $updatesql = "UPDATE afsprakenhist
                          SET afspraakbijz = :afspraakbijz, afspraakvacc = :afspraakvacc,
                              afspraakdatum = :afspraakdatum, afspraaktijd = :afspraaktijd, linktijd = :linktijd
                          WHERE afspraakid = :afspraakid";
            
            foreach ($afsprakenhist as $afspraakhist) {
                $id = $afspraakhist->afspraakid;
    
                $afspraakbijz = $afspraakhist->afspraakbijz;
                $afspraakvacc = $afspraakhist->afspraakvacc;
    
                $afspraakdatum = $afspraakhist->afspraakdatum;
                $afspraaktijd = $afspraakhist->afspraaktijd;
                $linktijd = $afspraakhist->linktijd;
    
                if (!empty($afspraakbijz)) {
                    $afspraakbijz = getRndDistinctEl($afsprbijzList);
                }
                if (!empty($afspraakvacc)) {
                    $afspraakvacc = getRndDistinctEl($afsprvacclist);
                }
    
                $dateOffset = $afspraakhist->date_offset;
                $addOffset = true;
                $sdateOffset = "P0D";
                if ($dateOffset < 0) {
                    $addOffset = false;
                    $repl = (-1 * $dateOffset);
                    $sdateOffset = str_replace("0", $repl, $sdateOffset);
                } else {
                    $sdateOffset = str_replace("0", $dateOffset, $sdateOffset);
                }
                $diOffset = new DateInterval($sdateOffset);
    
                $dtafspraakdatum = $dtafspraaktijd = $dtlinktijd = null;
                $afspraakdatum_new = $afspraaktijd_new = $linktijd_new = null;
                if (!empty($afspraakdatum)) {
                    $dtafspraakdatum = new DateTime($afspraakdatum);
                    $afspraakdatum_new = getClonedShiftedDTasString($dtafspraakdatum, $diOffset, $addOffset);
                }
                if (!empty($afspraaktijd)) {
                    $dtafspraaktijd = new DateTime($afspraaktijd);
                    $afspraaktijd_new = getClonedShiftedDTasString($dtafspraaktijd, $diOffset, $addOffset);
                }
                if (!empty($linktijd)) {
                    $dtlinktijd = new DateTime($linktijd);
                    $linktijd_new = getClonedShiftedDTasString($dtlinktijd, $diOffset, $addOffset);
                }
    
                $params = array('afspraakid' => $id,
                    'afspraakbijz' => $afspraakbijz, 'afspraakvacc' => $afspraakvacc,
                    'afspraakdatum' => $afspraakdatum_new, 'afspraaktijd' => $afspraaktijd_new, 'linktijd' => $linktijd_new
                );
                
                $updated = $DB->execute($updatesql, $params);
                $cnt++;
                show_status($cnt, $cntafsprakenhist, "AfsprakenHist process", $mcheck);
                if ($maxrecords > 0 && $cnt > $maxrecords) {
                    break;
                }
            }
        }
        $startid = $endId + 1;
        $endId = $startid + $idstep;
        if ($startid > $maxid) {
            break;
        }
    }
}

/*
 * Convert dossierinfodata.info to random text where type is text.
 */
function anonDossierInfoDataNonRTF($time, $maxrecords) {
    global $DB, $logger;
    $sql = "SELECT dataid, info FROM dossierinfodata WHERE infotype = 'TEXT'";
    $datarecords = $DB->get_records($sql);
    $cntdata = count($datarecords);
    if ($maxrecords > 0) {
        $cntdata = $maxrecords;
    }
    $mcheck = (int)($cntdata / (100 / SHOW_STATUS_MOD));
    $cnt = 0;
    if ($datarecords !== false) {
        $updatessql = "UPDATE dossierinfodata SET info = :info WHERE dataid = :dataid";
        foreach ($datarecords as $datarecord) {
            $dataid = $datarecord->dataid;
            $infotxt = $datarecord->info;

            $infotxt_new = shuffleText($infotxt);

            $updated = $DB->execute($updatessql, array('dataid' => $dataid, 'info' => $infotxt_new));
            $cnt++;
            show_status($cnt, $cntdata, "Update dossierinfodata TEXT records", $mcheck);
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}

/*
 * Read documents from dossierinfodata.info where type is rtf,
 * dl <= 62618263 (size in bytes)
 * and store them locally
 *
 * Expects field dossierinfodata.infotype and dossierinfodata.dl to exist
 *
 * Only run once to determine what type of documents are stored:
 * if they contain private information (they are likely to)
 *
 */
function storeDossierInfoDataRTF($time, $maxrecords, $anonFields = false) {
    global $DB, $logger;
    $tempfiledir = FILE_OUTPUT_DIR;
    $sql = "SELECT dosi.casnummer, dosi.info as dosiinfo, did.dataid, did.dl FROM dossierinfodata did
            LEFT JOIN dossieritems dosi ON (did.itemid = dosi.itemid)
            WHERE did.infotype = 'RTF' AND did.dl <= 62618263 
            ORDER BY dosi.casnummer, did.dl desc"; // 
    $datarecords = $DB->get_records($sql);
    if ($datarecords !== false) {
        $commandfilename = "{$tempfiledir}\\copydiddummyfiles.cmd";
        $fh = fopen($commandfilename, "w+");
        
        $cntdata = count($datarecords);
        if ($maxrecords > 0) {
            $cntdata = $maxrecords;
        }
        $mcheck = (int)($cntdata / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        $sql = "SELECT dataid, itemid, info, dl FROM dossierinfodata WHERE dataid = :dataid";
        $updateSql = "UPDATE dossierinfodata SET info = :filename WHERE dataid = :dataid";
        foreach ($datarecords as $datarecord) {
            $itemid = $dl = $infotxt = $casnummer = $dosiinfo = $dataid = $data = $filedir = $filename = $written = null;
            $casnummer = $datarecord->casnummer;
            $dl = $datarecord->dl;
            $dosiinfo = $datarecord->dosiinfo;
            $dosiinfo = str_replace(" ", "_", $dosiinfo);
            $dosiinfo = str_replace("/", "", $dosiinfo);
            $dataid = $datarecord->dataid;
            $data = $DB->get_record($sql, array('dataid' => $dataid));
            if ($data !== false) {
                $itemid = $data->itemid;
                
                $filedir = "{$tempfiledir}\\{$casnummer}";
                if (!file_exists($filedir)) {
                    mkdir($filedir, 0777, true);
                }
                    
                $match1 = array();
                preg_match_all("/[A-Za-z0-9\-_]+/", $dosiinfo, $match1);
                $dosiinfo = $match1[0][0];
                if ($anonFields) {
                    $dosiinfo = shuffleText($dosiinfo, true); 
                }
                $filename = "{$filedir}\\didinfo_{$dataid}_{$itemid}_{$dosiinfo}.rtf";
                
                if (!$anonFields) {
                    if (file_exists($filedir)) {
                        $written = file_put_contents($filename, $data->info);
                        if ($written === false) {
                            $logger->error("Creation of file {$filename} failed. [storeDossierInfoDataRTF]");
                        }
                        
                    } else {
                        $logger->error("Creation of directry {$filedir} failed. [storeDossierInfoDataRTF]");
                        
                    }
                }
                
                if ($anonFields) {
                    $updated = $DB->execute($updateSql, array('dataid' => $dataid, 'filename' => $filename));
                    if (!$updated) {
                        $logger->error("Update of dossierinfodata record {$dataid} failed.");
                    }
                    
                    $dummyfilename = getReplacementFileName($dl);
                    if ($fh !== false) {
                        fwrite($fh, ("copy {$dummyfilename} {$filename}" . PHP_EOL));
                    } else {
                        $logger->error("Creation of file line {$dummyfilename} to {$filename} failed. [storeDossierInfoDataRTF]");
                    }
                }
            }
            $cnt++;
            show_status($cnt, $cntdata, "Store RTF docs ", $mcheck);
            if ($cnt > $cntdata) {
                break;
            }
        }
        if ($fh !== null) {
            fclose($fh);
        }
    }
    $logger->info("Done.");
}

/*
 * This functionality is currently not in use anymore.
 * See anon option in storeDossierInfoDataRTF()
 * 
 * Replace rtf documents in DossierInfoData with dummy documents of about same size
 *
 * Expects field dossierinfodata.infotype and dossierinfodata.dl to exist
 *
 * Takes about 11min
 *
 */
function anonDossierInfoDataRTF($time, $maxrecords) {
    global $DB, $logger;
    $dummyfiledir = "\\prj\\ggd-db-migratie-tooling\\dummyfiles";
    $sql = "SELECT dataid, itemid, dl FROM dossierinfodata WHERE  infotype = 'RTF' ORDER BY dl desc";
    $datarecords = $DB->get_records($sql);
    $cntdata = count($datarecords);
    if ($maxrecords > 0) {
        $cntdata = $maxrecords;
    }
    $mcheck = (int)($cntdata / (100 / SHOW_STATUS_MOD));
    $cnt = 0;
    if ($datarecords!== false) {
        foreach ($datarecords as $datarecord) {
            $itemid = $dl = $infotxt = null;
            $dataid = $datarecord->dataid;
            $itemid = $datarecord->itemid;
            $dl = $datarecord->dl;
            $filename = getReplacementFileName($dl);
            $updatessql = "UPDATE dossierinfodata SET info = (SELECT * FROM OPENROWSET(BULK N'{$filename}', SINGLE_BLOB) rs) WHERE dataid = :dataid";
            $updated = $DB->execute($updatessql, array('dataid' => $dataid));
            if ($updated === false) {
                $error = $DB->getLastError();
                $logger->debug("Update of record id: {$dataid} failed. Error: {$error}");
            }
            $filename = $fhblob = $blob = null;
            $cnt++;
            show_status($cnt, $cntdata, "Replace RTF docs in DB"); // , $mcheck);
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}

/*
 * Read documents from DossierInterventie.doel, DossierInterventie.aanleiding, DossierInterventie.evaluatie and DossierInterventie.analyse where type is rtf,
 * totsize < 44707960 (size in bytes)
 * and store them locally
 *
 * Expects field DossierInterventie.?type and DossierInterventie.?size
 * as well as DossierInterventie.totsize and DossierInterventie.bin_rtf_type to exist
 * and bin_rtf_type to be filled with - or X
 *
 * Only run once to determine what type of documents are stored:
 * if they contain private information (they are likely to)
 *
 */
function storeDossierInterventieDataRTF($time, $maxrecords, $anonFields = false) {
    global $DB, $logger;
    $tempfiledir = FILE_OUTPUT_DIR;
    $sql = "SELECT divd.volgnummer, divd.totsize, dosi.casnummer, dosi.info as dosiinfo FROM dossierinterventiedata divd
            LEFT JOIN DossierItems dosi ON (divd.itemid = dosi.itemid)
            WHERE NOT bin_rtf_type = '----' AND totsize < 44707960 ORDER BY totsize desc"; //  AND totsize < 44707960 ORDER BY totsize desc"; // , 90001841, 44707960
    $datarecords = $DB->get_records($sql);
    if ($datarecords !== false) {
        $cntdata = count($datarecords);
        if ($maxrecords > 0) {
            $cntdata = $maxrecords;
        }
        $mcheck = (int)($cntdata / (100 / SHOW_STATUS_MOD));
        $cnt = 0;
        $sql = "SELECT volgnummer, interventieid, doelsize, aanleidingsize, evaluatiesize, analysesize, totsize, bin_rtf_type FROM dossierinterventiedata";
        if (!$anonFields) {
            $sql .= ", doel, aanleiding, evaluatie, analyse";
        }
        $sql .= " WHERE volgnummer = :volgnummer";
        $sqlUpdateBase = "UPDATE dossierinterventiedata SET ";
        $sqlUpdateWhere = " WHERE volgnummer = :volgnummer";
        
        $commandfilename = "{$tempfiledir}\\copydivddummyfiles.cmd";
        $fh = fopen($commandfilename, "w+");
        
        foreach ($datarecords as $datarecord) {
            $interventieid = $volgnummer = $casnummer = $dosiinfo = $data = $filedir = $filename = $filenamebase = $written = $rtf_type = null;
            $sqlUpdateFields = array();
            $volgnummer  = $datarecord->volgnummer;
            $casnummer = $datarecord->casnummer;
            $dosiinfo = $datarecord->dosiinfo;
            $dosiinfo = str_replace(" ", "_", $dosiinfo);
            $dosiinfo = str_replace("/", "", $dosiinfo);
            $data = $DB->get_record($sql, array('volgnummer' => $volgnummer));
            if ($data !== false) {
                $interventieid = $data->interventieid;
                $rtf_type = $data->bin_rtf_type;
                $doelsize = $data->doelsize;
                $aanleidingsize = $data->aanleidingsize;
                $evaluatiesize = $data->evaluatiesize;
                $analysesize = $data->analysesize;
                $doeltext = $aanleidingtext = $evaluatietext = $analysetext = "";
                
                $filedir = "{$tempfiledir}\\{$casnummer}";
                if (!file_exists($filedir)) {
                    mkdir($filedir, 0777, true);
                }
                
                if (!file_exists($filedir)) {
                    continue;
                }
                
                $match1 = array();
                preg_match_all("/[A-Za-z0-9\-_]+/", $dosiinfo, $match1);
                $dosiinfo = implode($match1[0]);
                $filenamebase = "{$filedir}\\divdinfo_{$volgnummer}_{$dosiinfo}";
    
                if (substr($rtf_type, 0, 1) == 'X') {
                    $filename = "{$filenamebase}_doel.rtf";
                    if (!$anonFields) {
                        $written = file_put_contents($filename, $data->doel);
                        if ($written === false) {
                            $logger->debug("Creation of file {$filename} failed. [storeDossierInterventieDataRTF]");
                        }
                    }
                    $sqlUpdateFields['doel'] = $filename;
                    $dummyfilename = getReplacementFileName($doelsize);
                    if ($fh !== false) {
                        fwrite($fh, ("copy {$dummyfilename} {$filename}" . PHP_EOL));
                    } else {
                        $logger->error("Creation of file line {$dummyfilename} to {$filename} failed. [storeDossierInterventieDataRTF]");
                    }
                }
    
                if (substr($rtf_type, 1, 1) == 'X') {
                    $filename = "{$filenamebase}_aanl.rtf";
                    if (!$anonFields) {
                        $written = file_put_contents($filename, $data->aanleiding);
                        if ($written === false) {
                            $logger->debug("Creation of file {$filename} failed. [storeDossierInterventieDataRTF]");
                        }
                    }
                    $sqlUpdateFields['aanleiding'] = $filename;
                    $dummyfilename = getReplacementFileName($aanleidingsize);
                    if ($fh !== false) {
                        fwrite($fh, ("copy {$dummyfilename} {$filename}" . PHP_EOL));
                    } else {
                        $logger->error("Creation of file line {$dummyfilename} to {$filename} failed. [storeDossierInterventieDataRTF]");
                    }
                }
    
                if (substr($rtf_type, 2, 1) == 'X') {
                    $filename = "{$filenamebase}_eval.rtf";
                    if (!$anonFields) {
                        $written = file_put_contents($filename, $data->evaluatie);
                        if ($written === false) {
                            $logger->debug("Creation of file {$filename} failed. [storeDossierInterventieDataRTF]");
                        }
                    }
                    $sqlUpdateFields['evaluatie'] = $filename;
                    $dummyfilename = getReplacementFileName($evaluatiesize);
                    if ($fh !== false) {
                        fwrite($fh, ("copy {$dummyfilename} {$filename}" . PHP_EOL));
                    } else {
                        $logger->error("Creation of file line {$dummyfilename} to {$filename} failed. [storeDossierInterventieDataRTF]");
                    }
                }
    
                if (substr($rtf_type, 3, 1) == 'X') {
                    $filename = "{$filenamebase}_analyse.rtf";
                    if (!$anonFields) {
                        $written = file_put_contents($filename, $data->analyse);
                        if ($written === false) {
                            $logger->debug("Creation of file {$filename} failed. [storeDossierInterventieDataRTF]");
                        }
                    }
                    $sqlUpdateFields['analyse'] = $filename;
                    $dummyfilename = getReplacementFileName($analysesize);
                    if ($fh !== false) {
                        fwrite($fh, ("copy {$dummyfilename} {$filename}" . PHP_EOL));
                    } else {
                        $logger->error("Creation of file line {$dummyfilename} to {$filename} failed. [storeDossierInterventieDataRTF]");
                    }
                }
                
                if ($anonFields && count($sqlUpdateFields) > 0) {
                    $params = array('volgnummer' => $volgnummer);
                    $sqlUpdate = $sqlUpdateBase;
                    $fieldCnt = 0;
                    foreach ($sqlUpdateFields as $fieldName => $fieldValue) {
                        if ($fieldCnt > 0) {
                            $sqlUpdate .= ", ";
                        }
                        $sqlUpdate .= "{$fieldName} = :{$fieldName}";
                        $params[$fieldName] = $fieldValue;
                        $fieldCnt++;
                    }
                    $sqlUpdate .= $sqlUpdateWhere;
                    $updated = $DB->execute($sqlUpdate, $params);
                    if (!$updated) {
                        $logger->debug("Failed to update dossierinterventiedata record {$volgnummer}.");
                    }
                }
                
            }
            $cnt++;
            show_status($cnt, $cntdata, "Store DossierInterventieData RTF docs ", $mcheck);
            if ($cnt > $cntdata) {
                break;
            }
        }
        if ($fh !== null) {
            fclose($fh);
        }
    }
}

/*
 * Replace rtf documents in dossierinterventiedata with dummy documents of about same size
 * or with shuffled Text
 *
 * Expects field totsize, doeltype, aanleidingtype, evaluatietype, analysetype to exist
 *
 * Takes about 3min
 *
 */
function anonDossierInterventieData($time, $maxrecords) {
    // TODO: no longer update RTF fields here
    global $DB, $logger;
    $sql = "SELECT volgnummer, totsize, doeltype, aanleidingtype, evaluatietype, analysetype FROM dossierinterventiedata ORDER BY totsize desc";
    $datarecords = $DB->get_records($sql);
    $cntdata = count($datarecords);
    if ($maxrecords > 0) {
        $cntdata = $maxrecords;
    }
    $mcheck = (int)($cntdata / (100 / SHOW_STATUS_MOD));
    $cnt = 0;
    if ($datarecords!== false) {
        foreach ($datarecords as $datarecord) {
            $volgnummer = $datarecord->volgnummer;
            $totsize = $datarecord->totsize;

            $rtffields = array();
            $textfields = array();

            $doeltype = $datarecord->doeltype;
            $aanleidingtype = $datarecord->aanleidingtype;
            $evaluatietype = $datarecord->evaluatietype;
            $analysetype = $datarecord->analysetype;

            $hasblob = false;
            if ($doeltype == 'RTF') {
                $rtffields['doel'] = 'doel';
            } else {
                $textfields['doel'] = 'doel';
            }
            if ($aanleidingtype == 'RTF') {
                $rtffields['aanleiding'] = 'aanleiding';
            } else {
                $textfields['aanleiding'] = 'aanleiding';
            }
            if ($evaluatietype == 'RTF') {
                $rtffields['evaluatie'] = 'evaluatie';
            } else {
                $textfields['evaluatie'] = 'evaluatie';
            }
            if ($analysetype == 'RTF') {
                $rtffields['analyse'] = 'analyse';
            } else {
                $textfields['analyse'] = 'analyse';
            }

            $selectsql = "SELECT ";
            $updatessql = "UPDATE dossierinterventiedata
                   SET ";
            $params = array('volgnummer' => $volgnummer);
            $hasprevparam = false;
            $paramname = $paramvalue = "";
            $executeupdate = false;
            
            $rtffields = array(); // TODO - remove RTF code alltogether, for now just ignore RTF fields
            if (count($rtffields) > 0) {
                $hasblob = true;
                foreach ($rtffields as $rtffield) {
                    list($paramname, $paramvalue) = createDossierInterventieDataParam($rtffield, $totsize, true);
                    $params[$paramname] = $paramvalue;
                    if (!$hasprevparam) {
                        $hasprevparam = true;
                    } else {
                        $updatessql .= ", ";
                    }
                    $updatessql .= "{$rtffield} = :{$paramname}";
                    $paramname = $paramvalue = "";
                    $executeupdate = true;
                }
            }

            $hasprevparam = false;
            if (count($textfields) > 0) {
                foreach ($textfields as $textfield) {
                    if (!$hasprevparam) {
                        $hasprevparam = true;
                    } else {
                        $selectsql .= ", ";
                    }
                    $selectsql .= "{$textfield}";
                }
                $selectsql .= " FROM dossierinterventiedata WHERE volgnummer = :volgnummer";
                $result = $DB->get_record($selectsql, array('volgnummer' => $volgnummer));
                $hasprevparam = false;
                foreach ($textfields as $textfield) {
                    $textvalue = $result->$textfield;
                    if (!empty($textvalue)) {
                        $paramname = "{$textfield}";
                        $paramvalue = shuffleText($textvalue, false, true);
                        $params[$paramname] = $paramvalue;
                        if (!$hasprevparam) {
                            $hasprevparam = true;
                        } else {
                            $updatessql .= ", ";
                        }
                        $updatessql .= "{$textfield} = :{$paramname}";
                        $paramname = $paramvalue = "";
                        $executeupdate = true;
                    }
                }
            }
            $updatessql .= " WHERE volgnummer = :volgnummer";
            if ($executeupdate) {
                $updated = $DB->execute($updatessql, $params, $hasblob);
                if ($updated === false) {
                    $logger->debug("Update of dossierinterventiedata record volgnummer: {$volgnummer} failed.");
                }
            }

            foreach ($params as $paramname => $paramvalue) {
                if (startsWith($paramname, "blob") && ($paramvalue !== false && $paramvalue !== '')) {
                    fclose($paramvalue);
                    $paramvalue = null;
                }
            }

            $params = null;

            $cnt++;
            show_status($cnt, $cntdata, "Replace dossierinterventiedata data in DB", $mcheck);
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}


function createDossierInterventieDataParam($fieldname, $totsize, $rtf = false) {
    global $logger;
    $dummyfiledir = "\\prj\\ggd-db-migratie-tooling\\dummyfiles";
    
    $paramname = "{$fieldname}";
    $paramvalue = "";
    if ($rtf) {
        $paramname = "blob{$fieldname}";

        $filename = getReplacementFileName($totsize);
        $fhblob = fopen($filename, 'rb');
        if ($fhblob === false) {
            $logger->debug("Reading file {$filename} failed.");
        } else {
            $paramvalue = $fhblob;
            // $logger->debug("Reading file {$filename} succes.");
        }
    }
    return array($paramname, $paramvalue);
}

function getReplacementFileName($totsize, $extension = ".rtf") {
    $dummyfiledir = "\\prj\\ggd-db-migratie-tooling\\dummyfiles";
    
    $refsize = - 1;
    $refbase = "kb";
    $sizekb = ($totsize / 1024);
    if ($sizekb < 2) {
        $refsize = 1;
    } else if ($sizekb < 5) {
        $refsize = 2;
    } else if ($sizekb < 10) {
        $refsize = 5;
    } else if ($sizekb < 25) {
        $refsize = 10;
    } else if ($sizekb < 50) {
        $refsize = 25;
    } else if ($sizekb < 100) {
        $refsize = 50;
    } else if ($sizekb < 200) {
        $refsize = 100;
    } else if ($sizekb < 500) {
        $refsize = 200;
    } else if ($sizekb < 1000) {
        $refsize = 500;
    } else {

        $refbase = "mb";
        $sizemb = round(($totsize / 1024000), 0, PHP_ROUND_HALF_UP);
        if ($sizemb < 1) {
            $refsize = 0;
        } else if ($sizemb < 5) {
            $refsize = 1;
        } else if ($sizemb < 10) {
            $refsize = 5;
        } else if ($sizemb < 40) {
            $refsize = 10;
        } else if ($sizemb < 60) {
            $refsize = 40;
        } else if ($sizemb < 70) {
            $refsize = 60;
        } else if ($sizemb >= 70) {
            $refsize = 70;
        }
    }
    $filename = "{$dummyfiledir}\\dummydoc_{$refsize}{$refbase}{$extension}";
    return $filename;
}

/*
 * Replace documents in archief with dummy rtf documents of about same size
 *
 * Expects field dl to exist
 *
 * Takes about
 *
 */
function anonArchief($time, $maxrecords) {
    global $DBC, $logger;
    $sql = "SELECT archiefid, filename, dl FROM archief ORDER BY dl desc"; // WHERE dl < 44996032 
    $datarecords = $DBC->get_records($sql);
    $cntdata = count($datarecords);
    if ($maxrecords > 0) {
        $cntdata = $maxrecords;
    }
    $mcheck = (int)($cntdata / (100 / SHOW_STATUS_MOD));
    $cnt = 0;
    if ($datarecords!== false) {
        foreach ($datarecords as $datarecord) {
            $archiefid = $datarecord->archiefid;
            $size = $datarecord->dl;
            $filename = $datarecord->filename;
            $nodataupdate = false;
            if ($size <= 4) {
                $nodataupdate = true;
            }
            
            $fn = trim($filename);
            $fnlastbit = strtolower(strrchr($fn, '.'));
            if ($fnlastbit == '.docx') {
                $fnlastbit = ".doc";
            }
            if ($fnlastbit != '.doc' && $fnlastbit != '.pdf' && $fnlastbit != '.rtf' && $fnlastbit != '.txt') {
                $fnlastbit = ".rtf";
            }
            
            $filename = utf8_encode(shuffleText($filename, true));
            $filename .= $fnlastbit;
            $params = array('archiefid' => $archiefid, 'filename' => $filename);
            
            if ($nodataupdate) {
                $updatessql = "UPDATE archief SET filename = :filename WHERE archiefid = :archiefid";
                
            } else if ($fnlastbit == '.txt') {
                $nwtext = str_repeat("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", round($size/52));
                $nwtext .= substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", -($size-52)); 
                $params['newdata'] = shuffleText($nwtext);
                $updatessql = "UPDATE archief SET data = CONVERT(VARBINARY(MAX), :newdata, 0), filename = :filename WHERE archiefid = :archiefid";
                
            } else {
                $dummyfilename = getReplacementFileName($size, $fnlastbit);
                $updatessql = "UPDATE archief SET data = (SELECT * FROM OPENROWSET(BULK N'{$dummyfilename}', SINGLE_BLOB) rs), filename = :filename WHERE archiefid = :archiefid";
                
            }

            $updated = $DBC->execute($updatessql, $params);
            if ($updated === false) {
                $error = $DBC->getLastError();
                $logger->debug("Update of archief record archiefid: {$archiefid} failed. " . print_r($error, true));
            }
            $filename = $filenamepath = $value = $datavalue = $updatessql = $params = $paramvalue = null;

            $cnt++;
            show_status($cnt, $cntdata, "Replace archief data in DB", $mcheck);
            if ($maxrecords > 0 && $cnt > $maxrecords) {
                break;
            }
        }
    }
}

function createLogboek($time, $maxrecords) {
    global $DB, $logger;
    $tempfiledir = FILE_OUTPUT_DIR;
    $sqlall = "SELECT * FROM (
               SELECT n.casnummer, count(n.casnummer) as  cnt FROM notities n
               LEFT JOIN persoonsgegevens pg ON (pg.casnummer = n.casnummer)
               GROUP BY n.casnummer) cntlog
               ORDER BY cnt desc, casnummer";
    
    // WHERE (pg.status = 0 OR pg.status = 5)
    
    
    $sql = "SELECT n.notitieid, n.tekst, n.timestamp, n.userid, co.casKort as soort FROM Notities n
            LEFT JOIN Casoms co ON (co.casWaarde = n.status AND co.casSoort = 40)
            WHERE casnummer = :casnummer
            ORDER BY timestamp desc"; //
    $statusText = "Process logboek data";
    $casrecords = $DB->get_records($sqlall);
    if ($casrecords !== false) {
        try {
            $cntdata = count($casrecords);
            if ($maxrecords > 0) {
                $cntdata = $maxrecords;
            }
            $mcheck = (int) ($cntdata / (100 / 0.025));
            $cnt = 0;
            $lastcasnummer = null;
            $fh = null;
            foreach ($casrecords as $casrecord) {
                $casnummer = $casrecord->casnummer;
                $lines = "Tijdstip;UserId;Soort;Tekst\n";
                $datarecords = $DB->get_records($sql, array(
                    'casnummer' => $casnummer
                ));
                if ($datarecords !== false) {
                    foreach ($datarecords as $datarecord) {
                        $lines .= $datarecord->timestamp . ";" . $datarecord->userid . ";" . $datarecord->soort . ";" . $datarecord->tekst . "\n";
                    }
                    if ($fh !== null) {
                        fclose($fh);
                    }
                    $fh = null;
                    $filename = null;
                    $filedir = "{$tempfiledir}\\{$casnummer}";
                    if (!file_exists($filedir)) {
                        mkdir($filedir, 0777, true);
                    }
                    if (file_exists($filedir)) {
                        $filename = "{$filedir}\\logboek_{$casnummer}.txt";
                    }
                    if (!empty($filename)) {
                        $fh = fopen($filename, "w+");
                        if ($fh !== false) {
                            fwrite($fh, $lines);
                        } else
                            $logger->error("Creation of file {$filename} failed. [createLogboek]");
                    }
                    if ($fh !== null) {
                        fclose($fh);
                        $fh = null;
                    }
                    $cnt++;
                    show_status($cnt, $cntdata, $statusText, $mcheck); //
                }
            }
        } catch (Exception $e) {
            print_r($e);
        }
    }
}


// Key velden Casnummer, BSN,

// Bureau, GehoorBureau, HuisartsID - or obscurify linked tables?

// Fields to check an obscurify for R003
// Persoonsgegevens
// BSN, BSNOuder1, BSNOuder2, Geslacht, Geboortedatum, Overlijdensdatum, DatumVestiging, DatumVertrek, GebLandcode, Nationaliteit, SchoolMutatie, UitZorg
// Email, Email2?, Telefoon, Telefoon2, Telefoon3, Leerjaar (niet groter dan 8?)

// Fields to obscurify
// Voornamen, GbaVoorvoegsel, Geslachtsnaam, Roepnaam, Voorvoegsel, Achternaam, KlasGroep, NaamOuder1, NaamOuder2, Polisnummer
// PersMemo
// Schoolnummer - Scholen - Location Postcode may change?
// VerwijzerID - Verwijzers - Location Postcode may change? (No other verwijzer info present?!)

// GebPlaats Always NULL for HN - Part of R003

// Non R003 fields persoonsgevens to obscurify
// Aanmelddatum, GebLandOuder1code, GebLandOuder2code, GebOuder1, GebOuder2

// Questionable - obscurify or not
// IndicatieGezag,
// IndicatieGeheim - always 0 for HN?
// Schooljaar - If so, how?

// Unclear persoonsgegevens
// Anummer
// Dossierlocatie
// Extern
// PersBijz
// PersMods
// PersOrg (always NULL for HN)
// SaveID

// R003 not supported?
// GebPlaats (always NULL for HN)

// Not obscurify
// ClientSrt
// ConsultSoort
// InZorg

// Clear
// Controle
// DossierEigenaar (only 6 entries for HN?)
// DossierToegang (oonly 1 entry for HN?)
// FailedLoginCount
// GbaIndicatieDat (always empty for HN?)
// Kostengroep
// LastLoginAttempt
// LastLoginDate
// LastPasswordChangedDate
// LastPincodeSentDate
// Machtigingsnummer (always empty for HN?)
// Password
// Pincode
// TaalCode
// Teamlocatie (always empty for HN?)
// TelefoonOms3 (always empty for HN?)
// TimeStamp
// Titelcode (1 x JH, 1 x JV)
// UserID
// Verwijsdatum (always empty for HN?)
// Verwijzersoort (always empty for HN?)
// VerzekeraarID (only 3 Id's present for HN?)
// WidIdDocNummer (always empty for HN?)
// WidIdDocType (always 0 for HN)


/*

Copyright (c) 2010, dealnews.com, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of dealnews.com, Inc. nor the names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

/**
 * show a status bar in the console
 *
 * <code>
 * for($x=1;$x<=100;$x++){
 *
 *     show_status($x, 100);
 *
 *     usleep(100000);
 *
 * }
 * </code>
 *
 * @param   int     $done   how many items are completed
 * @param   int     $total  how many items are to be done total
 * @param   string  $text   optional text to show on the status bar
 * @param   int     $size   optional size of the status bar
 * @return  void
 *
 */

function show_status($done, $total, $text = "", $mcheck = 0, $size=30) {

    static $start_time;

    if ($done > 1 && $mcheck > 0) {
        if (($done % $mcheck) !== 0) {
            return;
        }
    }

    // if we go over our bound, just ignore it
    if($done > $total) return;

    if(empty($start_time)) $start_time=time();
    $now = time();

    $perc=(double)($done/$total);

    $bar=floor($perc*$size);
    $bartext = "";
    if (!empty($text)) {
        $bartext = "{$text} ";
    }

    $status_bar="\r{$bartext}[";
    $status_bar.=str_repeat("=", $bar);
    if($bar<$size){
        $status_bar.=">";
        $status_bar.=str_repeat(" ", $size-$bar);
    } else {
        $status_bar.="=";
    }

    $disp=number_format($perc*100, 0);

    $status_bar.="] $disp%  $done/$total";

    $rate = ($now-$start_time)/$done;
    $left = $total - $done;
    $eta = round($rate * $left, 2);

    $elapsed = $now - $start_time;

    $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

    echo "$status_bar  ";

    flush();

    // when done, send a newline
    if($done == $total) {
        echo "\n";
    }

}