<?php
use PHPUnit\Framework\TestCase;

$filename = dirname(__FILE__) . '/../db-info.php';
require_once($filename);

class CrossTableTest extends TestCase {

    private $lasterror = null;

    protected function setUp() {
        //
    }
    /*
    public function testCodes() {
        global $DB;
        $gebLandcodes = getGebLandCodesList();
        for ($i = 0; $i < 100; $i++) {
            $testgblc = popGebLandCodesList($gebLandcodes);
            $this->assertNotEquals(-1, $testgblc[0], "No landcodes found!");
        }
    }
    
    public function testNationaliteiten() {
        global $DB;
        $nationaliteiten = getNationaliteitenList();
        for ($i = 0; $i < 100; $i++) {
            $testnat = popNationaliteitenList($nationaliteiten);
            $this->assertNotEquals(-1, $testnat[0], "No nationaliteiten found!");
        }
    }
*/
    public function testVoornamen() {
        global $DB, $logger;
        $voornamenList = getNamenList("voornamen", true);
        $lenvoornamenList = count($voornamenList);
        for ($i = 0; $i < 100; $i++) {
            $rndel = rand(0, ($lenvoornamenList - 1));
            $voornamen = $voornamenList[$rndel];
            $logger->debug($voornamen);
            $this->assertNotEquals("", $voornamen, "No qualified voornaam created!");
        }
    }

    public function testGeslachtsnamen() {
        global $DB, $logger;
        $voornamenList = getNamenList("roepnaam", true);
        $lenvoornamenList = count($voornamenList);
        for ($i = 0; $i < 100; $i++) {
            $rndel = rand(0, ($lenvoornamenList - 1));
            $voornamen = $voornamenList[$rndel];
            $logger->debug($voornamen);
            $this->assertNotEquals("", $voornamen, "No qualified Geslachtsnaam created!");
        }
    }
    
    /*
    public function testGbavv() {
        global $DB, $logger;
        $gbavvList = getGbavvList();
        $lengbavvList = count($gbavvList);
        for ($i = 0; $i < 100; $i++) {
            $rndel = rand(0, ($lengbavvList - 1));
            $gbavv = $gbavvList[$rndel];
            $logger->debug($gbavv );
            $this->assertNotEquals("", $gbavv , "No qualified gbavv created!");
        }
    }
    
    public function testLeerjaarKlasgroep() {
        global $DB, $logger;
        $ljkgList = getLeerjaarGroepList();
        $keylist = array_keys($ljkgList);
        $lenLjkgList = count($keylist);
        for ($i = 0; $i < 100; $i++) {
            $rndel = rand(0, ($lenLjkgList - 1));
            $key = $keylist[$rndel];
            $klasgroepen = $ljkgList[$key];
            // $logger->debug($ljkg);
            // $this->assertNotEquals("", $ljkg, "No qualified ljkg created!");
        }
    }
    
    public function testLeerjarenKlasgroep() {
        global $DB;
        $list = getLeerjaarGroepList();
        $sql = "SELECT DISTINCT(leerjaar) as leerjaar FROM persoonsgegevens";
        $records = $DB->get_records($sql );
        foreach ($records as $record) {
            $leerjaar = $record->leerjaar;
            $groep = getLeerjaarklasgroep($list, $leerjaar);
            if ($groep  === null) {
                $this->assertNotEquals(null, $groep , "No qualified groep found for leerjaar {$leerjaar} !");
            }
        }
    }
    
    public function testRegios() {
        global $DB;
        $regios = getRegioList();
        for ($i = 0; $i < 100; $i++) {
            $testreg = popRegioList($regios);
            $this->assertNotEquals(-1, $testreg[0], "No regios found!");
        }
    }
  
    public function testBureauXListPerRegio() {
        global $DB;
        $regiobureaus = getBureauXListPerRegio();
        foreach ($regiobureaus as $regio => $bureaus) {
            list($testbureau, $testafas, $testtopdek) = getNewBureauByRegio($regiobureaus[$regio]);
            $this->assertNotEquals(-1, $testbureau, "No buro found!");
            $this->assertNotEquals(-1, $testafas, "No afas found!");
            $this->assertNotEquals(-1, $testtopdek, "No afas found!");
        }
    }
    
  */
      
}
?>