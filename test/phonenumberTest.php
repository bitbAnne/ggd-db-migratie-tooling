<?php
use PHPUnit\Framework\TestCase;

$filename = dirname(__FILE__) . '/../db-info.php';
require_once($filename);

class PhoneTest extends TestCase {

    private $lasterror = null;

    protected function setUp() {
        //
    }

    public function testAddressTableExists() {
        global $DB;
        // '223860981M', '06119535504p', '228754326P', '06436697759m', '0034622755156v', '06499344233V', '754866moeder'
        $numbers = array('0652333220Mv'
        ); // '0651536229oma', '0728443613oma', '0681953722oma', '0725113281oma', '0639597786oma', '0228752782oma', '0725626979oma', '0229216621Opa', '0626210843oma', '0654686428Oma', '0725612227oma', '0610483849oma', '0229505442oma', '0636199481Oma', '0618691440Oma', '06617446139M', '622923941M', '06229949908p', '0725726852plm', '0611494270oma', '31685325021m', '1825CP', '624737055v', '0620446556pl', '0651339489pl', '0644600252oom', '0614568989m1', '0646273199mob', '0649035410stP', '0617094202oma', 'geheimadres', '0685805500mob', '629463906m', 'pleeg0653525513', '0657749400mob', '06013365757M', '0728441530om', '064023886m', 'broerMaarte', '0652314535m0', '0645958241mo', '00645200721M', '630597809M', '0685783223Amir', '0650418431Oma', '068733524M', '0685770072oom', '628713P', '0655362898opa', '061185450m', '725617821vader', '610013533m', '623890903M', '06644735337p', '653854511M', '06499344233V', '0610383359p06', '638430031v', '1628XP', '0610612814mob', '1825CM', '0655101344pl', '619661232p', '0642303630plm', '0653176793plee', '630401363m', '06851969601v', '0618774991oma', '063375985M', '0614416679vad', '0624292754mob', '0652442422mob', '447766129977m', '0625264328plm', '0620970713Oma', '630708voor1', '0613390445mo', '1825AM', '0648966012num', '0251655726opa', 'Geheimnumme', '0622442527mob', '0620044206opa', '0611001996mob', '06187287132M', '0227541002oma', '228720122P', '0611929512mob', '0642442784kim', '0645475521pleeg', '622428898p', '0612179358oma', '645489782M', '0627585640mo', '061462674m', '064156355p', '0654984336mob', '0637790361mob', '754866moeder', '0651248051sP', '642170054M', 'Geheimtelnr', '0623053620mob', '0651827053oma', '0619454106opa', '1825GM', '228526494M', '0639845579v0', 'janitasam@ho', '618168069m', '611527744m', '0616269739mob', '644197060M', '0223641492Cam', '647776982m', '0651355452oma', '651966920P', '0615087647mo', '611426369m', '627627803M', 'serefosman@hot', '0654962668mob', '634916748m', '657944215v', '613606064p', '637372896M', '617730788P', '0681012040vad', '1625VN', '612769907m', '06780316m', 'cmussies@gm', '623241210m', '0641797761mbm', '610054841v', '0682250805oma', '648763471M', '0229235483mluc', '0644232744nv', '0634018605ver', 'mohle5@zonne', '06419792216M', '0229234289plm', '621295599M', '061460027m', 'moederheeftte', '615091881M', 'ANNAPAULOWN', '611950659m', '0626309073oom', '630734481m', '633956745m', '0612363733v0', 'mpknuit@quic', '610549129m', '0651298609vrie', 'zieinterventie', '0627076151p0', '0640148883pm', '652306409m', '06633909106M', '06426657375M', '31627060924m');
        foreach ($numbers as $number) {
            list($relation, $number) = parseRelationFromTel($number);
            $isvalid = validPhoneNumber($number);
            
            $this->assertEquals(true, $isvalid, "Phonenumber {$number} is invalid");
        }
    }

    
    /*
     public function testAddressTableExists() {
        global $DB;
        $result = false;
        $sql = "SELECT COUNT(*) FROM table";
        try {
            $result = $DB->countQuery($sql);
        } catch (Exception $e) {
            // silent
            $this->lasterror = $e;
        }
        if ($result === false) {
            $result = -1;
        }
        
        $this->assertEquals(0, $result, "Table table does NOT exist!");
    }*/
}
?>