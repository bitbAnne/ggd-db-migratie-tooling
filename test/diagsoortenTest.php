<?php
use PHPUnit\Framework\TestCase;

$filename = dirname(__FILE__) . '/../db-info.php';
require_once($filename);

class DiagsoortenTest extends TestCase {

    private $lasterror = null;

    protected function setUp() {
        //
    }

    public function testCoded30() {
        global $DB;
        $result = false;
        $sql = "SELECT DISTINCT(CODED30) FROM DiagSoorten";
        $codeds = $DB->get_records($sql);
        $this->assertNotEquals(false, $codeds, "Table DiagSoorten has no records!");
        if ($codeds !== false) {
            foreach ($codeds as $coded) {
                $value = $coded->CODED30;
                $this->assertGreaterThan(-1, $value, "Value is not 0 or bigger!");
                
                $iswholenumber = false;
                if ($value == 0) {
                    $iswholenumber = true;
                
                } else {
                    $iswholenumber = $this->checkPowerOf($value, 2);
                } 
                $this->assertEquals(true, $iswholenumber, "Log {$value} / Log 2 is not a whole number!");
            }
        }
    }
    
    public function testCoded60() {
        global $DB;
        $result = false;
        $sql = "SELECT DISTINCT(CODED60) FROM DiagSoorten";
        $codeds = $DB->get_records($sql);
        $this->assertNotEquals(false, $codeds, "Table DiagSoorten has no records!");
        if ($codeds !== false) {
            foreach ($codeds as $coded) {
                $value = $coded->CODED60;
                $this->assertGreaterThan(-1, $value, "Value is not 0 or bigger!");
                
                $iswholenumber = false;
                if ($value == 0) {
                    $iswholenumber = true;
                    
                } else {
                    $iswholenumber = $this->checkPowerOf($value, 2);
                }
                $this->assertEquals(true, $iswholenumber, "Log {$value} / Log 2 is not a whole number!");
            }
        }
    }
    
    private function checkPowerOf($value, $power = 2) {
        $isWholenumber = false;
        $logvalue = log($value);
        $logPower = log($power);
        
        $tocheck = ($logvalue / $logPower);
        $check = "{$tocheck}";
        $isWholenumber = ctype_digit($check);
        return $isWholenumber;
    }
}
?>