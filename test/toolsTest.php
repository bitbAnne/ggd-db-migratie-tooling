<?php
use PHPUnit\Framework\TestCase;

$filename = dirname(__FILE__) . '/../db-info.php';
require_once($filename);

class ToolsTest extends TestCase {

    private $lasterror = null;

    protected function setUp() {
        //
    }

        /*
     * public function testShuffleText() {
     * global $DB;
     * $texts = array("Hello, world,how are you, doing", "This is not a good sentence, is it?", "No komma today");
     * // foreach ($texts as $text) {
     * // $test = shuffleText($text, false, true);
     * // }
     * foreach ($texts as $text) {
     * $test = shuffleText($text, false, true);
     * }
     * }
     */
        /*
     * public function testGenerateAnummer() {
     * global $DB;
     * $cntpersons = 246527;
     * // $cntpersons = 1000;
     * $min = 1.010109759E9;// 1.010109759E9
     * $max = 9.898971348E9; // 9.898971348E9
     * $anummers_new = generateAnummer($cntpersons, $min, $max);
     * $this->assertInternalType('array', $anummers_new);
     * }
     */
        
    /*
     * public function testWVHuisnumList() {
     * list($hnl, $hntl) = getWVHuisnumList();
     * $this->assertInternalType('array', $hnl);
     * $this->assertInternalType('array', $hntl);
     *
     * list($hn, $hnt) = getWVHuisnumToev($hnl, $hntl);
     * $this->assertInternalType('string', $hn);
     * $this->assertInternalType('string', $hnt);
     * }
     */
        
    /*
     * public function testStraatNaamList() {
     * $snl2 = getStraatnaamList(array('straatnaam'), "verwijzers");
     * $this->assertInternalType('array', $snl2);
     * $snl3 = getStraatnaamList(array('straatnaam', 'straatnaam2', 'straatnaam3', 'straatnaam4'), "woonverbanden");
     * $this->assertInternalType('array', $snl3);
     *
     * $cntsml = count($snl2);
     * $rndsnl = rand(0, $cntsml - 1);
     * $straatnaam = $snl2[$rndsnl];
     * $this->assertInternalType('string', $straatnaam);
     *
     * $cntsml = count($snl3);
     * $rndsnl = rand(0, $cntsml - 1);
     * $straatnaam = $snl3[$rndsnl];
     * $this->assertInternalType('string', $straatnaam);
     * }
     */
        
    /*
     * public function testWVWoonplaatsPCList() {
     * $wplpcl = getWVWoonplaatsList();
     * $this->assertInternalType('array', $wplpcl);
     *
     * list($wpl, $pc) = getWoonplaatsPC($wplpcl);
     * $woonplaats = $wpl[WPL_WOONPLAATS];
     * $woonplaatsnaam = $wpl[WPL_WOONPLAATSNAAM];
     * $codeNummerAanduid = $wpl[WPL_CODENUMMERAANDUID];
     * $codeVerblijfplaats = $wpl[WPL_CODEVERBLIJFPLAATS];
     * $gemeentecode = $wpl[WPL_GEMEENTECODE];
     * $pc_ = $pc;
     * }
     */
        
    /*
     * public function testTime() {
     * $stime = trackTime();
     * $time = $stime - 60;
     * trackTime($time);
     * $time = $stime - 600;
     * trackTime($time);
     * $time = $stime - 3600;
     * trackTime($time);
     * $time = $stime - 6000;
     * trackTime($time);
     * $time = $stime - 36000;
     * trackTime($time);
     * $time = $stime - 60000;
     * trackTime($time);
     * }
     */
        
    /*
     * public function testAGBList() {
     * $agbl = getDistinctList("agbcode", "medewerkers");
     * $this->assertInternalType('array', $agbl);
     *
     * for ($i = 0; $i < 10; $i++) {
     * $code = getRndDistinctEl($agbl);
     * $this->assertNotNull($code);
     * }
     * }
     */
        
    /*
     * public function testWoonplaatsPCList() {
     * $wplpcl = getWoonplaatsList(array('distinct(woonplaats) as woonplaats'), "verwijzers");
     * $this->assertInternalType('array', $wplpcl);
     *
     * list($wpl, $pc) = getWoonplaatsPC($wplpcl);
     * $woonplaats = $wpl[WPL_WOONPLAATS];
     * $pc_ = $pc;
     * }
     */
        
    /*
     * public function testAGBList() {
     * $agbl = getDistinctList("agbcode", "medewerkers");
     * $this->assertInternalType('array', $agbl);
     *
     * for ($i = 0; $i < 10; $i++) {
     * $code = getRndDistinctEl($agbl);
     * $this->assertNotNull($code);
     * }
     * }
     */
        
    /*
     * public function testHuisnumList() {
     *
     * $hnl = getHuisnumList(array('distinct(huisnummer) as huisnummer'), "verwijzers");
     * $cnthn = count($hnl);
     *
     * for ($i = 0; $i < 10; $i++) {
     * $rndhn = rand(0, $cnthn - 1);
     * $huisnummer = $hnl[$rndhn];
     * $this->assertNotNull($huisnummer);
     * }
     * }
     */
        
    /*
     * public function testDistinctList() {
     * $list = getDistinctList("agbcode", "medewerkers");
     * $this->assertInternalType('array', $list);
     *
     * $afsprbijzList = getDistinctList("afspraakbijz", "planning");
     * $afsprvaccList = getDistinctList("AfspraakVacc", "planning");
     *
     * for ($i = 0; $i < 10; $i++) {
     * $value = getRndDistinctEl($list);
     * $afb = getRndDistinctEl($afsprbijzList);
     * $afv = getRndDistinctEl($afsprbijzList);
     * $this->assertNotNull($value);
     * $this->assertNotNull($afb);
     * $this->assertNotNull($afv);
     * }
     * }
     */
        
    /*
     * public function testSchoolList() {
     *
     * $slpcl = getSchoolList();
     * $this->assertInternalType('array', $slpcl);
     * $scnnum = getSchoolPC($slpcl, 161);
     * $this->assertTrue($scnnum != null, "Not null");
     *
     * }
     */
        
    /*
     * public function testCasnummers() {
     * $cnt = 246527;
     * $newcasnummers = generateNewCasnummers($cnt);
     * $this->assertEquals($cnt, count($newcasnummers));
     * }
     */
        
    /*
     * public function testErpOrg() {
     * $initialresponse = "";
     *
     * $urls[0] = "https://52751.rest.afas.online/profitrestservices/connectors/DDJGZ_CRM_Organisaties";
     * $urls[1] = "https://52751.afasonlineconnector.nl/Profitrestservices/connectors/DDJGZ_CRM_Organisaties";
     * $header_name = "Authorization";
     * $header_key = "AfasToken";
     * $header_value = "";
     *
     * $jsonResponse = null;
     *
     * $cnt = 2000;
     * while ($cnt > 0) {
     * $rnd = mt_rand(0, 1);
     * $url = $urls[$rnd];
     * // Initiate curl
     * $ch = curl_init();
     * // Set The Response Format to Json
     * curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json", "AfasToken: ", "Authorization: AfasToken "));
     * // Disable SSL verification
     * curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     * // Will return the response, if false it print the response
     * curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     * // Set the url
     * curl_setopt($ch, CURLOPT_URL, $url);
     * // Execute
     * $jsonResponse = curl_exec($ch);
     * $error = curl_error($ch);
     * $errorno = curl_errno($ch);
     * // Closing
     * curl_close($ch);
     *
     * if (empty($initialresponse)) {
     * $initialresponse = $jsonResponse;
     * continue;
     * }
     * $sameMessage = ($jsonResponse == $initialresponse);
     * $this->assertTrue($sameMessage, "Same message");
     *
     * $cnt--;
     * }
     *
     * }
     */

    public function testFileParse() {
        
        $dummyfiledir = "\\prj\\ggd-db-migratie-tooling\\dummyfiles";
        
        for ($i = 0; $i < 1000; $i++) {
            
            $size = rand(1024000, (80 * 1024000));
            
            $refsize = -1;
            $dlmb = round(($size /1024000), 0, PHP_ROUND_HALF_UP);
            if ($dlmb < 1) {
                $refsize = 0;
                
            } else if ($dlmb < 5) {
                $refsize = 1;
                
            } else if ($dlmb < 10) {
                $refsize = 5;
                
            } else if ($dlmb < 40) {
                $refsize = 10;
                
            } else if ($dlmb < 60) {
                $refsize = 40;
                
            } else if ($dlmb < 70) {
                $refsize = 60;
                
            } else if ($dlmb >= 70) {
                $refsize = 70; // Bigger than 70Mb breaks PHP?
                
            }
            $filename1 = "{$dummyfiledir}\\dummydoc_{$refsize}mb.rtf";
            
            $filename2 = getReplacementFileName($size);
            
            if ($filename1 !== $filename2) {
                echo "break here";
            }
        }
    }    
    
}
?>