<?php

$filename = dirname(__FILE__) . '/../db-info.php';
require_once($filename);

$dummyfiledir = "\\prj\\ggd-db-migratie-tooling\\dummyfiles";

$dataid = 323691;
$refsize = 5;
$filename = "{$dummyfiledir}\\dummydoc_{$refsize}mb.rtf";

$updatessql = "UPDATE dossierinfodata SET Info = (SELECT * FROM OPENROWSET(BULK N'{$filename}', SINGLE_BLOB) rs) WHERE DataId = :dataid";

$updated = $DB->execute($updatessql, array('dataid' => $dataid));
if ($updated === false) {
    $error = $DB->getLastError();
    $logger->debug("Update of record id: {$dataid} failed. {$error}");
}