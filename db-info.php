<?php

// Key velden Casnummer, BSN, 

// Bureau, GehoorBureau, HuisartsID - or obscurify linked tables?

// Fields to check an obscurify for R003
// Persoonsgegevens
// BSN, BSNOuder1, BSNOuder2, Geslacht, Geboortedatum, Overlijdensdatum, DatumVestiging, DatumVertrek, GebLandcode, Nationaliteit, SchoolMutatie, UitZorg
// Email, Email2?, Telefoon, Telefoon2, Telefoon3, Leerjaar (niet groter dan 8?) 

// Fields to obscurify
// Voornamen, GbaVoorvoegsel, Geslachtsnaam, Roepnaam, Voorvoegsel, Achternaam, KlasGroep, NaamOuder1, NaamOuder2, Polisnummer
// PersMemo
// Schoolnummer - Scholen - Location Postcode may change?
// VerwijzerID - Verwijzers - Location Postcode may change? (No other verwijzer info present?!) 

// GebPlaats Always NULL for HN - Part of R003

// Non R003 fields persoonsgevens to obscurify
// Aanmelddatum, GebLandOuder1code, GebLandOuder2code, GebOuder1, GebOuder2

// Questionable - obscurify or not
// IndicatieGezag,
// IndicatieGeheim - always 0 for HN?
// Schooljaar - If so, how?

// Unclear persoonsgegevens
// Anummer
// Dossierlocatie
// Extern
// PersBijz
// PersMods
// PersOrg (always NULL for HN)
// SaveID

// R003 not supported?
// GebPlaats (always NULL for HN)

// Not obscurify
// ClientSrt
// ConsultSoort
// InZorg

// Clear
// Controle
// DossierEigenaar (only 6 entries for HN?)
// DossierToegang (oonly 1 entry for HN?)
// FailedLoginCount
// GbaIndicatieDat (always empty for HN?)
// Kostengroep
// LastLoginAttempt
// LastLoginDate
// LastPasswordChangedDate
// LastPincodeSentDate
// Machtigingsnummer (always empty for HN?)
// Password
// Pincode
// TaalCode
// Teamlocatie (always empty for HN?)
// TelefoonOms3 (always empty for HN?)
// TimeStamp
// Titelcode (1 x JH, 1 x JV)
// UserID
// Verwijsdatum (always empty for HN?)
// Verwijzersoort (always empty for HN?)
// VerzekeraarID (only 3 Id's present for HN?)
// WidIdDocNummer (always empty for HN?)
// WidIdDocType (always 0 for HN)

include_once("constants.php");
include_once("appconfig.php");

$showProgress = true;
$cmdLine = false;

if (isset($argv)) {
    $cmdLine = true;
    if (count($argv) > 1) {
        if (strtolower($argv[1]) == '--noprogress') {
            $showProgress = false;
        }
    }
}


if ($cmdLine) {
    // checkAfscriftJGZDossier();
    
    // checkArchiefDocTypes();

    // checkDuplicateFilenames();

    // checkBSNs();
    // checkBSNs("bsnouder1");
    // checkBSNs("bsnouder2");
    // checkAnumbers();
    
    // shuffleEmail();

    // shufflePhonenumbers(false, true);
}

function checkAfscriftJGZDossier() {
    global $DB, $logger;
    $sql = "SELECT dosi.casnummer, ddd.diagitemid, ddd.data FROM DossierItems dosi 
            JOIN DossierDiagData ddd on dosi.ItemID = ddd.ItemID and ddd.diagItemId IN (2075, 2076, 2077)
            ORDER BY casnummer";
    $records = $DB->get_records($sql);
    $casnrs2076 = array();
    $casnrs_non_2076 = array();
    foreach ($records as $record) {
        if($record->diagitemid == 2076) {
            $casnrs2076[] = $record->casnummer;
        } else {
            $casnrs_non_2076[] = $record->casnummer;
        } 
    }
    foreach ($casnrs_non_2076  as $casnummer) {
        if (!in_array($casnummer, $casnrs2076)) {
            $logger->error("Casnummer [{$casnummer}] heeft geen datum!");
        }
    }
   
}