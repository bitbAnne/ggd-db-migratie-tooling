<?php

include_once("constants.php");
include_once("appconfig.php");

$time = trackTime();

$showProgress = true;
$cmdLine = false;

if (isset($argv)) {
    $cmdLine = true;
    if (count($argv) > 1) {
        if (strtolower($argv[1]) == '--noprogress') {
            $showProgress = false;
        }
    }
}

$maxrecords = -1; // 1000; // TODO remove after test // -1;

anonBureauXRefAFAS($time, $maxrecords);
trackTime($time, true);

anonPersoonsGegevensBasevalues($time, $maxrecords, false);
trackTime($time, true);

anonDossieritems($time, $maxrecords);
trackTime($time);

anonDiagitems($time, $maxrecords);
trackTime($time);

anonDossierDiagCharData($time, $maxrecords); // 30min
trackTime($time);

anonDossierDiagData($time, $maxrecords);
trackTime($time);

anonWoonverbanden($time, $maxrecords);
trackTime($time);

anonBureaus($time, $maxrecords);
trackTime($time);

anonMedewerkers($time, $maxrecords);
trackTime($time);

anonVerwijzers($time, $maxrecords);
trackTime($time);

anonPlanning($time, $maxrecords);
trackTime($time);

anonAfspraken($time, $maxrecords);
trackTime($time);

anonAfsprakenHist($time, $maxrecords);
trackTime($time);

anonDossierInfoDataNonRTF($time, $maxrecords);
trackTime($time);
storeDossierInfoDataRTF($time, $maxrecords, true);
trackTime($time);

storeDossierInterventieDataRTF($time, $maxrecords, true);
trackTime($time);

anonDossierInterventieData($time, $maxrecords);
trackTime($time);

anonArchief($time, $maxrecords);
trackTime($time);
