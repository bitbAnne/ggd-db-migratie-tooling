<?php

include_once("constants.php");
include_once("appconfig.php");

$logger->info(print_r($argv, true));

$dbcode = "HNT"; 

$readdbfile = $argv[1];
if (count($argv) > 2) {
    $dbcode = $argv[2];
}

$newStructure = array();

// Open end product csv
$csvreadbase = fopen(GGD_DB_STRUCTURE_CSV, 'r');
if ($csvreadbase !== false) {
    while (($csvline = fgetcsv($csvreadbase, 0, ";"))) {
        $value = array();
        if (count($csvline) > 2) {
            $cnt = 0;
            foreach ($csvline as $element) {
                $cnt++;
                if ($cnt > 2) {
                    $value[$element] = "x"; 
                }
            }
        }
        $newStructure[$csvline[TABLE_NAME]][$csvline[COLUMN_NAME]] = $value;
    }
}

// Open structure db
$csvreaddb = fopen($readdbfile, 'r');

if ($csvreaddb !== false) {

    // Copy original structure into new structure
    // Iterate throug db structure, add to new structure
    // Iterate through  new structure, setting flag for ggd is column present
    $cnt = 0;
    
    while (($csvline = fgetcsv($csvreaddb, 0, ";"))) {
        $cnt++;
        If ($cnt > 1) {
            $newStructure[$csvline[TABLE_NAME]][$csvline[COLUMN_NAME]][$dbcode] = "x";
        }
    }
    
    fclose($csvreadbase);
    
    $logger->debug(print_r($newStructure));
    
    // Iterate through  new structure, saving it as csv
    $csvwritebase = fopen(GGD_DB_STRUCTURE_CSV, 'w');
    foreach ($newStructure as $table_name => $colums) {
        foreach ($colums as $colum_name => $value) {
            $csvline = array($table_name, $colum_name);
            foreach  ($value as $key => $keyvalue) {
                $csvline[] = $key;
            }
            fputcsv($csvwritebase, $csvline, ";");
        }
    }

}
