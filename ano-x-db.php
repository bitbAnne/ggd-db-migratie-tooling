<?php

include_once("constants.php");
include_once("appconfig.php");

$time = trackTime();

$showProgress = true;
$cmdLine = false;

if (isset($argv)) {
    $cmdLine = true;
    if (count($argv) > 1) {
        if (strtolower($argv[1]) == '--noprogress') {
            $showProgress = false;
        }
    }
}

$maxrecords = -1; // 1000; // TODO remove after test // -1;  

// Fill cross table from scratch
cleanAnCrossTable();
trackTime($time, true);

// Fill cross table with casNummer
fillAnCrossCasnummers();
trackTime($time, true);

fillCrossTable($time, $maxrecords);
trackTime($time);

createBsnxTable();
trackTime($time);

/*
*/
