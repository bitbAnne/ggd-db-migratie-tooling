﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.DTO;
using Services.Extensions;
using Services;

using System.Collections.Generic;

namespace TestServices
{
    [TestClass]
    public class PDFTests
    {

        [TestMethod]
        public void CreatePDFCorrectWay()
        {
            List<int> casenumbers = new List<int>(new int[] { 
                219435,
                232419,
                219906,
                221988,
                224435,
                219121
            
            });

            foreach (int casenumber in casenumbers)
            {
            int casNummer = casenumber; //172991, 14694, 14022, 11355, 10590, 5469, 5557, 15554 ; 
            var document = PDFLogic.CreateDocument(casNummer);
            document.Save(casNummer.ToString(),casNummer,true);

           
            Assert.IsTrue(true);
            }
        }
    }
}
