﻿using System.Net;
using Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Export
{
    class Program
    {
        static void Main(string[] args)
        {
            
            DateTime startDate;
            DateTime endDate;
            setConsoleSize();


            DateTime.TryParseExact(ConfigurationManager.AppSettings["aanvangsdatum"], "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
            DateTime.TryParseExact(ConfigurationManager.AppSettings["einddatum"], "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);
            endDate = endDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
     
            if (args.Length == 0)
            {
                Console.WriteLine(startDate.ToString() + " tot " + endDate.ToString());
                HomeLogic.RunDailyService(startDate, endDate);
            }

        }

        static void setConsoleSize()
        {
            System.Console.SetWindowPosition(0, 0);   // sets window position to upper left  
            System.Console.SetBufferSize(300, 400);   // make sure buffer is bigger than window  
            System.Console.SetWindowSize(132, 40);   //set window size to almost full screen   
        }
    }
}
