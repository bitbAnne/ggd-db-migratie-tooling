﻿using Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Extensions;

namespace Services
{
    public static class AppointmentLogic
    {
        public static List<Afspraak> AppointmentsPerPerson(Persoonsgegevens pg)
        {
            var appointments = pg.Planning.Select(CreateAppointment).ToList();
            appointments.AddRange(pg.Afspraken.Select(CreateAppointment).ToList());
            appointments.AddRange(pg.AfsprakenHist.Select(CreateAppointment).ToList());
            return appointments.OrderByDescending(p => p.Datum).ToList();
        }

        private static Afspraak CreateAppointment(AfsprakenHist ah)
        {
            string briefnummer = "";
            if (ah.BriefNummer != null)
            {
                briefnummer = ah.BriefNummer.ToString();
            }

            return new Afspraak()
            {
                Datum = ah.AfspraakDatum,
                Bijz = ah.AfspraakBijz,
                Brief = briefnummer,
                Code = ah.AfspraakTrig.GetDescription(),
                Discipline = ah.Disciplines.Afkorting,
                Duur = ah.Afspraakduur.ToString(),
                Soort = ah.ConsultSoorten.Afkorting,
                Status = ah.Resultaat.GetDescription(),
                Team = ah.Bureau.ToString(),
                Tijd = ah.AfspraakTijd,
                Vacc = ah.AfspraakVacc
            };
        }

        private static Afspraak CreateAppointment(Afspraken ah)
        {
            string briefnummer = "";
            if (ah.BriefNummer != null)
            {
                briefnummer = ah.BriefNummer.ToString();
            }

            return new Afspraak()
            {
                Datum = ah.AfspraakDatum,
                Bijz = ah.AfspraakBijz,
                Brief = briefnummer,
                Code = ah.AfspraakTrig.GetDescription(),
                Discipline = ah.Disciplines.Afkorting,
                Duur = ah.Afspraakduur.ToString(),
                Soort = ah.ConsultSoorten.Afkorting,
                Status = ah.Resultaat.GetDescription(),
                Team = ah.Bureau.ToString(),
                Tijd = ah.AfspraakTijd,
                Vacc = ah.AfspraakVacc
            };
        }

        private static Afspraak CreateAppointment(Planning ah)
        {
            string briefnummer = ah.BriefNummer.ToString();

            return new Afspraak()
            {
                Datum = ah.AfspraakDatum,
                Bijz = ah.AfspraakBijz,
                Brief = briefnummer,
                Code = ah.AfspraakTrig.GetDescription(),
                Discipline = ah.Disciplines.Afkorting,
                Duur = ah.Afspraakduur.ToString(),
                Soort = ah.ConsultSoorten.Afkorting,
                Status = ah.Resultaat.GetDescription(),
                Team = ah.Bureau.ToString(),
                Tijd = null,
                Vacc = ah.AfspraakVacc
            };
        }
    }

    public class Afspraak
    {
        public DateTime Datum { get; set; }
        public DateTime? Tijd { get; set; }
        public string Team { get; set; }
        public string Soort { get; set; }
        public string Discipline { get; set; }
        public string Bijz { get; set; }
        public string Vacc { get; set; }
        public string Brief { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
        public string Duur { get; set; }
    }
}
