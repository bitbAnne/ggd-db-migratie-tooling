//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class CurveInfo
    {
        public CurveInfo()
        {
            this.CurveHulplijnen = new HashSet<CurveHulplijnen>();
        }
    
        public int CurveID { get; set; }
        public string Info { get; set; }
        public byte Geslacht { get; set; }
        public short Chart { get; set; }
        public byte Periode { get; set; }
        public short Flags { get; set; }
        public short Status { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
    
        public virtual ICollection<CurveHulplijnen> CurveHulplijnen { get; set; }
    }
}
