//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kostengroepen
    {
        public Kostengroepen()
        {
            this.Kostencodes = new HashSet<Kostencodes>();
            this.Persoonsgegevens = new HashSet<Persoonsgegevens>();
        }
    
        public short Kostengroep { get; set; }
        public string Omschrijving { get; set; }
        public byte Status { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
    
        public virtual ICollection<Kostencodes> Kostencodes { get; set; }
        public virtual ICollection<Persoonsgegevens> Persoonsgegevens { get; set; }
    }
}
