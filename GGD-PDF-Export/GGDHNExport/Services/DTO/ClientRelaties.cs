//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClientRelaties
    {
        public int Casnummer1 { get; set; }
        public int Casnummer2 { get; set; }
        public short RelatieSoort { get; set; }
        public short Flags { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
        public int RelatieId { get; set; }
    
        public virtual Persoonsgegevens Persoonsgegevens { get; set; }
        public virtual Persoonsgegevens Persoonsgegevens1 { get; set; }
    }
}
