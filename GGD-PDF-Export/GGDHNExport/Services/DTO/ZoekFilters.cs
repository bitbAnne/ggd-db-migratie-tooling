//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZoekFilters
    {
        public short FilterID { get; set; }
        public string Omschrijving { get; set; }
        public short Positie { get; set; }
        public string ItemSQL { get; set; }
        public string ParamOms2 { get; set; }
        public string ParamOms1 { get; set; }
        public short Flags { get; set; }
        public byte Status { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
    }
}
