//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class MwActUren
    {
        public int MedewerkerID { get; set; }
        public short ActiviteitCode { get; set; }
        public double Percentage { get; set; }
        public double Uren { get; set; }
        public System.DateTime UrenCalc { get; set; }
        public byte Flags { get; set; }
    
        public virtual ActiviteitCodes ActiviteitCodes { get; set; }
        public virtual Medewerkers Medewerkers { get; set; }
    }
}
