//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class BdsElementen
    {
        public int BDSnummer { get; set; }
        public string BDSelement { get; set; }
        public string WaardenDomeinID { get; set; }
        public string RubriekID { get; set; }
        public string GroepID { get; set; }
        public int Flags { get; set; }
    
        public virtual BdsGroepen BdsGroepen { get; set; }
        public virtual BdsRubrieken BdsRubrieken { get; set; }
        public virtual BdsWaardenDomeinen BdsWaardenDomeinen { get; set; }
    }
}
