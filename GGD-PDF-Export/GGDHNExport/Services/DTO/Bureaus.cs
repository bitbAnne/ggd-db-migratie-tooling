//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Bureaus
    {
        public Bureaus()
        {
            this.Activiteiten = new HashSet<Activiteiten>();
            this.Afspraken = new HashSet<Afspraken>();
            this.Blokkeringen = new HashSet<Blokkeringen>();
            this.Bureaus1 = new HashSet<Bureaus>();
            this.ClientTeamHist = new HashSet<ClientTeamHist>();
            this.Contactmomenten = new HashSet<Contactmomenten>();
            this.Dagroosters = new HashSet<Dagroosters>();
            this.Overdrachten = new HashSet<Overdrachten>();
            this.Persoonsgegevens = new HashSet<Persoonsgegevens>();
            this.Persoonsgegevens1 = new HashSet<Persoonsgegevens>();
            this.Planning = new HashSet<Planning>();
            this.Postcodereeksen = new HashSet<Postcodereeksen>();
            this.Scholen = new HashSet<Scholen>();
            this.TeamMedewerkers = new HashSet<TeamMedewerkers>();
        }
    
        public short Bureau { get; set; }
        public string Locatie { get; set; }
        public string Brieflocatie { get; set; }
        public string Faxnummer { get; set; }
        public string Faxnummer2 { get; set; }
        public string FaxOms2 { get; set; }
        public string Telefoon { get; set; }
        public Nullable<short> Gehoorbureau { get; set; }
        public short Regio { get; set; }
        public int Blokmagic { get; set; }
        public short Blokdefault { get; set; }
        public short Blokrichting { get; set; }
        public short P1_ZitBloks { get; set; }
        public short P1_Weken { get; set; }
        public short P1_MaxBloks { get; set; }
        public short P2_ZitBloks { get; set; }
        public short P2_Weken { get; set; }
        public short P2_MaxBloks { get; set; }
        public short P3_ZitBloks { get; set; }
        public short P3_Weken { get; set; }
        public short P3_MaxBloks { get; set; }
        public byte Status { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
        public string Email { get; set; }
        public short Flags { get; set; }
        public string OmschrijvingPortaal { get; set; }
    
        public virtual ICollection<Activiteiten> Activiteiten { get; set; }
        public virtual ICollection<Afspraken> Afspraken { get; set; }
        public virtual ICollection<Blokkeringen> Blokkeringen { get; set; }
        public virtual ICollection<Bureaus> Bureaus1 { get; set; }
        public virtual Bureaus Bureaus2 { get; set; }
        public virtual Regios Regios { get; set; }
        public virtual ICollection<ClientTeamHist> ClientTeamHist { get; set; }
        public virtual ICollection<Contactmomenten> Contactmomenten { get; set; }
        public virtual ICollection<Dagroosters> Dagroosters { get; set; }
        public virtual ICollection<Overdrachten> Overdrachten { get; set; }
        public virtual ICollection<Persoonsgegevens> Persoonsgegevens { get; set; }
        public virtual ICollection<Persoonsgegevens> Persoonsgegevens1 { get; set; }
        public virtual ICollection<Planning> Planning { get; set; }
        public virtual ICollection<Postcodereeksen> Postcodereeksen { get; set; }
        public virtual ICollection<Scholen> Scholen { get; set; }
        public virtual ICollection<TeamMedewerkers> TeamMedewerkers { get; set; }
    }
}
