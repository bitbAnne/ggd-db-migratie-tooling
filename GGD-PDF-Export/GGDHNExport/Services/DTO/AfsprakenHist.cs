//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class AfsprakenHist
    {
        public AfsprakenHist()
        {
            this.AfsprakenHist1 = new HashSet<AfsprakenHist>();
        }
    
        public int AfspraakID { get; set; }
        public int CasNummer { get; set; }
        public Nullable<int> ZittingNummer { get; set; }
        public short Bureau { get; set; }
        public byte ConsultSoort { get; set; }
        public byte Discipline { get; set; }
        public string AfspraakBijz { get; set; }
        public string AfspraakVacc { get; set; }
        public byte ClientSrt { get; set; }
        public System.DateTime AfspraakDatum { get; set; }
        public System.DateTime AfspraakTijd { get; set; }
        public Services.DTO.AfspraakTriggerEnum AfspraakTrig { get; set; }
        public Nullable<short> BriefNummer { get; set; }
        public short BriefStat { get; set; }
        public Services.DTO.ResultaatEnum Resultaat { get; set; }
        public Nullable<short> Verwijzing { get; set; }
        public byte Voeding { get; set; }
        public byte BijzZorg { get; set; }
        public byte Units { get; set; }
        public double Controle { get; set; }
        public Nullable<int> LinkID { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
        public short Flags { get; set; }
        public short Afspraakduur { get; set; }
        public Nullable<int> KostencodeID { get; set; }
        public Nullable<int> ExportID { get; set; }
        public Nullable<int> VerzekeraarID { get; set; }
        public string Polisnummer { get; set; }
        public Nullable<int> Schoolnummer { get; set; }
        public Nullable<short> Contactmoment { get; set; }
        public Nullable<byte> LinkDiscipline { get; set; }
        public Nullable<System.DateTime> LinkTijd { get; set; }
    
        public virtual ICollection<AfsprakenHist> AfsprakenHist1 { get; set; }
        public virtual AfsprakenHist AfsprakenHist2 { get; set; }
        public virtual ConsultSoorten ConsultSoorten { get; set; }
        public virtual Contactmomenten Contactmomenten { get; set; }
        public virtual Disciplines Disciplines { get; set; }
        public virtual Disciplines Disciplines1 { get; set; }
        public virtual Kostencodes Kostencodes { get; set; }
        public virtual Persoonsgegevens Persoonsgegevens { get; set; }
        public virtual Scholen Scholen { get; set; }
        public virtual Verzekeraars Verzekeraars { get; set; }
        public virtual ZittingenHist ZittingenHist { get; set; }
        public virtual ZorgExport ZorgExport { get; set; }
    }
}
