//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class RapportLog
    {
        public int RapLogId { get; set; }
        public short RapportNummer { get; set; }
        public string Omschrijving { get; set; }
        public string RapportSQL { get; set; }
        public string ErrorLog { get; set; }
        public Nullable<int> Records { get; set; }
        public Nullable<double> Runtime { get; set; }
        public int Flags { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
    
        public virtual Rapporten Rapporten { get; set; }
    }
}
