//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Medewerkers
    {
        public Medewerkers()
        {
            this.Activiteiten = new HashSet<Activiteiten>();
            this.CasUsers = new HashSet<CasUsers>();
            this.DossierItems = new HashSet<DossierItems>();
            this.Contracturen = new HashSet<Contracturen>();
            this.MwActUren = new HashSet<MwActUren>();
            this.Persoonsgegevens = new HashSet<Persoonsgegevens>();
            this.Taken = new HashSet<Taken>();
            this.TeamMedewerkers = new HashSet<TeamMedewerkers>();
            this.UrenPerioden = new HashSet<UrenPerioden>();
            this.Zittingen = new HashSet<Zittingen>();
            this.Zittingen1 = new HashSet<Zittingen>();
            this.ZittingenHist = new HashSet<ZittingenHist>();
            this.ZittingenHist1 = new HashSet<ZittingenHist>();
            this.ZorgCorrectie = new HashSet<ZorgCorrectie>();
        }
    
        public int MedewerkerID { get; set; }
        public byte Discipline { get; set; }
        public string Naam { get; set; }
        public string BriefNaam { get; set; }
        public string Extern { get; set; }
        public short Status { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
        public string Email { get; set; }
        public Nullable<short> Regio { get; set; }
        public string AGBcode { get; set; }
        public Nullable<System.DateTime> InDienst { get; set; }
        public Nullable<System.DateTime> UitDienst { get; set; }
        public Nullable<System.DateTime> BlokActReg { get; set; }
        public Nullable<int> OrgEenheid { get; set; }
        public Nullable<int> DienstExtData { get; set; }
        public Nullable<int> Dienst2ExtData { get; set; }
        public Nullable<int> OrgEenheid2 { get; set; }
        public byte Flags { get; set; }
        public string MwKenmerk { get; set; }
        public Nullable<int> ExtFunctie { get; set; }
        public string Achternaam { get; set; }
        public string Voornaam { get; set; }
        public string Tussenvoegsel { get; set; }
        public string Voorletters { get; set; }
        public string Telefoon { get; set; }
        public Nullable<System.DateTime> VirStamp { get; set; }
        public string MwKenmerk2 { get; set; }
        public Nullable<System.DateTime> VirStamp2 { get; set; }
        public byte Geslacht { get; set; }
        public Nullable<int> ExtFunctie2 { get; set; }
        public string MwToken { get; set; }
    
        public virtual ICollection<Activiteiten> Activiteiten { get; set; }
        public virtual ICollection<CasUsers> CasUsers { get; set; }
        public virtual Disciplines Disciplines { get; set; }
        public virtual ICollection<DossierItems> DossierItems { get; set; }
        public virtual ICollection<Contracturen> Contracturen { get; set; }
        public virtual Regios Regios { get; set; }
        public virtual ICollection<MwActUren> MwActUren { get; set; }
        public virtual ICollection<Persoonsgegevens> Persoonsgegevens { get; set; }
        public virtual ICollection<Taken> Taken { get; set; }
        public virtual ICollection<TeamMedewerkers> TeamMedewerkers { get; set; }
        public virtual ICollection<UrenPerioden> UrenPerioden { get; set; }
        public virtual ICollection<Zittingen> Zittingen { get; set; }
        public virtual ICollection<Zittingen> Zittingen1 { get; set; }
        public virtual ICollection<ZittingenHist> ZittingenHist { get; set; }
        public virtual ICollection<ZittingenHist> ZittingenHist1 { get; set; }
        public virtual ICollection<ZorgCorrectie> ZorgCorrectie { get; set; }
    }
}
