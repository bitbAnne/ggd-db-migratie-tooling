//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Services.DTO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Teamlocaties
    {
        public Teamlocaties()
        {
            this.Persoonsgegevens = new HashSet<Persoonsgegevens>();
            this.Scholen = new HashSet<Scholen>();
        }
    
        public short Teamlocatie { get; set; }
        public string Naam { get; set; }
        public string Adres1 { get; set; }
        public string Adres2 { get; set; }
        public string LocatieFax { get; set; }
        public string LocatieTel { get; set; }
        public string LocatieEmail { get; set; }
        public short Regio { get; set; }
        public byte Status { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string UserID { get; set; }
    
        public virtual ICollection<Persoonsgegevens> Persoonsgegevens { get; set; }
        public virtual Regios Regios { get; set; }
        public virtual ICollection<Scholen> Scholen { get; set; }
    }
}
