﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using Services.DTO;
using System.IO;
using System.Configuration;
using ClosedXML.Excel;


namespace Services
{
    public enum LogLevelL4N
    {
        DEBUG = 1,
        INFO,
        WARN,
        ERROR,
        FATAL
    }

    public static class Logger
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Logger));

        static Logger()
        {
            XmlConfigurator.Configure();
        }

        public static void WriteLog(LogLevelL4N logLevel, String log)
        {
            log = string.Format("{0} ", log);

            if (logLevel.Equals(LogLevelL4N.DEBUG))
            {
                logger.Debug(log);
            }
            else if (logLevel.Equals(LogLevelL4N.ERROR))
            {
                logger.Error(log);
            }
            else if (logLevel.Equals(LogLevelL4N.FATAL))
            {
                logger.Fatal(log);
            }
            else if (logLevel.Equals(LogLevelL4N.INFO))
            {
                logger.Info(log);
            }
            else if (logLevel.Equals(LogLevelL4N.WARN))
            {
                logger.Warn(log);
            }
        }

        public static void WriteLog(String log)
        {
            logger.Debug(log);
        }

        public static void LogProcessedCasToIndexFile(Persoonsgegevens persoon)
        {
            ///todo: create file when directory already exists
            var indexFileDirectory = new DirectoryInfo(ConfigurationManager.AppSettings["indexFileDirectory"]);
            string indexFilePath = Path.Combine(ConfigurationManager.AppSettings["indexFileDirectory"], "indexfile.xlsx");
            if (!indexFileDirectory.Exists)
            {
                Directory.CreateDirectory(ConfigurationManager.AppSettings["indexFileDirectory"]);
                using (XLWorkbook workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("Processed Files");
                    worksheet.Columns().AdjustToContents();
                    worksheet.Cell("A1").Value = "CasNummer";
                    worksheet.Cell("B1").Value = "BSN";
                    worksheet.Cell("C1").Value = "GeboorteDatum";
                    worksheet.Cell("D1").Value = "Geslacht";
                    worksheet.Cell("E1").Value = "Datum uit zorg";
                    if (!File.Exists(indexFilePath))
                    {
                        workbook.SaveAs(indexFilePath);
                    }
                    else
                    {
                        workbook.Save();
                    }
                }
            }
            try
            {
                using (XLWorkbook Workbook = new XLWorkbook(indexFilePath))
                {
                    IXLWorksheet WorkSheet = Workbook.Worksheet("Processed Files");

                    int NumberOfNewRow = WorkSheet.LastRowUsed().RowNumber() + 1;
                    IXLCell CellForNewData = WorkSheet.Cell(NumberOfNewRow, 1);
                    WorkSheet.Cell("A" + NumberOfNewRow).Value = persoon.CasNummer;
                    WorkSheet.Cell("B" + NumberOfNewRow).Value = persoon.BSN;
                    WorkSheet.Cell("C" + NumberOfNewRow).Value = persoon.Geboortedatum;
                    WorkSheet.Cell("D" + NumberOfNewRow).Value = persoon.Geslacht.ToString();
                    WorkSheet.Cell("E" + NumberOfNewRow).Value = persoon.UitZorg;
                    WorkSheet.Columns().AdjustToContents();
                    Workbook.Save();

                }
            }
            catch
            {
                WriteLog(LogLevelL4N.WARN, "Het dossier van CAS-nummer " + persoon.CasNummer + " is mogelijk succesvol gegenereerd, maar kon niet worden toegevoegd aan het index-bestand op locatie: " + indexFilePath + " Na controle kunt u het bestand toevoegen aan het index-bestand.");
            }
        }


    }
}

