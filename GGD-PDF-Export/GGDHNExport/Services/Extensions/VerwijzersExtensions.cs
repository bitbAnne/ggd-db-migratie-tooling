﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.DTO;

namespace Services.Extensions
{
    public static class VerwijzersExtensions
    {
        public static Dictionary<string, string> GetVerwijzer(this Verwijzers verwijzer)
        {
            if (verwijzer == null)
                return null;

            string key = "Huisarts";

            string omschrijving = String.Format("{2} {0} ({1})", verwijzer.Naam, verwijzer.Telefoon, verwijzer.VerwijzerID);

            return new Dictionary<string, string> { { key, omschrijving } };
        }
    }
}
