﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Services.Extensions
{
    public static class TableExtensions
    {
        public static void AddHeaderTwoColumns(this Table table)
        {
            var koppen = new List<string>() { "Omschrijving", "Waarde" };
            var breedtesvlnr = new List<double>() { 9.6, 9.6 };

            foreach (var breedte in breedtesvlnr)
            {
                table.AddColumn(Unit.FromCentimeter(breedte));
            }
        }
        public static void AddHeader(this Table table, List<Afspraak> appointments)
        {
            var koppen = new List<string>() { "Datum", "Tijd", "Team", "Soort", "Discipline", "Bijz", "Vacc", "Brief", "Code", "Status", "Duur" };
            var breedtesvlnr = new List<double>() { 2.5, 1.8, 1, 1, 2, 1, 1.3, 1, 1, 3, 1 };

            foreach (var breedte in breedtesvlnr)
            {
                table.AddColumn(Unit.FromCentimeter(breedte));
            }

            Row row = table.AddRow();

            var teller = 0;
            foreach (var kop in koppen)
            {
                Cell cell = row.Cells[teller];
                var paragraaf = cell.AddParagraph();
                paragraaf.AddFormattedText(kop, TextFormat.Bold);
                teller++;
            }
        }

        public static void AddHeader(this Table table)
        {
            //string header = "Bestandsnaam";
            //table.AddColumn(Unit.FromCentimeter(19.2));

            //Row row = table.AddRow();
            //Cell cell = row.Cells[0];
            //var paragraaf = cell.AddParagraph();
            //paragraaf.AddFormattedText(header, TextFormat.Bold);
       
            var headers = new List<string>() { "Bestandsnaam", "Datum" };
            var breedtesvlnr = new List<double>() { 16.6, 2.6 };

            foreach (var breedte in breedtesvlnr)
            {
                table.AddColumn(Unit.FromCentimeter(breedte));
            }

            Row row = table.AddRow();

            var teller = 0;
            foreach (var header in headers)
            {
                Cell cell = row.Cells[teller];
                var paragraaf = cell.AddParagraph();
                paragraaf.AddFormattedText(header, TextFormat.Bold);
                teller++;
            }          
        }

        //public static void AddHeader(this Table table, List<string> filesInFolder)
        //{
        //    string kop = "Bestandsnaam";

        //    table.AddColumn(Unit.FromCentimeter(19.2));

        //    Row row = table.AddRow();
        //    Cell cell = row.Cells[0];
        //    var paragraaf = cell.AddParagraph();
        //    paragraaf.AddFormattedText(kop, TextFormat.Bold);


        //}

        public static void AddHeader(this Table table, Persoonsgegevens person)
        {
            table.AddHeaderTwoColumns();
        }

        public static void AddHeader(this Table table, Interventie intervention)
        {
            var koppen = new List<string>() { "Datum", "Volgnr.", "Srt", "Interventie", "Mw.", "Status", "Resultaat" };
            var breedtesvlnr = new List<double>() { 2.3, 1.6, 1, 6.2, 3, 2.4, 1.9 };
            foreach (var breedte in breedtesvlnr)
            {
                table.AddColumn(Unit.FromCentimeter(breedte));
            }

            Row row = table.AddRow();

            var teller = 0;
            foreach (var kop in koppen)
            {
                Cell cell = row.Cells[teller];
                var paragraaf = cell.AddParagraph();
                paragraaf.AddFormattedText(kop, TextFormat.Bold);
                teller++;
            }
        }

        public static void AddHeader(this Table table, Activiteit activity)
        {
            var koppen = new List<string>() { "Datum", "Volgnr.", "Code", "Omschrijving", "Mw.", "Uitvoering", "Duur" };
            var breedtesvlnr = new List<double>() { 2.1, 1.5, 1, 7.2, 3, 2.5, 1.1 };
            foreach (var breedte in breedtesvlnr)
            {
                table.AddColumn(Unit.FromCentimeter(breedte));
            }

            Row row = table.AddRow();
            //this will make sure that the (little) tables will always be on one page
            //row.KeepWith = 2;

            var teller = 0;
            foreach (var kop in koppen)
            {
                Cell cell = row.Cells[teller];
                var paragraaf = cell.AddParagraph();
                paragraaf.AddFormattedText(kop, TextFormat.Bold);
                teller++;
            }

        }

        public static void AddHeader(this Table table, InfoDossier infoDossier)
        {
            var koppen = new List<string>() { "Datum", "Volgnr.", "Omschrijving", "Mw." };
            var breedtesvlnr = new List<double>() { 2.3, 1.6, 11.5, 3 };
            foreach (var breedte in breedtesvlnr)
            {
                table.AddColumn(Unit.FromCentimeter(breedte));
            }

            Row row = table.AddRow();
            //keep table on one page (details must be fontsize 8 to fit);
            //row.KeepWith = 2;
            var teller = 0;
            foreach (var kop in koppen)
            {
                Cell cell = row.Cells[teller];
                var paragraaf = cell.AddParagraph();
                paragraaf.AddFormattedText(kop, TextFormat.Bold);
                teller++;
            }
        }

        public static void AddHeader(this Table table, Dossier dossier)
        {
            table.AddHeaderTwoColumns();
        }

        public static void AddRow(this Table table, Afspraak appointment)
        {
            Row row = table.AddRow();
            var kleur = Colors.LightGray;
            if (appointment.Status == "OK")
            {
                kleur = Colors.PaleGoldenrod;
            }
            row.Shading.Color = kleur;
            Cell cell = row.Cells[0];
            cell.AddParagraph(appointment.Datum.ToString("dd-MM-yyyy"));
            cell = row.Cells[1];

            cell.AddParagraph(appointment.Tijd != null ? appointment.Tijd.Value.ToString("HH:mm") : "");
            cell = row.Cells[2];
            cell.AddParagraph(appointment.Team ?? "");
            cell = row.Cells[3];
            cell.AddParagraph(appointment.Soort ?? "");
            cell = row.Cells[4];
            cell.AddParagraph(appointment.Discipline ?? "");
            cell = row.Cells[5];
            cell.AddParagraph(appointment.Bijz ?? "");
            cell = row.Cells[6];
            cell.AddParagraph(appointment.Vacc ?? "");
            cell = row.Cells[7];
            cell.AddParagraph(appointment.Brief ?? "");
            cell = row.Cells[8];
            cell.AddParagraph(appointment.Code ?? "");
            cell = row.Cells[9];
            cell.AddParagraph(appointment.Status ?? "");
            cell = row.Cells[10];
            var par = cell.AddParagraph(appointment.Duur ?? "");
            par.Format.Alignment = ParagraphAlignment.Right;
        }

        public static void AddRow(this Table table, KeyValuePair<string, string> kvp)
        {
            Row row = table.AddRow();
            Cell cell = row.Cells[0];
            var par = cell.AddParagraph();

            if (kvp.Key == "PersMemo")
            {
                row.Cells[0].MergeRight = 1;
                par.AddFormattedText("School/opmerkingen", TextFormat.Bold);
                par.AddLineBreak();
                par.AddText(kvp.Value);
            }
            else
            {
                par.AddText(kvp.Key);
                cell = row.Cells[1];
                cell.AddParagraph(kvp.Value);
            }
        }

        public static void AddRow(this Table table, Interventie intervention)
        {
            Row row = table.AddRow();
            var kleur = Colors.White;
            row.Shading.Color = kleur;

            Cell cell = row.Cells[0];
            cell.AddParagraph(intervention.Datum.ToString("dd-MM-yyyy"));
            cell = row.Cells[1];
            cell.AddParagraph(intervention.Volgnummer);
            cell = row.Cells[2];
            cell.AddParagraph(intervention.SoortInterventieCode);
            cell = row.Cells[3];
            cell.AddParagraph(intervention.SoortInterventieOms);
            cell = row.Cells[4];
            cell.AddParagraph(intervention.Medewerker);
            cell = row.Cells[5];
            cell.AddParagraph(intervention.Status);
            cell = row.Cells[6];
            cell.AddParagraph(string.Format("{0}/100", intervention.Resultaat));

            kleur = Colors.AliceBlue;

            if (!string.IsNullOrEmpty(intervention.Aanleiding))
            {
                var length = intervention.Aanleiding.Length;
                row = table.AddRow();
                row.Shading.Color = kleur;

                intervention.Aanleiding = intervention.Aanleiding.RtfToText();

                cell = row.Cells[0];
                cell.AddParagraph("Aanleiding:");
                cell = row.Cells[1];
                row.Cells[1].MergeRight = 5;
                cell.AddParagraph(intervention.Aanleiding);
                kleur = kleur.SwitchColor();
            }

            if (!string.IsNullOrEmpty(intervention.Analyse))
            {
                var length = intervention.Analyse.Length;
                row = table.AddRow();
                row.Shading.Color = kleur;

                intervention.Analyse = intervention.Analyse.RtfToText();

                cell = row.Cells[0];
                cell.AddParagraph("Analyse:");
                cell = row.Cells[1];
                row.Cells[1].MergeRight = 5;
                cell.AddParagraph(intervention.Analyse);

                kleur = kleur.SwitchColor();
            }
            if (!string.IsNullOrEmpty(intervention.Doel))
            {
                row = table.AddRow();
                row.Shading.Color = kleur;

                intervention.Doel = intervention.Doel.RtfToText();

                cell = row.Cells[0];
                cell.AddParagraph("Doel(en):");
                cell = row.Cells[1];
                row.Cells[1].MergeRight = 5;
                cell.AddParagraph(intervention.Doel);
                kleur = kleur.SwitchColor();
            }
            if (!string.IsNullOrEmpty(intervention.Evaluatie))
            {
                row = table.AddRow();
                row.Shading.Color = kleur;
                //var length = intervention.Evaluatie.Length;
                intervention.Evaluatie = intervention.Evaluatie.RtfToText();
                //var length2 = intervention.Evaluatie.Length;
                //var length3 = intervention.Evaluatie.Replace("\n", "__________________________________________________").Length;
                cell = row.Cells[0];
                cell.AddParagraph("Evaluatie:");
                cell = row.Cells[1];
                row.Cells[1].MergeRight = 5;

                if (intervention.Evaluatie.Replace("\n", "__________________________________________________").Length > 5000)
                {
                    cell.AddParagraph(intervention.Evaluatie.Replace("\n", "__________________________________________________").Substring(0, 4999).Replace("__________________________________________________", "\n"));
                    var rest = intervention.Evaluatie.Replace("\n", "__________________________________________________").Substring(4999).Replace("__________________________________________________", "\n");
                    AddRest(rest, table);
                }
                else
                {
                    cell.AddParagraph(intervention.Evaluatie);
                }
                //cell.AddParagraph(intervention.Evaluatie);

            }
        }

        //Method to ensure that if a table cell gets too large, it is continued in the next one.
        //Very bad implementation, refactor if possible.
        private static void AddRest(string restString, Table table)
        {
            var row = table.AddRow();
            Cell cell = row.Cells[0];

            cell = row.Cells[0];
            cell.AddParagraph("Evaluatie vervolg:");
            cell = row.Cells[1];
            row.Cells[1].MergeRight = 5;

            if (restString.Replace("\n", "__________________________________________________").Length > 5000)
            {
                cell.AddParagraph(restString.Replace("\n", "__________________________________________________").Substring(0, 4999).Replace("__________________________________________________", "\n"));
                var rest = restString.Replace("\n", "__________________________________________________").Substring(4999).Replace("__________________________________________________", "\n");
                AddRest(rest, table);
            }
            else
            {
                cell.AddParagraph(restString);
            }
        }

        public static void AddRow(this Table table, Activiteit activity)
        {
            Row row = table.AddRow();
            var kleur = Colors.White;
            row.Shading.Color = kleur;

            Cell cell = row.Cells[0];
            cell.AddParagraph(activity.Datum.ToString("dd-MM-yyyy"));
            cell = row.Cells[1];
            cell.AddParagraph(activity.Volgnummer);
            cell = row.Cells[2];
            cell.AddParagraph(activity.Code);
            cell = row.Cells[3];
            cell.AddParagraph(activity.Omschrijving);
            cell = row.Cells[4];
            cell.AddParagraph(activity.Medewerker);
            cell = row.Cells[5];
            cell.AddParagraph(activity.Uitvoering);
            cell = row.Cells[6];
            var par = cell.AddParagraph(activity.Duur);
            par.Format.Alignment = ParagraphAlignment.Right;

            if (!string.IsNullOrEmpty(activity.DossierId))
            {
                row = table.AddRow();
                kleur = Colors.LightGray;
                row.Shading.Color = kleur;
                cell = row.Cells[0];
                cell.AddParagraph(activity.DossierDatum.ToString("dd-MM-yyyy"));
                cell = row.Cells[1];
                cell.AddParagraph(activity.DossierId);
                cell = row.Cells[2];
                row.Cells[2].MergeRight = 2;
                cell.AddParagraph(activity.DossierNaam);
                cell = row.Cells[5];
                row.Cells[5].MergeRight = 1;
                cell.Format.Alignment = ParagraphAlignment.Right;
                cell.AddParagraph("<-- Gerelateerd dossier");
            }

            if (!string.IsNullOrEmpty(activity.AfwijzingId))
            {
                row = table.AddRow();
                kleur = Colors.White;
                row.Shading.Color = kleur;
                cell = row.Cells[0];
                cell.AddParagraph("reden niet uitgevoerd: ");
                row.Cells[0].MergeRight = 1;
                cell = row.Cells[2];
                cell.AddParagraph(activity.AfwijzingId);
                cell = row.Cells[3];
                row.Cells[3].MergeRight = 3;
                cell.AddParagraph(activity.AfwijzingOmschrijving);
            }

            row = table.AddRow();
            row.Shading.Color = Colors.AliceBlue;
            cell = row.Cells[0];
            row.Cells[0].MergeRight = 6;
            cell.AddParagraph(activity.Info ?? "");
        }

        public static void AddRow(this Table table, InfoDossier infoDossier)
        {
            Row row = table.AddRow();
            var kleur = Colors.White;
            row.Shading.Color = kleur;

            Cell cell = row.Cells[0];
            cell.AddParagraph(infoDossier.Datum.ToString("dd-MM-yyyy"));
            cell = row.Cells[1];
            cell.AddParagraph(infoDossier.Volgnummer);
            cell = row.Cells[2];
            cell.AddParagraph(infoDossier.Omschrijving);
            cell = row.Cells[3];
            cell.AddParagraph(infoDossier.Medewerker);

            kleur = kleur.SwitchColor();
            if (!string.IsNullOrEmpty(infoDossier.Tekstveld))
            {
                row = table.AddRow();
                row.Shading.Color = kleur;
                var tekst = GenericExtensions.RtfToText(infoDossier.Tekstveld);
                /*
                var txt = infoDossier.Tekstveld;
                if (txt.StartsWith(@"{\rtf")) {
                    RichTextBox rtfBox = new RichTextBox();
                    rtfBox.Clear();
                    rtfBox.Rtf = infoDossier.Tekstveld;
                    txt = rtfBox.Text;
                }
                var tekst = txt;
                // var tekst = infoDossier.Tekstveld.RtfToText();
                */

                cell = row.Cells[0];
                row.Cells[0].MergeRight = 3;
                Paragraph paragraph = cell.AddParagraph();
                paragraph.AddText(tekst);
                paragraph.Format.Font.Size = 8;
            }
        }

        public static void AddDate(this Table table, Dossier dossier)
        {

            Row row = table.AddRow();
            Cell cell = row.Cells[0];
            var par = cell.AddParagraph();
            row.Cells[0].MergeRight = 1;
            row.Cells[0].Borders.Visible = false;
            par.AddFormattedText(dossier.Datum.ToString("D"), TextFormat.Bold); ;
            par.Format.Alignment = ParagraphAlignment.Center;
            par.Format.Font.Size = "14pt";
            par.Format.SpaceAfter = "2mm";
            par.Format.SpaceBefore = "2mm";
        }

        public static void AddRow(this Table table, Rubriek rubriek)
        {
            if (rubriek.RubriekInhouden.Any() && rubriek.RubriekInhouden.Any(x => x.Inhouden.Any()))
            {
                Row row = table.AddRow();
                Cell cell = row.Cells[0];
                var par = cell.AddParagraph();
                row.Cells[0].MergeRight = 1;
                row.Cells[0].Borders.Visible = false;
                par.AddFormattedText(rubriek.Omschrijving, TextFormat.Bold);
                par.Format.Alignment = ParagraphAlignment.Center;
                par.Format.Font.Size = "14pt";
                par.Format.SpaceAfter = "2mm";
                par.Format.SpaceBefore = "2mm";
            }
        }

        public static void AddRow(this Table table, RubriekInhoud rubriekInhoud, bool overwrite = false)
        {
            if (rubriekInhoud.Inhouden.Any() || overwrite)
            {
                Row row = table.AddRow();
                Cell cell = row.Cells[0];
                var par = cell.AddParagraph();
                row.Cells[0].MergeRight = 1;
                row.Cells[0].Borders.Visible = false;
                par.AddFormattedText(rubriekInhoud.Subkop, TextFormat.Italic);
                par.Format.Alignment = ParagraphAlignment.Center;
                par.Format.SpaceAfter = "1mm";
                par.Format.SpaceBefore = "1mm";
            }
        }

        public static void AddRow(this Table table, Inhoud inhoud)
        {
            var row = table.AddRow();
            var cell = row.Cells[0];
            var cell2 = row.Cells[1];

            if (inhoud.IsFlagged)
            {
                cell.Format.Font.Color = Colors.IndianRed;
                cell2.Format.Font.Color = Colors.IndianRed;
            }

            var par = cell.AddParagraph();
            par.AddText(inhoud.Omschrijving);
            cell2.AddParagraph(inhoud.Waarde ?? string.Empty);
        }

        public static void AddRow(this Table table, string filename, DateTime date)
        {
            Row row = table.AddRow();
            Cell cell = row.Cells[0];
            cell.AddParagraph(filename);
            cell = row.Cells[1];
            cell.AddParagraph(date.ToShortDateString());
        }
    }
}
