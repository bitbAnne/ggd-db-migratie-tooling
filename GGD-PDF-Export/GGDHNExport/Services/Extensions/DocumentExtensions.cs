﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using Services.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;


namespace Services.Extensions
{
    public static class DocumentExtensions
    {
        public static void Save(this Document document, string fileName, int casNummer, bool open = false)
        {
            MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToFile(document, "MigraDoc.mdddl");
            const PdfFontEmbedding embedding = PdfFontEmbedding.Always;
            var pdfRenderer = new PdfDocumentRenderer(true, embedding);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();

            string tempFilename = Path.Combine(ConfigurationManager.AppSettings["outputDirectory"], casNummer.ToString(), fileName) + ".pdf";
            pdfRenderer.PdfDocument.Save(tempFilename);
            pdfRenderer.PdfDocument.Close();
            pdfRenderer.PdfDocument.Dispose();

            if (open)
                Process.Start(tempFilename);
        }

        public static void AddCover(this Document document, Persoonsgegevens person)
        {
            Section section = document.AddSection();

            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceAfter = "2cm";
            paragraph.Format.SpaceBefore = "-2cm";

            //Image image = section.AddImage("GGD_hvb_logo.png");
            //image.Width = "194pt";

            paragraph = section.AddParagraph(string.Format("Naam : {0}, {2} {1}", person.GetLastName(), person.Voorvoegsel, person.Roepnaam));
            paragraph.AddLineBreak();
            paragraph = section.AddParagraph(string.Format("GBA naam : {0}", person.Geslachtsnaam));
            paragraph.AddLineBreak();
            paragraph = section.AddParagraph(string.Format("Geslacht : {0}", person.Geslacht.ToString()));
            paragraph.AddLineBreak();
            paragraph = section.AddParagraph(string.Format("CasNummer : {0}", person.CasNummer.ToString()));
            paragraph.AddLineBreak();
            paragraph.AddText(string.Format("BSN : {0}", person.BSN));
            paragraph.AddLineBreak();
            paragraph.AddText(string.Format("A-nummer : {0}", person.ANummer));
            paragraph.AddLineBreak();
            paragraph.AddText(string.Format("Geboortedatum : {0}", person.GetBirthday()));
            paragraph.AddLineBreak();
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.Black;
            paragraph.Format.SpaceBefore = "1cm";
            paragraph.Format.SpaceAfter = "7cm";
            paragraph.Format.LeftIndent = "10cm";
            paragraph = section.AddParagraph("Overdrachtsdocument t.b.v. mlCAS");
            paragraph.Format.Font.Size = 16;
            paragraph.Format.Font.Color = Colors.DarkRed;
            paragraph.Format.SpaceBefore = "1cm";
            paragraph.Format.SpaceAfter = "8cm";
            paragraph = section.AddParagraph("Renderdatum: ");
            paragraph.AddDateField();
        }

        public static void AddTOC(this Document document, Persoonsgegevens person, List<KeyValuePair<string, string>> pgs, List<Afspraak> appointments, List<Interventie> interventions, List<Activiteit> activities, List<InfoDossier> infoDossiers, List<Dossier> dossier)
        {
            Section section = document.LastSection;

            section.AddPageBreak();
            Paragraph paragraph = section.AddParagraph("Inhoudsopgave");
            paragraph.Format.Font.Size = 14;
            paragraph.Format.Font.Bold = true;
            paragraph.Format.SpaceAfter = 24;
            paragraph.Format.OutlineLevel = OutlineLevel.Level1;

            if (pgs.Count > 0)
            {
                paragraph = section.AddParagraph();
                paragraph.Style = "TOC";
                Hyperlink hyperlink1 = paragraph.AddHyperlink("Persoonsgegevens");
                hyperlink1.AddText("Persoonsgegevens\t");
                hyperlink1.AddPageRefField("Persoonsgegevens");
            }

            if (appointments.Count > 0)
            {
                paragraph = section.AddParagraph();
                paragraph.Style = "TOC";
                Hyperlink hyperlink2 = paragraph.AddHyperlink("Afspraken");
                hyperlink2.AddText("Afspraken\t");
                hyperlink2.AddPageRefField("Afspraken");
            }

            if (activities.Count > 0)
            {
                paragraph = section.AddParagraph();
                paragraph.Style = "TOC";
                Hyperlink hyperlink4 = paragraph.AddHyperlink("Activiteiten");
                hyperlink4.AddText("Activiteiten\t");
                hyperlink4.AddPageRefField("Activiteiten");
            }

            if (interventions.Count > 0)
            {
                paragraph = section.AddParagraph();
                paragraph.Style = "TOC";
                Hyperlink hyperlink3 = paragraph.AddHyperlink("Interventies");
                hyperlink3.AddText("Interventies\t");
                hyperlink3.AddPageRefField("Interventies");
            }

            if (infoDossiers.Count > 0)
            {
                paragraph = section.AddParagraph();
                paragraph.Style = "TOC";
                Hyperlink hyperlink5 = paragraph.AddHyperlink("Dossierinformatie");
                hyperlink5.AddText("Dossierinformatie\t");
                hyperlink5.AddPageRefField("Dossierinformatie");
            }

            if (dossier.Count > 0)
            {
                paragraph = section.AddParagraph();
                paragraph.Style = "TOC";
                Hyperlink hyperlink = paragraph.AddHyperlink("Registratiedossiers");
                hyperlink.AddText("Registratiedossiers\t");
                hyperlink.AddPageRefField("Registratiedossiers");

                if (dossier.SelectMany(a => a.Rubrieken, (a, m) => new { a, m }).Where(x => x.m.Omschrijving == "Rijksvaccinatieprogramma").Select(x => x.a).ToList().Count() > 0)
                {
                    paragraph = section.AddParagraph();
                    paragraph.Style = "TOC";
                    hyperlink = paragraph.AddHyperlink("Vaccinaties");
                    hyperlink.AddText("Vaccinaties\t");
                    hyperlink.AddPageRefField("Vaccinaties");
                }

                if (dossier.SelectMany(a => a.Rubrieken, (a, m) => new { a, m }).Where(x => x.m.Omschrijving == "Van Wiechen 0-15 maanden").Select(x => x.a).ToList().Count() > 0)
                {
                    paragraph = section.AddParagraph();
                    paragraph.Style = "TOC";
                    hyperlink = paragraph.AddHyperlink("Van Wiechen Onderzoek 1");
                    hyperlink.AddText("Ontwikkeling / Van Wiechen Onderzoek zuigeling \t");
                    hyperlink.AddPageRefField("Van Wiechen Onderzoek 1");

                }
                if (dossier.SelectMany(a => a.Rubrieken, (a, m) => new { a, m }).Where(x => x.m.Omschrijving == "Van Wiechen 18-48 maanden").Select(x => x.a).ToList().Count() > 0)
                {
                    paragraph = section.AddParagraph();
                    paragraph.Style = "TOC";
                    hyperlink = paragraph.AddHyperlink("Van Wiechen Onderzoek 2");
                    hyperlink.AddText("Ontwikkeling / Van Wiechen Onderzoek peuter \t");
                    hyperlink.AddPageRefField("Van Wiechen Onderzoek 2");
                }

            }
            //if (fileEntries != null && fileEntries.Count > 0)
            //{
                paragraph = section.AddParagraph();
                paragraph.Style = "TOC";
                Hyperlink hyperlink6 = paragraph.AddHyperlink("Archieven");
                hyperlink6.AddText("Archieven\t");
                hyperlink6.AddPageRefField("Archieven");
            //}
        }

        public static void DefineContentSection(this Document document)
        {
            Section section = document.AddSection();
            //section.PageSetup.OddAndEvenPagesHeaderFooter = true;
            section.PageSetup.StartingNumber = 1;

            // Create a paragraph with centered page number. See definition of style "Footer".
            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            // Add paragraph to footer for odd pages.
            section.Footers.Primary.Add(paragraph);
            // Add clone of paragraph to footer for odd pages. Cloning is necessary because an object must
            // not belong to more than one other object. If you forget cloning an exception is thrown.
            // section.Footers.EvenPage.Add(paragraph.Clone());
        }

        public static void AddPersonalInformation(this Document document, Persoonsgegevens person, List<KeyValuePair<string, string>> pgs)
        {
            if (pgs.Count == 0)
                return;
            Paragraph paragraph = document.LastSection.AddParagraph("Persoonsgegevens", "Heading1");
            paragraph.AddBookmark("Persoonsgegevens");

            Table table = new Table();
            table.Borders.Width = 0.75;
            table.AddHeader(person);

            foreach (var pg in pgs)
            {
                table.AddRow(pg);
            }

            document.LastSection.Add(table);
        }

        public static void AddAppointments(this Document document, Persoonsgegevens person, List<Afspraak> appointments)
        {

            if (appointments.Count == 0)
                return;

            Paragraph paragraph = document.LastSection.AddParagraph("Afspraken", "Heading1");
            paragraph.AddBookmark("Afspraken");

            Table table = new Table();
            table.Borders.Width = 0.75;

            table.AddHeader(appointments);

            foreach (var afspraak in appointments)
            {
                table.AddRow(afspraak);
            }

            //table.SetEdge(0, 0, 2, 3, Edge.Box, BorderStyle.Single, 1.5, Colors.Black);
            document.LastSection.Add(table);
        }

        public static void AddInterventions(this Document document, Persoonsgegevens person, List<Interventie> interventions)
        {

            if (interventions.Count == 0)
                return;
            Paragraph paragraph = document.LastSection.AddParagraph("Interventies", "Heading1");
            paragraph.AddBookmark("Interventies");

            foreach (var intervention in interventions)
            {
                Paragraph par = document.LastSection.AddParagraph("Laatst gewijzigd : ");
                par.AddText(intervention.Gewijzigd.ToString("dd-MM-yyyy HH:mm"));
                par.Format.Alignment = ParagraphAlignment.Right;
                par.Format.Font.Superscript = true;
                //par.Format.RightIndent = "1cm";

                Table table = new Table();
                table.Borders.Width = 0.75;

                table.AddHeader(intervention);
                table.AddRow(intervention);
                document.LastSection.Add(table);
                par = document.LastSection.AddParagraph();
                par.Format.SpaceAfter = "0.5cm";
            }
        }

        public static void AddActivities(this Document document, Persoonsgegevens person, List<Activiteit> activities)
        {

            if (activities.Count == 0)
                return;
            Paragraph paragraph = document.LastSection.AddParagraph("Activiteiten", "Heading1");
            paragraph.AddBookmark("Activiteiten");

            foreach (var activity in activities)
            {
                Paragraph par = document.LastSection.AddParagraph("Laatst gewijzigd : ");
                par.AddText(activity.Gewijzigd.ToString("dd-MM-yyyy HH:mm") + " " + activity.Medewerker);
                par.Format.Alignment = ParagraphAlignment.Right;
                par.Format.Font.Superscript = true;
                //par.Format.RightIndent = "1cm";

                Table table = new Table();
                table.Borders.Width = 0.75;
                table.AddHeader(activity);
                table.AddRow(activity);
                document.LastSection.Add(table);
                par = document.LastSection.AddParagraph();
                par.Format.SpaceAfter = "0.5cm";
            }
        }

        public static void AddInfoDossiers(this Document document, Persoonsgegevens person, List<InfoDossier> infoDossiers)
        {
            if (infoDossiers.Count == 0)
                return;
            Paragraph paragraph = document.LastSection.AddParagraph("Dossierinformatie", "Heading1");
            paragraph.AddBookmark("Dossierinformatie");

            foreach (var infoDossier in infoDossiers)
            {
                Paragraph par = document.LastSection.AddParagraph("Laatst gewijzigd : ");
                par.AddText(infoDossier.Gewijzigd.ToString("dd-MM-yyyy HH:mm"));
                par.Format.Alignment = ParagraphAlignment.Right;
                par.Format.Font.Superscript = true;
                //par.Format.RightIndent = "1cm";

                Table table = new Table();
                table.Borders.Width = 0.75;

                table.AddHeader(infoDossier);
                table.AddRow(infoDossier);
                document.LastSection.Add(table);
                par = document.LastSection.AddParagraph();
                par.Format.SpaceAfter = "0.5cm";
            }
        }

        public static void AddDossiers(this Document document, Persoonsgegevens person, List<Dossier> dossiers)
        {
            if (dossiers.Count == 0)
                return;

            Paragraph paragraph = document.LastSection.AddParagraph("Registratiedossiers", "Heading1");
            paragraph.AddBookmark("Registratiedossiers");

            Logger.WriteLog(LogLevelL4N.DEBUG, String.Format("Begin maken PDF voor Casnummer {0}", person.CasNummer));

            Table laatsteTableVaccinaties = new Table();
            Table laatsteTablevanWiechen = null;           
            Table laatsteTablevanWiechen1 = null;           

            foreach (var dossier in dossiers)
            {
                document.LastSection.AddParagraph(dossier.Info + " " + dossier.Datum.ToString("D"), "Heading2");
                paragraph = document.LastSection.AddParagraph(String.Format("{0} - {1} - {2}", dossier.ContactMoment, dossier.DossierSoort, dossier.Medewerker));
                //paragraph = document.LastSection.AddParagraph(dossier.DiagnoseSoort);
                paragraph.Format.SpaceAfter = "4mm";

                Table table = new Table();
                table.Borders.Width = 0.5;
                table.Borders.Color = Colors.LightGray;
                table.AddHeader(dossier);
                Logger.WriteLog(LogLevelL4N.DEBUG, String.Format("Bezig met dossier {0}", dossier.Info));

                foreach (var rubriek in dossier.Rubrieken)
                {
                    //uitzondering ontwikkelingsonderzoek
                    if ( !rubriek.Omschrijving.StartsWith("Van Wiechen"))//rubriek.Omschrijving != "Rijksvaccinatieprogramma" 
                    {
                        table.AddRow(rubriek);
                        foreach (var rubriekInhoud in rubriek.RubriekInhouden)
                        {
                            Logger.WriteLog(LogLevelL4N.DEBUG,
                                            String.Format("Bezig met rijen voor subkop {0}", rubriekInhoud.Subkop));

                            //uitz. subkop bij bepaalde rubriek
                            if (!rubriek.Omschrijving.StartsWith("Van Wiechen") ||
                                (rubriek.Omschrijving.StartsWith("Van Wiechen") && rubriekInhoud.Subkop == "Ontwikkelingsonderzoek"))
                            {
                                table.AddRow(rubriekInhoud, false);
                                foreach (var inhoud in rubriekInhoud.Inhouden)
                                {
                                    Logger.WriteLog(LogLevelL4N.DEBUG,
                                                    String.Format("Bezig met rijen voor inhoud {0}", inhoud.Omschrijving));
                                   
                                    table.AddRow(inhoud);
                                }
                            }
                        }
                    }
                    //uitzondering wordt apart geschreven

                    if (rubriek.Omschrijving == "Rijksvaccinatieprogramma")
                    {
                        if (rubriek.RubriekInhouden.Count > 0 && rubriek.RubriekInhouden.Any(ri => ri.Inhouden.Count() > 0))
                        {
                            laatsteTableVaccinaties = new Table();
                            laatsteTableVaccinaties.Comment = "Vaccinaties";
                            laatsteTableVaccinaties.Borders.Width = 0.5;
                            laatsteTableVaccinaties.Borders.Color = Colors.LightGray;
                            laatsteTableVaccinaties.AddHeaderTwoColumns();
                            laatsteTableVaccinaties.AddRow(rubriek);
                        }
                        foreach (var rubriekInhoud in rubriek.RubriekInhouden)
                        {
                            Logger.WriteLog(LogLevelL4N.DEBUG,
                                            String.Format("Bezig met rijen voor rubriek {0}", rubriek.Omschrijving));

                            laatsteTableVaccinaties.AddRow(rubriekInhoud, false);
                            foreach (var inhoud in rubriekInhoud.Inhouden)
                            {
                                Logger.WriteLog(LogLevelL4N.DEBUG,
                                                String.Format("Bezig met rijen voor rubriek {0}", rubriek.Omschrijving));

                                laatsteTableVaccinaties.AddRow(inhoud);
                            }
                        }
                    }
                    else if (rubriek.Omschrijving == "Van Wiechen 18-48 maanden") //Ontwikkelingsonderzoek 15-48 mnd
                    {
                        if (laatsteTablevanWiechen == null)
                        {
                            laatsteTablevanWiechen = new Table();
                            laatsteTablevanWiechen.Comment = "Van Wiechen Onderzoek 2";
                            laatsteTablevanWiechen.Borders.Width = 0.5;
                            laatsteTablevanWiechen.Borders.Color = Colors.LightGray;
                            laatsteTablevanWiechen.AddHeaderTwoColumns();
                        }

                        if (rubriek.RubriekInhouden.Count > 0)
                        {
                            laatsteTablevanWiechen.AddDate(dossier);
                        }
                           
                        foreach (var rubriekInhoud in rubriek.RubriekInhouden)
                        {
                            Logger.WriteLog(LogLevelL4N.DEBUG,
                                            String.Format("Bezig met rijen voor rubriek {0}", rubriek.Omschrijving));

                            laatsteTablevanWiechen.AddRow(rubriekInhoud);
                            foreach (var inhoud in rubriekInhoud.Inhouden)
                            {
                                Logger.WriteLog(LogLevelL4N.DEBUG,
                                                String.Format("Bezig met rijen voor rubriek {0}",
                                                              rubriek.Omschrijving));

                                laatsteTablevanWiechen.AddRow(inhoud);
                            }
                        }
                    }
                    else if (rubriek.Omschrijving.StartsWith("Van Wiechen 0-15 maanden")) //Ontwikkelingsonderzoek 0-15 mnd
                    {
                        if (laatsteTablevanWiechen1 == null)
                        {
                            laatsteTablevanWiechen1 = new Table();
                            laatsteTablevanWiechen1.Comment = "Van Wiechen Onderzoek 1";
                            laatsteTablevanWiechen1.Borders.Width = 0.5;
                            laatsteTablevanWiechen1.Borders.Color = Colors.LightGray;
                            laatsteTablevanWiechen1.AddHeaderTwoColumns();
                        }
                        

                        if (rubriek.RubriekInhouden.Count > 0)
                        {
                            laatsteTablevanWiechen1.AddDate(dossier);
                        }
                        
                        foreach (var rubriekInhoud in rubriek.RubriekInhouden)
                        {
                            Logger.WriteLog(LogLevelL4N.DEBUG,
                                            String.Format("Bezig met rijen voor rubriek {0}", rubriek.Omschrijving));

                            laatsteTablevanWiechen1.AddRow(rubriekInhoud);
                            foreach (var inhoud in rubriekInhoud.Inhouden)
                            {
                                Logger.WriteLog(LogLevelL4N.DEBUG,
                                                String.Format("Bezig met rijen voor rubriek {0}",
                                                              rubriek.Omschrijving));

                                laatsteTablevanWiechen1.AddRow(inhoud);
                            }
                        }
                    }

                }
                Logger.WriteLog(LogLevelL4N.DEBUG, String.Format("Schrijven van de tafel, dit duurt even...."));
                document.LastSection.Add(table);
                Paragraph par = document.LastSection.AddParagraph();
                par.Format.SpaceAfter = "0.5cm";

            }

            //volgorde is belangrijk. schrijft laatst 'onthouden' vaccinaties
            if (laatsteTableVaccinaties.Comment == "Vaccinaties")
            {
                paragraph = document.LastSection.AddParagraph("Vaccinaties", "Heading1");
                paragraph.AddBookmark("Vaccinaties");
                document.LastSection.Add(laatsteTableVaccinaties);
            }
            if (laatsteTablevanWiechen1 != null && laatsteTablevanWiechen1.Comment == "Van Wiechen Onderzoek 1")
            {
                paragraph = document.LastSection.AddParagraph("Ontwikkelingsonderzoek Zuigeling", "Heading1");
                paragraph.AddBookmark("Van Wiechen Onderzoek 1");
                document.LastSection.Add(laatsteTablevanWiechen1);
            }
            if (laatsteTablevanWiechen != null && laatsteTablevanWiechen.Comment == "Van Wiechen Onderzoek 2")
            {
                paragraph = document.LastSection.AddParagraph("Ontwikkelingsonderzoek Peuter", "Heading1");
                paragraph.AddBookmark("Van Wiechen Onderzoek 2");
                document.LastSection.Add(laatsteTablevanWiechen);
            }
        }

        public static void AddArchives(this Document document, List<Archief> archives, string filePath)
        {
            Paragraph paragraph = document.LastSection.AddParagraph("Archieven", "Heading1");
            paragraph.AddBookmark("Archieven");

            Table table = new Table();
            table.Borders.Width = 0.75;
            table.AddHeader();

            Row row;
            Cell cell;
            if (archives.Count != 0)
            {
                foreach (var archive in archives)
                {
                    // voeg naam archiefdocument toe aan pdf
                    table.AddRow(archive.FileName.Split('\\').Last(), archive.SysStamp);
                }
            }
            else
            {
                row = table.AddRow();
                cell = row.Cells[0];
                cell.AddParagraph("Geen archiefitems aanwezig.");
                //cell.MergeRight = 1; // indien twee kolommen of meer
            }
            row = table.AddRow();
            cell = row.Cells[0];
            cell.AddParagraph("\r\nDisclaimer: vanwege het gekozen moment van conversie, kan het zijn dat de lijst aan archief-documenten INCOMPLEET is.\r\n\r\n");
            //cell.MergeRight = 1; // indien twee kolommen of meer

            document.LastSection.Add(table);
        }
    }
}
