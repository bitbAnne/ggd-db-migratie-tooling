﻿using Services.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Services.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum en) //extension method
        {
            if (en == null)
                return String.Empty;

            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(
                                              typeof(DescriptionAttribute),
                                              false);

                if (attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return en.ToString();
        }

        public static int GetInteger(this Enum en) //extension method
        {
            if (en == null)
                return 0;

            return Convert.ToInt32(en);
        }

        public static bool IsFlagDefined(this Enum en)
        {
            decimal d;
            return !Decimal.TryParse(en.ToString(), out d);
        }

        public static IEnumerable<Enum> ToEnumerable(this Enum input)
        {
            if (input == null)
                return null;

            if (input.GetType().GetCustomAttributes(typeof(FlagsAttribute), false).Length <= 0)
            {
                return Enumerable.Repeat(input, 1);
            }

            return Enum.GetValues(input.GetType()).Cast<Enum>().Where(input.HasFlag);
        }

        public static string GetDescriptions(this Enum input)
        {
            if (input.GetType().GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0 && input.IsFlagDefined())
            {
                return String.Join(", ", input.ToEnumerable().Select(x => x.GetDescription()));
            }

            if (Enum.IsDefined(input.GetType(), input))
                return input.GetDescription();

            return null;
        }
    }
}
