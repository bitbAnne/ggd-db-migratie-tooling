﻿using Services.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.Entity;


namespace Services.Extensions
{
    public static class PersoonsgegevensExtensions
    {
        public static string GetLastName(this Persoonsgegevens person)
        {
            return person.Achternaam ?? person.Geslachtsnaam ?? "";
        }

        public static string GetFilename(this Persoonsgegevens person)
        {
            if (person != null && person.BSN != null)
            {

                return string.Format("HN_{0}_dossierafdruk_{1}", person.CasNummer, person.BSN);
            }

            return string.Format("HN_{0}_dossierafdruk_BSN Onbekend", person.CasNummer);
        }

        public static string GetBirthday(this Persoonsgegevens person)
        {
            return person.Geboortedatum.HasValue ? person.Geboortedatum.Value.ToString("dd-MM-yyyy") : string.Empty;
        }

        public static bool IsOldEnoughForExport(this Persoonsgegevens person)
        {
            if (!person.Geboortedatum.HasValue)
                return false;

            return person.Geboortedatum.Value.AddYears(3).AddMonths(9) < DateTime.Today;
        }

        public static List<KeyValuePair<string, string>> GetPropertiesPerson(this Persoonsgegevens person)
        {
            var result = person.GetAllPropertiesInDictionary();

            if (person.Woonverbanden != null && person.Woonverbanden.Any())
                result = result.Concat(person.Woonverbanden.FirstOrDefault().GetAllPropertiesInDictionary()).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);

            if (person.Bureaus != null)
                result = result.Concat(person.Bureaus.GetTeams()).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);

            if (person.Verwijzers1 != null)
                result = result.Concat(person.Verwijzers1.GetVerwijzer()).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => d.First().Value);


            //lege of waarde zero niet tonen
            var uitzonderingsLijst = new List<string>() {"WoonverbandID", "ClientSrt", "GehoorStat",
                "IndicatieGeheim", "TimeStamp", "UserID", "HuisartsID", "WidIdMethode",
                "WidIdDocType", "IndicatieGezag", "IndicatieGeheim","WvbMods","Bureau","Extern","Controle", "Count", "SaveID",
                "Aantal", "Flags", "NaamOpenbRuimte", "WoonplaatsNaam", "CodeVerblijfplaats", "CodeNummerAanduid"};

            //vervangende labels (oud,nieuw)
            var vervangOmschrijving = new Dictionary<string, string>();
            vervangOmschrijving.Add("PersBijz", "Clientcode");
            vervangOmschrijving.Add("PersMods", "Bijzonderheden");
            vervangOmschrijving.Add("CasNummer", "CAS-nummer");
            vervangOmschrijving.Add("ANummer", "A-nummer");
            vervangOmschrijving.Add("GebLandcode", "Geboorteland");
            vervangOmschrijving.Add("GebOuder1", "GeboortedatumOuder1");
            vervangOmschrijving.Add("GebLandOuder1code", "GeboortelandOuder1");
            vervangOmschrijving.Add("GebOuder2", "GeboortedatumOuder2");
            vervangOmschrijving.Add("GebLandOuder2code", "GeboortelandOuder2");
            //vervangOmschrijving.Add("GemeenteCode", "Gemeente");
            vervangOmschrijving.Add("WidIdDocNummer", "ID-Document");

            foreach (var vv in vervangOmschrijving)
            {
                if (!result.TryGetValue(vv.Key, out var oldValue))
                {

                }
                else
                {
                    result[vv.Value] = oldValue;
                    result.Remove(vv.Key);
                }
            }
            string value;

            //vervangende waardes
            if (result.TryGetValue("Clientcode", out value))
            {
                var oms = new List<string>();

                using (var ent = new GGDHNoordEntities())
                {
                    oms = ent.Menukeuzes.Where(mk => mk.MenuSoort == 3 && value.Contains(mk.MenuKeuze)).Select(mk => mk.Omschrijving).ToList();
                }

                result["Clientcode"] = String.Join(", ", oms);
            }

            //vervangende waardes
            if (result.TryGetValue("Bijzonderheden", out value))
            {
                Bijzonderheden enumresult;
                Enum.TryParse(value, out enumresult);
                var oms = string.Empty;
                var teller = 0;
                foreach (var bijz in enumresult.ToEnumerable())
                {
                    if (!String.IsNullOrEmpty(bijz.GetDescription()))
                    {
                        if (teller++ != 0)
                            oms += "\r\n";

                        oms += bijz.GetDescription();
                    }
                }
                result["Bijzonderheden"] = oms;
            }

            // vervangende waardes; haal naam van geboorteland op o.b.v. landcode
            if (result.TryGetValue("Geboorteland", out value))
            {
                if (person.Landen != null)
                {
                    var bla = string.Format("{0} (code: {1})", person.Landen.Omschrijving, value);
                }
                result["Geboorteland"] = person.Landen != null ? string.Format("{0} (code: {1})", person.Landen.Omschrijving, value) : string.Empty;
            }

            if (result.TryGetValue("GeboortelandOuder1", out value))
            {
                result["GeboortelandOuder1"] = person.Landen1 != null ? string.Format("{0} (code: {1})", person.Landen1.Omschrijving, value) : string.Empty;
            }

            if (result.TryGetValue("GeboortelandOuder2", out value))
            {
                result["GeboortelandOuder2"] = person.Landen2 != null ? string.Format("{0} (code: {1})", person.Landen2.Omschrijving, value) : string.Empty;
            }

            var woonVerband = person.Woonverbanden.FirstOrDefault();

            if (result.TryGetValue("Gemeente", out value))
            {
                result["Gemeente"] = woonVerband != null && woonVerband.Gemeenten != null ? string.Format("{0} (code: {1})", woonVerband.Gemeenten.Omschrijving, value) : string.Empty;
            }

            if (result.TryGetValue("Nationaliteit", out value))
            {
                result["Nationaliteit"] = person.Nationaliteiten != null ? string.Format("{0} (code: {1})", person.Nationaliteiten.Omschrijving, value) : string.Empty;
            }

            // vervangende waardes; haal omschrijving van status van client op o.b.v. code
            if (result.TryGetValue("Status", out value))
            {
                DTO.StatusZorg status;
                string omschrijving = string.Empty;
                if (Enum.TryParse(value, out status))
                {
                    omschrijving = status.GetDescription();
                }
                result["Status"] = string.Format("{0} (code: {1})", omschrijving, value);
            }

            if (result.TryGetValue("Dossierlocatie", out value))
            {
                result["Dossierlocatie"] = person.Dossierlocaties != null ? string.Format("{0} (code: {1})", person.Dossierlocaties.Omschrijving, value) : string.Empty;
            }

            //vervangende waardes
            //dit hoefde niet bij ggd (versie ef ofzo)
            if (result.TryGetValue("Geslacht", out value))
            {
                StatusGeslachtEnum status;
                var oms = value.ToString();
                if (Enum.TryParse(value, out status))
                {
                    oms = status.GetDescription();
                }
                result["Geslacht"] = oms;
            }

            //volgorde lijst ->
            var volgordes = new Dictionary<string, int>();

            var properties = new string[]
            {
                "Geslachtsnaam", "Achternaam", "Voornamen",  "Voorletters", "Roepnaam", "CAS-nummer", "Clientcode", "BSN", "BsnBron", "A-nummer",
                "GbaIndicatieDat", "ID-Document", "Machtigingsnummer", "Voorvoegsel", "GbaVoorvoegsel",
                "Titelcode", "Geslacht", "Geboortedatum", "GebPlaats", "Geboorteland", "Nationaliteit", "TaalCode",
                "Overlijdensdatum", "Telefoon", "Telefoon2", "Telefoon3", "TelefoonOms3", "Email", "Email2",
                "Aanmelddatum", "DatumVestiging", "InZorg", "DatumVertrek", "UitZorg", "Status", "BSNouder1",
                "NaamOuder1", "GeboortedatumOuder1", "GeboortelandOuder1", "BSNouder2", "NaamOuder2", "GeboortedatumOuder2", "GeboortelandOuder2",
                "Huisarts", "Dossierlocatie", "DossierEigenaar", "DossierToegang", "Team",
                "Teamlocatie", "Kostengroep", "GehoorBureau", "Schoolnummer", "SchoolMutatie", "Schooljaar", "KlasGroep",
                "Leerjaar", "PersOrg"
            };
            for (int i = 0; i < properties.Length; i++)
            {
                volgordes.Add(properties[i], i);
            }
            volgordes.Add("Bijzonderheden", 1000);
            volgordes.Add("PersMemo", 1001);

            foreach (var keyValuePair in result.Where(kvp => !volgordes.ContainsKey(kvp.Key)))
            {
                volgordes.Add(keyValuePair.Key, 100);
            }

            return result
                .Join(volgordes, x => x.Key, y => y.Key, (x, y) => new { a = x, b = y })
                .Where(kvp => !String.IsNullOrWhiteSpace(kvp.a.Value) && kvp.a.Value.Trim() != "0" && !uitzonderingsLijst.Contains(kvp.a.Key))
                .OrderBy(kvp => kvp.b.Value)
                .Select(kvp => kvp.a)
                .ToList();
        }

        public static List<Interventie> GetInterventions(this Persoonsgegevens person)
        {
            return person.DossierItems
                 .SelectMany(di => di.DossierInterventieData)
                 .OrderByDescending(did => did.DossierItems.Datum)
                 .Select(di => new Interventie()
                 {
                     Datum = di.DossierItems.Datum,
                     Gewijzigd = di.DossierItems.TimeStamp,
                     Volgnummer = di.ItemID.ToString(),
                     SoortInterventieCode = di.InterventieID.ToString(),
                     SoortInterventieOms = di.DossierItems.Info,
                     Medewerker = di.DossierItems.Medewerkers != null ? di.DossierItems.Medewerkers.Naam : string.Empty,
                     Resultaat = di.Resultaat.ToString(),
                     Status = di.DossierItems.Status.GetDescription(),
                     Aanleiding = di.Aanleiding,
                     Analyse = di.Analyse,
                     Doel = di.Doel,
                     Evaluatie = di.Evaluatie
                 })
                 .ToList();
        }

        public static List<InfoDossier> GetInfoDossiers(this Persoonsgegevens person)
        {
            return person.DossierItems
                    .SelectMany(di => di.DossierInfoData)
                    .OrderByDescending(did => did.DossierItems.Datum)
                    .Select(di => new InfoDossier()
                    {
                        Datum = di.DossierItems.Datum,
                        Gewijzigd = di.DossierItems.TimeStamp,
                        Volgnummer = di.ItemID.ToString(),
                        Omschrijving = di.DossierItems.Info,
                        Medewerker = di.DossierItems.Medewerkers != null ? di.DossierItems.Medewerkers.Naam : string.Empty,
                        Tekstveld = di.Info
                    })
                    .ToList();
        }

        public static List<Activiteit> GetActivities(this Persoonsgegevens person)
        {
            return person.Activiteiten
                .OrderByDescending(ah => ah.UitvoerDatum)
                .Select(a => new Activiteit()
                {
                    Code = a.ActiviteitCode.ToString(),
                    Datum = a.UitvoerDatum,
                    Volgnummer = a.ActiviteitID.ToString(),
                    Omschrijving = a.ActiviteitCodes == null ? string.Empty : a.ActiviteitCodes.ActiviteitOms,
                    Medewerker = a.Medewerkers == null ? string.Empty : a.Medewerkers.Naam,
                    Duur = a.Duur.ToString(),
                    Uitvoering = ((StatusEnum)a.Status).GetDescription(),
                    Info = a.Info,
                    Gewijzigd = a.TimeStamp,
                    Verwijzing = a.VerwijzerSoorten == null ? string.Empty : a.VerwijzerSoorten.Omschrijving,
                    DossierDatum = a.DossierItems != null ? a.DossierItems.Datum : DateTime.MinValue,
                    DossierId = a.DossierItems != null ? a.DossierItems.ItemID.ToString() : string.Empty,
                    DossierNaam = a.DossierItems != null ? a.DossierItems.Info : string.Empty,
                    AfwijzingId = a.ActAfwijzingID != null ? a.ActAfwijzingID.ToString() : null,
                    AfwijzingOmschrijving = a.ActAfwijzing != null ? a.ActAfwijzing.Omschrijving : null
                })
                .ToList();
        }

        public static List<Dossier> GetDossiers(this Persoonsgegevens person)
        {
            var dossierItems = person.DossierItems.Where(x => x.DossierSoort == 1).OrderByDescending(di => di.Datum);

            try
            {
                var dossiers = new List<Dossier>();

                foreach (var item in dossierItems)
                {
                    dossiers.Add(new Dossier
                    {
                        Datum = item.Datum,
                        DossierSoort = "Registratie",
                        DiagnoseSoort = item.GetDiagSoort(),
                        ContactMoment = item.GetContactMoment(),
                        Info = item.Info,
                        Rubrieken = item.GetRubriekenPDF(),
                        Medewerker = (item.Medewerkers != null) ? item.Medewerkers.Naam : String.Empty
                    });
                }
                return dossiers;
            }
            catch
            {
                Logger.WriteLog(person.CasNummer.ToString());
            }

            return new List<Dossier>();
        }
    }

    [Serializable]
    [Flags]
    public enum Bijzonderheden
    {
        [Description("Geen toestemming elektronische uitwisseling gegevens")]
        GeenToestemming = 128,
        [Description("Geen afspraken / plandatums meer te plannen")]
        GeenAfspraken = 256,
        [Description("Aangevinkt")]
        Aangevinkt = 1024,

        [Description("")]
        Leeg = 0,
        [Description("")]
        Leeg1 = 1,
        [Description("")]
        Leeg2 = 2,
        [Description("")]
        Leeg4 = 4,
        [Description("")]
        Leeg8 = 8,
        [Description("")]
        Leeg16 = 16,
        [Description("")]
        Leeg32 = 32,
        [Description("")]
        Leeg64 = 64,
        [Description("")]
        Leeg512 = 512,
        [Description("")]
        Leeg32768 = 32768
    }

    public class Interventie
    {
        public DateTime Datum { get; set; }
        public DateTime Gewijzigd { get; set; }
        public string Volgnummer { get; set; }
        public string SoortInterventieCode { get; set; }
        public string SoortInterventieOms { get; set; }
        public string Medewerker { get; set; }
        public string Resultaat { get; set; }
        public string Status { get; set; }
        public string Aanleiding { get; set; }
        public string Analyse { get; set; }
        public string Doel { get; set; }
        public string Evaluatie { get; set; }
    }

    public class InfoDossier
    {
        public DateTime Datum { get; set; }
        public DateTime Gewijzigd { get; set; }
        public string Volgnummer { get; set; }
        public string Omschrijving { get; set; }
        public string Medewerker { get; set; }
        public string Tekstveld { get; set; }
    }

    public class Activiteit
    {
        public DateTime Datum { get; set; }
        public DateTime Gewijzigd { get; set; }
        public string Volgnummer { get; set; }
        public string Code { get; set; }
        public string Omschrijving { get; set; }
        public string Medewerker { get; set; }
        public string Uitvoering { get; set; }
        public string Duur { get; set; }
        public string Info { get; set; }
        public DateTime DossierDatum { get; set; }
        public string DossierNaam { get; set; }
        public string DossierId { get; set; }
        public string AfwijzingId { get; set; }
        public string AfwijzingOmschrijving { get; set; }
        public string Verwijzing { get; set; }
    }

    //puur voor pdf
    public class Dossier
    {
        public DateTime Datum { get; set; }
        public string DossierSoort { get; set; }
        public string DiagnoseSoort { get; set; }
        public string ContactMoment { get; set; }
        public string Info { get; set; }
        public string Medewerker { get; set; }
        public List<Rubriek> Rubrieken { get; set; }
    }

    public class Rubriek
    {
        public string Omschrijving { get; set; }
        public List<RubriekInhoud> RubriekInhouden { get; set; }
    }

    public class RubriekInhoud
    {
        public string Subkop { get; set; }
        public List<Inhoud> Inhouden { get; set; }
    }

    public class Inhoud
    {
        public bool IsFlagged { get; set; }
        public int DiagitemId { get; set; }
        public string Omschrijving { get; set; }
        public string Waarde { get; set; }
        public string Type { get; set; }
    }
}