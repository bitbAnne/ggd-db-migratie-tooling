﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MigraDoc.DocumentObjectModel;
using Services.DTO;


using System.Configuration;

namespace Services.Extensions
{
	public static class GenericExtensions
	{
		public static Dictionary<string, string> GetAllPropertiesInDictionary(this object table)
		{
			var result = new Dictionary<string, string>();

			foreach (var prop in table?.GetType()?.GetProperties())
			{
				string oms = prop.Name;
				string waarde = String.Empty;
				var x = prop.PropertyType;
				//prop.GetValue(table);

				if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
				{
					x = prop.PropertyType.GenericTypeArguments.First();
				}

				switch (Type.GetTypeCode(x))
				{
					case TypeCode.Decimal:
					case TypeCode.Byte:
					case TypeCode.Double:
					case TypeCode.Int16:
					case TypeCode.Int32:
					case TypeCode.Int64:
						waarde = prop.GetValue(table, null) == null ? String.Empty : prop.GetValue(table, null).ToString();
						break;

					case TypeCode.String:
						waarde = prop.GetValue(table, null) == null ? String.Empty : prop.GetValue(table, null).ToString();
						break;
					case TypeCode.DateTime:
						waarde = prop.GetValue(table, null) == null ? String.Empty : ((DateTime)prop.GetValue(table, null)).ToString("dd-MM-yyyy");
						break;
				}
				result.Add(oms, waarde);
			}
			return result;
		}

		public static string RtfToText(this string rtf)
		{
			if (rtf.StartsWith(@"{\rtf"))
			{
				using (RichTextBox rtb = new RichTextBox())
				{
					rtb.Rtf = rtf;
					return rtb.Text;
				}
			}
			return rtf;
		}

		public static Color SwitchColor(this Color color)
		{
			if (color == Colors.AliceBlue)
				return Colors.White;

			return Colors.AliceBlue;
		}

		public static decimal AdjustDecimalValuePDF(this decimal number, byte dataSoort)
		{
			//van db naar pdfwaarde
			switch (dataSoort)
			{
				case 16:
				case 26: //lengte in m (puur voor zb)
				case 25:
				case 31:
				case 32:
				case 34:
				case 35: //Hoofdomtrek geboorte voor HM
				case 36:
				case 37:
				case 11:

					Logger.WriteLog(LogLevelL4N.DEBUG, String.Format("Datamanipulatie met factor 100 is toegepast voor datasoort {0}", dataSoort));
					return number / 100;
			}
			return number;
		}
	}
}
