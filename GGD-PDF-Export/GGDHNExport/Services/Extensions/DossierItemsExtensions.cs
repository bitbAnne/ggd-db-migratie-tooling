﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.DTO;
using System.Data.Entity;


namespace Services.Extensions
{
    public static class DossierItemsExtensions
    {
        public static string GetDiagSoort(this DossierItems dossierItem)
        {
            var result = dossierItem.Data1.GetDescriptions();
            result += String.Join(", ", dossierItem.Data2.GetDescriptions());
            return result;           
        }

        public static List<Rubriek> GetRubriekenPDF(this DossierItems dossierItem)
        {
            List<int> diagSoortNummers = dossierItem.GetDiagSoortNumbers();

            if (diagSoortNummers.Count == 0)
            {
                return new List<Rubriek>()
                {
                    new Rubriek()
                    {
                        Omschrijving = "Geen onderwerp geselecteerd",
                        RubriekInhouden = new List<RubriekInhoud>
                        {
                            new RubriekInhoud()
                            {
                                Subkop = "Geen gegevens om te tonen",
                                Inhouden = new List<Inhoud>()
                            }
                        }
                    }
                };
            }

            List<DiagRubrieken> diagRubrieken;

            using (var entities = new GGDHNoordEntities())
            {
                diagRubrieken =
                    (entities.DiagSoorten
                        .Where(ds => diagSoortNummers.Contains(ds.DiagSoort))
                        .SelectMany(ds => ds.DiagRubrieken)
                        .OrderBy(dr => dr.Positie))
                    .Include(s => s.DiagItemLinks)
                    .Include(s => s.DiagItemLinks
                        .Select(d => d.DiagItems))
                    .ToList();
            }

            List<Rubriek> rubrieken = new List<Rubriek>();
            foreach (var rubriek in diagRubrieken)
            {
                rubrieken.Add(new Rubriek()
                {
                    Omschrijving = rubriek.Omschrijving,
                    RubriekInhouden = dossierItem.DossierDiagData.GetRubriekInhouden(rubriek.DiagItemLinks, rubriek.Omschrijving)
                });
            }

            return rubrieken;
        }

        public static string GetContactMoment(this DossierItems dossierItems)
        {
            if (!dossierItems.Contactmoment.HasValue)
                return string.Empty;

            return string.Format("{1}", dossierItems.Contactmomenten.Contactmoment, dossierItems.Contactmomenten.Omschrijving);
        }

        private static List<int> GetDiagSoortNumbers(this DossierItems dossierItems)
        {
            using (var ent = new GGDHNoordEntities())
            {
                var result = new List<int>();
                foreach (int a in (from DiagSoortenEnum1 a in dossierItems.Data1.ToEnumerable() select a.GetInteger()).ToList())
                {
                    var diagSoort = ent.DiagSoorten.FirstOrDefault(ds => ds.Coded30 == a);
                    if (diagSoort != null)
                        result.Add(diagSoort.DiagSoort);
                }

                foreach (int a in (from DiagSoortenEnum2 a in dossierItems.Data2.ToEnumerable() select a.GetInteger()).ToList())
                {
                    var diagSoort = ent.DiagSoorten.FirstOrDefault(ds => ds.Coded60 == a);
                    if (diagSoort != null)
                        result.Add(diagSoort.DiagSoort);
                }

                return result;
            }
        }      
    }
}
