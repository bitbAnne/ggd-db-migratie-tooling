﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.DTO;

namespace Services.Extensions
{
    public static class BureausExtensions
    {
        public static Dictionary<string, string> GetTeams(this Bureaus bureau)
        {
            string key = "Team";
            var medewerkers = new List<string>();

            foreach (var medewerker in bureau.TeamMedewerkers)
            {
                medewerkers.Add(medewerker.Medewerkers.Naam);
            }

            string omschrijving = String.Format("{0} {2} ({1})", bureau.Bureau.ToString(), String.Join("; ", medewerkers), bureau.Locatie);

            return new Dictionary<string, string> { { key, omschrijving } };
        }
    }
}
