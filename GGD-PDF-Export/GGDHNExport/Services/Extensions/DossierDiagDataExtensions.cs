﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.DTO;
using System.Globalization;

using Services.Extensions;

namespace Services.Extensions
{
    public static class DossierDiagDataExtensions
    {
        public static List<RubriekInhoud> GetRubriekInhouden(this ICollection<DossierDiagData> dossierDiagDatas, IEnumerable<DiagItemLinks> diagItemLinks, string temp)
        {
            var rubriekInhouden = new List<RubriekInhoud>();
            var rubriekInhoud = new RubriekInhoud() { Subkop = string.Empty, Inhouden = new List<Inhoud>() };
			var itemLinks = diagItemLinks.OrderBy(x => x.Positie);

			foreach (var diagItemLink in itemLinks)
            {
                if (diagItemLink.DiagItems.DataSoort == 14)
                {
                    rubriekInhouden.Add(rubriekInhoud);
                    rubriekInhoud = new RubriekInhoud()
                    {
                        Subkop = diagItemLink.DiagItems.Omschrijving,
                        Inhouden = new List<Inhoud>()
                    };
                }
                else
                {
                    var dossierDiagData = dossierDiagDatas.FirstOrDefault(x => x.DiagItemID == diagItemLink.DiagItems.DiagItemID);
                   
                    //hieronder de mapping naar de pdf
                    var inhoud = dossierDiagData.GetInhoudPDF();
                    if (inhoud != null)
                        rubriekInhoud.Inhouden.Add(inhoud);
                }

            }
            rubriekInhouden.Add(rubriekInhoud);

            return rubriekInhouden;
        }

        public static Inhoud GetInhoudPDF(this DossierDiagData dossierDiagData)
        {
            if (dossierDiagData == null)
                return null;

            return new Inhoud()
            {
                DiagitemId = dossierDiagData.DiagItemID,
                Omschrijving = dossierDiagData.DiagItems.Omschrijving,
                Type = dossierDiagData.DiagItems.DataSoort.ToString(),
                Waarde = dossierDiagData.GetWaardePDF(),
                IsFlagged = dossierDiagData.Flags == 1 || dossierDiagData.Flags == 5 || dossierDiagData.Flags == 7
            };
        }

        public static string GetWaardePDF(this DossierDiagData dossierDiagData)
        {
            Type returnType = dossierDiagData.DiagItems.GetReturnType();
            return dossierDiagData.GetWaardePDF(returnType);
        }

        public static string GetWaardePDF(this DossierDiagData dossierDiagData, Type returnType)
        {

            if (returnType.IsEnum)
            {
                var enumWaarde = (Enum)Enum.Parse(returnType, dossierDiagData.Data.ToString());
                return enumWaarde.GetDescriptions();
            }

            var waarde = dossierDiagData.Data.ToString();
            decimal decimaal = dossierDiagData.Data;
            TypeCode typeCode = Type.GetTypeCode(returnType);

            switch (typeCode)
            {
                case TypeCode.Decimal:
                case TypeCode.Int16:
                case TypeCode.Int32:
                    decimaal = decimaal.AdjustDecimalValuePDF(dossierDiagData.DiagItems.DataSoort);
                    waarde = decimaal.ToString("G29", CultureInfo.GetCultureInfo("nl-NL"));
                    break;
                case TypeCode.String:
                    waarde = dossierDiagData.CharData;
                    break;
                case TypeCode.DateTime:
                    IFormatProvider theCultureInfo = new CultureInfo("en-GB", true);
                    DateTime datum;
                    waarde = DateTime.TryParseExact(dossierDiagData.Data.ToString(), "yyyyMMdd", theCultureInfo, DateTimeStyles.None,
                                                    out datum) ? String.Format("{2}-{1}-{0}", datum.Year, datum.Month, datum.Day) : "01-01-1900";
                    break;
            }

            if (!string.IsNullOrEmpty(dossierDiagData.DiagItems.Eenheid))
            {
                waarde = waarde + " " + dossierDiagData.DiagItems.Eenheid;
            }

            return waarde;
        }     
    }
}
