﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace Services.Extensions
{
    public static class ArchiveDatabaseExtensions
    {
        public static void ConvertBinaryDataFromArchiveDbToFile(this List<DTO.Archief> archives, string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath)) { return; }

            for (int i = 0; i < archives.Count; i++)
            {
                string file = archives[i].FileName.Split('\\').Last();
                string extension = Path.GetExtension(file);
                string fileWithoutExtension = file.Substring(0, file.Length - extension.Length);
                string path = Path.GetDirectoryName(filePath);

                var binaryData = archives[i].Data;

                var outputDirectory = ConfigurationManager.AppSettings["outputDirectory"];
                var fullPath = Path.Combine(outputDirectory, filePath, file);

                int counter = 1;
                while (File.Exists(fullPath))
                {
                    string tempFileName = string.Format("{0}({1})", fileWithoutExtension, counter++);
                    fullPath = Path.Combine(filePath, tempFileName + extension);
                }

                using (var stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                {
                    stream.Write(binaryData, 0, binaryData.Length);
                }
            }
        }
    }
}
