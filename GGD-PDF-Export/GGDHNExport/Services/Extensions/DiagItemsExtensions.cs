﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.DTO;


namespace Services.Extensions
{
    public static class DiagItemsExtensions
    {
        public static Type GetReturnType(this DiagItems diagItem )
        {
            //datasoorten GGD HN komen overeen met ZZ
            //alleen diagitems hebben ander datasoort...
         
            switch (diagItem.DataSoort)
            {
                case 1://nummer
								case 8: // Bloeddruk Systolische waarde
								case 15: // Bloeddruk Diastolische waarde
                //case 3: 
                case 30://gewicht in gram
                case 33://geboortegewicht in gram

                    return typeof(int);

                case 4://goed/niet goed
                case 9://onvoldoende/redelijk/goed
                case 10://familieanamnese
                case 12://ja/nee
                case 22://onvoldoende/voldoende/goed
                case 29://wel/niet onderzocht/bijz
                    return typeof(MeerkeuzeEnum);

                case 3:
                case 11://decimaal
                case 16://BMI
                case 25://gewicht in kg
                case 26://lengte in m 
                case 31://lengte in cm
                case 32://hoofdomtrek
                case 34://geboortelengte in cm
                case 35://geboortehoofdomtrek
                case 36://lengte vader in cm
                case 37://lengte moeder in cm
                    return typeof(Decimal);
                case 17://vrij tekst klein
                case 43://vrije tekst middel
                    return typeof(String);
                case 27://datum
                    return typeof(DateTime);
                case 13://itemwaarden
                case 28://keuzelijst
                    return GeneratedHelperLogic.BepaalTypeVoorDiagnoseItem(diagItem.DataSoort, diagItem.DataValNummer);
            }

            Logger.WriteLog(diagItem.ToString() + "- & - was opgezocht voor - & - " + diagItem.DataSoort + " - & - " + diagItem.Afkorting + " - & - " + diagItem.BDSnummer);
            Logger.WriteLog(LogLevelL4N.FATAL, String.Format("Type voor diagnoseitem kan niet vastgesteld worden voor datasoort {0} en datavalnummer {1}", diagItem.DataSoort, diagItem.DataValNummer));
            return typeof(String); ;
        }
    }
}
