﻿using MigraDoc.DocumentObjectModel;
using Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Extensions;
using Services;
using System.IO;
using System.Configuration;
using System.Diagnostics;

namespace Services
{
	public class PDFLogic
	{
		public static Document CreateDocument(Persoonsgegevens person, string filePath)
		{
			Logger.WriteLog(LogLevelL4N.INFO, string.Format("PDF voor {0}/{1} wordt momenteel opgebouwd....", person.BSN, person.CasNummer));
			if (person == null)
				return null;

			CheckFolders(person.CasNummer);
			var document = new Document();
			//string[] fileEntries;
			List<string> files = new List<string>();
			var archives = new List<Archief>();

			document.Info.Title = "Afspraak-, interventie- en activiteitgegevens uit mlCAS";
			document.Info.Subject = "Afspraak-, interventie- en activiteitgegevens uit mlCAS zijn automatisch uitgelezen, zodat ze niet verloren gaan";
			document.Info.Author = "Betabit Amsterdam";

			var pgs = person.GetPropertiesPerson();
			var appointments = AppointmentLogic.AppointmentsPerPerson(person);
			var interventions = person.GetInterventions();
			var activities = person.GetActivities();
			var infoDossiers = person.GetInfoDossiers();
			var dossiers = person.GetDossiers();

			using (var entities = new MLCASarchiefGGDHN_PRDEntities())
			{
				var archiveFiles = entities.Archief.Where(a => a.Casnummer == person.CasNummer);
				foreach (var file in archiveFiles)
				{
                    files.Add(file.FileName);

                    if (file.Data != null)
					{
						archives.Add(file);
					}
				}
				archives = archives.OrderByDescending(a => a.SysStamp).ToList();

			}

			Styles.DefineStyles(document);
			document.DefaultPageSetup.LeftMargin = "1cm";

			document.AddCover(person);
			document.AddTOC(person, pgs, appointments, interventions, activities, infoDossiers, dossiers);
			document.DefineContentSection();
			document.AddPersonalInformation(person, pgs);
			document.AddAppointments(person, appointments);
			document.AddActivities(person, activities);
			document.AddInterventions(person, interventions);
			document.AddInfoDossiers(person, infoDossiers);
			document.AddDossiers(person, dossiers);

			archives = RemoveArchiveDocumentsFromListIfNecessary(archives);
			document.AddArchives(archives, filePath);
			// archives.ConvertBinaryDataFromArchiveDbToFile(filePath);

			return document;
		}

		public static void CheckFolders(int casNummer)
		{
			var directory = new DirectoryInfo(ConfigurationManager.AppSettings["outputDirectory"]);
			if (!directory.Exists)
				Directory.CreateDirectory(ConfigurationManager.AppSettings["outputDirectory"]);
			var casDirectory = new DirectoryInfo(Path.Combine(ConfigurationManager.AppSettings["outputDirectory"], casNummer.ToString()));
			if (!casDirectory.Exists)
			{
				Directory.CreateDirectory(Path.Combine(ConfigurationManager.AppSettings["outputDirectory"], casNummer.ToString()));
			}
		}

		private static List<Archief> RemoveArchiveDocumentsFromListIfNecessary(List<Archief> archives)
		{
			var acceptedExtensions = new string[]
			{
								".pdf", ".docx", ".doc", ".htm", ".msg", ".txt", ".rtf", ".zip", ".jpg", ".bmp", ".tif", ".odt",
								".xml", ".mht", ".xlsx", "xls", ".png", ".dotm", ".dot", ".xps", ".docm"
			};

			for (int i = 0; i < archives.Count; i++)
			{
				// controleer filename van elk archiefdocument op (aanwezigheid van een) extensie
				if (!acceptedExtensions.Any(x => archives[i].FileName.ToLower().EndsWith(x)))
				{
					if (archives[i].FileName.ToLower().Contains(@"\afsprakenbureau\"))
					{
						// betreft pdfs zonder extensie achter filename; deze moeten alsnog mee
						archives[i].FileName += ".pdf";
					}
					else
					{
						// negeer overige archiefdocumenten; waaronder bestanden met extensie .tmp
						archives.Remove(archives[i]);
					}
				}
			}

			return archives;
		}
	}
}
