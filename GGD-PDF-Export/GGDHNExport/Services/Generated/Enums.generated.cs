﻿        

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
 
namespace Services.DTO 
{
	[Serializable]
    [Flags]
    public enum DiagSoortenEnum1
    {
           
		[Description("Screeningshuisbezoek")]
        Screeningshuisbezoek = 1,
           
		[Description("Intake huisbezoek")]
        IntakeHuisbezoek = 2,
           
		[Description("Consult 0-15 maanden arts")]
        Consult015MaandenArts = 4,
           
		[Description("Consult 0-15 maanden verpleegkundige")]
        Consult015MaandenVerpleegkundige = 8,
           
		[Description("Consult 15-48 maanden arts")]
        Consult1548MaandenArts = 16,
           
		[Description("Consult 15-48 maanden verpleegkundige")]
        Consult1548MaandenVerpleegkundige = 32,
           
		[Description("Consult 4-19 jaar arts")]
        Consult419JaarArts = 64,
           
		[Description("Consult 4-19 jaar verpleegkundige")]
        Consult419JaarVerpleegkundige = 128,
           
		[Description("Consult 4-19 jaar doktersassistente")]
        Consult419JaarDoktersassistente = 256,
           
		[Description("Consult op indicatie 0-19 jaar")]
        ConsultOpIndicatie019Jaar = 512,
           
		[Description("Consult speciaal onderwijs 0-19 jaar")]
        ConsultSpeciaalOnderwijs019Jaar = 1024,
           
		[Description("Specifieke anamnese zuigeling")]
        SpecifiekeAnamneseZuigeling = 2048,
           
		[Description("Specifieke anamnese peuter")]
        SpecifiekeAnamnesePeuter = 4096,
           
		[Description("Specifieke anamnese kleuter")]
        SpecifiekeAnamneseKleuter = 8192,
           
		[Description("Specifieke anamnese jeugdige")]
        SpecifiekeAnamneseJeugdige = 16384,
           
		[Description("Uitslag neonatale screening")]
        UitslagNeonataleScreening = 32768,
           
		[Description("Gezinssamenstelling")]
        Gezinssamenstelling = 65536,
           
		[Description("Erfelijke belasting")]
        ErfelijkeBelasting = 131072,
           
		[Description("Draagkracht en -last")]
        DraagkrachtEnLast = 262144,
           
		[Description("Voor- en buitenschoolse voorzieningen")]
        VoorEnBuitenschoolseVoorzieningen = 524288,
           
		[Description("Zwangerschap")]
        Zwangerschap = 1048576,
           
		[Description("Bevalling")]
        Bevalling = 2097152,
           
		[Description("Pasgeborene")]
        Pasgeborene = 4194304,
           
		[Description("Conclusie onderzoek doktersassistente")]
        ConclusieOnderzoekDoktersassistente = 8388608,
           
		[Description("DMO-protocol")]
        DmoProtocol = 16777216,
           
		[Description("SDQ ouder")]
        SdqOuder = 33554432,
           
		[Description("SDQ tiener")]
        SdqTiener = 67108864,
           
		[Description("Signaleringsinstrument psychosociale ontwikkeling")]
        SignaleringsinstrumentPsychosocialeOntwikkeling = 134217728,
           
		[Description("Groei 0-15 maanden")]
        Groei015Maanden = 268435456,
           
		[Description("Groei")]
        Groei = 536870912  
		
		//Toegevoegd, omdat oude items hier nog aan voldoen
		,[Description("Verwijderd")]
		Verwijderd = 33554432 << 1
	}


	[Serializable]
    [Flags]
    public enum DiagSoortenEnum2
    {
           
		[Description("Van Wiechen 0-15 maanden")]
        VanWiechen015Maanden = 1,
           
		[Description("Van Wiechen 18-48 maanden")]
        VanWiechen1848Maanden = 2,
           
		[Description("Spraaktaalontwikkeling")]
        Spraaktaalontwikkeling = 4,
           
		[Description("Ontwikkelingsonderzoek 4-19 jaar")]
        Ontwikkelingsonderzoek419Jaar = 8,
           
		[Description("Motoriek volgens Baecke Fassaert")]
        MotoriekVolgensBaeckeFassaert = 16,
           
		[Description("Neuromotorisch onderzoek")]
        NeuromotorischOnderzoek = 32,
           
		[Description("VOV")]
        Vov = 64,
           
		[Description("Visusonderzoek")]
        Visusonderzoek = 128,
           
		[Description("Gehooronderzoek")]
        Gehooronderzoek = 256,
           
		[Description("Observatie verpleegkundige")]
        ObservatieVerpleegkundige = 512,
           
		[Description("Genitalia / puberteitsontwikkeling")]
        GenitaliaPuberteitsontwikkeling = 1024,
           
		[Description("Lichamelijk onderzoek 0-15 maanden")]
        LichamelijkOnderzoek015Maanden = 2048,
           
		[Description("Lichamelijk onderzoek")]
        LichamelijkOnderzoek = 4096,
           
		[Description("Heuponderzoek")]
        Heuponderzoek = 8192,
           
		[Description("Voorlichting en advies")]
        VoorlichtingEnAdvies = 16384,
           
		[Description("Rijksvaccinatieprogramma")]
        Rijksvaccinatieprogramma = 32768,
           
		[Description("Groeigegevens")]
        Groeigegevens = 65536,
           
		
           
		[Description("VVE")]
        Vve = 262144,
           
		[Description("Registratie-items voor 1 oktober 2012")]
        RegistratieItemsVoor1Oktober2012 = 524288,
           
		[Description("Opvoedspreekuur")]
        Opvoedspreekuur = 1048576,
           
		[Description("Groeigegevens VO verpleegkundige")]
        GroeigegevensVoVerpleegkundige = 2097152,
           
		[Description("Motoriektest Baecke-Fassaert (beoordeling na PGO2)")]
        MotoriektestBaeckeFassaertBeoordelingNaPgo2 = 8388608,
           
		[Description("Intake schoolkind")]
        IntakeSchoolkind = 16777216,
           
		[Description("Gericht onderzoek")]
        GerichtOnderzoek = 33554432,
           
		[Description("Historie cliëntgegevens")]
        HistorieCliNtgegevens = 131072,
           
		[Description("Contactmoment VO klas 2")]
        ContactmomentVoKlas2 = 268435456  

	}

	
	[Serializable]
    public enum ContactmomentenEnum : short
    {
           
		[Description("Screeningshuisbezoek")]
        Screeningshuisbezoek = 1,
           
		[Description("Intake huisbezoek")]
        IntakeHuisbezoek = 2,
           
		[Description("Consult 4 weken")]
        Consult4Weken = 3,
           
		[Description("Consult 8 weken")]
        Consult8Weken = 4,
           
		[Description("Consult 3 maanden")]
        Consult3Maanden = 5,
           
		[Description("Consult 4 maanden")]
        Consult4Maanden = 6,
           
		[Description("Consult 6 maanden")]
        Consult6Maanden = 7,
           
		[Description("Consult 7,5 maand")]
        Consult75Maand = 8,
           
		[Description("Consult 9 maanden")]
        Consult9Maanden = 9,
           
		[Description("Consult 11 maanden")]
        Consult11Maanden = 10,
           
		[Description("Consult 14 maanden")]
        Consult14Maanden = 11,
           
		[Description("Consult 18 maanden")]
        Consult18Maanden = 12,
           
		[Description("Consult 2 jaar")]
        Consult2Jaar = 13,
           
		[Description("Consult 3 jaar")]
        Consult3Jaar = 14,
           
		[Description("Consult 3,9 jaar")]
        Consult39Jaar = 15,
           
		[Description("Contactmoment logopedie 5 jaar")]
        ContactmomentLogopedie5Jaar = 16,
           
		[Description("Consult 5-6 jaar")]
        Consult56Jaar = 17,
           
		[Description("Consult 10-11 jaar DA")]
        Consult1011JaarDa = 18,
           
		[Description("Consult 10-11 jaar")]
        Consult1011Jaar = 19,
           
		[Description("Consult 13-14 jaar")]
        Consult1314Jaar = 20,
           
		[Description("Consult speciaal onderwijs")]
        ConsultSpeciaalOnderwijs = 21,
           
		[Description("Consult 15-16 jaar")]
        Consult1516Jaar = 22,
           
		[Description("Consult op indicatie 0-19 jaar")]
        ConsultOpIndicatie019Jaar = 23,
           
		[Description("Opvoedspreekuur")]
        Opvoedspreekuur = 24,
           
		[Description("Telefonisch contactmoment")]
        TelefonischContactmoment = 25,
           
		[Description("Spreekuur")]
        Spreekuur = 26,
           
		[Description("Groepsbijeenkomst (groepsvoorlichting)")]
        GroepsbijeenkomstGroepsvoorlichting = 27,
           
		[Description("Intake nieuwkomers")]
        IntakeNieuwkomers = 28,
           
		[Description("Ander contactmoment")]
        AnderContactmoment = 29,
           
		[Description("Consultatie/inlichtingen vragen")]
        ConsultatieInlichtingenVragen = 30,
           
		[Description("Melding")]
        Melding = 31,
           
		[Description("Terugkoppeling verwijzing")]
        TerugkoppelingVerwijzing = 32,
           
		[Description("Zorgcoordinatie")]
        Zorgcoordinatie = 33,
           
		[Description("Andere activiteit")]
        AndereActiviteit = 34  

	}

	
	[Serializable]
    public enum MeerkeuzeEnum
    {
           
		[Description("Geen")]
        Geen = 1,
           
		[Description("Matig")]
        Matig = 2,
           
		[Description("Veel")]
        Veel = 3,
           
		[Description("Laag")]
        Laag = 4,
           
		[Description("Normaal")]
        Normaal = 5,
           
		[Description("Hoog")]
        Hoog = 6,
           
		[Description("Goed")]
        Goed = 7,
           
		[Description("Niet goed")]
        NietGoed = 8,
           
		[Description("Beperkt")]
        Beperkt = 9,
           
		[Description("Sterk")]
        Sterk = 10,
           
		[Description("Negatief")]
        Negatief = 11,
           
		[Description("Neutraal")]
        Neutraal = 12,
           
		[Description("Onvoldoende")]
        Onvoldoende = 13,
           
		[Description("Redelijk")]
        Redelijk = 14,
           
		[Description("Positief")]
        Positief = 15,
           
		[Description("Ja")]
        Ja = 16,
           
		[Description("Nee")]
        Nee = 17,
           
		[Description("Uitgebreid")]
        Uitgebreid = 18,
           
		[Description("Aanbeveling")]
        Aanbeveling = 19,
           
		[Description("Voldoende")]
        Voldoende = 20,
           
		[Description("Wel")]
        Wel = 21,
           
		[Description("Niet")]
        Niet = 22,
           
		[Description("Onderzocht met bijz.")]
        OnderzochtMetBijz = 23,
           
		[Description("Onderzocht geen bijz.")]
        OnderzochtGeenBijz = 24,
           
		[Description("Niet Onderzocht")]
        NietOnderzocht = 25,
           
		[Description("Signaleren")]
        Signaleren = 26,
           
		[Description("Beëindigen")]
        BeIndigen = 27,
           
		[Description("Geen actie")]
        GeenActie = 28,
           
		[Description("4 weken")]
        Weken4 = 101,
           
		[Description("8 weken")]
        Weken8 = 102,
           
		[Description("3 maanden")]
        Maanden3 = 103,
           
		[Description("Extra 1 Zgl")]
        Extra1Zgl = 104,
           
		[Description("6 maanden")]
        Maanden6 = 105,
           
		[Description("Extra 2 Zgl")]
        Extra2Zgl = 106,
           
		[Description("9 maanden")]
        Maanden9 = 107,
           
		[Description("12 maanden")]
        Maanden12 = 108,
           
		[Description("15 maanden")]
        Maanden15 = 109,
           
		[Description("1,5 jaar")]
        Jaar15 = 110,
           
		[Description("Extra 1 Ptr")]
        Extra1Ptr = 111,
           
		[Description("2 jaar")]
        Jaar2 = 112,
           
		[Description("2,5 jaar")]
        Jaar25 = 113,
           
		[Description("3 jaar")]
        Jaar3 = 114,
           
		[Description("3,5 jaar")]
        Jaar35 = 115,
           
		[Description("4 jaar")]
        Jaar4 = 116,
           
		[Description("4,5 jaar")]
        Jaar45 = 117  

	}

	
	[Serializable]
    public enum BSNBron : short
    {
           
		[Description("Onbekend")]
        Onbekend = 0,
           
		[Description("GBA")]
        Gba = 1,
           
		[Description("SBV-Z")]
        SbvZ = 2,
           
		[Description("Andere gebruiker")]
        AndereGebruiker = 3,
           
		[Description("Eigen administratie")]
        EigenAdministratie = 4  

	}


	[Serializable]
    public enum StatusZorg : byte
    {
           
		[Description("In zorg")]
        InZorg = 0,
           
		[Description("Signaal")]
        Signaal = 2,
           
		[Description("Intake")]
        Intake = 4,
           
		[Description("Geen gebruik")]
        GeenGebruik = 5,
           
		[Description("Elders in zorg")]
        EldersInZorg = 6,
           
		[Description("Uit zorg")]
        UitZorg = 40,
           
		[Description("Verhuisd")]
        Verhuisd = 41,
           
		[Description("Overleden")]
        Overleden = 42,
           
		[Description("Niet bereikt")]
        NietBereikt = 43,
           
		[Description("Verwijderd")]
        Verwijderd = 95  

	}


	
	[Serializable]
    public enum ResultaatEnum  : byte
    {
           
		[Description("")]
        Leeg = 0,
           
		[Description("OK")]
        Ok = 1,
           
		[Description("Nv Zb")]
        NvZb = 2,
           
		[Description("AfgTijd")]
        Afgtijd = 3,
           
		[Description("AfgLaat")]
        Afglaat = 4,
           
		[Description("AfgAdm")]
        Afgadm = 5,
           
		[Description("Gh Vold")]
        GhVold = 10,
           
		[Description("Gh Onv")]
        GhOnv = 11,
           
		[Description("Afsprk")]
        Afsprk = 50,
           
		[Description("Onbek")]
        Onbek = 99  

	}
	
	
	[Serializable]
    public enum StatusGeslachtEnum  : byte
    {
           
		[Description("Onbekend")]
        Onbekend = 0,
           
		[Description("Jongen")]
        Jongen = 1,
           
		[Description("Meisje")]
        Meisje = 2,
           
		[Description("Geen selectie")]
        GeenSelectie = 3,
           
		[Description("Niet gespecificeerd")]
        NietGespecificeerd = 4  

	}
	
	
	[Serializable]
    public enum AfspraakTriggerEnum  : byte
    {
           
		[Description("Onb")]
        Onb = 0,
           
		[Description("Aut")]
        Aut = 1,
           
		[Description("Hnd")]
        Hnd = 2,
           
		[Description("Brf")]
        Brf = 3,
           
		[Description("Bur")]
        Bur = 4,
           
		[Description("Cor")]
        Cor = 5,
           
		[Description("Por")]
        Por = 6  

	}

	
	
	[Serializable]
    public enum StatusEnum : byte
    {
           
		[Description("Gepland")]
        Gepland = 0,
           
		[Description("Uitgevoerd")]
        Uitgevoerd = 1,
           
		[Description("Niet uitgevoerd")]
        NietUitgevoerd = 2  

	}

	
	
	[Serializable]
    public enum Status2Enum : byte
    {
           
		[Description("Gepland")]
        Gepland = 0,
           
		[Description("InUitvoering")]
        Inuitvoering = 1,
           
		[Description("Voltooid")]
        Voltooid = 10  

	}

	
	/// <summary>
	/// DataValNummer = 102 
	/// TRV werk ouder102 
	/// </summary>
	[Serializable]
	 
	public enum TrvWerkOuder102
	{   	 

			 

			           
		[Description("geen idee")]
        GeenIdee = 0,
           
		[Description("Geen betaald werk")]
        GeenBetaaldWerk = 1,
           
		[Description("Uitkering")]
        Uitkering = 2,
           
		[Description("Parttime")]
        Parttime = 3,
           
		[Description("Fulltime")]
        Fulltime = 4,
           
		[Description("N.v.t")]
        NVT = 5,
           
		[Description("Onbekend")]
        Onbekend = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 103 
	/// TRV geboorteland ouder103 
	/// </summary>
	[Serializable]
	 
	public enum TrvGeboortelandOuder103
	{   	 

			 

			           
		[Description("Nederland")]
        Nederland = 1,
           
		[Description("Suriname")]
        Suriname = 2,
           
		[Description("Aruba / Antillen")]
        ArubaAntillen = 3,
           
		[Description("Turkije")]
        Turkije = 4,
           
		[Description("Marokko")]
        Marokko = 5,
           
		[Description("Overig Westers")]
        OverigWesters = 6,
           
		[Description("Overig Niet Westers")]
        OverigNietWesters = 7,
           
		[Description("Onbekend")]
        Onbekend = 8,
           
		[Description("N.v.t.")]
        NVT = 9,
	} 
	
		/// <summary>
	/// DataValNummer = 105 
	/// TRV opleiding ouder105 
	/// </summary>
	[Serializable]
	 
	public enum TrvOpleidingOuder105
	{   	 

			 

			           
		[Description("Geen")]
        Geen = 1,
           
		[Description("Basisonderwijs")]
        Basisonderwijs = 2,
           
		[Description("LBO")]
        Lbo = 3,
           
		[Description("MBO")]
        Mbo = 4,
           
		[Description("HBO & universiteit")]
        HboUniversiteit = 5,
           
		[Description("Onbekend")]
        Onbekend = 6,
           
		[Description("N.v.t.")]
        NVT = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 114 
	/// Onderzoek beenlengte/kniehoogte114 
	/// </summary>
	[Serializable]
	 
	public enum OnderzoekBeenlengteKniehoogte114
	{   			[Description("Links")]
					Links = 4,
					[Description("Rechts")]
					Rechts = 5,
					 

			 

			           
		[Description("Nee")]
        Nee = 1,
           
		[Description("Links < rechts")]
        LinksRechts = 2,
           
		[Description("Rechts < links")]
        RechtsLinks = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 117 
	/// Reacties op vaccinatie117 
	/// </summary>
	[Serializable]
	 
	public enum ReactiesOpVaccinatie117
	{   	 

			 

			           
		[Description("Lokale reactie")]
        LokaleReactie = 1,
           
		[Description("Algemene reactie")]
        AlgemeneReactie = 2,
           
		[Description("Hoge koorts")]
        HogeKoorts = 3,
           
		[Description("Collaps")]
        Collaps = 4,
           
		[Description("Convulsie")]
        Convulsie = 5,
           
		[Description("Verkleurde benen")]
        VerkleurdeBenen = 6,
           
		[Description("Encefalopathie")]
        Encefalopathie = 7,
           
		[Description("Persistent screaming")]
        PersistentScreaming = 8,
           
		[Description("Anders")]
        Anders = 9,
	} 
	
		/// <summary>
	/// DataValNummer = 126 
	/// Fysiek milieu126 
	/// </summary>
	[Serializable]
	 
	public enum FysiekMilieu126
	{   	 

			 

			           
		[Description("Geen bijzonderheden")]
        GeenBijzonderheden = 1,
           
		[Description("Veel verkeer in buurt")]
        VeelVerkeerInBuurt = 2,
           
		[Description("Open water in buurt")]
        OpenWaterInBuurt = 3,
           
		[Description("Onveilige buurt (criminaliteit, drugsoverlast)")]
        OnveiligeBuurtCriminaliteitDrugsoverlast = 4,
           
		[Description("Weinig/geen speelgelegenheid")]
        WeinigGeenSpeelgelegenheid = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 130 
	/// Goed/links niet/rechts niet/beiderzijds niet130 
	/// </summary>
	[Serializable]
	 
	public enum GoedLinksNietRechtsNietBeiderzijdsNiet130
	{   	 

			 

			           
		[Description("Links niet")]
        LinksNiet = 2,
           
		[Description("Rechts niet")]
        RechtsNiet = 3,
           
		[Description("Beiderzijds niet")]
        BeiderzijdsNiet = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 133 
	/// Redenen verpleegkundige zorg op maat133 
	/// </summary>
	[Serializable]
	 
	public enum RedenenVerpleegkundigeZorgOpMaat133
	{   	 

			 

			           
		[Description("Huilen")]
        Huilen = 1,
           
		[Description("Hechting")]
        Hechting = 2,
           
		[Description("Gedrag")]
        Gedrag = 3,
           
		[Description("Ontwikkeling")]
        Ontwikkeling = 4,
           
		[Description("Lichamelijke problemen")]
        LichamelijkeProblemen = 5,
           
		[Description("Fasenproblematiek")]
        Fasenproblematiek = 6,
           
		[Description("Opvoedingsvragen")]
        Opvoedingsvragen = 7,
           
		[Description("Opvoedingsspanning")]
        Opvoedingsspanning = 8,
           
		[Description("Opvoedingscrisis")]
        Opvoedingscrisis = 9,
           
		[Description("Opvoedingsnood")]
        Opvoedingsnood = 10,
           
		[Description("Verslaving ouder")]
        VerslavingOuder = 11,
           
		[Description("Psychatrische problemen ouder")]
        PsychatrischeProblemenOuder = 12,
           
		[Description("PPD")]
        Ppd = 13,
           
		[Description("Sociaal isolement")]
        SociaalIsolement = 14,
           
		[Description("Onveilige omgeving")]
        OnveiligeOmgeving = 15,
           
		[Description("Overgewicht")]
        Overgewicht = 16,
           
		[Description("Spraak/taal ontwikkeling")]
        SpraakTaalOntwikkeling = 17,
           
		[Description("Voorkomen passief roken")]
        VoorkomenPassiefRoken = 18,
           
		[Description("Borstvoeding")]
        Borstvoeding = 19,
	} 
	
		/// <summary>
	/// DataValNummer = 162 
	/// Roken: aantal162 
	/// </summary>
	[Serializable]
	 
	public enum RokenAantal162
	{   	 

			 

			           
		[Description("Minder dan 5")]
        MinderDan5 = 1,
           
		[Description("5 tot 9")]
        Tot95 = 2,
           
		[Description("10 tot 14")]
        Tot1410 = 3,
           
		[Description("15 of meer")]
        OfMeer15 = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 168 
	/// Bijzonderheden bevalling168 
	/// </summary>
	[Serializable]
	 
	public enum BijzonderhedenBevalling168
	{   	 

			 

			           
		[Description("Afwijkende kleur vruchtwater")]
        AfwijkendeKleurVruchtwater = 1,
           
		[Description("Afwijkingen aan placenta")]
        AfwijkingenAanPlacenta = 2,
           
		[Description("Afwijking aan navelvaten")]
        AfwijkingAanNavelvaten = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 187 
	/// Verlegen|Druk |Aanha...187 
	/// </summary>
	[Serializable]
	 
	public enum VerlegenDrukAanha187
	{   	 

			 

			           
		[Description("Verlegen")]
        Verlegen = 1,
           
		[Description("Druk")]
        Druk = 2,
           
		[Description("Aanhankelijk")]
        Aanhankelijk = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 189 
	/// Vitamine K en D189 
	/// </summary>
	[Serializable]
	 
	public enum VitamineKEnD189
	{   	 

			 

			           
		[Description("Geadviseerd")]
        Geadviseerd = 0,
           
		[Description("Ja, wordt gegeven")]
        JaWordtGegeven = 1,
           
		[Description("Nee, wordt niet gegeven")]
        NeeWordtNietGegeven = 2,
           
		[Description("Onbekend")]
        Onbekend = 3,
           
		[Description("N.v.t.")]
        NVT = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 192 
	/// Bijzonderheden pasgeborene192 
	/// </summary>
	[Serializable]
	 
	public enum BijzonderhedenPasgeborene192
	{   	 

			 

			           
		[Description("Afwijkende apgarscore")]
        AfwijkendeApgarscore = 1,
           
		[Description("Aangeboren afwijking(en)")]
        AangeborenAfwijkingEn = 2,
           
		[Description("Bijzonderheden eerste levensuur")]
        BijzonderhedenEersteLevensuur = 3,
           
		[Description("Bijzonderheden eerste levensdagen")]
        BijzonderhedenEersteLevensdagen = 4,
           
		[Description("Bijzonderheden temperatuursverloop")]
        BijzonderhedenTemperatuursverloop = 5,
           
		[Description("Bijzonderheden ademhaling")]
        BijzonderhedenAdemhaling = 6,
           
		[Description("Bijzonderheden uitscheiding")]
        BijzonderhedenUitscheiding = 7,
           
		[Description("Bijzonderheden drinken")]
        BijzonderhedenDrinken = 8,
           
		[Description("Overige bijzonderheden")]
        OverigeBijzonderheden = 9,
	} 
	
		/// <summary>
	/// DataValNummer = 197 
	/// Inventarisatie P onderwerpen197 
	/// </summary>
	[Serializable]
	 
	public enum InventarisatiePOnderwerpen197
	{   	 

			 

			           
		[Description("Voeding / eten")]
        VoedingEten = 1,
           
		[Description("Huid")]
        Huid = 2,
           
		[Description("Mondverzorging")]
        Mondverzorging = 3,
           
		[Description("Slapen / waken")]
        SlapenWaken = 4,
           
		[Description("Omgang / spelen")]
        OmgangSpelen = 5,
           
		[Description("Temperament / gedrag")]
        TemperamentGedrag = 6,
           
		[Description("Gehechtheid")]
        Gehechtheid = 7,
           
		[Description("Ontwikkeling")]
        Ontwikkeling = 8,
           
		[Description("Zindelijkheid")]
        Zindelijkheid = 9,
           
		[Description("Lichamelijke verschijning")]
        LichamelijkeVerschijning = 10,
           
		[Description("Overig")]
        Overig = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 201 
	/// Bijzonderheden wervelkolom201 
	/// </summary>
	[Serializable]
	 
	public enum BijzonderhedenWervelkolom201
	{   	 

			 

			           
		[Description("Onvoldoende beweeglijkheid")]
        OnvoldoendeBeweeglijkheid = 1,
           
		[Description("Scoliose")]
        Scoliose = 2,
           
		[Description("Verticale lordose")]
        VerticaleLordose = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 203 
	/// Reden vervroegd consult 4-19203 
	/// </summary>
	[Serializable]
	 
	public enum RedenVervroegdConsult419203
	{   	 

			 

			           
		[Description("Visus")]
        Visus = 1,
           
		[Description("Gehoor")]
        Gehoor = 2,
           
		[Description("Lengte/gewicht")]
        LengteGewicht = 3,
           
		[Description("Spraak/taal")]
        SpraakTaal = 4,
           
		[Description("Motorische ontwikkeling")]
        MotorischeOntwikkeling = 5,
           
		[Description("Sociaal, emotionele ontwikkeling")]
        SociaalEmotioneleOntwikkeling = 6,
           
		[Description("Risicofactoren")]
        Risicofactoren = 7,
           
		[Description("Gedrag")]
        Gedrag = 8,
           
		[Description("Opvoeding")]
        Opvoeding = 9,
           
		[Description("Overig")]
        Overig = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 204 
	/// Gegeven folders indicatiepakket204 
	/// </summary>
	[Serializable]
	 
	public enum GegevenFoldersIndicatiepakket204
	{   	 

			 

			           
		[Description("Bescherming Hep B bij mensen met Syndroom van Down")]
        BeschermingHepBBijMensenMetSyndroomVanDown = 1,
           
		[Description("Stotteren, sta stil bij praten kind")]
        StotterenStaStilBijPratenKind = 2,
           
		[Description("Wie niet horen wil")]
        WieNietHorenWil = 3,
           
		[Description("BCG, de vaccinatie tegen tuberculose")]
        BcgDeVaccinatieTegenTuberculose = 4,
           
		[Description("Oudervereniging aangeboren heup afwijkingen")]
        OuderverenigingAangeborenHeupAfwijkingen = 5,
           
		[Description("Teken & ziekte v Lyme")]
        TekenZiekteVLyme = 6,
           
		[Description("Preventie voedselallergie")]
        PreventieVoedselallergie = 7,
           
		[Description("Cara C'air")]
        CaraCAir = 8,
           
		[Description("Voorkeurshouding")]
        Voorkeurshouding = 9,
           
		[Description("Zindelijkheid")]
        Zindelijkheid = 10,
           
		[Description("Slapen")]
        Slapen = 11,
           
		[Description("Eten")]
        Eten = 12,
           
		[Description("Kinderen willen alles weten")]
        KinderenWillenAllesWeten = 13,
           
		[Description("Speelgoed, speel beter")]
        SpeelgoedSpeelBeter = 14,
           
		[Description("Aandachtspunten, kopen schoenen")]
        AandachtspuntenKopenSchoenen = 15,
           
		[Description("Met kinderen op vakantie")]
        MetKinderenOpVakantie = 16,
           
		[Description("Neus om door te ademen")]
        NeusOmDoorTeAdemen = 17,
           
		[Description("Praten leer je niet vanzelf")]
        PratenLeerJeNietVanzelf = 18,
           
		[Description("Afbouwen bv")]
        AfbouwenBv = 19,
           
		[Description("Info voedselallergie")]
        InfoVoedselallergie = 20,
           
		[Description("Hypoallergene voeding M bv geeft")]
        HypoallergeneVoedingMBvGeeft = 21,
           
		[Description("Met plezier kijken naar je kind VHT kort")]
        MetPlezierKijkenNaarJeKindVhtKort = 22,
           
		[Description("Drukke kinderen")]
        DrukkeKinderen = 23,
	} 
	
		/// <summary>
	/// DataValNummer = 209 
	/// Slapen/waken209 
	/// </summary>
	[Serializable]
	 
	public enum SlapenWaken209
	{   	 

			 

			           
		[Description("Inslaapproblemen")]
        Inslaapproblemen = 1,
           
		[Description("Doorslaap problemen")]
        DoorslaapProblemen = 2,
           
		[Description("(Angst)dromen / nachtmerries")]
        AngstDromenNachtmerries = 3,
           
		[Description("Te vroeg wakker")]
        TeVroegWakker = 4,
           
		[Description("Weinig slapen overdag")]
        WeinigSlapenOverdag = 5,
           
		[Description("Hoofd bonken / schudden")]
        HoofdBonkenSchudden = 6,
           
		[Description("In ouderlijk bed slapen")]
        InOuderlijkBedSlapen = 7,
           
		[Description("Onrustig slapen")]
        OnrustigSlapen = 8,
           
		[Description("Chronisch slaaptekort")]
        ChronischSlaaptekort = 9,
           
		[Description("Anders")]
        Anders = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 210 
	/// Eetgedrag210 
	/// </summary>
	[Serializable]
	 
	public enum Eetgedrag210
	{   	 

			 

			           
		[Description("Selectief eten")]
        SelectiefEten = 1,
           
		[Description("Eetstrijd")]
        Eetstrijd = 2,
           
		[Description("Op snoep / frisdrank gericht")]
        OpSnoepFrisdrankGericht = 3,
           
		[Description("Ontbijt niet")]
        OntbijtNiet = 4,
           
		[Description("Eetpatroon onregelmatig")]
        EetpatroonOnregelmatig = 5,
           
		[Description("Eet niet volwaardig")]
        EetNietVolwaardig = 6,
           
		[Description("Voedsel weigering")]
        VoedselWeigering = 7,
           
		[Description("Spugen")]
        Spugen = 8,
           
		[Description("Pica")]
        Pica = 9,
           
		[Description("Rumineren")]
        Rumineren = 10,
           
		[Description("Vermoeden eetstoornis")]
        VermoedenEetstoornis = 11,
           
		[Description("Anders")]
        Anders = 12,
	} 
	
		/// <summary>
	/// DataValNummer = 211 
	/// Ontlasting/plassen/zindelijkheid211 
	/// </summary>
	[Serializable]
	 
	public enum OntlastingPlassenZindelijkheid211
	{   	 

			 

			           
		[Description("Overdag niet zindelijk urine")]
        OverdagNietZindelijkUrine = 1,
           
		[Description("'s Nachts niet zindelijk urine")]
        SNachtsNietZindelijkUrine = 2,
           
		[Description("Opnieuw onzindelijk urine")]
        OpnieuwOnzindelijkUrine = 3,
           
		[Description("Spel-lach enuresis")]
        SpelLachEnuresis = 4,
           
		[Description("Incontinentie")]
        Incontinentie = 5,
           
		[Description("Niet zindelijk ontlasting")]
        NietZindelijkOntlasting = 6,
           
		[Description("Opnieuw onzindelijk ontlasting")]
        OpnieuwOnzindelijkOntlasting = 7,
           
		[Description("Ophouden ontlasting")]
        OphoudenOntlasting = 8,
           
		[Description("Obstipatie")]
        Obstipatie = 9,
           
		[Description("Zindelijkheidsstrijd")]
        Zindelijkheidsstrijd = 10,
           
		[Description("Anders")]
        Anders = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 213 
	/// Temperament/gedrag213 
	/// </summary>
	[Serializable]
	 
	public enum TemperamentGedrag213
	{   	 

			 

			           
		[Description("Koppig, opstandig")]
        KoppigOpstandig = 1,
           
		[Description("Boos, driftig, prikkelbaar")]
        BoosDriftigPrikkelbaar = 2,
           
		[Description("Snel jaloers, achterdochtig")]
        SnelJaloersAchterdochtig = 3,
           
		[Description("Eist aandacht op")]
        EistAandachtOp = 4,
           
		[Description("Hangt de clown uit")]
        HangtDeClownUit = 5,
           
		[Description("Ruzie zoeken/maken")]
        RuzieZoekenMaken = 6,
           
		[Description("Overheersend (baas spelen)")]
        OverheersendBaasSpelen = 7,
           
		[Description("Dingen kapot maken")]
        DingenKapotMaken = 8,
           
		[Description("Lichamelijk geweld tegen anderen")]
        LichamelijkGeweldTegenAnderen = 9,
           
		[Description("Ongehoorzaam")]
        Ongehoorzaam = 10,
           
		[Description("Geen besef van regels en grenzen")]
        GeenBesefVanRegelsEnGrenzen = 11,
           
		[Description("Reageert niet op straffen en belonen")]
        ReageertNietOpStraffenEnBelonen = 12,
           
		[Description("Agressief gedrag")]
        AgressiefGedrag = 13,
           
		[Description("Liegen, stelen")]
        LiegenStelen = 14,
           
		[Description("Uitdagend")]
        Uitdagend = 15,
           
		[Description("Pesten")]
        Pesten = 16,
           
		[Description("Chanteren")]
        Chanteren = 17,
           
		[Description("Vandalisme")]
        Vandalisme = 18,
           
		[Description("Anders")]
        Anders = 19,
	} 
	
		/// <summary>
	/// DataValNummer = 214 
	/// Externaliserend gedrag214 
	/// </summary>
	[Serializable]
	 
	public enum ExternaliserendGedrag214
	{   	 

			 

			           
		[Description("Druk gedrag")]
        DrukGedrag = 1,
           
		[Description("Oppositioneel gedrag")]
        OppositioneelGedrag = 2,
           
		[Description("Agressief gedrag")]
        AgressiefGedrag = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 215 
	/// Internaliserend gedrag215 
	/// </summary>
	[Serializable]
	 
	public enum InternaliserendGedrag215
	{   	 

			 

			           
		[Description("Angst en depressie")]
        AngstEnDepressie = 1,
           
		[Description("Problemen in het sociaal functioneren")]
        ProblemenInHetSociaalFunctioneren = 2,
           
		[Description("Middelen slikken")]
        MiddelenSlikken = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 216 
	/// Stemmingsaspecten216 
	/// </summary>
	[Serializable]
	 
	public enum Stemmingsaspecten216
	{   	 

			 

			           
		[Description("Angst")]
        Angst = 1,
           
		[Description("Boosheid")]
        Boosheid = 2,
           
		[Description("Verdriet")]
        Verdriet = 3,
           
		[Description("Extreme dwarsheid")]
        ExtremeDwarsheid = 4,
           
		[Description("Twijfel")]
        Twijfel = 5,
           
		[Description("Onzekerheid")]
        Onzekerheid = 6,
           
		[Description("Schaamte")]
        Schaamte = 7,
           
		[Description("Onzelfstandigheid")]
        Onzelfstandigheid = 8,
           
		[Description("Driftig")]
        Driftig = 9,
           
		[Description("Neerslachtigheid,verdrietig")]
        NeerslachtigheidVerdrietig = 10,
           
		[Description("Snel overstuur")]
        SnelOverstuur = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 217 
	/// Risicoaspecten sociaal functioneren217 
	/// </summary>
	[Serializable]
	 
	public enum RisicoaspectenSociaalFunctioneren217
	{   	 

			 

			           
		[Description("Hechtingsproblematiek")]
        Hechtingsproblematiek = 1,
           
		[Description("Conflicten met ouders")]
        ConflictenMetOuders = 2,
           
		[Description("Conflicten met broers/zussen")]
        ConflictenMetBroersZussen = 3,
           
		[Description("Verlies belangrijke ander")]
        VerliesBelangrijkeAnder = 4,
           
		[Description("Geen/onvoldoende vrienden")]
        GeenOnvoldoendeVrienden = 5,
           
		[Description("Speelt niet/weinig met leeftijdsgenoten")]
        SpeeltNietWeinigMetLeeftijdsgenoten = 6,
           
		[Description("Legt moeilijk contact")]
        LegtMoeilijkContact = 7,
           
		[Description("Eenkennig")]
        Eenkennig = 8,
           
		[Description("Wordt gepest")]
        WordtGepest = 9,
           
		[Description("Pest zelf")]
        PestZelf = 10,
           
		[Description("Eenzaam")]
        Eenzaam = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 218 
	/// Inventarisatie functioneren218 
	/// </summary>
	[Serializable]
	 
	public enum InventarisatieFunctioneren218
	{   	 

			 

			           
		[Description("Slapen/waken")]
        SlapenWaken = 1,
           
		[Description("Eetgedrag")]
        Eetgedrag = 2,
           
		[Description("Ontlasten/plassen/zindelijkheid")]
        OntlastenPlassenZindelijkheid = 3,
           
		[Description("Spel")]
        Spel = 4,
           
		[Description("Omgang broer/zus/leeftijdsgenoten")]
        OmgangBroerZusLeeftijdsgenoten = 5,
           
		[Description("Concentratie/luisteren")]
        ConcentratieLuisteren = 6,
           
		[Description("Stemming/angsten")]
        StemmingAngsten = 7,
           
		[Description("Temperament/gedrag")]
        TemperamentGedrag = 8,
           
		[Description("Kijken, luisteren, ruiken, voelen")]
        KijkenLuisterenRuikenVoelen = 9,
           
		[Description("Lichaamsbeweging (sport,")]
        LichaamsbewegingSport = 10,
           
		[Description("Externaliserend gedrag")]
        ExternaliserendGedrag = 11,
           
		[Description("Internaliserend gedrag")]
        InternaliserendGedrag = 12,
           
		[Description("Stemmingsaspecten")]
        Stemmingsaspecten = 13,
           
		[Description("Risicoaspecten sociaal functioneren")]
        RisicoaspectenSociaalFunctioneren = 14,
	} 
	
		/// <summary>
	/// DataValNummer = 220 
	/// Tweetaligheid220 
	/// </summary>
	[Serializable]
	 
	public enum Tweetaligheid220
	{   	 

			 

			           
		[Description("Geen")]
        Geen = 1,
           
		[Description("Sumultane tweetaligheid")]
        SumultaneTweetaligheid = 2,
           
		[Description("Successieve tweetaligheid")]
        SuccessieveTweetaligheid = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 221 
	/// Stemstoornis221 
	/// </summary>
	[Serializable]
	 
	public enum Stemstoornis221
	{   	 

			 

			           
		[Description("Hyperkinetisch stemgebruik")]
        HyperkinetischStemgebruik = 1,
           
		[Description("Hypokinetisch stemgebruik")]
        HypokinetischStemgebruik = 2,
           
		[Description("Stoornis in stemkwaliteit")]
        StoornisInStemkwaliteit = 3,
           
		[Description("Foutieve spreekademhaling")]
        FoutieveSpreekademhaling = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 224 
	/// Nasaliteit224 
	/// </summary>
	[Serializable]
	 
	public enum Nasaliteit224
	{   	 

			 

			           
		[Description("Hypernasaliteit")]
        Hypernasaliteit = 1,
           
		[Description("Hyponasaliteit")]
        Hyponasaliteit = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 225 
	/// Stoornis vloeiendheid225 
	/// </summary>
	[Serializable]
	 
	public enum StoornisVloeiendheid225
	{   	 

			 

			           
		[Description("Stotteren")]
        Stotteren = 1,
           
		[Description("Broddelen")]
        Broddelen = 2,
           
		[Description("Inadequaat spreektempo")]
        InadequaatSpreektempo = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 226 
	/// Diepte- en kleurenzien226 
	/// </summary>
	[Serializable]
	 
	public enum DiepteEnKleurenzien226
	{   	 

			 

			           
		[Description("OD onvoldoende")]
        OdOnvoldoende = 1,
           
		[Description("OS onvoldoende")]
        OsOnvoldoende = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 227 
	/// Neuromotorische reflexen227 
	/// </summary>
	[Serializable]
	 
	public enum NeuromotorischeReflexen227
	{   	 

			 

			           
		[Description("Normaal voor de leeftijd")]
        NormaalVoorDeLeeftijd = 0,
           
		[Description("Afwijkende reflexen")]
        AfwijkendeReflexen = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 228 
	/// Cognitieve ontwikkeling228 
	/// </summary>
	[Serializable]
	 
	public enum CognitieveOntwikkeling228
	{   	 

			 

			           
		[Description("Achter op leeftijd")]
        AchterOpLeeftijd = 1,
           
		[Description("Voor op leeftijd")]
        VoorOpLeeftijd = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 229 
	/// Gegeven folder sociale kaart pakket229 
	/// </summary>
	[Serializable]
	 
	public enum GegevenFolderSocialeKaartPakket229
	{   	 

			 

			           
		[Description("FIOM")]
        Fiom = 1,
           
		[Description("Ver Ouders v Meerlingen")]
        VerOudersVMeerlingen = 2,
           
		[Description("Centrum v stottertherapie")]
        CentrumVStottertherapie = 3,
           
		[Description("Voedselallergie en intoleratie")]
        VoedselallergieEnIntoleratie = 4,
           
		[Description("Mishandeling een gewoonte?")]
        MishandelingEenGewoonte = 5,
           
		[Description("Gouden tips voor ouders met chr aandoening")]
        GoudenTipsVoorOudersMetChrAandoening = 6,
           
		[Description("Balans, alg info folder")]
        BalansAlgInfoFolder = 7,
           
		[Description("VOKCVS, veren ouders met kk chron voedselweigering")]
        VokcvsVerenOudersMetKkChronVoedselweigering = 8,
           
		[Description("Plato, land centr hoogbegaafd")]
        PlatoLandCentrHoogbegaafd = 9,
           
		[Description("Ouders ve overleden kind")]
        OudersVeOverledenKind = 10,
           
		[Description("BJZ: voor ouders en opvoeders")]
        BjzVoorOudersEnOpvoeders = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 239 
	/// Lichamelijke ontwikkeling (ortho)239 
	/// </summary>
	[Serializable]
	 
	public enum LichamelijkeOntwikkelingOrtho239
	{   	 

			 

			           
		[Description("Lichamelijke klachten/gezondheid")]
        LichamelijkeKlachtenGezondheid = 1,
           
		[Description("Ziekenhuisopname")]
        Ziekenhuisopname = 2,
           
		[Description("Voeding/eetprobleem")]
        VoedingEetprobleem = 3,
           
		[Description("Voedselallergie")]
        Voedselallergie = 4,
           
		[Description("Snoepen")]
        Snoepen = 5,
           
		[Description("Zindelijkheid")]
        Zindelijkheid = 6,
           
		[Description("Motorische ontwikkeling")]
        MotorischeOntwikkeling = 7,
           
		[Description("Seksuele ontwikkeling")]
        SeksueleOntwikkeling = 8,
           
		[Description("Duimzuigen/speen")]
        DuimzuigenSpeen = 9,
           
		[Description("Tics/hoofdbonken")]
        TicsHoofdbonken = 10,
           
		[Description("Verstoring dag/nachtritme")]
        VerstoringDagNachtritme = 11,
           
		[Description("Lichamelijke groei")]
        LichamelijkeGroei = 12,
	} 
	
		/// <summary>
	/// DataValNummer = 240 
	/// Verstandelijke ontwikkeling (ortho)240 
	/// </summary>
	[Serializable]
	 
	public enum VerstandelijkeOntwikkelingOrtho240
	{   	 

			 

			           
		[Description("Taal-spraakontwikkeling")]
        TaalSpraakontwikkeling = 1,
           
		[Description("Intelligentie")]
        Intelligentie = 2,
           
		[Description("Concentratieproblemen")]
        Concentratieproblemen = 3,
           
		[Description("Hoogbegaafd")]
        Hoogbegaafd = 4,
           
		[Description("Fantasie")]
        Fantasie = 5,
           
		[Description("Cognitieve achterstand")]
        CognitieveAchterstand = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 241 
	/// Sociale ontwikkeling (ortho)241 
	/// </summary>
	[Serializable]
	 
	public enum SocialeOntwikkelingOrtho241
	{   	 

			 

			           
		[Description("Contact andere kinderen")]
        ContactAndereKinderen = 1,
           
		[Description("Vriendjes/vriendinnetjes")]
        VriendjesVriendinnetjes = 2,
           
		[Description("Gepest worden")]
        GepestWorden = 3,
           
		[Description("Zelf pesten")]
        ZelfPesten = 4,
           
		[Description("Discirminatie/racisme")]
        DiscirminatieRacisme = 5,
           
		[Description("Conflict andere kinderen")]
        ConflictAndereKinderen = 6,
           
		[Description("Enig kind zijn")]
        EnigKindZijn = 7,
           
		[Description("Relatie broertjes/zusjes")]
        RelatieBroertjesZusjes = 8,
           
		[Description("Relatie familie/grootouders")]
        RelatieFamilieGrootouders = 9,
           
		[Description("Rolgedrag jongen/meisje")]
        RolgedragJongenMeisje = 10,
           
		[Description("Woonomgeving/veiligheid")]
        WoonomgevingVeiligheid = 11,
           
		[Description("Verhuizen")]
        Verhuizen = 12,
           
		[Description("Logeren")]
        Logeren = 13,
	} 
	
		/// <summary>
	/// DataValNummer = 242 
	/// Emotionele ontwikkeling (ortho)242 
	/// </summary>
	[Serializable]
	 
	public enum EmotioneleOntwikkelingOrtho242
	{   	 

			 

			           
		[Description("Scheidingsangst/eenkennig")]
        ScheidingsangstEenkennig = 0,
           
		[Description("Moelijk temperament")]
        MoelijkTemperament = 1,
           
		[Description("Bang/angstig")]
        BangAngstig = 2,
           
		[Description("Slaapproblemen")]
        Slaapproblemen = 4,
           
		[Description("Nachtmerries/dromen")]
        NachtmerriesDromen = 5,
           
		[Description("Faalangst")]
        Faalangst = 6,
           
		[Description("Rouwverwerking")]
        Rouwverwerking = 7,
           
		[Description("Verwerken echtscheiding")]
        VerwerkenEchtscheiding = 8,
           
		[Description("Jaloezie/rivaliteit")]
        JaloezieRivaliteit = 9,
           
		[Description("Onzeker/verlegen/weinig zelfvertrouwen")]
        OnzekerVerlegenWeinigZelfvertrouwen = 10,
           
		[Description("Niet weerbaar/onzelfstandig")]
        NietWeerbaarOnzelfstandig = 11,
           
		[Description("Emotionele achterstand")]
        EmotioneleAchterstand = 12,
	} 
	
		/// <summary>
	/// DataValNummer = 243 
	/// Opvallend gedrag (orhto)243 
	/// </summary>
	[Serializable]
	 
	public enum OpvallendGedragOrhto243
	{   	 

			 

			           
		[Description("Huilen")]
        Huilen = 1,
           
		[Description("Agressief gedrag")]
        AgressiefGedrag = 2,
           
		[Description("Druk/onrustig/hyperactief")]
        DrukOnrustigHyperactief = 3,
           
		[Description("Claimgedrag,aandacht vragen")]
        ClaimgedragAandachtVragen = 4,
           
		[Description("Niet luisteren/ongehoorzaam")]
        NietLuisterenOngehoorzaam = 5,
           
		[Description("Koppig/dwars/opstandig")]
        KoppigDwarsOpstandig = 6,
           
		[Description("Driftbuien")]
        Driftbuien = 7,
           
		[Description("Passief/teruggetrokken gedrag")]
        PassiefTeruggetrokkenGedrag = 8,
           
		[Description("Liegen/stiekem gedrag")]
        LiegenStiekemGedrag = 9,
           
		[Description("Uitdagen/brutaal/grote mond")]
        UitdagenBrutaalGroteMond = 10,
           
		[Description("Weglopen")]
        Weglopen = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 244 
	/// Opvoeding/ouderschap (ortho)244 
	/// </summary>
	[Serializable]
	 
	public enum OpvoedingOuderschapOrtho244
	{   	 

			 

			           
		[Description("Straf geven/grenzen stellen")]
        StrafGevenGrenzenStellen = 1,
           
		[Description("Verwennen")]
        Verwennen = 2,
           
		[Description("Verschil aanpak ouders")]
        VerschilAanpakOuders = 3,
           
		[Description("Relatie ouder/kind")]
        RelatieOuderKind = 4,
           
		[Description("Opvoedingsaanpak algemeen")]
        OpvoedingsaanpakAlgemeen = 5,
           
		[Description("Bezorgdheid ontwikkeling kind")]
        BezorgdheidOntwikkelingKind = 6,
           
		[Description("Verwachting/beleving ouderschap")]
        VerwachtingBelevingOuderschap = 7,
           
		[Description("Bevalling/kraamtijd")]
        BevallingKraamtijd = 8,
           
		[Description("Moeilijke start ouderschap")]
        MoeilijkeStartOuderschap = 9,
           
		[Description("Premature geboorte")]
        PrematureGeboorte = 10,
           
		[Description("Adoptieproblematiek")]
        Adoptieproblematiek = 11,
           
		[Description("Omgangsregeling")]
        Omgangsregeling = 12,
           
		[Description("Botsing culturen")]
        BotsingCulturen = 13,
           
		[Description("Invloed/relatie grootouders")]
        InvloedRelatieGrootouders = 14,
           
		[Description("Invloed omgeving")]
        InvloedOmgeving = 15,
           
		[Description("Seksueel misbruik")]
        SeksueelMisbruik = 16,
           
		[Description("Lichamelijke verwaarlozing")]
        LichamelijkeVerwaarlozing = 17,
           
		[Description("Lichamelijke mishandeling")]
        LichamelijkeMishandeling = 18,
           
		[Description("Psychische verwaarlozing")]
        PsychischeVerwaarlozing = 19,
           
		[Description("Psychische mishandeling")]
        PsychischeMishandeling = 20,
           
		[Description("Anders")]
        Anders = 21,
	} 
	
		/// <summary>
	/// DataValNummer = 245 
	/// Kenmerken opvoedingssituatie (ortho)245 
	/// </summary>
	[Serializable]
	 
	public enum KenmerkenOpvoedingssituatieOrtho245
	{   	 

			 

			           
		[Description("Weinig tijd/aandacht voor kinderen")]
        WeinigTijdAandachtVoorKinderen = 1,
           
		[Description("Weinig steun/opvoedingsisolement")]
        WeinigSteunOpvoedingsisolement = 2,
           
		[Description("Negatieve jeugdervaring ouder")]
        NegatieveJeugdervaringOuder = 3,
           
		[Description("Psychische problemen ouder/opvoeder/gezinslid")]
        PsychischeProblemenOuderOpvoederGezinslid = 4,
           
		[Description("Financiele problemen 5")]
        FinancieleProblemen5 = 5,
           
		[Description("Werkloosheid")]
        Werkloosheid = 6,
           
		[Description("Onveilige woonomgeving.slechte behuizing")]
        OnveiligeWoonomgevingSlechteBehuizing = 7,
           
		[Description("Verslavingsproblematiek ouders")]
        VerslavingsproblematiekOuders = 8,
           
		[Description("Relatieproblematiek/gezinsconflicten")]
        RelatieproblematiekGezinsconflicten = 9,
           
		[Description("Geen veerkracht/vermoeidheid ouder/opvoeder")]
        GeenVeerkrachtVermoeidheidOuderOpvoeder = 10,
           
		[Description("Ziekte/handicap ouder/gezinslid")]
        ZiekteHandicapOuderGezinslid = 11,
           
		[Description("Strafrechtelijke achtergrond")]
        StrafrechtelijkeAchtergrond = 12,
           
		[Description("Ingrijpende gebeurtenis")]
        IngrijpendeGebeurtenis = 13,
           
		[Description("Geweld in het gezin")]
        GeweldInHetGezin = 14,
           
		[Description("Anders")]
        Anders = 15,
	} 
	
		/// <summary>
	/// DataValNummer = 246 
	/// Inschatting zwaarte246 
	/// </summary>
	[Serializable]
	 
	public enum InschattingZwaarte246
	{   	 

			 

			           
		[Description("Alledaagse opvoedingsvragen")]
        AlledaagseOpvoedingsvragen = 1,
           
		[Description("Opvoedingsspanning")]
        Opvoedingsspanning = 2,
           
		[Description("Opvoedingscrisis")]
        Opvoedingscrisis = 3,
           
		[Description("Opvoedingsnood")]
        Opvoedingsnood = 4,
           
		[Description("Anders")]
        Anders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 247 
	/// Aanvrager consult orthopedagoog247 
	/// </summary>
	[Serializable]
	 
	public enum AanvragerConsultOrthopedagoog247
	{   	 

			 

			           
		[Description("Wijkverpleegkundige")]
        Wijkverpleegkundige = 1,
           
		[Description("CB-arts")]
        CbArts = 2,
           
		[Description("Peuterspeelzaal")]
        Peuterspeelzaal = 3,
           
		[Description("Kinderdagverblijf")]
        Kinderdagverblijf = 4,
           
		[Description("Huisarts")]
        Huisarts = 5,
           
		[Description("Logopediste")]
        Logopediste = 6,
           
		[Description("Fysiotherapeut")]
        Fysiotherapeut = 7,
           
		[Description("Anders")]
        Anders = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 248 
	/// Observatie Locaties248 
	/// </summary>
	[Serializable]
	 
	public enum ObservatieLocaties248
	{   	 

			 

			           
		[Description("Peuterspeelzaal")]
        Peuterspeelzaal = 1,
           
		[Description("Kinderdagverblijf")]
        Kinderdagverblijf = 2,
           
		[Description("Consultatiebureau")]
        Consultatiebureau = 3,
           
		[Description("Thuis")]
        Thuis = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 251 
	/// Folder(s) op indicatie251 
	/// </summary>
	[Serializable]
	 
	public enum FolderSOpIndicatie251
	{   	 

			 

			           
		[Description("BCG, de vaccinatie tegen tuberculose")]
        BcgDeVaccinatieTegenTuberculose = 1,
           
		[Description("Met plezier kijken naar je kind")]
        MetPlezierKijkenNaarJeKind = 2,
           
		[Description("Je hebt een neus om door te ademen")]
        JeHebtEenNeusOmDoorTeAdemen = 3,
           
		[Description("Afbouwen borstvoeding")]
        AfbouwenBorstvoeding = 4,
           
		[Description("Slapen, praktische adviezen")]
        SlapenPraktischeAdviezen = 5,
           
		[Description("Voedselallergie")]
        Voedselallergie = 6,
           
		[Description("Zindelijkheid, praktische adviezen")]
        ZindelijkheidPraktischeAdviezen = 7,
           
		[Description("Rust en regelmaat")]
        RustEnRegelmaat = 8,
           
		[Description("Rust, regelmaat en inbakeren")]
        RustRegelmaatEnInbakeren = 9,
	} 
	
		/// <summary>
	/// DataValNummer = 253 
	/// Screeningsinstrument STO253 
	/// </summary>
	[Serializable]
	 
	public enum ScreeningsinstrumentSto253
	{   	 

			 

			           
		[Description("Logopedische screeningsinstrument")]
        LogopedischeScreeningsinstrument = 1,
           
		[Description("Groninger Minimum Spreeknormen (GSM)")]
        GroningerMinimumSpreeknormenGsm = 2,
           
		[Description("Lexiconlijsten")]
        Lexiconlijsten = 3,
           
		[Description("SNEL (Spraak- en taalNormen EersteLijns gez.zorg)")]
        SnelSpraakEnTaalnormenEerstelijnsGezZorg = 4,
           
		[Description("VTO taalinstrument")]
        VtoTaalinstrument = 5,
           
		[Description("Van Wiechenonderzoek")]
        VanWiechenonderzoek = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 254 
	/// Aard bijzonderheden STO254 
	/// </summary>
	[Serializable]
	 
	public enum AardBijzonderhedenSto254
	{   	 

			 

			           
		[Description("Meer dan twee talen sprekend (derde taal etc.)")]
        MeerDanTweeTalenSprekendDerdeTaalEtc = 1,
           
		[Description("Problemen t.g.v. meertaligheid")]
        ProblemenTGVMeertaligheid = 2,
           
		[Description("Gehoor, perceptieve&conductieve verliezen")]
        GehoorPerceptieveConductieveVerliezen = 3,
           
		[Description("Mondgedrag")]
        Mondgedrag = 4,
           
		[Description("Stemproductie")]
        Stemproductie = 5,
           
		[Description("Stemkwaliteit")]
        Stemkwaliteit = 6,
           
		[Description("Foutieve spreekademhaling")]
        FoutieveSpreekademhaling = 7,
           
		[Description("Articulatie stoornissen")]
        ArticulatieStoornissen = 8,
           
		[Description("Nasaliteit")]
        Nasaliteit = 9,
           
		[Description("Onvloeiendheid")]
        Onvloeiendheid = 10,
           
		[Description("Taalbegrip")]
        Taalbegrip = 11,
           
		[Description("Taalproductie")]
        Taalproductie = 12,
           
		[Description("Anders")]
        Anders = 13,
	} 
	
		/// <summary>
	/// DataValNummer = 258 
	/// Omgaan met broer/zus/leeftijdgenoten258 
	/// </summary>
	[Serializable]
	 
	public enum OmgaanMetBroerZusLeeftijdgenoten258
	{   	 

			 

			           
		[Description("Problemen in contact met leeftijdgenoten")]
        ProblemenInContactMetLeeftijdgenoten = 1,
           
		[Description("Speelt niet met leeftijdgenoten")]
        SpeeltNietMetLeeftijdgenoten = 2,
           
		[Description("Kan niet overweg met leeftijdgenoten")]
        KanNietOverwegMetLeeftijdgenoten = 3,
           
		[Description("Heeft geen vrienden")]
        HeeftGeenVrienden = 4,
           
		[Description("Wordt gepest")]
        WordtGepest = 5,
           
		[Description("Pest")]
        Pest = 6,
           
		[Description("Schopt, slaat of bijt")]
        SchoptSlaatOfBijt = 7,
           
		[Description("Anders")]
        Anders = 98,
	} 
	
		/// <summary>
	/// DataValNummer = 259 
	/// Taalomgeving stimulerend259 
	/// </summary>
	[Serializable]
	 
	public enum TaalomgevingStimulerend259
	{   	 

			 

			           
		[Description("Voldoende")]
        Voldoende = 1,
           
		[Description("Matig")]
        Matig = 2,
           
		[Description("Onvoldoende")]
        Onvoldoende = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 264 
	/// Bijz onderste extremiteiten (4-19)264 
	/// </summary>
	[Serializable]
	 
	public enum BijzOndersteExtremiteiten419264
	{   	 

			 

			           
		[Description("O-benen")]
        OBenen = 1,
           
		[Description("X-benen")]
        XBenen = 2,
           
		[Description("Vermoeden van de ziekte van Osgood-Schlatter")]
        VermoedenVanDeZiekteVanOsgoodSchlatter = 3,
           
		[Description("Vermoeden van adolescenten patellapijn")]
        VermoedenVanAdolescentenPatellapijn = 4,
           
		[Description("Beenlengteverschil")]
        Beenlengteverschil = 5,
           
		[Description("Overig")]
        Overig = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 269 
	/// LH kaart uitslag269 
	/// </summary>
	[Serializable]
	 
	public enum LhKaartUitslag269
	{   	 

			 

			           
		[Description("0,10")]
        Waarde010 = 1,
           
		[Description("0,16")]
        Waarde016 = 2,
           
		[Description("0,25")]
        Waarde025 = 3,
           
		[Description("0,40")]
        Waarde040 = 4,
           
		[Description("0,50")]
        Waarde050 = 5,
           
		[Description("0,63")]
        Waarde063 = 6,
           
		[Description("0,80")]
        Waarde080 = 7,
           
		[Description("1,00")]
        Waarde100 = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 275 
	/// Zorg ontvangen in het gezin275 
	/// </summary>
	[Serializable]
	 
	public enum ZorgOntvangenInHetGezin275
	{   	 

			 

			           
		[Description("Onbekend")]
        Onbekend = 1,
           
		[Description("Ja")]
        Ja = 2,
           
		[Description("Nee")]
        Nee = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 282 
	/// Zorgbeeindiging282 
	/// </summary>
	[Serializable]
	 
	public enum Zorgbeeindiging282
	{   	 

			 

			           
		[Description("Overdracht naar een andere JGZ-organisatie")]
        OverdrachtNaarEenAndereJgzOrganisatie = 1,
           
		[Description("Op eigen verzoek")]
        OpEigenVerzoek = 2,
           
		[Description("Verhuizing naar buitenland")]
        VerhuizingNaarBuitenland = 3,
           
		[Description("Overlijden")]
        Overlijden = 4,
           
		[Description("Bereiken 19 jarige leeftijd")]
        Bereiken19JarigeLeeftijd = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 283 
	/// Hoorstoornissen283 
	/// </summary>
	[Serializable]
	 
	public enum Hoorstoornissen283
	{   	 

			 

			           
		[Description("Geleidingsslechthorendheid")]
        Geleidingsslechthorendheid = 1,
           
		[Description("Perceptieslechthorendheid")]
        Perceptieslechthorendheid = 2,
           
		[Description("Stoornis in spraakverstaan")]
        StoornisInSpraakverstaan = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 284 
	/// Afwijkend mondgedrag logopedie284 
	/// </summary>
	[Serializable]
	 
	public enum AfwijkendMondgedragLogopedie284
	{   	 

			 

			           
		[Description("Duim- en vingerzuigen en ander zuiggedrag")]
        DuimEnVingerzuigenEnAnderZuiggedrag = 1,
           
		[Description("Habitueel mondademen")]
        HabitueelMondademen = 2,
           
		[Description("Afwijkend slikken")]
        AfwijkendSlikken = 3,
           
		[Description("Afwijkende tongligging in rust")]
        AfwijkendeTongliggingInRust = 4,
           
		[Description("Stoornissen in mondmotoriek")]
        StoornissenInMondmotoriek = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 286 
	/// Antwoorden LSPPK286 
	/// </summary>
	[Serializable]
	 
	public enum AntwoordenLsppk286
	{   	 

			 

			           
		[Description("Waar")]
        Waar = 1,
           
		[Description("Een beetje waar")]
        EenBeetjeWaar = 2,
           
		[Description("Niet waar")]
        NietWaar = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 287 
	/// Ingrijpende gebeurtenis LSPPK287 
	/// </summary>
	[Serializable]
	 
	public enum IngrijpendeGebeurtenisLsppk287
	{   	 

			 

			           
		[Description("Onbekend")]
        Onbekend = 1,
           
		[Description("Ja")]
        Ja = 2,
           
		[Description("Nee")]
        Nee = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 288 
	/// LSPPK ingevuld door288 
	/// </summary>
	[Serializable]
	 
	public enum LsppkIngevuldDoor288
	{   	 

			 

			           
		[Description("Ouder")]
        Ouder = 1,
           
		[Description("JGZ medewerker")]
        JgzMedewerker = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 289 
	/// Door huisarts verwezen naar ivm heuponderzoek289 
	/// </summary>
	[Serializable]
	 
	public enum DoorHuisartsVerwezenNaarIvmHeuponderzoek289
	{   	 

			 

			           
		[Description("Kinderarts")]
        Kinderarts = 1,
           
		[Description("Orthopaed")]
        Orthopaed = 2,
           
		[Description("Verwezen voor onderzoek")]
        VerwezenVoorOnderzoek = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 290 
	/// Soort onderzoek ivm vermoeden DHO290 
	/// </summary>
	[Serializable]
	 
	public enum SoortOnderzoekIvmVermoedenDho290
	{   	 

			 

			           
		[Description("Echo")]
        Echo = 1,
           
		[Description("Rontgen")]
        Rontgen = 2,
           
		[Description("Consult orthopaed")]
        ConsultOrthopaed = 3,
           
		[Description("Ouders zijn niet voor onderzoek geweest")]
        OudersZijnNietVoorOnderzoekGeweest = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 291 
	/// Uitslag onderzoek heupen291 
	/// </summary>
	[Serializable]
	 
	public enum UitslagOnderzoekHeupen291
	{   	 

			 

			           
		[Description("Geen bijzonderheden")]
        GeenBijzonderheden = 1,
           
		[Description("Twijfelachtig")]
        Twijfelachtig = 2,
           
		[Description("Dysplasie")]
        Dysplasie = 3,
           
		[Description("(Sub)luxatie")]
        SubLuxatie = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 292 
	/// Behandeling DHO292 
	/// </summary>
	[Serializable]
	 
	public enum BehandelingDho292
	{   	 

			 

			           
		[Description("Spreidbroek alleen 's nachts")]
        SpreidbroekAlleenSNachts = 1,
           
		[Description("Spreidbroek")]
        Spreidbroek = 2,
           
		[Description("Gipsbroek")]
        Gipsbroek = 3,
           
		[Description("Tractie")]
        Tractie = 4,
           
		[Description("Anders")]
        Anders = 5,
           
		[Description("Geen behandeling, afwachten")]
        GeenBehandelingAfwachten = 6,
           
		[Description("Operatie")]
        Operatie = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 293 
	/// Reden niet verwijzen DHO293 
	/// </summary>
	[Serializable]
	 
	public enum RedenNietVerwijzenDho293
	{   	 

			 

			           
		[Description("HA vindt het niet nodig")]
        HaVindtHetNietNodig = 1,
           
		[Description("Ouders niet gemotiveerd")]
        OudersNietGemotiveerd = 2,
           
		[Description("CBa: rechstreeks verwezen nr heupenpoli SpaarneZH")]
        CbaRechstreeksVerwezenNrHeupenpoliSpaarnezh = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 294 
	/// Door huisarts vewezen voor onderzoek294 
	/// </summary>
	[Serializable]
	 
	public enum DoorHuisartsVewezenVoorOnderzoek294
	{   	 

			 

			           
		[Description("Echo")]
        Echo = 1,
           
		[Description("Rontgenonderzoek")]
        Rontgenonderzoek = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 297 
	/// Overleg met (itemwaarden JGZ 4-19)297 
	/// </summary>
	[Serializable]
	 
	public enum OverlegMetItemwaardenJgz419297
	{   	 

			 

			           
		[Description("CB/HA/specialist")]
        CbHaSpecialist = 1,
           
		[Description("JGZ-arts")]
        JgzArts = 2,
           
		[Description("JGZ-vp")]
        JgzVp = 3,
           
		[Description("School")]
        School = 4,
           
		[Description("Logopediste")]
        Logopediste = 5,
           
		[Description("AMK/RvdK")]
        AmkRvdk = 6,
           
		[Description("MEE")]
        Mee = 7,
           
		[Description("Fysiotherapeut")]
        Fysiotherapeut = 8,
           
		[Description("Riagg")]
        Riagg = 9,
           
		[Description("Bureau Jeugdzorg")]
        BureauJeugdzorg = 10,
           
		[Description("Ouders")]
        Ouders = 11,
           
		[Description("Opvoedingsondersteuning")]
        Opvoedingsondersteuning = 12,
           
		[Description("Mobiel team")]
        MobielTeam = 13,
           
		[Description("Overig")]
        Overig = 14,
           
		[Description("Inhaalspreekuur vaccinatie")]
        InhaalspreekuurVaccinatie = 15,
	} 
	
		/// <summary>
	/// DataValNummer = 298 
	/// Acties nav LSPPK298 
	/// </summary>
	[Serializable]
	 
	public enum ActiesNavLsppk298
	{   	 

			 

			           
		[Description("Geen actie")]
        GeenActie = 1,
           
		[Description("Geruststelling")]
        Geruststelling = 2,
           
		[Description("Advisering")]
        Advisering = 3,
           
		[Description("Revisie")]
        Revisie = 4,
           
		[Description("Overleg met school")]
        OverlegMetSchool = 5,
           
		[Description("Verwijzen naar logopedie")]
        VerwijzenNaarLogopedie = 6,
           
		[Description("Verwijzen naar Riagg/BJZ")]
        VerwijzenNaarRiaggBjz = 7,
           
		[Description("Verwijzen naar OBD")]
        VerwijzenNaarObd = 8,
           
		[Description("Verwijzen naar AMW")]
        VerwijzenNaarAmw = 9,
           
		[Description("Verwijzen naar opvoedingsondersteuning")]
        VerwijzenNaarOpvoedingsondersteuning = 10,
           
		[Description("Verwijzen naar huisarts")]
        VerwijzenNaarHuisarts = 11,
           
		[Description("Overige acties")]
        OverigeActies = 12,
           
		[Description("Reeds in behandeling voor psychosociale problemat.")]
        ReedsInBehandelingVoorPsychosocialeProblemat = 13,
	} 
	
		/// <summary>
	/// DataValNummer = 299 
	/// Gezinssamenstelling (JGZ 4-19)299 
	/// </summary>
	[Serializable]
	 
	public enum GezinssamenstellingJgz419299
	{   	 

			 

			           
		[Description("Eigen (biologische) vader van het kind")]
        EigenBiologischeVaderVanHetKind = 1,
           
		[Description("Eigen (biologische) moeder van het kind")]
        EigenBiologischeMoederVanHetKind = 2,
           
		[Description("Partner/vriend(in) van de vader of moeder")]
        PartnerVriendInVanDeVaderOfMoeder = 3,
           
		[Description("Andere kinderen ((half)broer(s)/zus(sen))")]
        AndereKinderenHalfBroerSZusSen = 4,
           
		[Description("Co-ouderschap")]
        CoOuderschap = 5,
           
		[Description("Pleeg- of adoptief vader")]
        PleegOfAdoptiefVader = 6,
           
		[Description("Pleeg- of adoptief moeder")]
        PleegOfAdoptiefMoeder = 7,
           
		[Description("Andere familieleden (oom, tante, oma, opa)")]
        AndereFamilieledenOomTanteOmaOpa = 8,
           
		[Description("Het kind woont in internaat of tehuis")]
        HetKindWoontInInternaatOfTehuis = 9,
           
		[Description("Anderen")]
        Anderen = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 309 
	/// Top-neus309 
	/// </summary>
	[Serializable]
	 
	public enum TopNeus309
	{   	 

			 

			           
		[Description("0) Fout uitgevoerd")]
        FoutUitgevoerd0 = 0,
           
		[Description("1) Goed uitgevoerd")]
        GoedUitgevoerd1 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 310 
	/// Diadochokinese310 
	/// </summary>
	[Serializable]
	 
	public enum Diadochokinese310
	{   	 

			 

			           
		[Description("0) Niet soepele ritmische draaibeweging")]
        NietSoepeleRitmischeDraaibeweging0 = 0,
           
		[Description("1) Soepele ritmisch omdraaibeweging")]
        SoepeleRitmischOmdraaibeweging1 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 311 
	/// 1 been staan311 
	/// </summary>
	[Serializable]
	 
	public enum BeenStaan3111
	{   	 

			 

			           
		[Description("0) minder dan 7 seconden")]
        MinderDan7Seconden0 = 0,
           
		[Description("1) 7 seconden of meer")]
        SecondenOfMeer17 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 314 
	/// Hinkelen314 
	/// </summary>
	[Serializable]
	 
	public enum Hinkelen314
	{   	 

			 

			           
		[Description("0) minder dan 9 sprongen")]
        MinderDan9Sprongen0 = 0,
           
		[Description("1) 9 of meer sprongen")]
        OfMeerSprongen19 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 318 
	/// Vinger-duim oppositie (f)318 
	/// </summary>
	[Serializable]
	 
	public enum VingerDuimOppositieF318
	{   	 

			 

			           
		[Description("0) Duidelijke of contralatale meebewegen")]
        DuidelijkeOfContralataleMeebewegen0 = 0,
           
		[Description("1) Geen/discrete meebewegingen")]
        GeenDiscreteMeebewegingen1 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 320 
	/// Diadochokinese (f)320 
	/// </summary>
	[Serializable]
	 
	public enum DiadochokineseF320
	{   	 

			 

			           
		[Description("0) ab- en addactie bovenarm")]
        AbEnAddactieBovenarm0 = 0,
           
		[Description("1) vanuit elleboog:arm blijft tegen romp")]
        VanuitElleboogArmBlijftTegenRomp1 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 321 
	/// 1 been staan (g)321 
	/// </summary>
	[Serializable]
	 
	public enum BeenStaanG3211
	{   	 

			 

			           
		[Description("0) Duidelijk heffen armen/zwaaien romp")]
        DuidelijkHeffenArmenZwaaienRomp0 = 0,
           
		[Description("1) Geen/discrete correcties armen/romp")]
        GeenDiscreteCorrectiesArmenRomp1 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 322 
	/// Hielen lopen (g)322 
	/// </summary>
	[Serializable]
	 
	public enum HielenLopenG322
	{   	 

			 

			           
		[Description("0) Elleboogflexie/polsextensie/rompdraai")]
        ElleboogflexiePolsextensieRompdraai0 = 0,
           
		[Description("1) geen of gering meebewegen")]
        GeenOfGeringMeebewegen1 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 324 
	/// Hinkelen (g)324 
	/// </summary>
	[Serializable]
	 
	public enum HinkelenG324
	{   	 

			 

			           
		[Description("0) Op hele voet / armbeweg. boven navel")]
        OpHeleVoetArmbewegBovenNavel0 = 0,
           
		[Description("1) Op voorvoet / arm beweg. onder navel")]
        OpVoorvoetArmBewegOnderNavel1 = 1,
	} 
	
		/// <summary>
	/// DataValNummer = 327 
	/// Basis deelname Stevig Ouderschap ouders327 
	/// </summary>
	[Serializable]
	 
	public enum BasisDeelnameStevigOuderschapOuders327
	{   	 

			 

			           
		[Description("Persoonlijke omstandigheden")]
        PersoonlijkeOmstandigheden = 1,
           
		[Description("Voorgeschiedenis")]
        Voorgeschiedenis = 2,
           
		[Description("Onzekerheid ouderschap")]
        OnzekerheidOuderschap = 3,
           
		[Description("Sociale steun")]
        SocialeSteun = 4,
           
		[Description("Overig")]
        Overig = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 328 
	/// Basis deelname Stevig ouderschap overig328 
	/// </summary>
	[Serializable]
	 
	public enum BasisDeelnameStevigOuderschapOverig328
	{   	 

			 

			           
		[Description("Relationele steun")]
        RelationeleSteun = 1,
           
		[Description("Problemen kind")]
        ProblemenKind = 2,
           
		[Description("Problemen overige kinderen")]
        ProblemenOverigeKinderen = 3,
           
		[Description("Advies JGZ verpleegkundige")]
        AdviesJgzVerpleegkundige = 4,
           
		[Description("Geen informatie op verzoek v ouders")]
        GeenInformatieOpVerzoekVOuders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 329 
	/// Aandachts en vervolgpunten na Stevig ouderschap329 
	/// </summary>
	[Serializable]
	 
	public enum AandachtsEnVervolgpuntenNaStevigOuderschap329
	{   	 

			 

			           
		[Description("Persoonlijke omstandigheden moeder")]
        PersoonlijkeOmstandighedenMoeder = 1,
           
		[Description("Persoonlijke omstandigheden vader")]
        PersoonlijkeOmstandighedenVader = 2,
           
		[Description("Relatie ouders")]
        RelatieOuders = 3,
           
		[Description("Ontwikkeling kind")]
        OntwikkelingKind = 4,
           
		[Description("Ontwikeling overige kinderen")]
        OntwikelingOverigeKinderen = 5,
           
		[Description("Ouderschap")]
        Ouderschap = 6,
           
		[Description("Sociale relaties binnen familie")]
        SocialeRelatiesBinnenFamilie = 7,
           
		[Description("Sociale relaties buiten familie")]
        SocialeRelatiesBuitenFamilie = 8,
           
		[Description("Aanvullende ondersteuning")]
        AanvullendeOndersteuning = 9,
           
		[Description("Overig")]
        Overig = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 330 
	/// Effect huisbezoeken330 
	/// </summary>
	[Serializable]
	 
	public enum EffectHuisbezoeken330
	{   	 

			 

			           
		[Description("Positief")]
        Positief = 1,
           
		[Description("Neutraal")]
        Neutraal = 2,
           
		[Description("Negatief")]
        Negatief = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 332 
	/// Verwezen nav Stevig Ouderschap332 
	/// </summary>
	[Serializable]
	 
	public enum VerwezenNavStevigOuderschap332
	{   	 

			 

			           
		[Description("Huisarts")]
        Huisarts = 1,
           
		[Description("(Kinder) fysiotherapeut")]
        KinderFysiotherapeut = 2,
           
		[Description("Bureau Jeugdzorg")]
        BureauJeugdzorg = 3,
           
		[Description("Jeugdriagg")]
        Jeugdriagg = 4,
           
		[Description("GGZ")]
        Ggz = 5,
           
		[Description("MEE")]
        Mee = 6,
           
		[Description("Integrale Vroeghulp")]
        IntegraleVroeghulp = 7,
           
		[Description("VVE")]
        Vve = 8,
           
		[Description("Home start")]
        HomeStart = 9,
           
		[Description("Peutergym")]
        Peutergym = 10,
           
		[Description("Tien voor toekomst")]
        TienVoorToekomst = 11,
           
		[Description("Peuterspeelzaal")]
        Peuterspeelzaal = 12,
           
		[Description("Maatschappelijk werk")]
        MaatschappelijkWerk = 13,
           
		[Description("Vroegbehandeling Heliomare")]
        VroegbehandelingHeliomare = 14,
           
		[Description("St. Basis")]
        StBasis = 15,
	} 
	
		/// <summary>
	/// DataValNummer = 333 
	/// Ishiharatest333 
	/// </summary>
	[Serializable]
	 
	public enum Ishiharatest333
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Zwak")]
        Zwak = 2,
           
		[Description("Totaal afwezig")]
        TotaalAfwezig = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 335 
	/// SLS onderdeel A (op welke manier?)335 
	/// </summary>
	[Serializable]
	 
	public enum SlsOnderdeelAOpWelkeManier335
	{   	 

			 

			           
		[Description("1. N.v.t. herhaalt zinnen of hele woorden")]
        NVTHerhaaltZinnenOfHeleWoorden1 = 1,
           
		[Description("2. Herhaalt eerste klanken 2/3 x zonder spanning")]
        HerhaaltEersteKlanken23XZonderSpanning2 = 2,
           
		[Description("3. 4 of meer keer herhalen van klanken")]
        OfMeerKeerHerhalenVanKlanken34 = 3,
           
		[Description("4. Gespannen stem bij herhalen")]
        GespannenStemBijHerhalen4 = 4,
           
		[Description("5. Vastzitten op het woord (klank/letter)")]
        VastzittenOpHetWoordKlankLetter5 = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 336 
	/// SLS onderdeel B (wat voor reacties?)336 
	/// </summary>
	[Serializable]
	 
	public enum SlsOnderdeelBWatVoorReacties336
	{   	 

			 

			           
		[Description("1. Geen merkbare reacties")]
        GeenMerkbareReacties1 = 1,
           
		[Description("2. Zoekt andere woorden uit angst voor stotteren")]
        ZoektAndereWoordenUitAngstVoorStotteren2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 337 
	/// SLS onderdeel C (hoe vaak?)337 
	/// </summary>
	[Serializable]
	 
	public enum SlsOnderdeelCHoeVaak337
	{   	 

			 

			           
		[Description("3. Heel vaak")]
        HeelVaak3 = 0,
           
		[Description("1. Niet opvallend")]
        NietOpvallend1 = 1,
           
		[Description("1. Niet vaak (minder van 2%)")]
        NietVaakMinderVan21 = 2,
           
		[Description("2. Vaak (hapert 1 maal per 2 of 3 zinnen)")]
        VaakHapert1MaalPer2Of3Zinnen2 = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 338 
	/// SLS onderdeel D (merkt het kind het haperen?)338 
	/// </summary>
	[Serializable]
	 
	public enum SlsOnderdeelDMerktHetKindHetHaperen338
	{   	 

			 

			           
		[Description("1. N.v.t. lijkt het niet te merken")]
        NVTLijktHetNietTeMerken1 = 1,
           
		[Description("2. Merkt het op, doet moeite, blijft proberen")]
        MerktHetOpDoetMoeiteBlijftProberen2 = 2,
           
		[Description("3. Vindt het lastig, geeft pogingen op")]
        VindtHetLastigGeeftPogingenOp3 = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 339 
	/// SLS onderdeel E (hoe reageren andere mensen?)339 
	/// </summary>
	[Serializable]
	 
	public enum SlsOnderdeelEHoeReagerenAndereMensen339
	{   	 

			 

			           
		[Description("1. N.v.t. niemand stoort zicht eraan")]
        NVTNiemandStoortZichtEraan1 = 1,
           
		[Description("2. Ouders ongerust")]
        OudersOngerust2 = 2,
           
		[Description("3. Plagerijen of andere reacties luisteraars")]
        PlagerijenOfAndereReactiesLuisteraars3 = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 340 
	/// SLS onderdeel F (hoe lang geleden?)340 
	/// </summary>
	[Serializable]
	 
	public enum SlsOnderdeelFHoeLangGeleden340
	{   	 

			 

			           
		[Description("1. N.v.t. niet gemerkt")]
        NVTNietGemerkt1 = 1,
           
		[Description("2. Minder dan 4 maanden geleden")]
        MinderDan4MaandenGeleden2 = 2,
           
		[Description("3. 4 tot 12 maanden geleden")]
        Tot12MaandenGeleden34 = 3,
           
		[Description("4. Meer dan 12 maanden geleden")]
        MeerDan12MaandenGeleden4 = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 341 
	/// Acties nav SLS341 
	/// </summary>
	[Serializable]
	 
	public enum ActiesNavSls341
	{   	 

			 

			           
		[Description("Geen actie; ouders geruststellen")]
        GeenActieOudersGeruststellen = 1,
           
		[Description("Follow-up na 3 maanden")]
        FollowUpNa3Maanden = 2,
           
		[Description("Verwijzing")]
        Verwijzing = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 342 
	/// Aard van de vraag (opvoedspreekuur)342 
	/// </summary>
	[Serializable]
	 
	public enum AardVanDeVraagOpvoedspreekuur342
	{   	 

			 

			           
		[Description("Lichamelijke ontwikkeling")]
        LichamelijkeOntwikkeling = 1,
           
		[Description("Spraak/taal ontwikkeling")]
        SpraakTaalOntwikkeling = 2,
           
		[Description("Gedrag")]
        Gedrag = 3,
           
		[Description("Verstandelijke ontwikkeling")]
        VerstandelijkeOntwikkeling = 4,
           
		[Description("Sociale ontwikkeling")]
        SocialeOntwikkeling = 5,
           
		[Description("Emotionele ontwikkeling")]
        EmotioneleOntwikkeling = 6,
           
		[Description("Spel en vrijetijdsbesteding")]
        SpelEnVrijetijdsbesteding = 7,
           
		[Description("Kinderopvang/school")]
        KinderopvangSchool = 8,
           
		[Description("Opvoeding/ouderschap")]
        OpvoedingOuderschap = 9,
           
		[Description("Overig")]
        Overig = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 343 
	/// Samenvatting (opvoedspreekuur)343 
	/// </summary>
	[Serializable]
	 
	public enum SamenvattingOpvoedspreekuur343
	{   	 

			 

			           
		[Description("Voorlichting, advies gegeven")]
        VoorlichtingAdviesGegeven = 1,
           
		[Description("Verwezen naar JGZ")]
        VerwezenNaarJgz = 2,
           
		[Description("Verwezen naar BJZ")]
        VerwezenNaarBjz = 3,
           
		[Description("Verwezen naar Jeugdriagg")]
        VerwezenNaarJeugdriagg = 4,
           
		[Description("Begleiding pedagoog")]
        BegleidingPedagoog = 5,
           
		[Description("Anders")]
        Anders = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 344 
	/// VP 4-19 overgewicht Oorzaken344 
	/// </summary>
	[Serializable]
	 
	public enum Vp419OvergewichtOorzaken344
	{   	 

			 

			           
		[Description("Voedingsteveel")]
        Voedingsteveel = 1,
           
		[Description("Weinig of geen sport")]
        WeinigOfGeenSport = 2,
           
		[Description("Veel tv kijken; computeren")]
        VeelTvKijkenComputeren = 3,
           
		[Description("Kennistekort ouders")]
        KennistekortOuders = 4,
           
		[Description("Kennistekort kind")]
        KennistekortKind = 5,
           
		[Description("Ontbreken van duidelijke regels/afspraken")]
        OntbrekenVanDuidelijkeRegelsAfspraken = 6,
           
		[Description("Niet opvolgen van regels/afspraken")]
        NietOpvolgenVanRegelsAfspraken = 7,
           
		[Description("Emotie-eten")]
        EmotieEten = 8,
           
		[Description("Eten voor tv / PC")]
        EtenVoorTvPc = 9,
           
		[Description("Erfelijkheid")]
        Erfelijkheid = 10,
           
		[Description("Frequent snoepen")]
        FrequentSnoepen = 11,
           
		[Description("Overig")]
        Overig = 12,
	} 
	
		/// <summary>
	/// DataValNummer = 345 
	/// VP 4-19 overgewicht Zich uitend in345 
	/// </summary>
	[Serializable]
	 
	public enum Vp419OvergewichtZichUitendIn345
	{   	 

			 

			           
		[Description("BMI >")]
        Bmi = 1,
           
		[Description("Zichtbare vetophoping")]
        ZichtbareVetophoping = 2,
           
		[Description("Bij inspanning kortademig, snel moet, slechte cond")]
        BijInspanningKortademigSnelMoetSlechteCond = 3,
           
		[Description("Negatief zelfbeeld (omvang, uiterlijk, uitoef act)")]
        NegatiefZelfbeeldOmvangUiterlijkUitoefAct = 4,
           
		[Description("Gepest worden ivm overgewicht")]
        GepestWordenIvmOvergewicht = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 346 
	/// VP 4-19 overgewicht Korte termijn doelen346 
	/// </summary>
	[Serializable]
	 
	public enum Vp419OvergewichtKorteTermijnDoelen346
	{   	 

			 

			           
		[Description("Bewustwording van het probleem")]
        BewustwordingVanHetProbleem = 1,
           
		[Description("Stabiliseren binnen 3 maanden (BMI blijft gelijk)")]
        StabiliserenBinnen3MaandenBmiBlijftGelijk = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 347 
	/// VP 4-19 overgewicht Lange termijn doelen347 
	/// </summary>
	[Serializable]
	 
	public enum Vp419OvergewichtLangeTermijnDoelen347
	{   	 

			 

			           
		[Description("BMI verminderen binnen 1 jaar")]
        BmiVerminderenBinnen1Jaar = 1,
           
		[Description("BMI stabiel houden")]
        BmiStabielHouden = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 348 
	/// VP 4-19 overgewicht Motivatie348 
	/// </summary>
	[Serializable]
	 
	public enum Vp419OvergewichtMotivatie348
	{   	 

			 

			           
		[Description("1")]
        Waarde1 = 1,
           
		[Description("2")]
        Waarde2 = 2,
           
		[Description("3")]
        Waarde3 = 3,
           
		[Description("4")]
        Waarde4 = 4,
           
		[Description("5")]
        Waarde5 = 5,
           
		[Description("6")]
        Waarde6 = 6,
           
		[Description("7")]
        Waarde7 = 7,
           
		[Description("8")]
        Waarde8 = 8,
           
		[Description("9")]
        Waarde9 = 9,
           
		[Description("10")]
        Waarde10 = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 349 
	/// VP 4-19 overgewicht Interventies349 
	/// </summary>
	[Serializable]
	 
	public enum Vp419OvergewichtInterventies349
	{   	 

			 

			           
		[Description("(Vervolg) anamnese binnen 1-3 maand(en)")]
        VervolgAnamneseBinnen13MaandEn = 1,
           
		[Description("Voedingsdagboek bijhouden")]
        VoedingsdagboekBijhouden = 2,
           
		[Description("Overzicht geven van voedingsmiddelen en calorieen")]
        OverzichtGevenVanVoedingsmiddelenEnCalorieen = 3,
           
		[Description("Manieren leren om eetgedrag te veranderen")]
        ManierenLerenOmEetgedragTeVeranderen = 4,
           
		[Description("Informatie geven over gezond eetpatroon")]
        InformatieGevenOverGezondEetpatroon = 5,
           
		[Description("Informatie gegeven over gezond beweegpatroon")]
        InformatieGegevenOverGezondBeweegpatroon = 6,
           
		[Description("Informatie geven over overige zaken")]
        InformatieGevenOverOverigeZaken = 7,
           
		[Description("Opvoedingsadvies over opstellen regels/afspraken")]
        OpvoedingsadviesOverOpstellenRegelsAfspraken = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 350 
	/// VP 4-19 overgewicht Evaluatie350 
	/// </summary>
	[Serializable]
	 
	public enum Vp419OvergewichtEvaluatie350
	{   	 

			 

			           
		[Description("Is zich bewust van het probleem")]
        IsZichBewustVanHetProbleem = 1,
           
		[Description("BMI blijft gelijk")]
        BmiBlijftGelijk = 2,
           
		[Description("Eet aan tafel")]
        EetAanTafel = 3,
           
		[Description("Beweegt dagelijks 1/2 - 1 uur matig")]
        BeweegtDagelijks121UurMatig = 4,
           
		[Description("Beweegt dagelijks 1/2 t-1 uur intensief")]
        BeweegtDagelijks12T1UurIntensief = 5,
           
		[Description("Eetpatroon is veranderd")]
        EetpatroonIsVeranderd = 6,
           
		[Description("Er zijn regels/afspraken opgesteld")]
        ErZijnRegelsAfsprakenOpgesteld = 7,
           
		[Description("Voedingsdagboek bijgehouden")]
        VoedingsdagboekBijgehouden = 8,
           
		[Description("Overig")]
        Overig = 9,
	} 
	
		/// <summary>
	/// DataValNummer = 351 
	/// VTO-taal signaleringsinstrument 2 jaar Woorden351 
	/// </summary>
	[Serializable]
	 
	public enum VtoTaalSignaleringsinstrument2JaarWoorden351
	{   	 

			 

			           
		[Description("zinnetjes (2 of meer woorden)")]
        Zinnetjes2OfMeerWoorden = 1,
           
		[Description("woord of naam")]
        WoordOfNaam = 2,
           
		[Description("noemt bij geluid of duidt aan (bv okke=oma)")]
        NoemtBijGeluidOfDuidtAanBvOkkeOma = 3,
           
		[Description("papa, mama")]
        PapaMama = 4,
           
		[Description("aanwijzen met geluid")]
        AanwijzenMetGeluid = 5,
           
		[Description("aanwijzen zonder geluid")]
        AanwijzenZonderGeluid = 6,
           
		[Description("duidt dat niet aan")]
        DuidtDatNietAan = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 352 
	/// VTO-taal signaleringsinstument 2 jaar Spelen352 
	/// </summary>
	[Serializable]
	 
	public enum VtoTaalSignaleringsinstument2JaarSpelen352
	{   	 

			 

			           
		[Description("Nooit")]
        Nooit = 1,
           
		[Description("Af en toe")]
        AfEnToe = 2,
           
		[Description("Vaak")]
        Vaak = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 353 
	/// VTO-taal signaleringsinstrument 2 jaar Lichaamsdel353 
	/// </summary>
	[Serializable]
	 
	public enum VtoTaalSignaleringsinstrument2JaarLichaamsdel353
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Fout")]
        Fout = 2,
           
		[Description("Weigert, thuis goed")]
        WeigertThuisGoed = 3,
           
		[Description("Weigert, thuis fout")]
        WeigertThuisFout = 4,
           
		[Description("Weigert, ouder weet niet")]
        WeigertOuderWeetNiet = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 354 
	/// VTO-taal signaleringsinstrument 2 jaar score354 
	/// </summary>
	[Serializable]
	 
	public enum VtoTaalSignaleringsinstrument2JaarScore354
	{   	 

			 

			           
		[Description("0-1 (onvoldoende; verwijzing AC)")]
        OnvoldoendeVerwijzingAc01 = 1,
           
		[Description("2-3 (net voldoende; zorg op maat)")]
        NetVoldoendeZorgOpMaat23 = 2,
           
		[Description("4-5 (voldoende)")]
        Voldoende45 = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 355 
	/// VTO-taal signaleringsinstrument 2 jaar Actie355 
	/// </summary>
	[Serializable]
	 
	public enum VtoTaalSignaleringsinstrument2JaarActie355
	{   	 

			 

			           
		[Description("Verwijzing AC")]
        VerwijzingAc = 1,
           
		[Description("Verwijzing KNO")]
        VerwijzingKno = 2,
           
		[Description("Verwijzing KA")]
        VerwijzingKa = 3,
           
		[Description("Begeleiding w.v.")]
        BegeleidingWV = 4,
           
		[Description("Lexilijst")]
        Lexilijst = 5,
           
		[Description("Verwijzing VVE")]
        VerwijzingVve = 6,
           
		[Description("Overig")]
        Overig = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 356 
	/// VTO taal: onderzocht door356 
	/// </summary>
	[Serializable]
	 
	public enum VtoTaalOnderzochtDoor356
	{   	 

			 

			           
		[Description("ST team AC")]
        StTeamAc = 1,
           
		[Description("KNO-arts")]
        KnoArts = 2,
           
		[Description("Kinderarts")]
        Kinderarts = 3,
           
		[Description("Integrale Vroeghulp")]
        IntegraleVroeghulp = 4,
           
		[Description("Logopedist")]
        Logopedist = 5,
           
		[Description("Jeugdriagg")]
        Jeugdriagg = 6,
           
		[Description("Overig")]
        Overig = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 357 
	/// VTO taal: behandeling/begeleiding357 
	/// </summary>
	[Serializable]
	 
	public enum VtoTaalBehandelingBegeleiding357
	{   	 

			 

			           
		[Description("Logopedie")]
        Logopedie = 1,
           
		[Description("Spec taal PSZ (NSDSK)")]
        SpecTaalPszNsdsk = 2,
           
		[Description("VVE")]
        Vve = 3,
           
		[Description("(Hanen) oudercursus")]
        HanenOudercursus = 4,
           
		[Description("VHT")]
        Vht = 5,
           
		[Description("MKD")]
        Mkd = 6,
           
		[Description("Gezinsbegeleiding Effatha Guyot")]
        GezinsbegeleidingEffathaGuyot = 7,
           
		[Description("Gezinsbegeleiding MEE")]
        GezinsbegeleidingMee = 8,
           
		[Description("GV")]
        Gv = 9,
           
		[Description("Adenotomie")]
        Adenotomie = 10,
           
		[Description("Anders")]
        Anders = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 358 
	/// VTO-taal uitslag358 
	/// </summary>
	[Serializable]
	 
	public enum VtoTaalUitslag358
	{   	 

			 

			           
		[Description("Geen bijzonderheden")]
        GeenBijzonderheden = 1,
           
		[Description("Taalproductie achterstand")]
        TaalproductieAchterstand = 2,
           
		[Description("Taalbegrip achterstand")]
        TaalbegripAchterstand = 3,
           
		[Description("Algehele ontwikkelingsachterstand")]
        AlgeheleOntwikkelingsachterstand = 4,
           
		[Description("Vermoeden contactstoornis/psychiatrische stoornis")]
        VermoedenContactstoornisPsychiatrischeStoornis = 5,
           
		[Description("Gehoorverlies")]
        Gehoorverlies = 6,
           
		[Description("Anders")]
        Anders = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 366 
	/// Plaats bevalling366 
	/// </summary>
	[Serializable]
	 
	public enum PlaatsBevalling366
	{   	 

			 

			           
		[Description("Thuis")]
        Thuis = 1,
           
		[Description("Poliklinisch")]
        Poliklinisch = 2,
           
		[Description("Primair klinisch")]
        PrimairKlinisch = 3,
           
		[Description("Secundair klinisch")]
        SecundairKlinisch = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 384 
	/// Conclusie groei384 
	/// </summary>
	[Serializable]
	 
	public enum ConclusieGroei384
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Controle")]
        Controle = 2,
           
		[Description("Verwijzing")]
        Verwijzing = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 386 
	/// Conclusie lengte386 
	/// </summary>
	[Serializable]
	 
	public enum ConclusieLengte386
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Controle")]
        Controle = 2,
           
		[Description("Verwijzing")]
        Verwijzing = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 387 
	/// Visusbepaling uitgevoerd387 
	/// </summary>
	[Serializable]
	 
	public enum VisusbepalingUitgevoerd387
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Ja, met bril")]
        JaMetBril = 2,
           
		[Description("Ja, zonder bril")]
        JaZonderBril = 3,
           
		[Description("Nee")]
        Nee = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 388 
	/// Gezond ziek388 
	/// </summary>
	[Serializable]
	 
	public enum GezondZiek388
	{   	 

			 

			           
		[Description("Gezond")]
        Gezond = 1,
           
		[Description("Ziek")]
        Ziek = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 389 
	/// goed controle overige389 
	/// </summary>
	[Serializable]
	 
	public enum GoedControleOverige389
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Controle")]
        Controle = 2,
           
		[Description("Overige")]
        Overige = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 396 
	/// Leuk, niet leuk, saai, stom396 
	/// </summary>
	[Serializable]
	 
	public enum LeukNietLeukSaaiStom396
	{   	 

			 

			           
		[Description("Leuk")]
        Leuk = 1,
           
		[Description("Niet leuk")]
        NietLeuk = 2,
           
		[Description("Saai")]
        Saai = 3,
           
		[Description("Stom")]
        Stom = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 398 
	/// Ja, nee, onbekend398 
	/// </summary>
	[Serializable]
	 
	public enum JaNeeOnbekend398
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
           
		[Description("Onbekend")]
        Onbekend = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 399 
	/// Goed, controle, verwijzing399 
	/// </summary>
	[Serializable]
	 
	public enum GoedControleVerwijzing399
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Controle")]
        Controle = 2,
           
		[Description("Verwijzing")]
        Verwijzing = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 400 
	/// Spraak / taal400 
	/// </summary>
	[Serializable]
	 
	public enum SpraakTaal400
	{   	 

			 

			           
		[Description("Slissen")]
        Slissen = 1,
           
		[Description("Stotteren/broddelen")]
        StotterenBroddelen = 2,
           
		[Description("Hese stem")]
        HeseStem = 3,
           
		[Description("Zacht praten")]
        ZachtPraten = 4,
           
		[Description("Open mond gedrag")]
        OpenMondGedrag = 5,
           
		[Description("Overige")]
        Overige = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 401 
	/// Beh. spraak/taal401 
	/// </summary>
	[Serializable]
	 
	public enum BehSpraakTaal401
	{   	 

			 

			           
		[Description("Onder behandeling")]
        OnderBehandeling = 1,
           
		[Description("Behandeling gehad")]
        BehandelingGehad = 2,
           
		[Description("Verwijzing logopedist")]
        VerwijzingLogopedist = 3,
           
		[Description("Wil geen behandeling")]
        WilGeenBehandeling = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 406 
	/// Keuzelijst zien406 
	/// </summary>
	[Serializable]
	 
	public enum KeuzelijstZien406
	{   	 

			 

			           
		[Description("Verwijzing oogarts")]
        VerwijzingOogarts = 0,
           
		[Description("Al onder behandeling")]
        AlOnderBehandeling = 1,
           
		[Description("Follow up")]
        FollowUp = 2,
           
		[Description("Verwijzing opticiën")]
        VerwijzingOpticiN = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 407 
	/// Keuzelijst gehoor407 
	/// </summary>
	[Serializable]
	 
	public enum KeuzelijstGehoor407
	{   	 

			 

			           
		[Description("Al onder behandeling")]
        AlOnderBehandeling = 1,
           
		[Description("Follow up")]
        FollowUp = 2,
           
		[Description("Verwijzing huisarts/kno arts")]
        VerwijzingHuisartsKnoArts = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 409 
	/// Leuk Niet leuk409 
	/// </summary>
	[Serializable]
	 
	public enum LeukNietLeuk409
	{   	 

			 

			           
		[Description("Leuk")]
        Leuk = 1,
           
		[Description("Niet leuk")]
        NietLeuk = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 410 
	/// Goed, gemiddeld, matig, onvold410 
	/// </summary>
	[Serializable]
	 
	public enum GoedGemiddeldMatigOnvold410
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Gemiddeld")]
        Gemiddeld = 2,
           
		[Description("Matig")]
        Matig = 3,
           
		[Description("Onvoldoende")]
        Onvoldoende = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 411 
	/// 1 uur, 2 uur enz411 
	/// </summary>
	[Serializable]
	 
	public enum Uur2UurEnz4111
	{   	 

			 

			           
		[Description("1 uur")]
        Uur1 = 1,
           
		[Description("2 uur")]
        Uur2 = 2,
           
		[Description("3 uur")]
        Uur3 = 3,
           
		[Description("4 uur")]
        Uur4 = 4,
           
		[Description("meer dan 4 uur")]
        MeerDan4Uur = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 413 
	/// Goed, Controle, Overleg JA413 
	/// </summary>
	[Serializable]
	 
	public enum GoedControleOverlegJa413
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Controle")]
        Controle = 2,
           
		[Description("Overleg jeugdarts")]
        OverlegJeugdarts = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 414 
	/// ja, nee, twijfel414 
	/// </summary>
	[Serializable]
	 
	public enum JaNeeTwijfel414
	{   	 

			 

			           
		[Description("ja")]
        Ja = 1,
           
		[Description("nee")]
        Nee = 2,
           
		[Description("twijfel")]
        Twijfel = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 416 
	/// Soort activiteit416 
	/// </summary>
	[Serializable]
	 
	public enum SoortActiviteit416
	{   	 

			 

			           
		[Description("Huisbezoek 4-7 dagen")]
        Huisbezoek47Dagen = 1,
           
		[Description("Huisbezoek 2 weken")]
        Huisbezoek2Weken = 2,
           
		[Description("Contactmoment 4 weken")]
        Contactmoment4Weken = 3,
           
		[Description("Contactmoment 8 weken")]
        Contactmoment8Weken = 4,
           
		[Description("Contactmoment 3 maanden")]
        Contactmoment3Maanden = 5,
           
		[Description("Contactmoment 4 maanden")]
        Contactmoment4Maanden = 6,
           
		[Description("Contactmoment 6 maanden")]
        Contactmoment6Maanden = 7,
           
		[Description("Contactmoment 7,5 maand")]
        Contactmoment75Maand = 8,
           
		[Description("Contactmoment 9 maanden")]
        Contactmoment9Maanden = 9,
           
		[Description("Contactmoment 11 maanden")]
        Contactmoment11Maanden = 10,
           
		[Description("Contactmoment 14 maanden")]
        Contactmoment14Maanden = 11,
           
		[Description("Contactmoment 18 maanden")]
        Contactmoment18Maanden = 12,
           
		[Description("Contactmoment 2 jaar")]
        Contactmoment2Jaar = 13,
           
		[Description("Contactmoment 3 jaar")]
        Contactmoment3Jaar = 14,
           
		[Description("Contactmoment 3,9 jaar")]
        Contactmoment39Jaar = 15,
           
		[Description("Contactmoment logopedie 5 jaar")]
        ContactmomentLogopedie5Jaar = 16,
           
		[Description("Contactmoment groep 2")]
        ContactmomentGroep2 = 17,
           
		[Description("Contactmoment groep 4")]
        ContactmomentGroep4 = 18,
           
		[Description("Contactmoment groep 7")]
        ContactmomentGroep7 = 19,
           
		[Description("Contactmoment klas 2")]
        ContactmomentKlas2 = 20,
           
		[Description("Contactmoment speciaal onderwijs")]
        ContactmomentSpeciaalOnderwijs = 21,
           
		[Description("Contactmoment op indicatie")]
        ContactmomentOpIndicatie = 22,
           
		[Description("Contactmoment op verzoek")]
        ContactmomentOpVerzoek = 23,
           
		[Description("Telefonisch contactmoment")]
        TelefonischContactmoment = 24,
           
		[Description("Massavaccinatie")]
        Massavaccinatie = 25,
           
		[Description("Huisbezoek op indicatie")]
        HuisbezoekOpIndicatie = 26,
           
		[Description("Spreekuur (0-19)")]
        Spreekuur019 = 27,
           
		[Description("Groepsbijeenkomst (of groepsvoorlichting)")]
        GroepsbijeenkomstOfGroepsvoorlichting = 28,
           
		[Description("Intake nieuwkomers")]
        IntakeNieuwkomers = 29,
           
		[Description("Ander contactmoment")]
        AnderContactmoment = 30,
           
		[Description("Consultatie/inlichtingen vragen")]
        ConsultatieInlichtingenVragen = 31,
           
		[Description("Melding")]
        Melding = 32,
           
		[Description("Terugkoppeling verwijzing")]
        TerugkoppelingVerwijzing = 33,
           
		[Description("Zorgcoordinatie")]
        Zorgcoordinatie = 34,
           
		[Description("Andere activiteit")]
        AndereActiviteit = 98,
	} 
	
		/// <summary>
	/// DataValNummer = 417 
	/// Status activiteit417 
	/// </summary>
	[Serializable]
	 
	public enum StatusActiviteit417
	{   	 

			 

			           
		[Description("Uitgevoerd/verschenen op afspraak")]
        UitgevoerdVerschenenOpAfspraak = 1,
           
		[Description("Niet uitgevoerd/niet verschenen op afspraak")]
        NietUitgevoerdNietVerschenenOpAfspraak = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 418 
	/// Toelichting niet verschenen418 
	/// </summary>
	[Serializable]
	 
	public enum ToelichtingNietVerschenen418
	{   	 

			 

			           
		[Description("Niet verschenen met bericht")]
        NietVerschenenMetBericht = 1,
           
		[Description("Niet verschenen zonder bericht")]
        NietVerschenenZonderBericht = 2,
           
		[Description("Afgezegd door JGZ")]
        AfgezegdDoorJgz = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 419 
	/// Ja/nee BDS419 
	/// </summary>
	[Serializable]
	 
	public enum JaNeeBds419
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 424 
	/// Oogonderzoek uitgevoerd424 
	/// </summary>
	[Serializable]
	 
	public enum OogonderzoekUitgevoerd424
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 427 
	/// Meldingen427 
	/// </summary>
	[Serializable]
	 
	public enum Meldingen427
	{   	 

			 

			           
		[Description("VIR")]
        Vir = 1,
           
		[Description("AMK")]
        Amk = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 428 
	/// Resultaat verwijzing428 
	/// </summary>
	[Serializable]
	 
	public enum ResultaatVerwijzing428
	{   	 

			 

			           
		[Description("Schriftelijk antwoord")]
        SchriftelijkAntwoord = 1,
           
		[Description("Geen schriftelijk antwoord")]
        GeenSchriftelijkAntwoord = 2,
           
		[Description("Advies opgevolgd")]
        AdviesOpgevolgd = 3,
           
		[Description("Nader onderzoek gedaan")]
        NaderOnderzoekGedaan = 4,
           
		[Description("Verwezen naar specialist")]
        VerwezenNaarSpecialist = 5,
           
		[Description("Anders")]
        Anders = 6,
           
		[Description("Brief toegevoegd")]
        BriefToegevoegd = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 436 
	/// Zindelijkheid436 
	/// </summary>
	[Serializable]
	 
	public enum Zindelijkheid436
	{   	 

			 

			           
		[Description("Overdag zindelijk voor urine")]
        OverdagZindelijkVoorUrine = 1,
           
		[Description("'s Nachts zindelijk voor urine")]
        SNachtsZindelijkVoorUrine = 2,
           
		[Description("Opnieuw onzindelijk voor urine")]
        OpnieuwOnzindelijkVoorUrine = 3,
           
		[Description("Zindelijk voor ontlasting")]
        ZindelijkVoorOntlasting = 4,
           
		[Description("Opnieuw onzindelijk voor ontlasting")]
        OpnieuwOnzindelijkVoorOntlasting = 5,
           
		[Description("Anders")]
        Anders = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 440 
	/// Interventiesoort440 
	/// </summary>
	[Serializable]
	 
	public enum Interventiesoort440
	{   	 

			 

			           
		[Description("Groei")]
        Groei = 1,
           
		[Description("Voeding")]
        Voeding = 2,
           
		[Description("Medisch / fysiek")]
        MedischFysiek = 3,
           
		[Description("Ontwikkeling")]
        Ontwikkeling = 4,
           
		[Description("Opgroeien en opvoeden")]
        OpgroeienEnOpvoeden = 5,
           
		[Description("VVE")]
        Vve = 6,
           
		[Description("Zorgelijke situatie")]
        ZorgelijkeSituatie = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 453 
	/// goed, matig, niet goed453 
	/// </summary>
	[Serializable]
	 
	public enum GoedMatigNietGoed453
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Matig")]
        Matig = 2,
           
		[Description("Niet goed")]
        NietGoed = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 107 
	/// Andere taal die ouder met kind spreekt107 
	/// </summary>
	[Serializable]
	 
	public enum AndereTaalDieOuderMetKindSpreekt107
	{   	 

			 

			           
		[Description("Arabisch (standaard)")]
        ArabischStandaard = 1,
           
		[Description("Berbertaal")]
        Berbertaal = 2,
           
		[Description("Chinees")]
        Chinees = 3,
           
		[Description("Duits")]
        Duits = 4,
           
		[Description("Engels")]
        Engels = 5,
           
		[Description("Farsi")]
        Farsi = 6,
           
		[Description("Frans")]
        Frans = 7,
           
		[Description("Hindi/saramacci")]
        HindiSaramacci = 8,
           
		[Description("Italiaans")]
        Italiaans = 9,
           
		[Description("Papiamento")]
        Papiamento = 10,
           
		[Description("Pools")]
        Pools = 11,
           
		[Description("Russisch")]
        Russisch = 12,
           
		[Description("Servo-kroatisch")]
        ServoKroatisch = 13,
           
		[Description("Somalisch")]
        Somalisch = 14,
           
		[Description("Spaans")]
        Spaans = 15,
           
		[Description("Tamil")]
        Tamil = 16,
           
		[Description("Turks")]
        Turks = 17,
           
		[Description("Vietnamees")]
        Vietnamees = 18,
           
		[Description("Anders")]
        Anders = 19,
	} 
	
		/// <summary>
	/// DataValNummer = 108 
	/// Bezoek tandarts108 
	/// </summary>
	[Serializable]
	 
	public enum BezoekTandarts108
	{   	 

			 

			           
		[Description("1 x per jaar")]
        XPerJaar1 = 1,
           
		[Description("Nog geen controle")]
        NogGeenControle = 2,
           
		[Description("Geen controle")]
        GeenControle = 3,
           
		[Description("Onduidelijk")]
        Onduidelijk = 4,
           
		[Description("2x per jaar/vaker")]
        xPerJaarVaker2 = 5,
           
		[Description("Wel eens")]
        WelEens = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 115 
	/// Behandeling geel zien115 
	/// </summary>
	[Serializable]
	 
	public enum BehandelingGeelZien115
	{   	 

			 

			           
		[Description("Lichttherapie")]
        Lichttherapie = 1,
           
		[Description("Wisseltransfusie")]
        Wisseltransfusie = 2,
           
		[Description("Anders")]
        Anders = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 118 
	/// Kleur huid118 
	/// </summary>
	[Serializable]
	 
	public enum KleurHuid118
	{   	 

			 

			           
		[Description("Bleekheid")]
        Bleekheid = 1,
           
		[Description("Centrale cyanose")]
        CentraleCyanose = 2,
           
		[Description("Perifere cyanose")]
        PerifereCyanose = 3,
           
		[Description("Anders")]
        Anders = 4,
           
		[Description("Geel")]
        Geel = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 119 
	/// Reden behandeling geelzien119 
	/// </summary>
	[Serializable]
	 
	public enum RedenBehandelingGeelzien119
	{   	 

			 

			           
		[Description("Bloedgroepantagonisme")]
        Bloedgroepantagonisme = 1,
           
		[Description("Infectie")]
        Infectie = 2,
           
		[Description("Leveraandoening")]
        Leveraandoening = 3,
           
		[Description("Fysiologisch")]
        Fysiologisch = 4,
           
		[Description("Onbekend")]
        Onbekend = 5,
           
		[Description("Anders")]
        Anders = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 121 
	/// conform leeftijd|nie...121 
	/// </summary>
	[Serializable]
	 
	public enum ConformLeeftijdNie121
	{   	 

			 

			           
		[Description("Conform leeftijd")]
        ConformLeeftijd = 1,
           
		[Description("Niet conform leeftijd")]
        NietConformLeeftijd = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 125 
	/// Relatie ouder tot kind125 
	/// </summary>
	[Serializable]
	 
	public enum RelatieOuderTotKind125
	{   	 

			 

			           
		[Description("Vader (biologische)")]
        VaderBiologische = 1,
           
		[Description("Moeder (biologisch)")]
        MoederBiologisch = 2,
           
		[Description("Partner/vriend v vader/moeder")]
        PartnerVriendVVaderMoeder = 3,
           
		[Description("Partner/vriendin van vader/moeder")]
        PartnerVriendinVanVaderMoeder = 4,
           
		[Description("Adoptief vader")]
        AdoptiefVader = 5,
           
		[Description("Adoptief moeder")]
        AdoptiefMoeder = 6,
           
		[Description("Pleegvader")]
        Pleegvader = 7,
           
		[Description("Pleegmoeder")]
        Pleegmoeder = 8,
           
		[Description("Anders")]
        Anders = 98,
	} 
	
		/// <summary>
	/// DataValNummer = 128 
	/// Overdracht vanuit kraamzorg128 
	/// </summary>
	[Serializable]
	 
	public enum OverdrachtVanuitKraamzorg128
	{   	 

			 

			           
		[Description("Nee, geen overdracht")]
        NeeGeenOverdracht = 1,
           
		[Description("Overdracht, geen bijzonderheden")]
        OverdrachtGeenBijzonderheden = 2,
           
		[Description("Overdracht, bijzonderheden")]
        OverdrachtBijzonderheden = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 129 
	/// Opleid ouder129 
	/// </summary>
	[Serializable]
	 
	public enum OpleidOuder129
	{   	 

			 

			           
		[Description("Geen")]
        Geen = 1,
           
		[Description("Basisonderwijs")]
        Basisonderwijs = 2,
           
		[Description("Lager of voorbereidend beroepsonderwijs")]
        LagerOfVoorbereidendBeroepsonderwijs = 3,
           
		[Description("Middelbaar algemeen voortgezet onderwijs")]
        MiddelbaarAlgemeenVoortgezetOnderwijs = 4,
           
		[Description("Middelbaar beroepsonderwijs en beroepbgl o")]
        MiddelbaarBeroepsonderwijsEnBeroepbglO = 5,
           
		[Description("Hoger algemeen en voorb wetensch o")]
        HogerAlgemeenEnVoorbWetenschO = 6,
           
		[Description("Hoger beroepsonderwijs")]
        HogerBeroepsonderwijs = 7,
           
		[Description("Wetenschappelijk onderwijs")]
        WetenschappelijkOnderwijs = 8,
           
		[Description("Wil ouder niet zeggen")]
        WilOuderNietZeggen = 9,
           
		[Description("Anders")]
        Anders = 98,
	} 
	
		/// <summary>
	/// DataValNummer = 134 
	/// Bijzonderheden scrotum134 
	/// </summary>
	[Serializable]
	 
	public enum BijzonderhedenScrotum134
	{   	 

			 

			           
		[Description("Hydrokele")]
        Hydrokele = 1,
           
		[Description("Varicokele")]
        Varicokele = 2,
           
		[Description("Anders")]
        Anders = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 135 
	/// Bijzonderheden penis135 
	/// </summary>
	[Serializable]
	 
	public enum BijzonderhedenPenis135
	{   	 

			 

			           
		[Description("Hypospadie")]
        Hypospadie = 1,
           
		[Description("Epispadie")]
        Epispadie = 2,
           
		[Description("Phimosis")]
        Phimosis = 3,
           
		[Description("Circumcisie")]
        Circumcisie = 4,
           
		[Description("Anders")]
        Anders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 136 
	/// Onderzoek tonus136 
	/// </summary>
	[Serializable]
	 
	public enum OnderzoekTonus136
	{   	 

			 

			           
		[Description("Verlaagd")]
        Verlaagd = 1,
           
		[Description("Verhoogd")]
        Verhoogd = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 141 
	/// Redenen geen NGS door JGZ Kennemerland141 
	/// </summary>
	[Serializable]
	 
	public enum RedenenGeenNgsDoorJgzKennemerland141
	{   	 

			 

			           
		[Description("Geen toestemming ouders")]
        GeenToestemmingOuders = 2,
           
		[Description("Andere reden")]
        AndereReden = 3,
           
		[Description("Elders uitgevoerd")]
        EldersUitgevoerd = 4,
           
		[Description("Gehooronderzoek op NICU")]
        GehooronderzoekOpNicu = 5,
           
		[Description("N.v.t.")]
        NVT = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 143 
	/// Van Wiechen: links/rechts143 
	/// </summary>
	[Serializable]
	 
	public enum VanWiechenLinksRechts143
	{   	 

			 

			           
		[Description("Nee, links niet")]
        NeeLinksNiet = 0,
           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
           
		[Description("Nee, rechts niet")]
        NeeRechtsNiet = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 144 
	/// Ven Wiechen: links/rechts/mededeling144 
	/// </summary>
	[Serializable]
	 
	public enum VenWiechenLinksRechtsMededeling144
	{   	 

			 

			           
		[Description("Mededeling")]
        Mededeling = 0,
           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
           
		[Description("Nee, rechts niet")]
        NeeRechtsNiet = 3,
           
		[Description("Nee, links niet")]
        NeeLinksNiet = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 147 
	/// Van Wiechen: ja/nee/mededeling147 
	/// </summary>
	[Serializable]
	 
	public enum VanWiechenJaNeeMededeling147
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
           
		[Description("Mededeling")]
        Mededeling = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 148 
	/// Overleden (achtergrond ouders)148 
	/// </summary>
	[Serializable]
	 
	public enum OverledenAchtergrondOuders148
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
           
		[Description("Onbekend")]
        Onbekend = 3,
           
		[Description("Onduidelijk")]
        Onduidelijk = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 149 
	/// Geslacht broer/zus149 
	/// </summary>
	[Serializable]
	 
	public enum GeslachtBroerZus149
	{   	 

			 

			           
		[Description("Mannelijk")]
        Mannelijk = 1,
           
		[Description("Vrouwelijk")]
        Vrouwelijk = 2,
           
		[Description("Onbekend")]
        Onbekend = 3,
           
		[Description("Niet gespecificeerd")]
        NietGespecificeerd = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 152 
	/// Landencodering GBA152 
	/// </summary>
	[Serializable]
	 
	public enum LandencoderingGba152
	{   	 

			 

			           
		[Description("Onbekend")]
        Onbekend = 0,
           
		[Description("Canada")]
        Canada = 5001,
           
		[Description("Frankrijk")]
        Frankrijk = 5002,
           
		[Description("Zwitserland")]
        Zwitserland = 5003,
           
		[Description("Rhodesië")]
        Rhodesi = 5004,
           
		[Description("Malawi")]
        Malawi = 5005,
           
		[Description("Cuba")]
        Cuba = 5006,
           
		[Description("Suriname")]
        Suriname = 5007,
           
		[Description("Tunesië")]
        Tunesi = 5008,
           
		[Description("Oostenrijk")]
        Oostenrijk = 5009,
           
		[Description("België")]
        Belgi = 5010,
           
		[Description("Botswana")]
        Botswana = 5011,
           
		[Description("Iran")]
        Iran = 5012,
           
		[Description("Nieuw-Zeeland")]
        NieuwZeeland = 5013,
           
		[Description("Zuid-Afrika")]
        ZuidAfrika = 5014,
           
		[Description("Denemarken")]
        Denemarken = 5015,
           
		[Description("Noordjemen")]
        Noordjemen = 5016,
           
		[Description("Hongarije")]
        Hongarije = 5017,
           
		[Description("Saoedi-Arabië")]
        SaoediArabi = 5018,
           
		[Description("Liberia")]
        Liberia = 5019,
           
		[Description("Ethiopië")]
        Ethiopi = 5020,
           
		[Description("Chili")]
        Chili = 5021,
           
		[Description("Marokko")]
        Marokko = 5022,
           
		[Description("Togo")]
        Togo = 5023,
           
		[Description("Ghana")]
        Ghana = 5024,
           
		[Description("Laos")]
        Laos = 5025,
           
		[Description("Angola")]
        Angola = 5026,
           
		[Description("Filippijnen")]
        Filippijnen = 5027,
           
		[Description("Zambia")]
        Zambia = 5028,
           
		[Description("Mali")]
        Mali = 5029,
           
		[Description("Ivoorkust")]
        Ivoorkust = 5030,
           
		[Description("Burma")]
        Burma = 5031,
           
		[Description("Monaco")]
        Monaco = 5032,
           
		[Description("Colombia")]
        Colombia = 5033,
           
		[Description("Albanië")]
        Albani = 5034,
           
		[Description("Kameroen")]
        Kameroen = 5035,
           
		[Description("Zuid-Viëtnam")]
        ZuidViTnam = 5036,
           
		[Description("Singapore")]
        Singapore = 5037,
           
		[Description("Paraguay")]
        Paraguay = 5038,
           
		[Description("Zweden")]
        Zweden = 5039,
           
		[Description("Cyprus")]
        Cyprus = 5040,
           
		[Description("Austr. Nieuwguinea")]
        AustrNieuwguinea = 5041,
           
		[Description("Brunei")]
        Brunei = 5042,
           
		[Description("Irak")]
        Irak = 5043,
           
		[Description("Mauritius")]
        Mauritius = 5044,
           
		[Description("Vaticaanstad")]
        Vaticaanstad = 5045,
           
		[Description("Kashmir")]
        Kashmir = 5046,
           
		[Description("Myanmar")]
        Myanmar = 5047,
           
		[Description("Jemen")]
        Jemen = 5048,
           
		[Description("Slovenië")]
        Sloveni = 5049,
           
		[Description("Zaïre")]
        ZaRe = 5050,
           
		[Description("Kroatië")]
        Kroati = 5051,
           
		[Description("Taiwan")]
        Taiwan = 5052,
           
		[Description("Rusland")]
        Rusland = 5053,
           
		[Description("Armenië")]
        Armeni = 5054,
           
		[Description("Ascension")]
        Ascension = 5055,
           
		[Description("Azoren")]
        Azoren = 5056,
           
		[Description("Bahrein")]
        Bahrein = 5057,
           
		[Description("Bhutan")]
        Bhutan = 5058,
           
		[Description("Britse Antillen")]
        BritseAntillen = 5059,
           
		[Description("Comoren")]
        Comoren = 5060,
           
		[Description("Falklandeilanden")]
        Falklandeilanden = 5061,
           
		[Description("Frans Guyana")]
        FransGuyana = 5062,
           
		[Description("Frans Somaliland")]
        FransSomaliland = 5063,
           
		[Description("Gilbert en Ellice-Eil.")]
        GilbertEnElliceEil = 5064,
           
		[Description("Groenland")]
        Groenland = 5065,
           
		[Description("Guadeloupe")]
        Guadeloupe = 5066,
           
		[Description("Kaapverdische Eilanden")]
        KaapverdischeEilanden = 5067,
           
		[Description("Macau")]
        Macau = 5068,
           
		[Description("Martinique")]
        Martinique = 5069,
           
		[Description("Mozambique")]
        Mozambique = 5070,
           
		[Description("Pitcairneilanden")]
        Pitcairneilanden = 5071,
           
		[Description("Guinee Bissau")]
        GuineeBissau = 5072,
           
		[Description("Réunion")]
        RUnion = 5073,
           
		[Description("Saint Pierre en Miquelon")]
        SaintPierreEnMiquelon = 5074,
           
		[Description("Seychellen & Amiranten")]
        SeychellenAmiranten = 5075,
           
		[Description("Tonga")]
        Tonga = 5076,
           
		[Description("Wallis en Futuna")]
        WallisEnFutuna = 5077,
           
		[Description("Zuidwest Afrika")]
        ZuidwestAfrika = 5078,
           
		[Description("Frans Indië")]
        FransIndi = 5079,
           
		[Description("Johnston")]
        Johnston = 5080,
           
		[Description("Kedah")]
        Kedah = 5081,
           
		[Description("Kelantan")]
        Kelantan = 5082,
           
		[Description("Malakka")]
        Malakka = 5083,
           
		[Description("Mayotte")]
        Mayotte = 5084,
           
		[Description("Negri Sembilan")]
        NegriSembilan = 5085,
           
		[Description("Pahang")]
        Pahang = 5086,
           
		[Description("Perak")]
        Perak = 5087,
           
		[Description("Perlis")]
        Perlis = 5088,
           
		[Description("Portugees Indië")]
        PortugeesIndi = 5089,
           
		[Description("Selangor")]
        Selangor = 5090,
           
		[Description("Sikkim")]
        Sikkim = 5091,
           
		[Description("Sint Vincent en de Gren.")]
        SintVincentEnDeGren = 5092,
           
		[Description("Spitsbergen")]
        Spitsbergen = 5093,
           
		[Description("Trengganu")]
        Trengganu = 5094,
           
		[Description("Aruba")]
        Aruba = 5095,
           
		[Description("Burkina Faso")]
        BurkinaFaso = 5096,
           
		[Description("Azerbeidzjan")]
        Azerbeidzjan = 5097,
           
		[Description("Wit-Rusland")]
        WitRusland = 5098,
           
		[Description("Kazachstan")]
        Kazachstan = 5099,
           
		[Description("Macedonië")]
        Macedoni = 5100,
           
		[Description("Moldavië")]
        Moldavi = 6000,
           
		[Description("Burundi")]
        Burundi = 6001,
           
		[Description("Finland")]
        Finland = 6002,
           
		[Description("Griekenland")]
        Griekenland = 6003,
           
		[Description("Guatemala")]
        Guatemala = 6004,
           
		[Description("Nigeria")]
        Nigeria = 6005,
           
		[Description("Libië")]
        Libi = 6006,
           
		[Description("Ierland")]
        Ierland = 6007,
           
		[Description("Brazilië")]
        Brazili = 6008,
           
		[Description("Rwanda")]
        Rwanda = 6009,
           
		[Description("Venezuela")]
        Venezuela = 6010,
           
		[Description("IJsland")]
        Ijsland = 6011,
           
		[Description("Liechtenstein")]
        Liechtenstein = 6012,
           
		[Description("Somalië")]
        Somali = 6013,
           
		[Description("Ver. Staten v. Amerika")]
        VerStatenVAmerika = 6014,
           
		[Description("Bolivia")]
        Bolivia = 6015,
           
		[Description("Australië")]
        Australi = 6016,
           
		[Description("Jamaica")]
        Jamaica = 6017,
           
		[Description("Luxemburg")]
        Luxemburg = 6018,
           
		[Description("Tsjaad")]
        Tsjaad = 6019,
           
		[Description("Mauritanië")]
        Mauritani = 6020,
           
		[Description("Kirgizië")]
        Kirgizi = 6021,
           
		[Description("China")]
        China = 6022,
           
		[Description("Afghanistan")]
        Afghanistan = 6023,
           
		[Description("Indonesië")]
        Indonesi = 6024,
           
		[Description("Guyana")]
        Guyana = 6025,
           
		[Description("Noord-Viëtnam")]
        NoordViTnam = 6026,
           
		[Description("Noorwegen")]
        Noorwegen = 6027,
           
		[Description("San Marino")]
        SanMarino = 6028,
           
		[Description("Duitsland (oud)")]
        DuitslandOud = 6029,
           
		[Description("Nederland")]
        Nederland = 6030,
           
		[Description("Cambodja")]
        Cambodja = 6031,
           
		[Description("Fiji")]
        Fiji = 6032,
           
		[Description("Bahama-eilanden")]
        BahamaEilanden = 6033,
           
		[Description("Israël")]
        IsraL = 6034,
           
		[Description("Nepal")]
        Nepal = 6035,
           
		[Description("Zuid-Korea")]
        ZuidKorea = 6036,
           
		[Description("Spanje")]
        Spanje = 6037,
           
		[Description("Oekraïne")]
        OekraNe = 6038,
           
		[Description("Grootbrittannië")]
        Grootbrittanni = 6039,
           
		[Description("Niger")]
        Niger = 6040,
           
		[Description("Haïti")]
        HaTi = 6041,
           
		[Description("Jordanië")]
        Jordani = 6042,
           
		[Description("Turkije")]
        Turkije = 6043,
           
		[Description("Trinidad en Tobago")]
        TrinidadEnTobago = 6044,
           
		[Description("Joegoslavië")]
        Joegoslavi = 6045,
           
		[Description("Bovenvolta")]
        Bovenvolta = 6046,
           
		[Description("Algerije")]
        Algerije = 6047,
           
		[Description("Gabon")]
        Gabon = 6048,
           
		[Description("Noord-Korea")]
        NoordKorea = 6049,
           
		[Description("Oezbekistan")]
        Oezbekistan = 6050,
           
		[Description("Sierra Leone")]
        SierraLeone = 6051,
           
		[Description("Brits Honduras")]
        BritsHonduras = 6052,
           
		[Description("Canarische eilanden")]
        CanarischeEilanden = 6053,
           
		[Description("Frans-Polynesië")]
        FransPolynesi = 6054,
           
		[Description("Gibraltar")]
        Gibraltar = 6055,
           
		[Description("Portugees Timor")]
        PortugeesTimor = 6056,
           
		[Description("Tadzjikistan")]
        Tadzjikistan = 6057,
           
		[Description("Britse Salomons-eilanden")]
        BritseSalomonsEilanden = 6058,
           
		[Description("São Tomé en Principe")]
        SOTomEnPrincipe = 6059,
           
		[Description("Sint-Helena")]
        SintHelena = 6060,
           
		[Description("Tristan Da Cunha")]
        TristanDaCunha = 6061,
           
		[Description("Westsamoa")]
        Westsamoa = 6062,
           
		[Description("Turkmenistan")]
        Turkmenistan = 6063,
           
		[Description("Georgië")]
        Georgi = 6064,
           
		[Description("Bosnië-Herzegovina")]
        BosniHerzegovina = 6065,
           
		[Description("Tsjechië")]
        Tsjechi = 6066,
           
		[Description("Slowakije")]
        Slowakije = 6067,
           
		[Description("Fed. Rep. Joegoslavië")]
        FedRepJoegoslavi = 6068,
           
		[Description("Dem. Rep. Congo")]
        DemRepCongo = 6069,
           
		[Description("Uganda")]
        Uganda = 7001,
           
		[Description("Kenia")]
        Kenia = 7002,
           
		[Description("Malta")]
        Malta = 7003,
           
		[Description("Barbados")]
        Barbados = 7004,
           
		[Description("Andorra")]
        Andorra = 7005,
           
		[Description("Mexico")]
        Mexico = 7006,
           
		[Description("Costa Rica")]
        CostaRica = 7007,
           
		[Description("Gambia")]
        Gambia = 7008,
           
		[Description("Syrië")]
        Syri = 7009,
           
		[Description("Nederlandse Antillen")]
        NederlandseAntillen = 7011,
           
		[Description("Zuidjemen")]
        Zuidjemen = 7012,
           
		[Description("Egypte")]
        Egypte = 7014,
           
		[Description("Argentinië")]
        Argentini = 7015,
           
		[Description("Lesotho")]
        Lesotho = 7016,
           
		[Description("Honduras")]
        Honduras = 7017,
           
		[Description("Nicaragua")]
        Nicaragua = 7018,
           
		[Description("Pakistan")]
        Pakistan = 7020,
           
		[Description("Senegal")]
        Senegal = 7021,
           
		[Description("Dahomey")]
        Dahomey = 7023,
           
		[Description("Bulgarije")]
        Bulgarije = 7024,
           
		[Description("Maleisië")]
        Maleisi = 7026,
           
		[Description("Dominicaanse Republiek")]
        DominicaanseRepubliek = 7027,
           
		[Description("Polen")]
        Polen = 7028,
           
		[Description("Rusland (oud)")]
        RuslandOud = 7029,
           
		[Description("Britse Maagdeneilanden")]
        BritseMaagdeneilanden = 7030,
           
		[Description("Tanzania")]
        Tanzania = 7031,
           
		[Description("El Salvador")]
        ElSalvador = 7032,
           
		[Description("Sri Lanka")]
        SriLanka = 7033,
           
		[Description("Soedan")]
        Soedan = 7034,
           
		[Description("Japan")]
        Japan = 7035,
           
		[Description("Hongkong")]
        Hongkong = 7036,
           
		[Description("Panama")]
        Panama = 7037,
           
		[Description("Uruguay")]
        Uruguay = 7038,
           
		[Description("Ecuador")]
        Ecuador = 7039,
           
		[Description("Guinee")]
        Guinee = 7040,
           
		[Description("Maldiven")]
        Maldiven = 7041,
           
		[Description("Thailand")]
        Thailand = 7042,
           
		[Description("Libanon")]
        Libanon = 7043,
           
		[Description("Italië")]
        Itali = 7044,
           
		[Description("Koeweit")]
        Koeweit = 7045,
           
		[Description("India")]
        India = 7046,
           
		[Description("Roemenië")]
        Roemeni = 7047,
           
		[Description("Tsjecho-Slowakije")]
        TsjechoSlowakije = 7048,
           
		[Description("Peru")]
        Peru = 7049,
           
		[Description("Portugal")]
        Portugal = 7050,
           
		[Description("Oman")]
        Oman = 7051,
           
		[Description("Mongolië")]
        Mongoli = 7052,
           
		[Description("Verenigde Arabische Emiraten")]
        VerenigdeArabischeEmiraten = 7054,
           
		[Description("Tibet")]
        Tibet = 7055,
           
		[Description("Nauru")]
        Nauru = 7057,
           
		[Description("Ned. Nieuwguinea")]
        NedNieuwguinea = 7058,
           
		[Description("Tanganyika")]
        Tanganyika = 7059,
           
		[Description("Palestina")]
        Palestina = 7060,
           
		[Description("Brits Westindië")]
        BritsWestindi = 7062,
           
		[Description("Portugees Afrika")]
        PortugeesAfrika = 7063,
           
		[Description("Letland")]
        Letland = 7064,
           
		[Description("Estland")]
        Estland = 7065,
           
		[Description("Litouwen")]
        Litouwen = 7066,
           
		[Description("Brits Afrika")]
        BritsAfrika = 7067,
           
		[Description("Belgisch Congo")]
        BelgischCongo = 7068,
           
		[Description("Brits Indië")]
        BritsIndi = 7070,
           
		[Description("Noordrhodesië")]
        Noordrhodesi = 7071,
           
		[Description("Zuidrhodesië")]
        Zuidrhodesi = 7072,
           
		[Description("Saarland")]
        Saarland = 7073,
           
		[Description("Frans Indo China")]
        FransIndoChina = 7074,
           
		[Description("Brits Westborneo")]
        BritsWestborneo = 7075,
           
		[Description("Goudkust")]
        Goudkust = 7076,
           
		[Description("Ras-El-Cheima")]
        RasElCheima = 7077,
           
		[Description("Frans Kongo")]
        FransKongo = 7079,
           
		[Description("Siam")]
        Siam = 7080,
           
		[Description("Brits Oostafrika")]
        BritsOostafrika = 7082,
           
		[Description("Brits Noordborneo")]
        BritsNoordborneo = 7083,
           
		[Description("Bangladesh")]
        Bangladesh = 7084,
           
		[Description("Duitse Democr. Rep.")]
        DuitseDemocrRep = 7085,
           
		[Description("Madeira-eilanden")]
        MadeiraEilanden = 7087,
           
		[Description("Amerikaanse Maagdeneilanden")]
        AmerikaanseMaagdeneilanden = 7088,
           
		[Description("Australische Salomonseil.")]
        AustralischeSalomonseil = 7089,
           
		[Description("Spaanse Sahara")]
        SpaanseSahara = 7091,
           
		[Description("Caymaneilanden")]
        Caymaneilanden = 7092,
           
		[Description("Caicoseilanden")]
        Caicoseilanden = 7093,
           
		[Description("Turkseilanden")]
        Turkseilanden = 7094,
           
		[Description("Brits terr. in Antarctica")]
        BritsTerrInAntarctica = 7095,
           
		[Description("Brits Terr. i/d Ind. Oceaan")]
        BritsTerrIDIndOceaan = 7096,
           
		[Description("Cookeilanden")]
        Cookeilanden = 7097,
           
		[Description("Tokelau-eilanden")]
        TokelauEilanden = 7098,
           
		[Description("Nieuw-Caledonië")]
        NieuwCaledoni = 7099,
           
		[Description("Hawaii-eilanden")]
        HawaiiEilanden = 8000,
           
		[Description("Guam")]
        Guam = 8001,
           
		[Description("Amerikaans Samoa")]
        AmerikaansSamoa = 8002,
           
		[Description("Midway")]
        Midway = 8003,
           
		[Description("Ryukyueilanden")]
        Ryukyueilanden = 8004,
           
		[Description("Wake")]
        Wake = 8005,
           
		[Description("Pacific eilanden")]
        PacificEilanden = 8006,
           
		[Description("Grenada")]
        Grenada = 8008,
           
		[Description("Marianen")]
        Marianen = 8009,
           
		[Description("Cabinda")]
        Cabinda = 8010,
           
		[Description("Canton en Enderbury")]
        CantonEnEnderbury = 8011,
           
		[Description("Christmaseiland")]
        Christmaseiland = 8012,
           
		[Description("Cocoseilanden")]
        Cocoseilanden = 8013,
           
		[Description("Faeröer")]
        FaerEr = 8014,
           
		[Description("Montserrat")]
        Montserrat = 8015,
           
		[Description("Norfolk")]
        Norfolk = 8016,
           
		[Description("Belize")]
        Belize = 8017,
           
		[Description("Tasmanië")]
        Tasmani = 8018,
           
		[Description("Turks- en Caicoseil.")]
        TurksEnCaicoseil = 8019,
           
		[Description("Puerto Rico")]
        PuertoRico = 8020,
           
		[Description("Papua-Nieuwguinea")]
        PapuaNieuwguinea = 8021,
           
		[Description("Solomoneilanden")]
        Solomoneilanden = 8022,
           
		[Description("Benin")]
        Benin = 8023,
           
		[Description("Viëtnam")]
        ViTnam = 8024,
           
		[Description("Kaapverdië")]
        Kaapverdi = 8025,
           
		[Description("Seychellen")]
        Seychellen = 8026,
           
		[Description("Kiribati")]
        Kiribati = 8027,
           
		[Description("Tuvalu")]
        Tuvalu = 8028,
           
		[Description("Sint Lucia")]
        SintLucia = 8029,
           
		[Description("Dominica")]
        Dominica = 8030,
           
		[Description("Zimbabwe")]
        Zimbabwe = 8031,
           
		[Description("Doebai")]
        Doebai = 8032,
           
		[Description("Nieuwehebriden")]
        Nieuwehebriden = 8033,
           
		[Description("Kanaaleilanden")]
        Kanaaleilanden = 8034,
           
		[Description("Man")]
        Man = 8035,
           
		[Description("Anguilla")]
        Anguilla = 8036,
           
		[Description("Saint Kitts-Nevis")]
        SaintKittsNevis = 8037,
           
		[Description("Antigua")]
        Antigua = 8038,
           
		[Description("Sint Vincent")]
        SintVincent = 8039,
           
		[Description("Gilberteilanden")]
        Gilberteilanden = 8040,
           
		[Description("Panama Kanaalzone")]
        PanamaKanaalzone = 8041,
           
		[Description("Saint Kitts-Nevis-Anguilla")]
        SaintKittsNevisAnguilla = 8042,
           
		[Description("Belau")]
        Belau = 8043,
           
		[Description("Republiek van Palau")]
        RepubliekVanPalau = 8044,
           
		[Description("Antigua en Barbuda")]
        AntiguaEnBarbuda = 8045,
           
		[Description("Newfoundland")]
        Newfoundland = 9000,
           
		[Description("Nyasaland")]
        Nyasaland = 9001,
           
		[Description("Eritrea")]
        Eritrea = 9003,
           
		[Description("Ifni")]
        Ifni = 9005,
           
		[Description("Brits Kameroen")]
        BritsKameroen = 9006,
           
		[Description("Kaiser Wilhelmsland")]
        KaiserWilhelmsland = 9007,
           
		[Description("Kongo")]
        Kongo = 9008,
           
		[Description("Kongo Kinshasa")]
        KongoKinshasa = 9009,
           
		[Description("Madagaskar")]
        Madagaskar = 9010,
           
		[Description("Kongo Brazaville")]
        KongoBrazaville = 9013,
           
		[Description("Leeuwardeilanden")]
        Leeuwardeilanden = 9014,
           
		[Description("Windwardeilanden")]
        Windwardeilanden = 9015,
           
		[Description("Frans geb. Afars en Issa's")]
        FransGebAfarsEnIssaS = 9016,
           
		[Description("Phoenixeilanden")]
        Phoenixeilanden = 9017,
           
		[Description("Portugees Guinee")]
        PortugeesGuinee = 9020,
           
		[Description("Duits Zuidwestafrika")]
        DuitsZuidwestafrika = 9022,
           
		[Description("Namibië")]
        Namibi = 9023,
           
		[Description("Brits Somaliland")]
        BritsSomaliland = 9027,
           
		[Description("Italiaans Somaliland")]
        ItaliaansSomaliland = 9028,
           
		[Description("Nederlands Indië")]
        NederlandsIndi = 9030,
           
		[Description("Brits Guyana")]
        BritsGuyana = 9031,
           
		[Description("Swaziland")]
        Swaziland = 9036,
           
		[Description("Katar")]
        Katar = 9037,
           
		[Description("Aden")]
        Aden = 9041,
           
		[Description("Zuidarabische Fed.")]
        ZuidarabischeFed = 9042,
           
		[Description("Equatoriaalguinee")]
        Equatoriaalguinee = 9043,
           
		[Description("Spaans Guinee")]
        SpaansGuinee = 9044,
           
		[Description("Ver. Arab. Republiek")]
        VerArabRepubliek = 9047,
           
		[Description("Bermuda")]
        Bermuda = 9048,
           
		[Description("Sovjet-Unie")]
        SovjetUnie = 9049,
           
		[Description("Duits Oostafrika")]
        DuitsOostafrika = 9050,
           
		[Description("Zanzibar")]
        Zanzibar = 9051,
           
		[Description("Ceylon")]
        Ceylon = 9052,
           
		[Description("Muscat en Oman")]
        MuscatEnOman = 9053,
           
		[Description("Trucial Oman")]
        TrucialOman = 9054,
           
		[Description("Indo China")]
        IndoChina = 9055,
           
		[Description("Marshalleilanden")]
        Marshalleilanden = 9056,
           
		[Description("Sarawak")]
        Sarawak = 9057,
           
		[Description("Brits Borneo")]
        BritsBorneo = 9058,
           
		[Description("Sabah")]
        Sabah = 9060,
           
		[Description("Aboe Dhabi")]
        AboeDhabi = 9061,
           
		[Description("Adjman")]
        Adjman = 9062,
           
		[Description("Basoetoland")]
        Basoetoland = 9063,
           
		[Description("Bechuanaland")]
        Bechuanaland = 9064,
           
		[Description("Foedjaira")]
        Foedjaira = 9065,
           
		[Description("Frans Kameroen")]
        FransKameroen = 9066,
           
		[Description("Johore")]
        Johore = 9067,
           
		[Description("Korea")]
        Korea = 9068,
           
		[Description("Labuan")]
        Labuan = 9069,
           
		[Description("Oem el Koewein")]
        OemElKoewein = 9070,
           
		[Description("Oostenrijk-Hongarije")]
        OostenrijkHongarije = 9071,
           
		[Description("Portugees Oost Afrika")]
        PortugeesOostAfrika = 9072,
           
		[Description("Portugees West Afrika")]
        PortugeesWestAfrika = 9073,
           
		[Description("Sjardja")]
        Sjardja = 9074,
           
		[Description("Straits Settlements")]
        StraitsSettlements = 9075,
           
		[Description("Abessinië")]
        Abessini = 9076,
           
		[Description("Frans West Afrika")]
        FransWestAfrika = 9077,
           
		[Description("Fr. Equatoriaal Afrika")]
        FrEquatoriaalAfrika = 9078,
           
		[Description("Oeroendi")]
        Oeroendi = 9081,
           
		[Description("Roeanda-Oeroendi")]
        RoeandaOeroendi = 9082,
           
		[Description("Goa")]
        Goa = 9084,
           
		[Description("Dantzig")]
        Dantzig = 9085,
           
		[Description("Centrafrika")]
        Centrafrika = 9086,
           
		[Description("Djibouti")]
        Djibouti = 9087,
           
		[Description("Transjordanië")]
        Transjordani = 9088,
           
		[Description("Bondsrepubliek Duitsland")]
        BondsrepubliekDuitsland = 9089,
           
		[Description("Vanuatu")]
        Vanuatu = 9090,
           
		[Description("Niue")]
        Niue = 9091,
           
		[Description("Spaans Noordafrika")]
        SpaansNoordafrika = 9092,
           
		[Description("Westelijke Sahara")]
        WestelijkeSahara = 9093,
           
		[Description("Micronesia")]
        Micronesia = 9094,
           
		[Description("Svalbardeilanden")]
        Svalbardeilanden = 9095,
           
		[Description("Internationaal gebied")]
        InternationaalGebied = 9999,
	} 
	
		/// <summary>
	/// DataValNummer = 155 
	/// Ligging van het kind155 
	/// </summary>
	[Serializable]
	 
	public enum LiggingVanHetKind155
	{   	 

			 

			           
		[Description("Achterhoofd voor")]
        AchterhoofdVoor = 1,
           
		[Description("Achterhoofd achter")]
        AchterhoofdAchter = 2,
           
		[Description("Kruin")]
        Kruin = 3,
           
		[Description("Aangezicht")]
        Aangezicht = 4,
           
		[Description("Hoofdligging anders")]
        HoofdliggingAnders = 5,
           
		[Description("Volkomen stuit")]
        VolkomenStuit = 6,
           
		[Description("Onvolkomen stuit")]
        OnvolkomenStuit = 7,
           
		[Description("Dwars")]
        Dwars = 8,
           
		[Description("Anders")]
        Anders = 9,
           
		[Description("Onbekend")]
        Onbekend = 10,
           
		[Description("Voorhoofd")]
        Voorhoofd = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 156 
	/// links/rechts/beiderzijds156 
	/// </summary>
	[Serializable]
	 
	public enum LinksRechtsBeiderzijds156
	{   	 

			 

			           
		[Description("Links")]
        Links = 2,
           
		[Description("Rechts")]
        Rechts = 3,
           
		[Description("Beiderzijds")]
        Beiderzijds = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 160 
	/// Redenen afwijkend vaccinatieschema160 
	/// </summary>
	[Serializable]
	 
	public enum RedenenAfwijkendVaccinatieschema160
	{   	 

			 

			           
		[Description("Anders")]
        Anders = 0,
           
		[Description("Medische indicatie")]
        MedischeIndicatie = 1,
           
		[Description("Verzoek ouders")]
        VerzoekOuders = 2,
           
		[Description("Komst uit buitenland")]
        KomstUitBuitenland = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 161 
	/// Bijzonderheden voeten161 
	/// </summary>
	[Serializable]
	 
	public enum BijzonderhedenVoeten161
	{   	 

			 

			           
		[Description("Klompvoet")]
        Klompvoet = 1,
           
		[Description("Platvoet corrigeerbaar")]
        PlatvoetCorrigeerbaar = 2,
           
		[Description("Platvoet niet corrigeerbaar")]
        PlatvoetNietCorrigeerbaar = 3,
           
		[Description("Te korte achillespees")]
        TeKorteAchillespees = 4,
           
		[Description("Anders")]
        Anders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 164 
	/// Taal die ouder met kind spreekt164 
	/// </summary>
	[Serializable]
	 
	public enum TaalDieOuderMetKindSpreekt164
	{   	 

			 

			           
		[Description("Nederlands")]
        Nederlands = 1,
           
		[Description("Nederlands en andere taal")]
        NederlandsEnAndereTaal = 2,
           
		[Description("Andere taal")]
        AndereTaal = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 165 
	/// Roken in nabijheid van kind165 
	/// </summary>
	[Serializable]
	 
	public enum RokenInNabijheidVanKind165
	{   	 

			 

			           
		[Description("Nee, er wordt nooit gerookt")]
        NeeErWordtNooitGerookt = 1,
           
		[Description("Nee, niet in afgelopen 7 dagen")]
        NeeNietInAfgelopen7Dagen = 2,
           
		[Description("Ja")]
        Ja = 3,
           
		[Description("Nee, nooit als kind er bij is")]
        NeeNooitAlsKindErBijIs = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 166 
	/// Ja, nee, onbekend166 
	/// </summary>
	[Serializable]
	 
	public enum JaNeeOnbekend166
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
           
		[Description("Onbekend")]
        Onbekend = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 167 
	/// Meertalig opvoeden167 
	/// </summary>
	[Serializable]
	 
	public enum MeertaligOpvoeden167
	{   	 

			 

			           
		[Description("Geen")]
        Geen = 1,
           
		[Description("Simultane twee/meertaligheid")]
        SimultaneTweeMeertaligheid = 2,
           
		[Description("Successieve twee/meertaligheid")]
        SuccessieveTweeMeertaligheid = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 169 
	/// Uitslag hielprik169 
	/// </summary>
	[Serializable]
	 
	public enum UitslagHielprik169
	{   	 

			 

			           
		[Description("Negatief")]
        Negatief = 1,
           
		[Description("Dubieus")]
        Dubieus = 2,
           
		[Description("Afwijkend")]
        Afwijkend = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 170 
	/// Klinische blik (over)gewicht170 
	/// </summary>
	[Serializable]
	 
	public enum KlinischeBlikOverGewicht170
	{   	 

			 

			           
		[Description("Normaal gewicht")]
        NormaalGewicht = 1,
           
		[Description("Overgewicht")]
        Overgewicht = 2,
           
		[Description("Ondergewicht")]
        Ondergewicht = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 171 
	/// Bijz knieen171 
	/// </summary>
	[Serializable]
	 
	public enum BijzKnieen171
	{   	 

			 

			           
		[Description("O benen")]
        OBenen = 1,
           
		[Description("X benen")]
        XBenen = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 173 
	/// Hartgeruis intensiteit173 
	/// </summary>
	[Serializable]
	 
	public enum HartgeruisIntensiteit173
	{   	 

			 

			           
		[Description("1/6")]
        Waarde16 = 1,
           
		[Description("2/6")]
        Waarde26 = 2,
           
		[Description("3/6")]
        Waarde36 = 3,
           
		[Description("4/6")]
        Waarde46 = 4,
           
		[Description("5/6")]
        Waarde56 = 5,
           
		[Description("6/6")]
        Waarde66 = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 174 
	/// Redenen 2e hielprik174 
	/// </summary>
	[Serializable]
	 
	public enum Redenen2eHielprik174
	{   	 

			 

			           
		[Description("Onvoldoende vulling")]
        OnvoldoendeVulling = 1,
           
		[Description("Te vroeg geprikt")]
        TeVroegGeprikt = 2,
           
		[Description("Eerste set niet aangekomen")]
        EersteSetNietAangekomen = 3,
           
		[Description("Uitslag dubieus")]
        UitslagDubieus = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 176 
	/// A. femorales176 
	/// </summary>
	[Serializable]
	 
	public enum AFemorales176
	{   	 

			 

			           
		[Description("1) Palpabel")]
        Palpabel1 = 1,
           
		[Description("2) Niet palpabel")]
        NietPalpabel2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 178 
	/// Tanden poetsen178 
	/// </summary>
	[Serializable]
	 
	public enum TandenPoetsen178
	{   	 

			 

			           
		[Description("2 maal per dag of vaker")]
        MaalPerDagOfVaker2 = 1,
           
		[Description("1 maal per dag")]
        MaalPerDag1 = 2,
           
		[Description("Niet elke dag")]
        NietElkeDag = 3,
           
		[Description("Nooit")]
        Nooit = 4,
           
		[Description("N.v.t.")]
        NVT = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 180 
	/// Onderzoek testis180 
	/// </summary>
	[Serializable]
	 
	public enum OnderzoekTestis180
	{   	 

			 

			           
		[Description("Retractiele testis")]
        RetractieleTestis = 1,
           
		[Description("Aangeboren niet-scrotale testis")]
        AangeborenNietScrotaleTestis = 2,
           
		[Description("Niet palpabele, verworven niet-scrotale testis")]
        NietPalpabeleVerworvenNietScrotaleTestis = 3,
           
		[Description("Palpabele, verworven niet-scrotale testis")]
        PalpabeleVerworvenNietScrotaleTestis = 4,
           
		[Description("Anders")]
        Anders = 5,
           
		[Description("Stabiel scrotaal")]
        StabielScrotaal = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 182 
	/// Onderzoek genitalia externa meisje182 
	/// </summary>
	[Serializable]
	 
	public enum OnderzoekGenitaliaExternaMeisje182
	{   	 

			 

			           
		[Description("Synechiae")]
        Synechiae = 1,
           
		[Description("Besneden")]
        Besneden = 2,
           
		[Description("Anders")]
        Anders = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 185 
	/// Melkvoeding185 
	/// </summary>
	[Serializable]
	 
	public enum Melkvoeding185
	{   	 

			 

			           
		[Description("Uitsluitend borstvoeding")]
        UitsluitendBorstvoeding = 1,
           
		[Description("Gemengde voeding")]
        GemengdeVoeding = 2,
           
		[Description("Kunstvoeding")]
        Kunstvoeding = 3,
           
		[Description("Borstvoeding en bijvoeding")]
        BorstvoedingEnBijvoeding = 5,
           
		[Description("Gemengde voeding en bijvoeding")]
        GemengdeVoedingEnBijvoeding = 6,
           
		[Description("Kunstvoeding en bijvoeding")]
        KunstvoedingEnBijvoeding = 7,
           
		[Description("Anders")]
        Anders = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 186 
	/// Onderzoek lever/milt186 
	/// </summary>
	[Serializable]
	 
	public enum OnderzoekLeverMilt186
	{   	 

			 

			           
		[Description("Vergroot")]
        Vergroot = 1,
           
		[Description("Niet vergroot")]
        NietVergroot = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 188 
	/// Werk ouder188 
	/// </summary>
	[Serializable]
	 
	public enum WerkOuder188
	{   	 

			 

			           
		[Description("Verricht betaald werk")]
        VerrichtBetaaldWerk = 1,
           
		[Description("Verricht geen betaald werk")]
        VerrichtGeenBetaaldWerk = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 190 
	/// Uitslag screeningsinstrument (vold,onv)190 
	/// </summary>
	[Serializable]
	 
	public enum UitslagScreeningsinstrumentVoldOnv190
	{   	 

			 

			           
		[Description("Voldoende")]
        Voldoende = 1,
           
		[Description("Onvoldoende")]
        Onvoldoende = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 191 
	/// Wijze van bevalling191 
	/// </summary>
	[Serializable]
	 
	public enum WijzeVanBevalling191
	{   	 

			 

			           
		[Description("Spontaan")]
        Spontaan = 1,
           
		[Description("Ingeleid")]
        Ingeleid = 2,
           
		[Description("Vacuumbevalling")]
        Vacuumbevalling = 3,
           
		[Description("Tangverlossing")]
        Tangverlossing = 4,
           
		[Description("Keizersnede")]
        Keizersnede = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 194 
	/// Reflexbeeldjes194 
	/// </summary>
	[Serializable]
	 
	public enum Reflexbeeldjes194
	{   	 

			 

			           
		[Description("Links asym")]
        LinksAsym = 1,
           
		[Description("Rechts asym")]
        RechtsAsym = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 195 
	/// Instel- en herstelbeweging VOV195 
	/// </summary>
	[Serializable]
	 
	public enum InstelEnHerstelbewegingVov195
	{   	 

			 

			           
		[Description("Links")]
        Links = 1,
           
		[Description("Rechts")]
        Rechts = 2,
           
		[Description("Beiderzijds")]
        Beiderzijds = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 196 
	/// VOV onvold links, onvold rechts196 
	/// </summary>
	[Serializable]
	 
	public enum VovOnvoldLinksOnvoldRechts196
	{   	 

			 

			           
		[Description("Rechts onvoldoende")]
        RechtsOnvoldoende = 1,
           
		[Description("Links onvoldoende")]
        LinksOnvoldoende = 2,
           
		[Description("Beiderzijds onvoldoende")]
        BeiderzijdsOnvoldoende = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 198 
	/// Begeleider 1198 
	/// </summary>
	[Serializable]
	 
	public enum Begeleider1198
	{   	 

			 

			           
		[Description("Moeder")]
        Moeder = 1,
           
		[Description("Vader")]
        Vader = 2,
           
		[Description("Oma/opa")]
        OmaOpa = 3,
           
		[Description("Zorgverlener")]
        Zorgverlener = 4,
           
		[Description("(Gezins)voogd")]
        GezinsVoogd = 5,
           
		[Description("Oppas")]
        Oppas = 6,
           
		[Description("Vriend(in)")]
        VriendIn = 7,
           
		[Description("Familielid")]
        Familielid = 8,
           
		[Description("Broertje(s)/zusje(s)")]
        BroertjeSZusjeS = 9,
           
		[Description("Pleegmoeder")]
        Pleegmoeder = 10,
           
		[Description("Pleegvader")]
        Pleegvader = 11,
           
		[Description("Stiefmoeder")]
        Stiefmoeder = 12,
           
		[Description("Stiefvader")]
        Stiefvader = 13,
           
		[Description("Anders")]
        Anders = 14,
	} 
	
		/// <summary>
	/// DataValNummer = 199 
	/// Begeleider 2199 
	/// </summary>
	[Serializable]
	 
	public enum Begeleider2199
	{   	 

			 

			           
		[Description("N.v.t.")]
        NVT = 1,
           
		[Description("Moeder")]
        Moeder = 2,
           
		[Description("Vader")]
        Vader = 3,
           
		[Description("Zorgverlener")]
        Zorgverlener = 5,
           
		[Description("(Gezins) voogd")]
        GezinsVoogd = 6,
           
		[Description("Oppas")]
        Oppas = 7,
           
		[Description("Vriend(in)")]
        VriendIn = 8,
           
		[Description("Andere familileden")]
        AndereFamilileden = 9,
           
		[Description("Andere kinderen")]
        AndereKinderen = 10,
           
		[Description("Pleegmoeder")]
        Pleegmoeder = 11,
           
		[Description("Pleegvader")]
        Pleegvader = 12,
           
		[Description("Stiefmoeder")]
        Stiefmoeder = 13,
           
		[Description("Stiefvader")]
        Stiefvader = 14,
	} 
	
		/// <summary>
	/// DataValNummer = 200 
	/// Ligging laatste trimester zwangerschap200 
	/// </summary>
	[Serializable]
	 
	public enum LiggingLaatsteTrimesterZwangerschap200
	{   	 

			 

			           
		[Description("Geen bijzonderheden")]
        GeenBijzonderheden = 1,
           
		[Description("Stuitligging")]
        Stuitligging = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 202 
	/// Redenen hielprik niet uitgevoerd202 
	/// </summary>
	[Serializable]
	 
	public enum RedenenHielprikNietUitgevoerd202
	{   	 

			 

			           
		[Description("Onbekend")]
        Onbekend = 0,
           
		[Description("Geen bezwaar")]
        GeenBezwaar = 1,
           
		[Description("Medisch bezwaar")]
        MedischBezwaar = 2,
           
		[Description("Principieel bezwaar")]
        PrincipieelBezwaar = 3,
           
		[Description("Bezwaar reden onbekend")]
        BezwaarRedenOnbekend = 4,
           
		[Description("Onderzoek elders uitgevoerd")]
        OnderzoekEldersUitgevoerd = 5,
           
		[Description("Ziekte(s) doorgemaakt")]
        ZiekteSDoorgemaakt = 6,
           
		[Description("Overleden")]
        Overleden = 7,
           
		[Description("Vertrokken")]
        Vertrokken = 8,
           
		[Description("Dubbel uitgeschreven")]
        DubbelUitgeschreven = 9,
	} 
	
		/// <summary>
	/// DataValNummer = 205 
	/// Hartgeruis timing205 
	/// </summary>
	[Serializable]
	 
	public enum HartgeruisTiming205
	{   	 

			 

			           
		[Description("Systolisch")]
        Systolisch = 1,
           
		[Description("Diastolisch")]
        Diastolisch = 2,
           
		[Description("Holosystolisch")]
        Holosystolisch = 3,
           
		[Description("Continu geruis")]
        ContinuGeruis = 4,
           
		[Description("Onduidelijk")]
        Onduidelijk = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 206 
	/// Interactie bij Van Wiechenonderzoek206 
	/// </summary>
	[Serializable]
	 
	public enum InteractieBijVanWiechenonderzoek206
	{   	 

			 

			           
		[Description("Cooperatief")]
        Cooperatief = 1,
           
		[Description("Terughoudend, moet gestimuleerd worden")]
        TerughoudendMoetGestimuleerdWorden = 2,
           
		[Description("Verlegen, terughoudend zonder actief verzet")]
        VerlegenTerughoudendZonderActiefVerzet = 3,
           
		[Description("Actief verzet")]
        ActiefVerzet = 4,
           
		[Description("Anders")]
        Anders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 207 
	/// Gedragstoestand bij Van Wiechenonderzoek207 
	/// </summary>
	[Serializable]
	 
	public enum GedragstoestandBijVanWiechenonderzoek207
	{   	 

			 

			           
		[Description("Wakker en alert")]
        WakkerEnAlert = 1,
           
		[Description("Maakt vermoeide indruk")]
        MaaktVermoeideIndruk = 2,
           
		[Description("Huilerig")]
        Huilerig = 3,
           
		[Description("Huilt door")]
        HuiltDoor = 4,
           
		[Description("Anders")]
        Anders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 208 
	/// Visuskaarten208 
	/// </summary>
	[Serializable]
	 
	public enum Visuskaarten208
	{   	 

			 

			           
		[Description("5 meter APK kaart")]
        MeterApkKaart5 = 1,
           
		[Description("3 meter APK-TOV kaart")]
        MeterApkTovKaart3 = 2,
           
		[Description("Landolt C kaart")]
        LandoltCKaart = 3,
           
		[Description("LH kaart")]
        LhKaart = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 223 
	/// Articulatie223 
	/// </summary>
	[Serializable]
	 
	public enum Articulatie223
	{   	 

			 

			           
		[Description("Fonetische articulatiestoornis")]
        FonetischeArticulatiestoornis = 1,
           
		[Description("Fonologische articulatiestoornis")]
        FonologischeArticulatiestoornis = 2,
           
		[Description("Algemene articulatiestoornis")]
        AlgemeneArticulatiestoornis = 3,
           
		[Description("Anders")]
        Anders = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 231 
	/// Manier verkrijgen lengte ouder231 
	/// </summary>
	[Serializable]
	 
	public enum ManierVerkrijgenLengteOuder231
	{   	 

			 

			           
		[Description("Gemeten")]
        Gemeten = 1,
           
		[Description("Anamnestisch")]
        Anamnestisch = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 232 
	/// Reden indicatie Hepatitis B232 
	/// </summary>
	[Serializable]
	 
	public enum RedenIndicatieHepatitisB232
	{   	 

			 

			           
		[Description("Geboren na 01-08-2011")]
        GeborenNa01082011 = 1,
           
		[Description("HbsAg-positieve moeder")]
        HbsagPositieveMoeder = 2,
           
		[Description("Ouder(s) geboren in land waar Hep B endemisch is")]
        OuderSGeborenInLandWaarHepBEndemischIs = 3,
           
		[Description("Syndroom v Down")]
        SyndroomVDown = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 233 
	/// Geen PSZ bezoek233 
	/// </summary>
	[Serializable]
	 
	public enum GeenPszBezoek233
	{   	 

			 

			           
		[Description("Geen belangstelling")]
        GeenBelangstelling = 1,
           
		[Description("Financiele drempel")]
        FinancieleDrempel = 2,
           
		[Description("Afstand te groot")]
        AfstandTeGroot = 3,
           
		[Description("Gaat naar KDV")]
        GaatNaarKdv = 4,
           
		[Description("Andere vorm van oppas aanwezig")]
        AndereVormVanOppasAanwezig = 5,
           
		[Description("Wachtlijst")]
        Wachtlijst = 6,
           
		[Description("Anders")]
        Anders = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 234 
	/// Uitslag Hep B serologie234 
	/// </summary>
	[Serializable]
	 
	public enum UitslagHepBSerologie234
	{   	 

			 

			           
		[Description("N.v.t.")]
        NVT = 1,
           
		[Description("Voldoende")]
        Voldoende = 2,
           
		[Description("Onvoldoende")]
        Onvoldoende = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 235 
	/// Besneden?235 
	/// </summary>
	[Serializable]
	 
	public enum Besneden235
	{   	 

			 

			           
		[Description("Ja, besneden")]
        JaBesneden = 1,
           
		[Description("Nee, niet besneden")]
        NeeNietBesneden = 2,
           
		[Description("Onduidelijk")]
        Onduidelijk = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 236 
	/// Vormen meisjesbesnijdenis236 
	/// </summary>
	[Serializable]
	 
	public enum VormenMeisjesbesnijdenis236
	{   	 

			 

			           
		[Description("N.v.t.")]
        NVT = 1,
           
		[Description("Incisie")]
        Incisie = 2,
           
		[Description("Clitorectomie")]
        Clitorectomie = 3,
           
		[Description("Excisie")]
        Excisie = 4,
           
		[Description("Infibulatie")]
        Infibulatie = 5,
           
		[Description("Onduidelijk")]
        Onduidelijk = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 237 
	/// Risico meisjesbesnijdenis237 
	/// </summary>
	[Serializable]
	 
	public enum RisicoMeisjesbesnijdenis237
	{   	 

			 

			           
		[Description("Nee, geen risico")]
        NeeGeenRisico = 1,
           
		[Description("Ja, risico aanwezig")]
        JaRisicoAanwezig = 2,
           
		[Description("Risico twijfelachtig")]
        RisicoTwijfelachtig = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 249 
	/// Dagdelen kinderopvang249 
	/// </summary>
	[Serializable]
	 
	public enum DagdelenKinderopvang249
	{   	 

			 

			           
		[Description("1")]
        Waarde1 = 1,
           
		[Description("2")]
        Waarde2 = 2,
           
		[Description("3")]
        Waarde3 = 3,
           
		[Description("4")]
        Waarde4 = 4,
           
		[Description("5")]
        Waarde5 = 5,
           
		[Description("6")]
        Waarde6 = 6,
           
		[Description("7")]
        Waarde7 = 7,
           
		[Description("8")]
        Waarde8 = 8,
           
		[Description("9")]
        Waarde9 = 9,
           
		[Description("10")]
        Waarde10 = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 250 
	/// Lengtemeting250 
	/// </summary>
	[Serializable]
	 
	public enum Lengtemeting250
	{   	 

			 

			           
		[Description("Liggend")]
        Liggend = 1,
           
		[Description("Staand")]
        Staand = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 252 
	/// Lateralisatie252 
	/// </summary>
	[Serializable]
	 
	public enum Lateralisatie252
	{   	 

			 

			           
		[Description("Rechtshandig")]
        Rechtshandig = 1,
           
		[Description("Linkshandig")]
        Linkshandig = 2,
           
		[Description("Geen voorkeur")]
        GeenVoorkeur = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 255 
	/// Beoordeling STO Nederlands255 
	/// </summary>
	[Serializable]
	 
	public enum BeoordelingStoNederlands255
	{   	 

			 

			           
		[Description("Leefstijdsadequaat of sneller")]
        LeefstijdsadequaatOfSneller = 1,
           
		[Description("Vertraagd")]
        Vertraagd = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 256 
	/// Ervaren gezondheid256 
	/// </summary>
	[Serializable]
	 
	public enum ErvarenGezondheid256
	{   	 

			 

			           
		[Description("Heel goed")]
        HeelGoed = 0,
           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Gaat wel")]
        GaatWel = 2,
           
		[Description("Niet zo best")]
        NietZoBest = 3,
           
		[Description("Slecht")]
        Slecht = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 260 
	/// Bijzonderheden trommelvliezen (4-19)260 
	/// </summary>
	[Serializable]
	 
	public enum BijzonderhedenTrommelvliezen419260
	{   	 

			 

			           
		[Description("Bomberend")]
        Bomberend = 1,
           
		[Description("Roodheid")]
        Roodheid = 2,
           
		[Description("Intrekking")]
        Intrekking = 3,
           
		[Description("Perforatie")]
        Perforatie = 4,
           
		[Description("Loopoor")]
        Loopoor = 5,
           
		[Description("Beluchtingsbuisjes")]
        Beluchtingsbuisjes = 6,
           
		[Description("Anders")]
        Anders = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 262 
	/// Scoliose hoekmeting262 
	/// </summary>
	[Serializable]
	 
	public enum ScolioseHoekmeting262
	{   	 

			 

			           
		[Description("Hoek < 4 graden")]
        Hoek4Graden = 0,
           
		[Description("Hoek 4 tot 7 graden")]
        Hoek4Tot7Graden = 1,
           
		[Description("Hoek gelijk of groter dan 7 graden")]
        HoekGelijkOfGroterDan7Graden = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 265 
	/// Methode gewichtsmeting265 
	/// </summary>
	[Serializable]
	 
	public enum MethodeGewichtsmeting265
	{   	 

			 

			           
		[Description("Met kleren gewogen")]
        MetKlerenGewogen = 1,
           
		[Description("Zonder kleren gewogen")]
        ZonderKlerenGewogen = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 266 
	/// Geluidssterkte gehooronderzoek266 
	/// </summary>
	[Serializable]
	 
	public enum GeluidssterkteGehooronderzoek266
	{   	 

			 

			           
		[Description("15 dB")]
        Db15 = 1,
           
		[Description("20 dB")]
        Db20 = 2,
           
		[Description("25 dB")]
        Db25 = 3,
           
		[Description("30 dB")]
        Db30 = 4,
           
		[Description("35 dB")]
        Db35 = 5,
           
		[Description("40 dB")]
        Db40 = 6,
           
		[Description("45 dB")]
        Db45 = 7,
           
		[Description("50 dB")]
        Db50 = 8,
           
		[Description("55 dB")]
        Db55 = 9,
           
		[Description("60 dB")]
        Db60 = 10,
           
		[Description("65 dB")]
        Db65 = 11,
           
		[Description("70 dB")]
        Db70 = 12,
           
		[Description("75dB")]
        db75 = 13,
           
		[Description("80 dB")]
        Db80 = 14,
	} 
	
		/// <summary>
	/// DataValNummer = 267 
	/// APK kaart uitslag267 
	/// </summary>
	[Serializable]
	 
	public enum ApkKaartUitslag267
	{   	 

			 

			           
		[Description("5/30")]
        Waarde530 = 1,
           
		[Description("5/20")]
        Waarde520 = 2,
           
		[Description("5/15")]
        Waarde515 = 3,
           
		[Description("5/10")]
        Waarde510 = 4,
           
		[Description("5/6")]
        Waarde56 = 5,
           
		[Description("5/5")]
        Waarde55 = 6,
           
		[Description("Wil niet")]
        WilNiet = 9,
           
		[Description("Niet gelukt, niet geconcentreerd")]
        NietGeluktNietGeconcentreerd = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 268 
	/// APK-TOV kaart uitslag268 
	/// </summary>
	[Serializable]
	 
	public enum ApkTovKaartUitslag268
	{   	 

			 

			           
		[Description("3/30")]
        Waarde330 = 1,
           
		[Description("3/20")]
        Waarde320 = 2,
           
		[Description("3/15")]
        Waarde315 = 3,
           
		[Description("3/10")]
        Waarde310 = 4,
           
		[Description("3/6")]
        Waarde36 = 5,
           
		[Description("3/5")]
        Waarde35 = 6,
           
		[Description("3/4")]
        Waarde34 = 7,
           
		[Description("3/3")]
        Waarde33 = 8,
           
		[Description("Wl niet")]
        WlNiet = 9,
           
		[Description("Lukt niet, niet geconcentreerd")]
        LuktNietNietGeconcentreerd = 10,
	} 
	
		/// <summary>
	/// DataValNummer = 270 
	/// Landolt-C kaart uitslag270 
	/// </summary>
	[Serializable]
	 
	public enum LandoltCKaartUitslag270
	{   	 

			 

			           
		[Description("0,1")]
        Waarde01 = 1,
           
		[Description("0,12")]
        Waarde012 = 2,
           
		[Description("0,15")]
        Waarde015 = 3,
           
		[Description("0,2")]
        Waarde02 = 4,
           
		[Description("0,25")]
        Waarde025 = 5,
           
		[Description("0,3")]
        Waarde03 = 6,
           
		[Description("0,4")]
        Waarde04 = 7,
           
		[Description("0,5")]
        Waarde05 = 8,
           
		[Description("0,65")]
        Waarde065 = 9,
           
		[Description("0,8")]
        Waarde08 = 10,
           
		[Description("1,0")]
        Waarde10 = 11,
           
		[Description("1,25")]
        Waarde125 = 12,
           
		[Description("1,5")]
        Waarde15 = 13,
           
		[Description("2,0")]
        Waarde20 = 14,
           
		[Description("2,5")]
        Waarde25 = 15,
           
		[Description("3,0")]
        Waarde30 = 16,
	} 
	
		/// <summary>
	/// DataValNummer = 271 
	/// voldoende, onvoldoende, twijfelachtig271 
	/// </summary>
	[Serializable]
	 
	public enum VoldoendeOnvoldoendeTwijfelachtig271
	{   	 

			 

			           
		[Description("Voldoende")]
        Voldoende = 1,
           
		[Description("Twijfelachtig")]
        Twijfelachtig = 2,
           
		[Description("Onvoldoende")]
        Onvoldoende = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 272 
	/// Kleurenzin test272 
	/// </summary>
	[Serializable]
	 
	public enum KleurenzinTest272
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Zwak")]
        Zwak = 2,
           
		[Description("Totaal afwezig")]
        TotaalAfwezig = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 285 
	/// Overig bij logopedische screening285 
	/// </summary>
	[Serializable]
	 
	public enum OverigBijLogopedischeScreening285
	{   	 

			 

			           
		[Description("Hyperventilatie")]
        Hyperventilatie = 1,
           
		[Description("Geheugenstoornis (auditief)")]
        GeheugenstoornisAuditief = 2,
           
		[Description("Aandachts- en concentratiestoornis")]
        AandachtsEnConcentratiestoornis = 3,
           
		[Description("Stoornis in lichaamshouding")]
        StoornisInLichaamshouding = 4,
           
		[Description("Anders")]
        Anders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 295 
	/// Conclusie STO in PGO 2295 
	/// </summary>
	[Serializable]
	 
	public enum ConclusieStoInPgo2295
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Matig")]
        Matig = 2,
           
		[Description("Slecht")]
        Slecht = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 300 
	/// Pubesbeharing jongens300 
	/// </summary>
	[Serializable]
	 
	public enum PubesbeharingJongens300
	{   	 

			 

			           
		[Description("P1")]
        P1 = 1,
           
		[Description("P2")]
        P2 = 2,
           
		[Description("P3")]
        P3 = 3,
           
		[Description("P4")]
        P4 = 4,
           
		[Description("P5")]
        P5 = 5,
           
		[Description("P6")]
        P6 = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 301 
	/// Ontwikkeling genitalia jongens301 
	/// </summary>
	[Serializable]
	 
	public enum OntwikkelingGenitaliaJongens301
	{   	 

			 

			           
		[Description("G1")]
        G1 = 1,
           
		[Description("G2")]
        G2 = 2,
           
		[Description("G3")]
        G3 = 3,
           
		[Description("G4")]
        G4 = 4,
           
		[Description("G5")]
        G5 = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 302 
	/// Pubesbeharing meisjes302 
	/// </summary>
	[Serializable]
	 
	public enum PubesbeharingMeisjes302
	{   	 

			 

			           
		[Description("P1")]
        P1 = 1,
           
		[Description("P2")]
        P2 = 2,
           
		[Description("P3")]
        P3 = 3,
           
		[Description("P4")]
        P4 = 4,
           
		[Description("P5")]
        P5 = 5,
           
		[Description("P6")]
        P6 = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 303 
	/// Borstontwikkeling303 
	/// </summary>
	[Serializable]
	 
	public enum Borstontwikkeling303
	{   	 

			 

			           
		[Description("M1")]
        M1 = 1,
           
		[Description("M2")]
        M2 = 2,
           
		[Description("M3")]
        M3 = 3,
           
		[Description("M4")]
        M4 = 4,
           
		[Description("M5")]
        M5 = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 304 
	/// Figuren natekenen304 
	/// </summary>
	[Serializable]
	 
	public enum FigurenNatekenen304
	{   	 

			 

			           
		[Description("1) 2 of minder figuren goed")]
        OfMinderFigurenGoed12 = 1,
           
		[Description("2) 3 of 4 figuren goed")]
        Of4FigurenGoed23 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 305 
	/// Lijn trekken305 
	/// </summary>
	[Serializable]
	 
	public enum LijnTrekken305
	{   	 

			 

			           
		[Description("1) één of meer keer lijn overschreden")]
        NOfMeerKeerLijnOverschreden1 = 1,
           
		[Description("2) lijn niet overschreden")]
        LijnNietOverschreden2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 306 
	/// Stippen zetten - kwantiteit306 
	/// </summary>
	[Serializable]
	 
	public enum StippenZettenKwantiteit306
	{   	 

			 

			           
		[Description("1) 15 of minder circels met één stip")]
        OfMinderCircelsMetNStip115 = 1,
           
		[Description("2) 16 of meer circels met één stip")]
        OfMeerCircelsMetNStip216 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 307 
	/// Lukt niet/Lukt wel307 
	/// </summary>
	[Serializable]
	 
	public enum LuktNietLuktWel307
	{   	 

			 

			           
		[Description("1) lukt niet")]
        LuktNiet1 = 1,
           
		[Description("2) lukt wel")]
        LuktWel2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 308 
	/// Oogbeweging kwantiteit308 
	/// </summary>
	[Serializable]
	 
	public enum OogbewegingKwantiteit308
	{   	 

			 

			           
		[Description("1) volgt niet gehele beweging")]
        VolgtNietGeheleBeweging1 = 1,
           
		[Description("2) volgt gehele beweging")]
        VolgtGeheleBeweging2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 312 
	/// Hielen lopen312 
	/// </summary>
	[Serializable]
	 
	public enum HielenLopen312
	{   	 

			 

			           
		[Description("1) Voorvoet geheel/deels op grond")]
        VoorvoetGeheelDeelsOpGrond1 = 1,
           
		[Description("2) Opdracht correct uitgevoerd")]
        OpdrachtCorrectUitgevoerd2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 313 
	/// Streeplopen kwantiteit313 
	/// </summary>
	[Serializable]
	 
	public enum StreeplopenKwantiteit313
	{   	 

			 

			           
		[Description("1) stapt regelmatig naast/valt of slaat over")]
        StaptRegelmatigNaastValtOfSlaatOver1 = 1,
           
		[Description("2) stap max 2X naast")]
        StapMax2xNaast2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 315 
	/// Springen kwan315 
	/// </summary>
	[Serializable]
	 
	public enum SpringenKwan315
	{   	 

			 

			           
		[Description("1) Springt niet over blok / voeten niet bij elkaar")]
        SpringtNietOverBlokVoetenNietBijElkaar1 = 1,
           
		[Description("2) Correct uitgevoerd")]
        CorrectUitgevoerd2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 316 
	/// Lijn trekken316 
	/// </summary>
	[Serializable]
	 
	public enum LijnTrekken316
	{   	 

			 

			           
		[Description("1) Lijn > 3 keer onderbroken")]
        Lijn3KeerOnderbroken1 = 1,
           
		[Description("2) Lijn niet of <= 3 keer onderbroken")]
        LijnNietOf3KeerOnderbroken2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 317 
	/// Pengreep - kwaliteit317 
	/// </summary>
	[Serializable]
	 
	public enum PengreepKwaliteit317
	{   	 

			 

			           
		[Description("1) geen driepuntsgreep")]
        GeenDriepuntsgreep1 = 1,
           
		[Description("2) driepuntsgreep")]
        Driepuntsgreep2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 319 
	/// Oogbewegingen kwaliteit319 
	/// </summary>
	[Serializable]
	 
	public enum OogbewegingenKwaliteit319
	{   	 

			 

			           
		[Description("1) Volgen schokkerig/dwalen af")]
        VolgenSchokkerigDwalenAf1 = 1,
           
		[Description("2) Volgen vloeiend")]
        VolgenVloeiend2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 323 
	/// Streeplopen kwaliteit323 
	/// </summary>
	[Serializable]
	 
	public enum StreeplopenKwaliteit323
	{   	 

			 

			           
		[Description("1) Onbalans romp/veel armbewegingen")]
        OnbalansRompVeelArmbewegingen1 = 1,
           
		[Description("2) Balans v.d. romp/armen ontspannen")]
        BalansVDRompArmenOntspannen2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 325 
	/// Springen kwaliteit325 
	/// </summary>
	[Serializable]
	 
	public enum SpringenKwaliteit325
	{   	 

			 

			           
		[Description("1) Stijve benen bij afzet/landing")]
        StijveBenenBijAfzetLanding1 = 1,
           
		[Description("2) Gebogen benen bij afzet/landing")]
        GebogenBenenBijAfzetLanding2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 326 
	/// Vinger-duim oppositie326 
	/// </summary>
	[Serializable]
	 
	public enum VingerDuimOppositie326
	{   	 

			 

			           
		[Description("1) Niet alle vingers e/o juiste volgorde")]
        NietAlleVingersEOJuisteVolgorde1 = 1,
           
		[Description("2) Wel met alle vingers & juiste volgorde")]
        WelMetAlleVingersJuisteVolgorde2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 334 
	/// Bevindingen BFM334 
	/// </summary>
	[Serializable]
	 
	public enum BevindingenBfm334
	{   	 

			 

			           
		[Description("Geen actie")]
        GeenActie = 1,
           
		[Description("Controle")]
        Controle = 2,
           
		[Description("Advies gegeven")]
        AdviesGegeven = 3,
           
		[Description("Verwezen")]
        Verwezen = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 359 
	/// Relatie tot broer zus359 
	/// </summary>
	[Serializable]
	 
	public enum RelatieTotBroerZus359
	{   	 

			 

			           
		[Description("Broer of zus")]
        BroerOfZus = 1,
           
		[Description("Halfbroer of halfzus")]
        HalfbroerOfHalfzus = 2,
           
		[Description("Kind v stiefmoeder of stiefvader")]
        KindVStiefmoederOfStiefvader = 3,
           
		[Description("Anders")]
        Anders = 98,
	} 
	
		/// <summary>
	/// DataValNummer = 360 
	/// Gezag360 
	/// </summary>
	[Serializable]
	 
	public enum Gezag360
	{   	 

			 

			           
		[Description("Ouder 1 heeft gezag")]
        Ouder1HeeftGezag = 1,
           
		[Description("Ouder 2 heeft gezag")]
        Ouder2HeeftGezag = 2,
           
		[Description("Een of meer derden hebben gezag")]
        EenOfMeerDerdenHebbenGezag = 3,
           
		[Description("Ouder 1 en  derden")]
        Ouder1EnDerden = 4,
           
		[Description("Ouder 2 en derden")]
        Ouder2EnDerden = 5,
           
		[Description("Ouder 1 en 2 hebben gezag")]
        Ouder1En2HebbenGezag = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 362 
	/// Spraak-taal omgeving stimulerend362 
	/// </summary>
	[Serializable]
	 
	public enum SpraakTaalOmgevingStimulerend362
	{   	 

			 

			           
		[Description("Voldoende")]
        Voldoende = 1,
           
		[Description("Matig")]
        Matig = 2,
           
		[Description("Onvoldoende")]
        Onvoldoende = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 368 
	/// Type hielprik368 
	/// </summary>
	[Serializable]
	 
	public enum TypeHielprik368
	{   	 

			 

			           
		[Description("Eerste hielprik")]
        EersteHielprik = 1,
           
		[Description("Herhaalde eerste hieplrik")]
        HerhaaldeEersteHieplrik = 2,
           
		[Description("Tweede hielprik")]
        TweedeHielprik = 3,
           
		[Description("Herhaalde tweede hielprik")]
        HerhaaldeTweedeHielprik = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 369 
	/// Kleur vruchtwater369 
	/// </summary>
	[Serializable]
	 
	public enum KleurVruchtwater369
	{   	 

			 

			           
		[Description("Kleurloos")]
        Kleurloos = 1,
           
		[Description("Meconium")]
        Meconium = 2,
           
		[Description("Bloederig")]
        Bloederig = 3,
           
		[Description("Anders")]
        Anders = 98,
           
		[Description("Onbekend")]
        Onbekend = 99,
	} 
	
		/// <summary>
	/// DataValNummer = 371 
	/// Rechts/links371 
	/// </summary>
	[Serializable]
	 
	public enum RechtsLinks371
	{   	 

			 

			           
		[Description("Rechts")]
        Rechts = 1,
           
		[Description("Links")]
        Links = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 372 
	/// Vinger-duim oppositie kwaliteit372 
	/// </summary>
	[Serializable]
	 
	public enum VingerDuimOppositieKwaliteit372
	{   	 

			 

			           
		[Description("1) Duidelijke meebewegingen e/o mimiek")]
        DuidelijkeMeebewegingenEOMimiek1 = 1,
           
		[Description("2) Geen/Discrete meebewegingen")]
        GeenDiscreteMeebewegingen2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 373 
	/// Top-neus proef kwant373 
	/// </summary>
	[Serializable]
	 
	public enum TopNeusProefKwant373
	{   	 

			 

			           
		[Description("1) 1 of 2 keer fout uitgevoerd")]
        Of2KeerFoutUitgevoerd11 = 1,
           
		[Description("2) 2 keer correct uitgevoerd")]
        KeerCorrectUitgevoerd22 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 374 
	/// Diadochokinese kwantiteit374 
	/// </summary>
	[Serializable]
	 
	public enum DiadochokineseKwantiteit374
	{   	 

			 

			           
		[Description("1) Niet soepele ritmische omdraaibeweging")]
        NietSoepeleRitmischeOmdraaibeweging1 = 1,
           
		[Description("2) Soepel ritmische omdraaibeweging")]
        SoepelRitmischeOmdraaibeweging2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 375 
	/// Diadochokinese kwaliteit375 
	/// </summary>
	[Serializable]
	 
	public enum DiadochokineseKwaliteit375
	{   	 

			 

			           
		[Description("1) Ab- en adductie bovenarm")]
        AbEnAdductieBovenarm1 = 1,
           
		[Description("2) Vanuit elleboog: arm blijft tegen romp")]
        VanuitElleboogArmBlijftTegenRomp2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 376 
	/// Aantal sec op 1 been376 
	/// </summary>
	[Serializable]
	 
	public enum AantalSecOp1Been376
	{   	 

			 

			           
		[Description("1) 6 seconden of minder:")]
        SecondenOfMinder16 = 1,
           
		[Description("2) 7 seconden of meer")]
        SecondenOfMeer27 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 377 
	/// Een been staan - kwaliteit377 
	/// </summary>
	[Serializable]
	 
	public enum EenBeenStaanKwaliteit377
	{   	 

			 

			           
		[Description("1) Duidelijk heffen armen/zwaaien romp")]
        DuidelijkHeffenArmenZwaaienRomp1 = 1,
           
		[Description("2) Geen/discrete correcties armen/romp")]
        GeenDiscreteCorrectiesArmenRomp2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 378 
	/// Hielen lopen - kwaliteit378 
	/// </summary>
	[Serializable]
	 
	public enum HielenLopenKwaliteit378
	{   	 

			 

			           
		[Description("1) Elleboogflexie/polsextensie/rompdraai")]
        ElleboogflexiePolsextensieRompdraai1 = 1,
           
		[Description("2) Geen/Gering meebewegen")]
        GeenGeringMeebewegen2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 379 
	/// Hinkelen kwantiteit379 
	/// </summary>
	[Serializable]
	 
	public enum HinkelenKwantiteit379
	{   	 

			 

			           
		[Description("1) 8 of minder sprongen")]
        OfMinderSprongen18 = 1,
           
		[Description("2) 9 of meer sprongen")]
        OfMeerSprongen29 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 380 
	/// Hinkelen kwaliteit380 
	/// </summary>
	[Serializable]
	 
	public enum HinkelenKwaliteit380
	{   	 

			 

			           
		[Description("1) Op hele voet/armbew. boven navel")]
        OpHeleVoetArmbewBovenNavel1 = 1,
           
		[Description("2) Op voorvoet/armbew. onder navel")]
        OpVoorvoetArmbewOnderNavel2 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 381 
	/// Zittend/liggend381 
	/// </summary>
	[Serializable]
	 
	public enum ZittendLiggend381
	{   	 

			 

			           
		[Description("Zittend")]
        Zittend = 1,
           
		[Description("Liggend")]
        Liggend = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 383 
	/// Indicatie383 
	/// </summary>
	[Serializable]
	 
	public enum Indicatie383
	{   	 

			 

			           
		[Description("Vaccinaties")]
        Vaccinaties = 1,
           
		[Description("Uitstraling/Indruk")]
        UitstralingIndruk = 2,
           
		[Description("Huid")]
        Huid = 3,
           
		[Description("Hoofd/Hals")]
        HoofdHals = 4,
           
		[Description("Romp")]
        Romp = 5,
           
		[Description("Extremiteiten")]
        Extremiteiten = 6,
           
		[Description("Genitalia")]
        Genitalia = 7,
           
		[Description("Lengte")]
        Lengte = 8,
           
		[Description("Gewicht")]
        Gewicht = 9,
           
		[Description("Psychosociale/emotionele ontwikkeling")]
        PsychosocialeEmotioneleOntwikkeling = 10,
           
		[Description("Motorische ontwikkeling")]
        MotorischeOntwikkeling = 11,
           
		[Description("Spraak/Taal ontwikkeling")]
        SpraakTaalOntwikkeling = 12,
           
		[Description("Enuresis/defaecatie-problemen")]
        EnuresisDefaecatieProblemen = 13,
           
		[Description("Gedrag")]
        Gedrag = 14,
           
		[Description("Opvoedingsproblematiek/slapen")]
        OpvoedingsproblematiekSlapen = 15,
           
		[Description("Leerproblemen")]
        Leerproblemen = 16,
           
		[Description("Syndromale afwijking")]
        SyndromaleAfwijking = 17,
           
		[Description("Amblyopie")]
        Amblyopie = 18,
           
		[Description("Oogpathologie")]
        Oogpathologie = 19,
           
		[Description("Visusafwijkingen")]
        Visusafwijkingen = 20,
           
		[Description("Niet scrotale testis")]
        NietScrotaleTestis = 21,
           
		[Description("Hartafwijkingen")]
        Hartafwijkingen = 22,
           
		[Description("Houdingsafwijkingen")]
        Houdingsafwijkingen = 23,
           
		[Description("Heupdysplasie/luxatie")]
        HeupdysplasieLuxatie = 24,
           
		[Description("Hydro-/microcephalus")]
        HydroMicrocephalus = 25,
           
		[Description("Voedingsallergie/intolerantie")]
        VoedingsallergieIntolerantie = 26,
           
		[Description("Gehoor")]
        Gehoor = 27,
           
		[Description("(Kinder)Psychiatrische aandoening")]
        KinderPsychiatrischeAandoening = 28,
           
		[Description("(Vermoeden) Kindermishandeling in de brede zin")]
        VermoedenKindermishandelingInDeBredeZin = 29,
           
		[Description("Onveilige situatie")]
        OnveiligeSituatie = 30,
           
		[Description("Oorproblematiek overig")]
        OorproblematiekOverig = 31,
           
		[Description("Infectieziekten")]
        Infectieziekten = 32,
           
		[Description("Anders")]
        Anders = 98,
	} 
	
		/// <summary>
	/// DataValNummer = 385 
	/// Verwijzing naar385 
	/// </summary>
	[Serializable]
	 
	public enum VerwijzingNaar385
	{   	 

			 

			           
		[Description("Huisarts")]
        Huisarts = 1,
           
		[Description("Kinderarts")]
        Kinderarts = 2,
           
		[Description("(kinder)Fysiotherapeut")]
        KinderFysiotherapeut = 3,
           
		[Description("Logopedist (extern)")]
        LogopedistExtern = 4,
           
		[Description("AMW")]
        Amw = 5,
           
		[Description("AMK")]
        Amk = 6,
           
		[Description("Bureau Jeugdzorg")]
        BureauJeugdzorg = 7,
           
		[Description("GGZ")]
        Ggz = 8,
           
		[Description("Integrale Vroeghulp")]
        IntegraleVroeghulp = 9,
           
		[Description("VVE")]
        Vve = 10,
           
		[Description("Home start")]
        HomeStart = 11,
           
		[Description("Audiologisch Centrum")]
        AudiologischCentrum = 12,
           
		[Description("Peuterspeelzalen (PSZ)")]
        PeuterspeelzalenPsz = 13,
           
		[Description("Oogarts/Ortoptist")]
        OogartsOrtoptist = 14,
           
		[Description("Diëtist")]
        DiTist = 15,
           
		[Description("Thuisbegeleiding")]
        Thuisbegeleiding = 16,
           
		[Description("Voorzorg")]
        Voorzorg = 17,
           
		[Description("Verloskundige")]
        Verloskundige = 18,
           
		[Description("Kraamzorg")]
        Kraamzorg = 19,
           
		[Description("Lactatiekundige")]
        Lactatiekundige = 20,
           
		[Description("Triple P niveau 3")]
        TriplePNiveau3 = 21,
           
		[Description("Triple P niveau 4 individueel")]
        TriplePNiveau4Individueel = 22,
           
		[Description("Triple P niveau 4 groep")]
        TriplePNiveau4Groep = 23,
           
		[Description("Triple P niveau 5 Triple P plus")]
        TriplePNiveau5TriplePPlus = 24,
           
		[Description("Anders")]
        Anders = 25,
	} 
	
		/// <summary>
	/// DataValNummer = 393 
	/// Samenvatting DMO393 
	/// </summary>
	[Serializable]
	 
	public enum SamenvattingDmo393
	{   	 

			 

			           
		[Description("Prima")]
        Prima = 1,
           
		[Description("Zo zo")]
        ZoZo = 2,
           
		[Description("Probleem")]
        Probleem = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 395 
	/// Vervolg DMO395 
	/// </summary>
	[Serializable]
	 
	public enum VervolgDmo395
	{   	 

			 

			           
		[Description("Regulier contact")]
        RegulierContact = 1,
           
		[Description("Huisbezoek")]
        Huisbezoek = 2,
           
		[Description("Extra consult")]
        ExtraConsult = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 397 
	/// Goed, gemiddeld397 
	/// </summary>
	[Serializable]
	 
	public enum GoedGemiddeld397
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Gemiddeld")]
        Gemiddeld = 2,
           
		[Description("Matig")]
        Matig = 3,
           
		[Description("Onvoldoende")]
        Onvoldoende = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 404 
	/// Soort onderwijs404 
	/// </summary>
	[Serializable]
	 
	public enum SoortOnderwijs404
	{   	 

			 

			           
		[Description("Basisonderwijs")]
        Basisonderwijs = 1,
           
		[Description("VMBO TH")]
        VmboTh = 2,
           
		[Description("VMBO overig")]
        VmboOverig = 3,
           
		[Description("(I)VBO")]
        IVbo = 4,
           
		[Description("MAVO")]
        Mavo = 5,
           
		[Description("HAVO")]
        Havo = 6,
           
		[Description("VWO")]
        Vwo = 7,
           
		[Description("MBO")]
        Mbo = 8,
           
		[Description("HBO")]
        Hbo = 9,
           
		[Description("SBO")]
        Sbo = 10,
           
		[Description("SVO")]
        Svo = 11,
           
		[Description("REC")]
        Rec = 12,
           
		[Description("Anders")]
        Anders = 98,
           
		[Description("Onbekend")]
        Onbekend = 99,
	} 
	
		/// <summary>
	/// DataValNummer = 412 
	/// Classificatie gewicht412 
	/// </summary>
	[Serializable]
	 
	public enum ClassificatieGewicht412
	{   	 

			 

			           
		[Description("Normaal gewicht")]
        NormaalGewicht = 1,
           
		[Description("Overgewicht")]
        Overgewicht = 2,
           
		[Description("Obesitas")]
        Obesitas = 3,
           
		[Description("Ondergewicht")]
        Ondergewicht = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 420 
	/// Onvolledige vaccinatiestatus420 
	/// </summary>
	[Serializable]
	 
	public enum OnvolledigeVaccinatiestatus420
	{   	 

			 

			           
		[Description("Aanmelden vangnet spreekuur")]
        AanmeldenVangnetSpreekuur = 1,
           
		[Description("Heeft inhaalschema vaccinaties")]
        HeeftInhaalschemaVaccinaties = 2,
           
		[Description("Bezwaar ouders/verzorgers")]
        BezwaarOudersVerzorgers = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 422 
	/// Balans422 
	/// </summary>
	[Serializable]
	 
	public enum Balans422
	{   	 

			 

			           
		[Description("Bevorderend")]
        Bevorderend = 1,
           
		[Description("In evenwicht")]
        InEvenwicht = 2,
           
		[Description("Belemmerend")]
        Belemmerend = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 423 
	/// Voldoende/onvoldoende423 
	/// </summary>
	[Serializable]
	 
	public enum VoldoendeOnvoldoende423
	{   	 

			 

			           
		[Description("Voldoende")]
        Voldoende = 1,
           
		[Description("Onvoldoende")]
        Onvoldoende = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 425 
	/// Bril/lenzen dragend425 
	/// </summary>
	[Serializable]
	 
	public enum BrilLenzenDragend425
	{   	 

			 

			           
		[Description("Bril")]
        Bril = 1,
           
		[Description("Lenzen")]
        Lenzen = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 426 
	/// Zwemles426 
	/// </summary>
	[Serializable]
	 
	public enum Zwemles426
	{   	 

			 

			           
		[Description("Zwemles")]
        Zwemles = 1,
           
		[Description("Schoolzwemmen")]
        Schoolzwemmen = 2,
           
		[Description("Geen zwemles en geen schoolzwemmen")]
        GeenZwemlesEnGeenSchoolzwemmen = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 430 
	/// Leerling gewicht430 
	/// </summary>
	[Serializable]
	 
	public enum LeerlingGewicht430
	{   	 

			 

			           
		[Description("0")]
        Waarde0 = 1,
           
		[Description("0,3")]
        Waarde03 = 2,
           
		[Description("1,2")]
        Waarde12 = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 431 
	/// Afschrift JGZ dossier verstrekt aan431 
	/// </summary>
	[Serializable]
	 
	public enum AfschriftJgzDossierVerstrektAan431
	{   	 

			 

			           
		[Description("Kind")]
        Kind = 1,
           
		[Description("Ouder")]
        Ouder = 2,
           
		[Description("Anders")]
        Anders = 98,
	} 
	
		/// <summary>
	/// DataValNummer = 434 
	/// Onderzoek op verzoek434 
	/// </summary>
	[Serializable]
	 
	public enum OnderzoekOpVerzoek434
	{   	 

			 

			           
		[Description("ZAT BaO")]
        ZatBao = 1,
           
		[Description("ZAT VO")]
        ZatVo = 2,
           
		[Description("ZAT MBO")]
        ZatMbo = 3,
           
		[Description("Buurtnetwerk/zorgnetwerk")]
        BuurtnetwerkZorgnetwerk = 4,
           
		[Description("Huisarts")]
        Huisarts = 5,
           
		[Description("Arts overig")]
        ArtsOverig = 6,
           
		[Description("Op eigen verzoek")]
        OpEigenVerzoek = 7,
           
		[Description("Kind zelf/ouder")]
        KindZelfOuder = 8,
           
		[Description("Verwijsindex")]
        Verwijsindex = 9,
           
		[Description("Opvoedspreekuur")]
        Opvoedspreekuur = 10,
           
		[Description("Verloskundige")]
        Verloskundige = 11,
           
		[Description("Anders")]
        Anders = 12,
	} 
	
		/// <summary>
	/// DataValNummer = 435 
	/// Begeleider435 
	/// </summary>
	[Serializable]
	 
	public enum Begeleider435
	{   	 

			 

			           
		[Description("Moeder (Biologische of adoptief)")]
        MoederBiologischeOfAdoptief = 1,
           
		[Description("Vader (Biologische of adoptief)")]
        VaderBiologischeOfAdoptief = 2,
           
		[Description("Stiefmoeder")]
        Stiefmoeder = 3,
           
		[Description("Stiefvader")]
        Stiefvader = 4,
           
		[Description("Andere kinderen, zoals broer(s) en/of zus(sen)")]
        AndereKinderenZoalsBroerSEnOfZusSen = 5,
           
		[Description("Pleegmoeder")]
        Pleegmoeder = 6,
           
		[Description("Pleegvader")]
        Pleegvader = 7,
           
		[Description("Andere familieleden")]
        AndereFamilieleden = 8,
           
		[Description("Oppas")]
        Oppas = 9,
           
		[Description("Vriend(in)")]
        VriendIn = 10,
           
		[Description("Gezinsvoogd / voogd")]
        GezinsvoogdVoogd = 11,
           
		[Description("Zorgverlener")]
        Zorgverlener = 12,
           
		[Description("Alleen gekomen")]
        AlleenGekomen = 13,
           
		[Description("Anders")]
        Anders = 14,
	} 
	
		/// <summary>
	/// DataValNummer = 438 
	/// Opleiding ouder438 
	/// </summary>
	[Serializable]
	 
	public enum OpleidingOuder438
	{   	 

			 

			           
		[Description("Geen opleiding (lagere school niet afgemaakt)")]
        GeenOpleidingLagereSchoolNietAfgemaakt = 1,
           
		[Description("Basisonderwijs (lagere school, basisonderwijs, SBO")]
        BasisonderwijsLagereSchoolBasisonderwijsSbo = 2,
           
		[Description("VSO-MLK / (V)BO / VMBO-LWOO / Praktijkonderwijs")]
        VsoMlkVBoVmboLwooPraktijkonderwijs = 3,
           
		[Description("LBO / VBO / VMBO-BBL&KBL")]
        LboVboVmboBblKbl = 4,
           
		[Description("MAVO / VMBO-GL&TL")]
        MavoVmboGlTl = 5,
           
		[Description("MBO")]
        Mbo = 6,
           
		[Description("HAVO / VWO")]
        HavoVwo = 7,
           
		[Description("HBO / HTS / HEAO")]
        HboHtsHeao = 8,
           
		[Description("WO")]
        Wo = 9,
           
		[Description("Anders")]
        Anders = 10,
           
		[Description("Onbekend")]
        Onbekend = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 439 
	/// Relatie kind tot ouder439 
	/// </summary>
	[Serializable]
	 
	public enum RelatieKindTotOuder439
	{   	 

			 

			           
		[Description("Vader van het kind (eigen, biologisch)")]
        VaderVanHetKindEigenBiologisch = 1,
           
		[Description("Moeder van het kind (eigen, biologisch)")]
        MoederVanHetKindEigenBiologisch = 2,
           
		[Description("Partner / vriend van vader / moeder (stiefvader)")]
        PartnerVriendVanVaderMoederStiefvader = 3,
           
		[Description("Partner / vriendin van vader / moeder (stiefmoeder")]
        PartnerVriendinVanVaderMoederStiefmoeder = 4,
           
		[Description("Pleegvader")]
        Pleegvader = 5,
           
		[Description("Pleegmoeder")]
        Pleegmoeder = 6,
           
		[Description("Adoptievader")]
        Adoptievader = 7,
           
		[Description("Adoptiemoeder")]
        Adoptiemoeder = 8,
           
		[Description("Anders")]
        Anders = 9,
	} 
	
		/// <summary>
	/// DataValNummer = 441 
	/// Relatie biol ouders441 
	/// </summary>
	[Serializable]
	 
	public enum RelatieBiolOuders441
	{   	 

			 

			           
		[Description("Getrouwd")]
        Getrouwd = 1,
           
		[Description("Geregistreerd partnerschap")]
        GeregistreerdPartnerschap = 2,
           
		[Description("Samenwonend")]
        Samenwonend = 3,
           
		[Description("Gescheiden / uit elkaar")]
        GescheidenUitElkaar = 4,
           
		[Description("Latrelatie")]
        Latrelatie = 5,
           
		[Description("Anders")]
        Anders = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 443 
	/// Risico-inschatting VGV443 
	/// </summary>
	[Serializable]
	 
	public enum RisicoInschattingVgv443
	{   	 

			 

			           
		[Description("Geen risico")]
        GeenRisico = 1,
           
		[Description("Twijfelachtig risico")]
        TwijfelachtigRisico = 2,
           
		[Description("Reëel risico")]
        ReElRisico = 3,
           
		[Description("Vermoeden uitgevoerde VGV")]
        VermoedenUitgevoerdeVgv = 4,
           
		[Description("Vastgestelde VGV")]
        VastgesteldeVgv = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 448 
	/// methode meting hartonderzoek448 
	/// </summary>
	[Serializable]
	 
	public enum MethodeMetingHartonderzoek448
	{   	 

			 

			           
		[Description("Zittend")]
        Zittend = 1,
           
		[Description("Liggend")]
        Liggend = 2,
           
		[Description("Staand")]
        Staand = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 449 
	/// Testislocalisatie449 
	/// </summary>
	[Serializable]
	 
	public enum Testislocalisatie449
	{   	 

			 

			           
		[Description("Retractiele testis")]
        RetractieleTestis = 1,
           
		[Description("Aangeboren niet scrotale testis")]
        AangeborenNietScrotaleTestis = 2,
           
		[Description("Verworven niet scotale testis, niet palpabel")]
        VerworvenNietScotaleTestisNietPalpabel = 3,
           
		[Description("Verworven niet scrotale testis, palpabel")]
        VerworvenNietScrotaleTestisPalpabel = 4,
           
		[Description("Anders")]
        Anders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 451 
	/// Bezwaar vaccineren451 
	/// </summary>
	[Serializable]
	 
	public enum BezwaarVaccineren451
	{   	 

			 

			           
		[Description("Medisch bezwaar")]
        MedischBezwaar = 1,
           
		[Description("Principieel bezwaar")]
        PrincipieelBezwaar = 2,
           
		[Description("Anders")]
        Anders = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 454 
	/// Nummer NGS454 
	/// </summary>
	[Serializable]
	 
	public enum NummerNgs454
	{   	 

			 

			           
		[Description("Eerste neonatale gehoorscreening")]
        EersteNeonataleGehoorscreening = 1,
           
		[Description("Tweede neonatale gehoorscreening")]
        TweedeNeonataleGehoorscreening = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 456 
	/// Locatie bevalling456 
	/// </summary>
	[Serializable]
	 
	public enum LocatieBevalling456
	{   	 

			 

			           
		[Description("Thuis")]
        Thuis = 1,
           
		[Description("Poliklinisch")]
        Poliklinisch = 2,
           
		[Description("Klinisch primair")]
        KlinischPrimair = 3,
           
		[Description("Klinisch secundair")]
        KlinischSecundair = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 457 
	/// Tandarts457 
	/// </summary>
	[Serializable]
	 
	public enum Tandarts457
	{   	 

			 

			           
		[Description("Niet")]
        Niet = 1,
           
		[Description("Wel eens")]
        WelEens = 2,
           
		[Description("1x per jaar")]
        xPerJaar1 = 3,
           
		[Description("2x per jaar of vaker")]
        xPerJaarOfVaker2 = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 461 
	/// SDQ waar/niet waar461 
	/// </summary>
	[Serializable]
	 
	public enum SdqWaarNietWaar461
	{   	 

			 

			           
		[Description("1) Niet waar")]
        NietWaar1 = 1,
           
		[Description("2) Een beetje waar")]
        EenBeetjeWaar2 = 2,
           
		[Description("3) Zeker waar")]
        ZekerWaar3 = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 462 
	/// SDQ moeilijkheden462 
	/// </summary>
	[Serializable]
	 
	public enum SdqMoeilijkheden462
	{   	 

			 

			           
		[Description("1) Nee")]
        Nee1 = 1,
           
		[Description("2) Ja, kleine moeilijkheden")]
        JaKleineMoeilijkheden2 = 2,
           
		[Description("3) Ja, duidelijke moeilijkheden")]
        JaDuidelijkeMoeilijkheden3 = 3,
           
		[Description("4) Ja, ernstige moeilijkheden")]
        JaErnstigeMoeilijkheden4 = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 463 
	/// SDQ duur moeilijkheden463 
	/// </summary>
	[Serializable]
	 
	public enum SdqDuurMoeilijkheden463
	{   	 

			 

			           
		[Description("1) Korter dan een maand")]
        KorterDanEenMaand1 = 1,
           
		[Description("2) 1-5 maanden")]
        Maanden215 = 2,
           
		[Description("3) 6-12 maanden")]
        Maanden3612 = 3,
           
		[Description("4) Meer dan een jaar")]
        MeerDanEenJaar4 = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 464 
	/// SDQ ernst moeilijkheden464 
	/// </summary>
	[Serializable]
	 
	public enum SdqErnstMoeilijkheden464
	{   	 

			 

			           
		[Description("1) Helemaal niet")]
        HelemaalNiet1 = 1,
           
		[Description("2) Een beetje maar")]
        EenBeetjeMaar2 = 2,
           
		[Description("3) Tamelijk")]
        Tamelijk3 = 3,
           
		[Description("4) Heel erg")]
        HeelErg4 = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 465 
	/// Lateralisatie R/L465 
	/// </summary>
	[Serializable]
	 
	public enum LateralisatieRL465
	{   	 

			 

			           
		[Description("Rechtshandig")]
        Rechtshandig = 1,
           
		[Description("Linkshandig")]
        Linkshandig = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 466 
	/// Percentiel motoriek BF466 
	/// </summary>
	[Serializable]
	 
	public enum PercentielMotoriekBf466
	{   	 

			 

			           
		[Description("< P5")]
        P5 = 1,
           
		[Description(">= P5 en < P10")]
        P5EnP10 = 2,
           
		[Description(">= P10 en < gemiddeld")]
        P10EnGemiddeld = 3,
           
		[Description("Gemiddeld")]
        Gemiddeld = 4,
           
		[Description("Boven gemiddeld")]
        BovenGemiddeld = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 467 
	/// Kwaliteitsscore mot BF467 
	/// </summary>
	[Serializable]
	 
	public enum KwaliteitsscoreMotBf467
	{   	 

			 

			           
		[Description("0 (slecht / onrijp)")]
        SlechtOnrijp0 = 0,
           
		[Description("1 (slecht / onrijp)")]
        SlechtOnrijp1 = 1,
           
		[Description("2 (matig)")]
        Matig2 = 2,
           
		[Description("3 (matig)")]
        Matig3 = 3,
           
		[Description("4 (goed)")]
        Goed4 = 4,
           
		[Description("5 (goed)")]
        Goed5 = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 468 
	/// APK-TOV 5 meter468 
	/// </summary>
	[Serializable]
	 
	public enum ApkTov5Meter468
	{   	 

			 

			           
		[Description("5/3")]
        Waarde53 = 1,
           
		[Description("5/4")]
        Waarde54 = 2,
           
		[Description("5/5")]
        Waarde55 = 3,
           
		[Description("5/6")]
        Waarde56 = 4,
           
		[Description("5/10")]
        Waarde510 = 5,
           
		[Description("5/15")]
        Waarde515 = 6,
           
		[Description("5/20")]
        Waarde520 = 7,
           
		[Description("5/30")]
        Waarde530 = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 469 
	/// APK-TOV 3 meter469 
	/// </summary>
	[Serializable]
	 
	public enum ApkTov3Meter469
	{   	 

			 

			           
		[Description("3/3")]
        Waarde33 = 1,
           
		[Description("3/4")]
        Waarde34 = 2,
           
		[Description("3/5")]
        Waarde35 = 3,
           
		[Description("3/6")]
        Waarde36 = 4,
           
		[Description("3/10")]
        Waarde310 = 5,
           
		[Description("3/15")]
        Waarde315 = 6,
           
		[Description("3/20")]
        Waarde320 = 7,
           
		[Description("3/30")]
        Waarde330 = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 470 
	/// Landolt C470 
	/// </summary>
	[Serializable]
	 
	public enum LandoltC470
	{   	 

			 

			           
		[Description("1,0")]
        Waarde10 = 1,
           
		[Description("0,8")]
        Waarde08 = 2,
           
		[Description("0,65")]
        Waarde065 = 3,
           
		[Description("0,5")]
        Waarde05 = 4,
           
		[Description("0,4")]
        Waarde04 = 5,
           
		[Description("0,3")]
        Waarde03 = 6,
           
		[Description("0,25")]
        Waarde025 = 7,
           
		[Description("0,2")]
        Waarde02 = 8,
           
		[Description("0,15")]
        Waarde015 = 9,
           
		[Description("0,12")]
        Waarde012 = 10,
           
		[Description("0,1")]
        Waarde01 = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 471 
	/// Drempel gehooronderzoek471 
	/// </summary>
	[Serializable]
	 
	public enum DrempelGehooronderzoek471
	{   	 

			 

			           
		[Description("0")]
        Waarde0 = 1,
           
		[Description("5")]
        Waarde5 = 2,
           
		[Description("10")]
        Waarde10 = 3,
           
		[Description("15")]
        Waarde15 = 4,
           
		[Description("20")]
        Waarde20 = 5,
           
		[Description("25")]
        Waarde25 = 6,
           
		[Description("30")]
        Waarde30 = 7,
           
		[Description("35")]
        Waarde35 = 8,
           
		[Description("40")]
        Waarde40 = 9,
           
		[Description("45")]
        Waarde45 = 10,
           
		[Description("50")]
        Waarde50 = 11,
           
		[Description("55")]
        Waarde55 = 12,
           
		[Description("60")]
        Waarde60 = 13,
           
		[Description("65")]
        Waarde65 = 14,
           
		[Description("70")]
        Waarde70 = 15,
           
		[Description("75")]
        Waarde75 = 16,
           
		[Description("80")]
        Waarde80 = 17,
           
		[Description("85")]
        Waarde85 = 18,
           
		[Description("90")]
        Waarde90 = 19,
	} 
	
		/// <summary>
	/// DataValNummer = 472 
	/// Methode onderzoek ontw puberteit472 
	/// </summary>
	[Serializable]
	 
	public enum MethodeOnderzoekOntwPuberteit472
	{   	 

			 

			           
		[Description("Onderzocht")]
        Onderzocht = 1,
           
		[Description("Mededeling")]
        Mededeling = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 473 
	/// Scoliose kant473 
	/// </summary>
	[Serializable]
	 
	public enum ScolioseKant473
	{   	 

			 

			           
		[Description("Rechts")]
        Rechts = 1,
           
		[Description("Links")]
        Links = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 474 
	/// functiemot onderzoek 012474 
	/// </summary>
	[Serializable]
	 
	public enum FunctiemotOnderzoek012474
	{   	 

			 

			           
		[Description("0")]
        Waarde0 = 1,
           
		[Description("1")]
        Waarde1 = 2,
           
		[Description("2")]
        Waarde2 = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 475 
	/// Functiemot onderzoek 0123475 
	/// </summary>
	[Serializable]
	 
	public enum FunctiemotOnderzoek0123475
	{   	 

			 

			           
		[Description("0")]
        Waarde0 = 1,
           
		[Description("1")]
        Waarde1 = 2,
           
		[Description("2")]
        Waarde2 = 3,
           
		[Description("3")]
        Waarde3 = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 476 
	/// Functiemot onderzoek 01476 
	/// </summary>
	[Serializable]
	 
	public enum FunctiemotOnderzoek01476
	{   	 

			 

			           
		[Description("0")]
        Waarde0 = 1,
           
		[Description("1")]
        Waarde1 = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 477 
	/// Goed - matig - slecht477 
	/// </summary>
	[Serializable]
	 
	public enum GoedMatigSlecht477
	{   	 

			 

			           
		[Description("Goed")]
        Goed = 1,
           
		[Description("Matig")]
        Matig = 2,
           
		[Description("Slecht")]
        Slecht = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 478 
	/// info ouders / overdracht478 
	/// </summary>
	[Serializable]
	 
	public enum InfoOudersOverdracht478
	{   	 

			 

			           
		[Description("Ouders")]
        Ouders = 1,
           
		[Description("Overdracht")]
        Overdracht = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 479 
	/// Reden COI479 
	/// </summary>
	[Serializable]
	 
	public enum RedenCoi479
	{   	 

			 

			           
		[Description("Op verzoek JGZ-team")]
        OpVerzoekJgzTeam = 1,
           
		[Description("Inloopspreekuur")]
        Inloopspreekuur = 2,
           
		[Description("Huisbezoek")]
        Huisbezoek = 3,
           
		[Description("Begeleidingscontact")]
        Begeleidingscontact = 4,
           
		[Description("Op verzoek van school")]
        OpVerzoekVanSchool = 5,
           
		[Description("Naar aanleiding van multidisciplinair overleg")]
        NaarAanleidingVanMultidisciplinairOverleg = 6,
           
		[Description("Op verzoek van ouder / jeugdige")]
        OpVerzoekVanOuderJeugdige = 7,
           
		[Description("Inverhuizing")]
        Inverhuizing = 8,
           
		[Description("Anders")]
        Anders = 9,
           
		[Description("Intake asielzoekerskind")]
        IntakeAsielzoekerskind = 10,
           
		[Description("Intake adoptiekind")]
        IntakeAdoptiekind = 11,
           
		[Description("Intake pleegkind")]
        IntakePleegkind = 12,
           
		[Description("Vangnet rijksvaccinatieprogramma")]
        VangnetRijksvaccinatieprogramma = 13,
           
		[Description("Overloopconsult")]
        Overloopconsult = 14,
           
		[Description("Schoolverzuim")]
        Schoolverzuim = 15,
	} 
	
		/// <summary>
	/// DataValNummer = 480 
	/// vold, redelijk, matig, slecht480 
	/// </summary>
	[Serializable]
	 
	public enum VoldRedelijkMatigSlecht480
	{   	 

			 

			           
		[Description("Voldoende")]
        Voldoende = 1,
           
		[Description("Redelijk")]
        Redelijk = 2,
           
		[Description("Matig")]
        Matig = 3,
           
		[Description("Slecht")]
        Slecht = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 481 
	/// Reflex481 
	/// </summary>
	[Serializable]
	 
	public enum Reflex481
	{   	 

			 

			           
		[Description("Normaal")]
        Normaal = 1,
           
		[Description("Verhoogd")]
        Verhoogd = 2,
           
		[Description("Verlaagd")]
        Verlaagd = 3,
           
		[Description("Afwezig")]
        Afwezig = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 482 
	/// positief / negatief482 
	/// </summary>
	[Serializable]
	 
	public enum PositiefNegatief482
	{   	 

			 

			           
		[Description("Positief")]
        Positief = 1,
           
		[Description("Negatief")]
        Negatief = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 483 
	/// ja/nee/actief/passief483 
	/// </summary>
	[Serializable]
	 
	public enum JaNeeActiefPassief483
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Nee")]
        Nee = 2,
           
		[Description("Actief")]
        Actief = 3,
           
		[Description("Passief")]
        Passief = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 484 
	/// positief / negatief VOV484 
	/// </summary>
	[Serializable]
	 
	public enum PositiefNegatiefVov484
	{   	 

			 

			           
		[Description("Positief")]
        Positief = 1,
           
		[Description("Negatief")]
        Negatief = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 487 
	/// positief / negatief art femorales487 
	/// </summary>
	[Serializable]
	 
	public enum PositiefNegatiefArtFemorales487
	{   	 

			 

			           
		[Description("Positief")]
        Positief = 1,
           
		[Description("Negatief")]
        Negatief = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 489 
	/// APK-TOV 4 meter489 
	/// </summary>
	[Serializable]
	 
	public enum ApkTov4Meter489
	{   	 

			 

			           
		[Description("4/4")]
        Waarde44 = 1,
           
		[Description("4/5")]
        Waarde45 = 2,
           
		[Description("4/6")]
        Waarde46 = 3,
           
		[Description("4/10")]
        Waarde410 = 4,
           
		[Description("4/15")]
        Waarde415 = 5,
           
		[Description("4/20")]
        Waarde420 = 6,
           
		[Description("4/30")]
        Waarde430 = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 491 
	/// Aanwezig / afwezig491 
	/// </summary>
	[Serializable]
	 
	public enum AanwezigAfwezig491
	{   	 

			 

			           
		[Description("Aanwezig")]
        Aanwezig = 1,
           
		[Description("Afwezig")]
        Afwezig = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 492 
	/// Tonus neuromot onderzoek492 
	/// </summary>
	[Serializable]
	 
	public enum TonusNeuromotOnderzoek492
	{   	 

			 

			           
		[Description("Normaal")]
        Normaal = 1,
           
		[Description("Hypertoon")]
        Hypertoon = 2,
           
		[Description("Hypotoon")]
        Hypotoon = 3,
	} 
	
		/// <summary>
	/// DataValNummer = 494 
	/// Opv reden bezoek494 
	/// </summary>
	[Serializable]
	 
	public enum OpvRedenBezoek494
	{   	 

			 

			           
		[Description("Eigen initiatief")]
        EigenInitiatief = 1,
           
		[Description("Naar aanleiding van ouders on line")]
        NaarAanleidingVanOudersOnLine = 2,
           
		[Description("Op verzoek/advies van (S)MW  (school)maatsch werk")]
        OpVerzoekAdviesVanSMwSchoolMaatschWerk = 3,
           
		[Description("Op verzoek/advies van huisarts")]
        OpVerzoekAdviesVanHuisarts = 4,
           
		[Description("Op verzoek/advies van JGZ-team")]
        OpVerzoekAdviesVanJgzTeam = 5,
           
		[Description("Op verzoek/advies van school")]
        OpVerzoekAdviesVanSchool = 6,
           
		[Description("Vervolg eerder consult opvoedspreekuur")]
        VervolgEerderConsultOpvoedspreekuur = 7,
           
		[Description("Anders")]
        Anders = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 495 
	/// Opv kennis spreekuur495 
	/// </summary>
	[Serializable]
	 
	public enum OpvKennisSpreekuur495
	{   	 

			 

			           
		[Description("CJG-professional")]
        CjgProfessional = 1,
           
		[Description("Digitale Media")]
        DigitaleMedia = 2,
           
		[Description("Eerder contact met opvoedspreekuur gehad")]
        EerderContactMetOpvoedspreekuurGehad = 3,
           
		[Description("Familie / vrienden / kennisen")]
        FamilieVriendenKennisen = 4,
           
		[Description("Huisarts")]
        Huisarts = 5,
           
		[Description("Instelling (bv. GGZ, diëtist, fysiotherapie)")]
        InstellingBvGgzDiTistFysiotherapie = 6,
           
		[Description("Opvoedcursus")]
        Opvoedcursus = 7,
           
		[Description("School")]
        School = 8,
           
		[Description("Schriftelijke Media")]
        SchriftelijkeMedia = 9,
           
		[Description("Voorschoolse voorziening")]
        VoorschoolseVoorziening = 10,
           
		[Description("Anders")]
        Anders = 11,
	} 
	
		/// <summary>
	/// DataValNummer = 496 
	/// Opv gezinssituatie496 
	/// </summary>
	[Serializable]
	 
	public enum OpvGezinssituatie496
	{   	 

			 

			           
		[Description("Adoptiegezin / pleeggezin")]
        AdoptiegezinPleeggezin = 1,
           
		[Description("Co-ouderschap")]
        CoOuderschap = 2,
           
		[Description("Eenoudergezin")]
        Eenoudergezin = 3,
           
		[Description("Meer generatiegezin")]
        MeerGeneratiegezin = 4,
           
		[Description("Nieuw samengesteld gezin")]
        NieuwSamengesteldGezin = 5,
           
		[Description("Tweeoudergezin")]
        Tweeoudergezin = 6,
           
		[Description("Anders")]
        Anders = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 497 
	/// Opv gezinsgrootte497 
	/// </summary>
	[Serializable]
	 
	public enum OpvGezinsgrootte497
	{   	 

			 

			           
		[Description("1 kind")]
        Kind1 = 1,
           
		[Description("2 kinderen")]
        Kinderen2 = 2,
           
		[Description("3 kinderen")]
        Kinderen3 = 3,
           
		[Description("4 kinderen")]
        Kinderen4 = 4,
           
		[Description("5 kinderen")]
        Kinderen5 = 5,
           
		[Description("6 kinderen")]
        Kinderen6 = 6,
           
		[Description("Anders")]
        Anders = 7,
	} 
	
		/// <summary>
	/// DataValNummer = 499 
	/// Opv leeftijd kind499 
	/// </summary>
	[Serializable]
	 
	public enum OpvLeeftijdKind499
	{   	 

			 

			           
		[Description("0-1 jaar")]
        Jaar01 = 1,
           
		[Description("2-4 jaar")]
        Jaar24 = 2,
           
		[Description("5-12 jaar")]
        Jaar512 = 3,
           
		[Description("13-18 jaar")]
        Jaar1318 = 4,
           
		[Description("Ouder dan 18 jaar")]
        OuderDan18Jaar = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 509 
	/// Opv inschatting zwaarte509 
	/// </summary>
	[Serializable]
	 
	public enum OpvInschattingZwaarte509
	{   	 

			 

			           
		[Description("Alledaagse opvoedingsvragen")]
        AlledaagseOpvoedingsvragen = 1,
           
		[Description("Opvoedingsspanning")]
        Opvoedingsspanning = 2,
           
		[Description("Opvoedingsnood")]
        Opvoedingsnood = 3,
           
		[Description("Opvoedingscrisis")]
        Opvoedingscrisis = 4,
           
		[Description("Anders")]
        Anders = 5,
	} 
	
		/// <summary>
	/// DataValNummer = 510 
	/// Opv duur problematiek510 
	/// </summary>
	[Serializable]
	 
	public enum OpvDuurProblematiek510
	{   	 

			 

			           
		[Description("< 1 maand")]
        Maand1 = 1,
           
		[Description("1 tot 6 maanden")]
        Tot6Maanden1 = 2,
           
		[Description("6 maanden tot 1 jaar")]
        MaandenTot1Jaar6 = 3,
           
		[Description("1 - 2 jaar")]
        Jaar12 = 4,
           
		[Description("> 2 jaar")]
        Jaar2 = 5,
           
		[Description("Anders")]
        Anders = 6,
	} 
	
		/// <summary>
	/// DataValNummer = 515 
	/// Opv client geholpen515 
	/// </summary>
	[Serializable]
	 
	public enum OpvClientGeholpen515
	{   	 

			 

			           
		[Description("Ja")]
        Ja = 1,
           
		[Description("Een beetje")]
        EenBeetje = 2,
           
		[Description("Nee")]
        Nee = 3,
           
		[Description("Weet niet")]
        WeetNiet = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 516 
	/// Opv Client516 
	/// </summary>
	[Serializable]
	 
	public enum OpvClient516
	{   	 

			 

			           
		[Description("(Gezins)voogd")]
        GezinsVoogd = 1,
           
		[Description("Andere familieleden")]
        AndereFamilieleden = 2,
           
		[Description("Andere kinderen zoals broer (s) en/of zus(sen)")]
        AndereKinderenZoalsBroerSEnOfZusSen = 3,
           
		[Description("Jeugdige")]
        Jeugdige = 4,
           
		[Description("Moeder (biologische of adoptief)")]
        MoederBiologischeOfAdoptief = 5,
           
		[Description("Oppas")]
        Oppas = 6,
           
		[Description("Pleegmoeder")]
        Pleegmoeder = 7,
           
		[Description("Pleegvader")]
        Pleegvader = 8,
           
		[Description("Stiefmoeder")]
        Stiefmoeder = 9,
           
		[Description("Stiefvader")]
        Stiefvader = 10,
           
		[Description("Vader (biologische of adoptief)")]
        VaderBiologischeOfAdoptief = 11,
           
		[Description("Vriend(in)")]
        VriendIn = 12,
           
		[Description("Zorgverlener")]
        Zorgverlener = 13,
           
		[Description("Anders")]
        Anders = 14,
	} 
	
		/// <summary>
	/// DataValNummer = 519 
	/// Bloeddruk519 
	/// </summary>
	[Serializable]
	 
	public enum Bloeddruk519
	{   	 

			 

			           
		[Description("Normaal")]
        Normaal = 1,
           
		[Description("Verhoogd")]
        Verhoogd = 2,
	} 
	
		/// <summary>
	/// DataValNummer = 116 
	/// Bijzonderheden mond116Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BijzonderhedenMond116multiple
	{   	 

			 

			           
		[Description("Schisis")]
        Schisis = 2,
           
		[Description("Te korte tongriem")]
        TeKorteTongriem = 4,
           
		[Description("Spruw")]
        Spruw = 8,
           
		[Description("Rhagaden")]
        Rhagaden = 16,
           
		[Description("Aanwezigheid beslag")]
        AanwezigheidBeslag = 32,
           
		[Description("Afwijkende vorm/kleur tong")]
        AfwijkendeVormKleurTong = 64,
           
		[Description("Anders")]
        Anders = 128,
	} 
	
		/// <summary>
	/// DataValNummer = 120 
	/// Slaaphouding120Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Slaaphouding120multiple
	{   	 

			 

			           
		[Description("Buikligging")]
        Buikligging = 2,
           
		[Description("Zijligging")]
        Zijligging = 4,
           
		[Description("Fixatie")]
        Fixatie = 8,
           
		[Description("In ouderlijk bed")]
        InOuderlijkBed = 16,
           
		[Description("Dekbed")]
        Dekbed = 32,
	} 
	
		/// <summary>
	/// DataValNummer = 122 
	/// Drugs122Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Drugs122multiple
	{   	 

			 

			           
		[Description("Heroïne")]
        HeroNe = 4,
           
		[Description("Methadon")]
        Methadon = 8,
           
		[Description("Cocaïne")]
        CocaNe = 16,
           
		[Description("Crack base coke")]
        CrackBaseCoke = 32,
           
		[Description("XTC")]
        Xtc = 64,
           
		[Description("Speed (amfetamine)")]
        SpeedAmfetamine = 128,
           
		[Description("Cannabis/marihuana")]
        CannabisMarihuana = 256,
           
		[Description("GHB")]
        Ghb = 512,
           
		[Description("Poppers")]
        Poppers = 1024,
           
		[Description("LSD")]
        Lsd = 2048,
           
		[Description("Paddo's/ecodrugs")]
        PaddoSEcodrugs = 4096,
	} 
	
		/// <summary>
	/// DataValNummer = 123 
	/// Mondgedrag123Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Mondgedrag123multiple
	{   	 

			 

			           
		[Description("Geen bijzonderheden")]
        GeenBijzonderheden = 1,
           
		[Description("Duim- / vingerzuigen")]
        DuimVingerzuigen = 2,
           
		[Description("Speen gebruik")]
        SpeenGebruik = 4,
           
		[Description("Habitueel mondademen")]
        HabitueelMondademen = 8,
           
		[Description("Slikproblemen")]
        Slikproblemen = 32,
           
		[Description("Gestoorde (senso)motoriek")]
        GestoordeSensoMotoriek = 64,
           
		[Description("Anders")]
        Anders = 128,
	} 
	
		/// <summary>
	/// DataValNummer = 124 
	/// Onderzoek hoofd124Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OnderzoekHoofd124multiple
	{   	 

			 

			           
		[Description("Dwangstand van het hoofd")]
        DwangstandVanHetHoofd = 2,
           
		[Description("Afwijkende vorm v.d. schedel")]
        AfwijkendeVormVDSchedel = 4,
           
		[Description("Fontanel ingezonken")]
        FontanelIngezonken = 8,
           
		[Description("Schedelnaden te vroeg dicht")]
        SchedelnadenTeVroegDicht = 16,
           
		[Description("Fontanel bomberend")]
        FontanelBomberend = 32,
           
		[Description("Anders")]
        Anders = 64,
	} 
	
		/// <summary>
	/// DataValNummer = 131 
	/// Huidafwijkingen131Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Huidafwijkingen131multiple
	{   	 

			 

			           
		[Description("Hematoom")]
        Hematoom = 2,
           
		[Description("Wond, litteken")]
        WondLitteken = 4,
           
		[Description("Ooievaarsbeet")]
        Ooievaarsbeet = 8,
           
		[Description("Naevus")]
        Naevus = 16,
           
		[Description("Café au lait")]
        CafAuLait = 32,
           
		[Description("Vitiligo")]
        Vitiligo = 64,
           
		[Description("Haemangioom")]
        Haemangioom = 128,
           
		[Description("Molluscum contagiosum")]
        MolluscumContagiosum = 256,
           
		[Description("Wrat")]
        Wrat = 512,
           
		[Description("Luieruitslag")]
        Luieruitslag = 1024,
           
		[Description("Milien")]
        Milien = 2048,
           
		[Description("Eczeem")]
        Eczeem = 4096,
           
		[Description("Berg")]
        Berg = 8192,
           
		[Description("Smetplekken")]
        Smetplekken = 16384,
           
		[Description("Mongolenvlek")]
        Mongolenvlek = 32768,
           
		[Description("Anders")]
        Anders = 65536,
           
		[Description("Hoofdluis")]
        Hoofdluis = 131072,
           
		[Description("Acne")]
        Acne = 262144,
           
		[Description("Schimmel")]
        Schimmel = 524288,
           
		[Description("Ringworm")]
        Ringworm = 1048576,
           
		[Description("Impetigo")]
        Impetigo = 2097152,
           
		[Description("Striae")]
        Striae = 4194304,
           
		[Description("Hirsutisme")]
        Hirsutisme = 8388608,
	} 
	
		/// <summary>
	/// DataValNummer = 132 
	/// Bijzonderheden aan navel132Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BijzonderhedenAanNavel132multiple
	{   	 

			 

			           
		[Description("Granuloom")]
        Granuloom = 4,
           
		[Description("Hernia umbilicalis")]
        HerniaUmbilicalis = 8,
           
		[Description("Nattende navel")]
        NattendeNavel = 16,
           
		[Description("Anders")]
        Anders = 32,
	} 
	
		/// <summary>
	/// DataValNummer = 153 
	/// Onderzoek ledematen153Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OnderzoekLedematen153multiple
	{   	 

			 

			           
		[Description("Armen hypertoon")]
        ArmenHypertoon = 2,
           
		[Description("Armen hypotoon")]
        ArmenHypotoon = 4,
           
		[Description("R/L verschil armen")]
        RLVerschilArmen = 8,
           
		[Description("Schouderretractie")]
        Schouderretractie = 16,
           
		[Description("Benen hypertoon")]
        BenenHypertoon = 32,
           
		[Description("Benen hypotoon")]
        BenenHypotoon = 64,
           
		[Description("Enkels zijn stug")]
        EnkelsZijnStug = 128,
           
		[Description("Bijzonderheden handen")]
        BijzonderhedenHanden = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 154 
	/// Ouderkenmerken154Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Ouderkenmerken154multiple
	{   	 

			 

			           
		[Description("Leeftijd moeder bij bevalling<20 jaar")]
        LeeftijdMoederBijBevalling20Jaar = 2,
           
		[Description("Alcohol of drugsgebruik in zwangerschap")]
        AlcoholOfDrugsgebruikInZwangerschap = 4,
           
		[Description("Geen JGZ of alleen vaccinaties")]
        GeenJgzOfAlleenVaccinaties = 8,
           
		[Description("Opvoedingsproblemen")]
        Opvoedingsproblemen = 16,
           
		[Description("Zorgtekort")]
        Zorgtekort = 32,
           
		[Description("Ontbreken sociaal netwerk")]
        OntbrekenSociaalNetwerk = 64,
           
		[Description("Eénouder gezin")]
        ENouderGezin = 128,
           
		[Description("Langdurig werkloos/arbeidsongeschikt")]
        LangdurigWerkloosArbeidsongeschikt = 256,
           
		[Description("Verzorgende ouder spreekt geen Nederlands")]
        VerzorgendeOuderSpreektGeenNederlands = 512,
           
		[Description("Leeft van minimum inkomen")]
        LeeftVanMinimumInkomen = 1024,
           
		[Description("Chronisch zieke ouder")]
        ChronischZiekeOuder = 2048,
           
		[Description("Ouder(s) verslaafd aan alcohol")]
        OuderSVerslaafdAanAlcohol = 4096,
           
		[Description("Ouder(s) verslaafd aan drugs")]
        OuderSVerslaafdAanDrugs = 8192,
           
		[Description("Ouder (s) met psychi(atri)sche problemen")]
        OuderSMetPsychiAtriScheProblemen = 16384,
           
		[Description("Ouder(s) als kind zelf mishandeld")]
        OuderSAlsKindZelfMishandeld = 32768,
           
		[Description("Ouder(s) laag of niet geletterd")]
        OuderSLaagOfNietGeletterd = 65536,
           
		[Description("Anders")]
        Anders = 131072,
           
		[Description("Geen")]
        Geen = 262144,
	} 
	
		/// <summary>
	/// DataValNummer = 157 
	/// Onderzoek longen157Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OnderzoekLongen157multiple
	{   	 

			 

			           
		[Description("Crepitaties")]
        Crepitaties = 2,
           
		[Description("Dyspnoe")]
        Dyspnoe = 4,
           
		[Description("Exspiratoir piepen")]
        ExspiratoirPiepen = 8,
           
		[Description("Inspiratoir piepen")]
        InspiratoirPiepen = 16,
           
		[Description("Rhonchi")]
        Rhonchi = 32,
           
		[Description("Tachypnoe")]
        Tachypnoe = 64,
           
		[Description("Anders")]
        Anders = 128,
           
		[Description("Verlengd expirium")]
        VerlengdExpirium = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 158 
	/// Onderzoek liezen158Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OnderzoekLiezen158multiple
	{   	 

			 

			           
		[Description("Vergrote lymfeklieren")]
        VergroteLymfeklieren = 2,
           
		[Description("Liesbreuk")]
        Liesbreuk = 4,
           
		[Description("Anders")]
        Anders = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 159 
	/// Medicijngebruik in zwangerschap159Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum MedicijngebruikInZwangerschap159multiple
	{   	 

			 

			           
		[Description("Thyreostatica")]
        Thyreostatica = 2,
           
		[Description("Insuline")]
        Insuline = 4,
           
		[Description("Anti-epileptica")]
        AntiEpileptica = 8,
           
		[Description("Anti-hypertensiva")]
        AntiHypertensiva = 16,
           
		[Description("Corticosteroiden")]
        Corticosteroiden = 32,
           
		[Description("Psychofarmaca")]
        Psychofarmaca = 64,
           
		[Description("Anders")]
        Anders = 128,
           
		[Description("Antibiotica")]
        Antibiotica = 256,
           
		[Description("Antimycotica")]
        Antimycotica = 512,
           
		[Description("Immonusuppresiva")]
        Immonusuppresiva = 1024,
           
		[Description("Middelen bij astma")]
        MiddelenBijAstma = 2048,
           
		[Description("NSAID's")]
        NsaidS = 4096,
	} 
	
		/// <summary>
	/// DataValNummer = 175 
	/// Bijzonderheden gebit175Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BijzonderhedenGebit175multiple
	{   	 

			 

			           
		[Description("Open beet")]
        OpenBeet = 2,
           
		[Description("Overbeet")]
        Overbeet = 4,
           
		[Description("Onderbeet")]
        Onderbeet = 8,
           
		[Description("Scheve beet")]
        ScheveBeet = 16,
           
		[Description("Micrognathie")]
        Micrognathie = 32,
           
		[Description("Onregelmatig gebit")]
        OnregelmatigGebit = 64,
           
		[Description("Caries")]
        Caries = 128,
           
		[Description("Tandplaque")]
        Tandplaque = 512,
           
		[Description("Beugel")]
        Beugel = 1024,
           
		[Description("Anders")]
        Anders = 2048,
	} 
	
		/// <summary>
	/// DataValNummer = 179 
	/// Onderzoek oren179Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OnderzoekOren179multiple
	{   	 

			 

			           
		[Description("Resten van kieuwboogspleten")]
        RestenVanKieuwboogspleten = 2,
           
		[Description("Afwijkende vorm kraakbenig deel vh oor")]
        AfwijkendeVormKraakbenigDeelVhOor = 4,
           
		[Description("Afwijkende stand")]
        AfwijkendeStand = 8,
           
		[Description("Bijoortje links")]
        BijoortjeLinks = 32,
           
		[Description("Bijoortje rechts")]
        BijoortjeRechts = 64,
           
		[Description("Lage implantatie")]
        LageImplantatie = 128,
           
		[Description("Anders")]
        Anders = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 181 
	/// Familieleden ivm erfelijke belasting181Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum FamilieledenIvmErfelijkeBelasting181multiple
	{   	 

			 

			           
		[Description("Vader")]
        Vader = 2,
           
		[Description("Moeder")]
        Moeder = 4,
           
		[Description("Broer")]
        Broer = 8,
           
		[Description("Zus")]
        Zus = 16,
           
		[Description("Vader van vader")]
        VaderVanVader = 32,
           
		[Description("Moeder van vader")]
        MoederVanVader = 64,
           
		[Description("Vader van moeder")]
        VaderVanMoeder = 128,
           
		[Description("Moeder van moeder")]
        MoederVanMoeder = 256,
           
		[Description("Broer van vader")]
        BroerVanVader = 512,
           
		[Description("Broer van moeder")]
        BroerVanMoeder = 1024,
           
		[Description("Zus van vader")]
        ZusVanVader = 2048,
           
		[Description("Zus van moeder")]
        ZusVanMoeder = 4096,
           
		[Description("3e graads")]
        eGraads3 = 8192,
	} 
	
		/// <summary>
	/// DataValNummer = 183 
	/// Bijzonderheden hals183Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BijzonderhedenHals183multiple
	{   	 

			 

			           
		[Description("Torticollis")]
        Torticollis = 2,
           
		[Description("Vergrote lymfeklieren")]
        VergroteLymfeklieren = 4,
           
		[Description("Resten van kieuwboogspleten")]
        RestenVanKieuwboogspleten = 8,
           
		[Description("Anders")]
        Anders = 16,
	} 
	
		/// <summary>
	/// DataValNummer = 184 
	/// Onderzoek thorax184Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OnderzoekThorax184multiple
	{   	 

			 

			           
		[Description("Pectus excavatum")]
        PectusExcavatum = 2,
           
		[Description("Pectus carinatum")]
        PectusCarinatum = 4,
           
		[Description("Asymmetrie")]
        Asymmetrie = 8,
           
		[Description("Intrekkingen")]
        Intrekkingen = 16,
           
		[Description("Rachitische rozenkrans")]
        RachitischeRozenkrans = 32,
           
		[Description("Gynaecomastie")]
        Gynaecomastie = 64,
           
		[Description("Tepelvloed bij kinderen")]
        TepelvloedBijKinderen = 128,
           
		[Description("Anders (o.a. tonthorax)")]
        AndersOATonthorax = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 193 
	/// Inventarisatie ZG onderwerpen193Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum InventarisatieZgOnderwerpen193multiple
	{   	 

			 

			           
		[Description("Voeding / eten")]
        VoedingEten = 2,
           
		[Description("Verzorging")]
        Verzorging = 4,
           
		[Description("Uitscheiding")]
        Uitscheiding = 8,
           
		[Description("Vitamines")]
        Vitamines = 16,
           
		[Description("Huid")]
        Huid = 32,
           
		[Description("Mondverzorging")]
        Mondverzorging = 64,
           
		[Description("Temperament")]
        Temperament = 128,
           
		[Description("Slapen/waken")]
        SlapenWaken = 256,
           
		[Description("Ontwikkeling")]
        Ontwikkeling = 512,
           
		[Description("Lichamelijke verschijning")]
        LichamelijkeVerschijning = 1024,
           
		[Description("Gedrag")]
        Gedrag = 2048,
           
		[Description("Overig")]
        Overig = 4096,
	} 
	
		/// <summary>
	/// DataValNummer = 222 
	/// Taalstoornis222Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Taalstoornis222multiple
	{   	 

			 

			           
		[Description("Taalontwikkelingsstoornis")]
        Taalontwikkelingsstoornis = 2,
           
		[Description("Taalvorm receptief")]
        TaalvormReceptief = 4,
           
		[Description("Taalvorm productief")]
        TaalvormProductief = 8,
           
		[Description("Taalinhoud receptief")]
        TaalinhoudReceptief = 16,
           
		[Description("Taalinhoud productief")]
        TaalinhoudProductief = 32,
           
		[Description("Taalgebruik receptief")]
        TaalgebruikReceptief = 64,
           
		[Description("Taalgebruik productief")]
        TaalgebruikProductief = 128,
           
		[Description("Anders")]
        Anders = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 230 
	/// Gezinssamenstelling230Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Gezinssamenstelling230multiple
	{   	 

			 

			           
		[Description("Gezinsverband")]
        Gezinsverband = 2,
           
		[Description("Internaat of tehuis")]
        InternaatOfTehuis = 4,
	} 
	
		/// <summary>
	/// DataValNummer = 257 
	/// Zindelijkheid257Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Zindelijkheid257multiple
	{   	 

			 

			           
		[Description("Overdag onzindelijk voor urine")]
        OverdagOnzindelijkVoorUrine = 2,
           
		[Description("'s Nachts onzindelijk voor urine")]
        SNachtsOnzindelijkVoorUrine = 4,
           
		[Description("Overdag onzindelijk voor ontlasting")]
        OverdagOnzindelijkVoorOntlasting = 8,
           
		[Description("'s Nachts onzindelijk voor ontlasting")]
        SNachtsOnzindelijkVoorOntlasting = 16,
	} 
	
		/// <summary>
	/// DataValNummer = 261 
	/// Bijzonderheden wervelkolom (4-19)261Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BijzonderhedenWervelkolom419261multiple
	{   	 

			 

			           
		[Description("Scoliose structureel")]
        ScolioseStructureel = 2,
           
		[Description("Scoliose houdingsafhankelijk")]
        ScolioseHoudingsafhankelijk = 4,
           
		[Description("Hyperkyfose")]
        Hyperkyfose = 8,
           
		[Description("Hyperkyfose redresseerbaar")]
        HyperkyfoseRedresseerbaar = 16,
           
		[Description("Kyfose")]
        Kyfose = 32,
           
		[Description("Lordose")]
        Lordose = 64,
           
		[Description("Anders")]
        Anders = 128,
	} 
	
		/// <summary>
	/// DataValNummer = 273 
	/// Type zorg in het gezin273Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum TypeZorgInHetGezin273multiple
	{   	 

			 

			           
		[Description("Medische zorg: huisarts")]
        MedischeZorgHuisarts = 2,
           
		[Description("Medische zorg: specialist")]
        MedischeZorgSpecialist = 4,
           
		[Description("Geestelijke gezondheidszorg")]
        GeestelijkeGezondheidszorg = 8,
           
		[Description("Gehandicaptenzorg")]
        Gehandicaptenzorg = 16,
           
		[Description("Jeugdzorg")]
        Jeugdzorg = 32,
           
		[Description("Maatschappelijk werk")]
        MaatschappelijkWerk = 64,
           
		[Description("Ouderenzorg")]
        Ouderenzorg = 128,
           
		[Description("Sociaal juridische dienstverlening")]
        SociaalJuridischeDienstverlening = 256,
           
		[Description("Welzijnswerk")]
        Welzijnswerk = 512,
           
		[Description("Paramedisch")]
        Paramedisch = 1024,
           
		[Description("Jeugdgezondheidszorg")]
        Jeugdgezondheidszorg = 2048,
           
		[Description("Gespecialiseerde gezinsverzorging")]
        GespecialiseerdeGezinsverzorging = 4096,
           
		[Description("Integrale Vroeghulp")]
        IntegraleVroeghulp = 8192,
           
		[Description("Extra zorg op school")]
        ExtraZorgOpSchool = 16384,
           
		[Description("Externe pedagogische ondersteuning")]
        ExternePedagogischeOndersteuning = 32768,
           
		[Description("Logopedie")]
        Logopedie = 65536,
           
		[Description("Anders")]
        Anders = 131072,
	} 
	
		/// <summary>
	/// DataValNummer = 274 
	/// Zorg in het gezin voor274Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum ZorgInHetGezinVoor274multiple
	{   	 

			 

			           
		[Description("Kind")]
        Kind = 2,
           
		[Description("Vader")]
        Vader = 4,
           
		[Description("Moeder")]
        Moeder = 8,
           
		[Description("Beide ouders")]
        BeideOuders = 16,
	} 
	
		/// <summary>
	/// DataValNummer = 277 
	/// Bedreigingen sociaal milieu277Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BedreigingenSociaalMilieu277multiple
	{   	 

			 

			           
		[Description("Vermoeden overmatige zorg")]
        VermoedenOvermatigeZorg = 4,
           
		[Description("Vermoeden verwaarlozing")]
        VermoedenVerwaarlozing = 8,
           
		[Description("Vermoeden fysieke mishandeling")]
        VermoedenFysiekeMishandeling = 16,
           
		[Description("Vermoeden psychische mishandeling")]
        VermoedenPsychischeMishandeling = 32,
           
		[Description("Vermoeden seksuele mishandeling")]
        VermoedenSeksueleMishandeling = 64,
           
		[Description("Onhygienische woonsituatie")]
        OnhygienischeWoonsituatie = 128,
           
		[Description("Slecht binnenmilieu")]
        SlechtBinnenmilieu = 256,
           
		[Description("Geen bedreigingen")]
        GeenBedreigingen = 512,
	} 
	
		/// <summary>
	/// DataValNummer = 278 
	/// Bedreigingen fysiek milieu278Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BedreigingenFysiekMilieu278multiple
	{   	 

			 

			           
		[Description("Veel verkeer in buurt")]
        VeelVerkeerInBuurt = 2,
           
		[Description("Open water in buurt")]
        OpenWaterInBuurt = 4,
           
		[Description("Onveilige buurt (criminaliteit, drugsoverlast)")]
        OnveiligeBuurtCriminaliteitDrugsoverlast = 8,
           
		[Description("Weinig/geen speelgelegenheid")]
        WeinigGeenSpeelgelegenheid = 16,
           
		[Description("Geen")]
        Geen = 32,
	} 
	
		/// <summary>
	/// DataValNummer = 296 
	/// Voorlichtinglijst BDSplus296Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum VoorlichtinglijstBdsplus296multiple
	{   	 

			 

			           
		[Description("Voeding")]
        Voeding = 2,
           
		[Description("Vitamines")]
        Vitamines = 4,
           
		[Description("Verzorging/hoofdluis")]
        VerzorgingHoofdluis = 8,
           
		[Description("Ontlasten/plassen/zindelijkheid")]
        OntlastenPlassenZindelijkheid = 16,
           
		[Description("Mondgezondheid")]
        Mondgezondheid = 32,
           
		[Description("Slapen")]
        Slapen = 64,
           
		[Description("Bewegen/houding")]
        BewegenHouding = 128,
           
		[Description("Veiligheid")]
        Veiligheid = 256,
           
		[Description("Roken in bijzijn van kind")]
        RokenInBijzijnVanKind = 512,
           
		[Description("Ouderschap/opvoeding/dagindeling")]
        OuderschapOpvoedingDagindeling = 1024,
           
		[Description("Speelgoed/vrijetijdsteding/verenigingen")]
        SpeelgoedVrijetijdstedingVerenigingen = 2048,
           
		[Description("Ontwikkeling/spraak- en taal")]
        OntwikkelingSpraakEnTaal = 4096,
           
		[Description("Relaties/vrienden/pesten")]
        RelatiesVriendenPesten = 8192,
           
		[Description("Vaccinaties")]
        Vaccinaties = 16384,
           
		[Description("Klachten zoals pijn, angst, aggressie")]
        KlachtenZoalsPijnAngstAggressie = 32768,
           
		[Description("Behandeling- en zorgvoorzieningen")]
        BehandelingEnZorgvoorzieningen = 65536,
           
		[Description("Verslavingsrisico's")]
        VerslavingsrisicoS = 131072,
           
		[Description("Kinderopvang/peuterspeelzaal/onderwijs")]
        KinderopvangPeuterspeelzaalOnderwijs = 262144,
           
		[Description("(seksuele) Rijping/relaties")]
        SeksueleRijpingRelaties = 524288,
           
		[Description("SOA-preventie/anticonceptie")]
        SoaPreventieAnticonceptie = 1048576,
           
		[Description("Verblijf in het buitenland")]
        VerblijfInHetBuitenland = 2097152,
           
		[Description("Psychosociaal")]
        Psychosociaal = 4194304,
           
		[Description("(Opvoed) cursus")]
        OpvoedCursus = 8388608,
           
		[Description("Regelmaat/rust")]
        RegelmaatRust = 134217728,
           
		[Description("Anders")]
        Anders = 268435456,
	} 
	
		/// <summary>
	/// DataValNummer = 361 
	/// Gegeven voorlichting361Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum GegevenVoorlichting361multiple
	{   	 

			 

			           
		[Description("Voeding")]
        Voeding = 2,
           
		[Description("Vitamines")]
        Vitamines = 4,
           
		[Description("Verzorging/hoofdluis")]
        VerzorgingHoofdluis = 8,
           
		[Description("Ontlasten/plassen/zindelijkheid")]
        OntlastenPlassenZindelijkheid = 16,
           
		[Description("Mondgezondheid")]
        Mondgezondheid = 32,
           
		[Description("Slapen")]
        Slapen = 64,
           
		[Description("Beweging/houding")]
        BewegingHouding = 128,
           
		[Description("Veligheid")]
        Veligheid = 256,
           
		[Description("Roken in bijzijn kind")]
        RokenInBijzijnKind = 512,
           
		[Description("Ouderschap/opvoeding/dagindeling")]
        OuderschapOpvoedingDagindeling = 1024,
           
		[Description("Speelgoed/vrijetijdsbesteding/verenigingen")]
        SpeelgoedVrijetijdsbestedingVerenigingen = 2048,
           
		[Description("Ontwikkelingsfase problematiek/spraak-taalontwi")]
        OntwikkelingsfaseProblematiekSpraakTaalontwi = 4096,
           
		[Description("Relaties/vrienden/leeftijdgenoten")]
        RelatiesVriendenLeeftijdgenoten = 8192,
           
		[Description("Vaccinaties")]
        Vaccinaties = 16384,
           
		[Description("Klachten (pijn, angst, concentratie, aggressie")]
        KlachtenPijnAngstConcentratieAggressie = 32768,
           
		[Description("Behandelingsmogelijkheden/welzijns voorz")]
        BehandelingsmogelijkhedenWelzijnsVoorz = 65536,
           
		[Description("Verslavingsrisico")]
        Verslavingsrisico = 131072,
           
		[Description("Kinderopvang/peuterspeelzaal/onderwijs")]
        KinderopvangPeuterspeelzaalOnderwijs = 262144,
           
		[Description("(seksuele) Rijping, relaties")]
        SeksueleRijpingRelaties = 524288,
           
		[Description("SOA preventie/anticonceptie")]
        SoaPreventieAnticonceptie = 1048576,
           
		[Description("Verblijf in buitenland")]
        VerblijfInBuitenland = 2097152,
           
		[Description("Psychosociaal")]
        Psychosociaal = 4194304,
           
		[Description("Productinformatie GGD")]
        ProductinformatieGgd = 8388608,
           
		[Description("Verwijzers")]
        Verwijzers = 16777216,
           
		[Description("Anders")]
        Anders = 33554432,
	} 
	
		/// <summary>
	/// DataValNummer = 363 
	/// Aard bijzonderheden spraak/taal ontw363Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum AardBijzonderhedenSpraakTaalOntw363multiple
	{   	 

			 

			           
		[Description("Meer dan twee talen sprekend")]
        MeerDanTweeTalenSprekend = 2,
           
		[Description("Problemen ten gevolge van meertaligheid")]
        ProblemenTenGevolgeVanMeertaligheid = 4,
           
		[Description("Gehoor (perceptieve of conductieve  verliezen)")]
        GehoorPerceptieveOfConductieveVerliezen = 8,
           
		[Description("Mondgedrag (habitueel mondademen, kwijlen,speen)")]
        MondgedragHabitueelMondademenKwijlenSpeen = 16,
           
		[Description("Stemproductie (hyper /hypokinetisch")]
        StemproductieHyperHypokinetisch = 32,
           
		[Description("Stemkwaliteit (hees, schor,)")]
        StemkwaliteitHeesSchor = 64,
           
		[Description("Foutieve spreekademhaling")]
        FoutieveSpreekademhaling = 128,
           
		[Description("Articulatiestoornissen")]
        Articulatiestoornissen = 256,
           
		[Description("Nasaliteit")]
        Nasaliteit = 512,
           
		[Description("Onvloeiendheid (stotteren, broddelen, )")]
        OnvloeiendheidStotterenBroddelen = 1024,
           
		[Description("Taalbegrip")]
        Taalbegrip = 2048,
           
		[Description("Taalproductie")]
        Taalproductie = 4096,
           
		[Description("Anders")]
        Anders = 8192,
	} 
	
		/// <summary>
	/// DataValNummer = 364 
	/// Risico inventarisatie meisjesbesnijdenis364Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum RisicoInventarisatieMeisjesbesnijdenis364multiple
	{   	 

			 

			           
		[Description("Geen")]
        Geen = 2,
           
		[Description("Land v herkomst hoog percentage")]
        LandVHerkomstHoogPercentage = 4,
           
		[Description("Moeder is besneden")]
        MoederIsBesneden = 8,
           
		[Description("Oudere zusje(s) besneden")]
        OudereZusjeSBesneden = 16,
           
		[Description("Gezin met veel fam in Nederland")]
        GezinMetVeelFamInNederland = 32,
           
		[Description("Gezin niet of slecht geintregeerd")]
        GezinNietOfSlechtGeintregeerd = 64,
           
		[Description("Gezin keert regelmatig terug land herkomst")]
        GezinKeertRegelmatigTerugLandHerkomst = 128,
	} 
	
		/// <summary>
	/// DataValNummer = 365 
	/// Ingrijpende gebeurtenis365Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum IngrijpendeGebeurtenis365multiple
	{   	 

			 

			           
		[Description("Conflicten/ruzies binnen gezin")]
        ConflictenRuziesBinnenGezin = 2,
           
		[Description("Conflicten/ruzies buiten gezin")]
        ConflictenRuziesBuitenGezin = 4,
           
		[Description("Echtscheiding of langere tijd weggaan gezinslid")]
        EchtscheidingOfLangereTijdWeggaanGezinslid = 8,
           
		[Description("Een meisje zwanger gemaakt")]
        EenMeisjeZwangerGemaakt = 16,
           
		[Description("Geweld of mishandeling tussen ouders")]
        GeweldOfMishandelingTussenOuders = 32,
           
		[Description("Langdurig ziekhuis opname kind")]
        LangdurigZiekhuisOpnameKind = 64,
           
		[Description("Langdurig ziekte of opname broer/zus")]
        LangdurigZiekteOfOpnameBroerZus = 128,
           
		[Description("Langdurig ziekte of opname ouder(S)")]
        LangdurigZiekteOfOpnameOuderS = 256,
           
		[Description("Ongevallen")]
        Ongevallen = 512,
           
		[Description("Overlijden ander persoon")]
        OverlijdenAnderPersoon = 1024,
           
		[Description("Overlijden van broer of zus")]
        OverlijdenVanBroerOfZus = 2048,
           
		[Description("Overlijden vader/moeder")]
        OverlijdenVaderMoeder = 4096,
           
		[Description("Problemen drank,verslaving van ouder")]
        ProblemenDrankVerslavingVanOuder = 8192,
           
		[Description("Problemen geld/inkomen ouder")]
        ProblemenGeldInkomenOuder = 16384,
           
		[Description("Psychische ziekte broer/zus")]
        PsychischeZiekteBroerZus = 32768,
           
		[Description("Problemen met ander kind in gezin")]
        ProblemenMetAnderKindInGezin = 65536,
           
		[Description("Problemen met werk/werkloosheid ouder")]
        ProblemenMetWerkWerkloosheidOuder = 131072,
           
		[Description("Problemen met nieuwe ouder")]
        ProblemenMetNieuweOuder = 262144,
           
		[Description("Psychische ziekte va ouder(s)")]
        PsychischeZiekteVaOuderS = 524288,
	} 
	
		/// <summary>
	/// DataValNummer = 370 
	/// Soort voor buitenschoolse voorz370Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum SoortVoorBuitenschoolseVoorz370multiple
	{   	 

			 

			           
		[Description("Kinderdagopvang")]
        Kinderdagopvang = 2,
           
		[Description("Buitenschoolse opvang")]
        BuitenschoolseOpvang = 4,
           
		[Description("Gastouderopvang")]
        Gastouderopvang = 8,
           
		[Description("Ouderparticipatie creche")]
        OuderparticipatieCreche = 16,
           
		[Description("Peuterspeelzaal")]
        Peuterspeelzaal = 32,
           
		[Description("Peuterspeelzaal, VVE")]
        PeuterspeelzaalVve = 64,
           
		[Description("Informeel geregelde gastouder/opppas")]
        InformeelGeregeldeGastouderOpppas = 128,
           
		[Description("Kinderdagopvang plus")]
        KinderdagopvangPlus = 256,
           
		[Description("Kinderdagcentrum")]
        Kinderdagcentrum = 512,
           
		[Description("Gespecialiseerde opvang kinderen met handicap")]
        GespecialiseerdeOpvangKinderenMetHandicap = 1024,
           
		[Description("MKD")]
        Mkd = 2048,
           
		[Description("Anders")]
        Anders = 4096,
	} 
	
		/// <summary>
	/// DataValNummer = 382 
	/// Keuzelijst bijz. heupen382Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum KeuzelijstBijzHeupen382multiple
	{   	 

			 

			           
		[Description("Abductie beperking")]
        AbductieBeperking = 2,
           
		[Description("Kniehoogteverschil")]
        Kniehoogteverschil = 4,
           
		[Description("Bilplooiverschil")]
        Bilplooiverschil = 8,
           
		[Description("Beenlengteverschil")]
        Beenlengteverschil = 16,
           
		[Description("Anders")]
        Anders = 32,
	} 
	
		/// <summary>
	/// DataValNummer = 385 
	/// Verwijzing naar385Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum VerwijzingNaar385multiple
	{   	 

			 

			           
		[Description("Huisarts")]
        Huisarts = 2,
           
		[Description("Kinderarts")]
        Kinderarts = 4,
           
		[Description("(kinder)Fysiotherapeut")]
        KinderFysiotherapeut = 8,
           
		[Description("Logopedist (extern)")]
        LogopedistExtern = 16,
           
		[Description("AMW")]
        Amw = 32,
           
		[Description("AMK")]
        Amk = 64,
           
		[Description("Bureau Jeugdzorg")]
        BureauJeugdzorg = 128,
           
		[Description("GGZ")]
        Ggz = 256,
           
		[Description("Integrale Vroeghulp")]
        IntegraleVroeghulp = 512,
           
		[Description("VVE")]
        Vve = 1024,
           
		[Description("Home start")]
        HomeStart = 2048,
           
		[Description("Audiologisch Centrum")]
        AudiologischCentrum = 4096,
           
		[Description("Peuterspeelzalen (PSZ)")]
        PeuterspeelzalenPsz = 8192,
           
		[Description("Oogarts/Ortoptist")]
        OogartsOrtoptist = 16384,
           
		[Description("Diëtist")]
        DiTist = 32768,
           
		[Description("Thuisbegeleiding")]
        Thuisbegeleiding = 65536,
           
		[Description("Voorzorg")]
        Voorzorg = 131072,
           
		[Description("Verloskundige")]
        Verloskundige = 262144,
           
		[Description("Kraamzorg")]
        Kraamzorg = 524288,
           
		[Description("Lactatiekundige")]
        Lactatiekundige = 1048576,
           
		[Description("Triple P niveau 3")]
        TriplePNiveau3 = 2097152,
           
		[Description("Triple P niveau 4 individueel")]
        TriplePNiveau4Individueel = 4194304,
           
		[Description("Triple P niveau 4 groep")]
        TriplePNiveau4Groep = 8388608,
           
		[Description("Triple P niveau 5 Triple P plus")]
        TriplePNiveau5TriplePPlus = 16777216,
           
		[Description("Anders")]
        Anders = 33554432,
	} 
	
		/// <summary>
	/// DataValNummer = 394 
	/// Sport394Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Sport394multiple
	{   	 

			 

			           
		[Description("Voetbal")]
        Voetbal = 2,
           
		[Description("Hockey")]
        Hockey = 4,
           
		[Description("Gymnastiek")]
        Gymnastiek = 8,
           
		[Description("Dansen")]
        Dansen = 16,
           
		[Description("Paardrijden")]
        Paardrijden = 32,
           
		[Description("Atletiek")]
        Atletiek = 64,
           
		[Description("Honkbal")]
        Honkbal = 128,
           
		[Description("Rugby")]
        Rugby = 256,
           
		[Description("Handbal")]
        Handbal = 512,
           
		[Description("Judo")]
        Judo = 1024,
           
		[Description("Zwemmen")]
        Zwemmen = 2048,
           
		[Description("Turnen")]
        Turnen = 4096,
           
		[Description("Tennis")]
        Tennis = 8192,
           
		[Description("Korfbal")]
        Korfbal = 16384,
           
		[Description("Overige sporten")]
        OverigeSporten = 32768,
           
		[Description("Anders")]
        Anders = 65536,
	} 
	
		/// <summary>
	/// DataValNummer = 402 
	/// Advies logopedie402Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum AdviesLogopedie402multiple
	{   	 

			 

			           
		[Description("Stem")]
        Stem = 2,
           
		[Description("Taal")]
        Taal = 4,
           
		[Description("Articulatie")]
        Articulatie = 8,
           
		[Description("Auditief geheugen")]
        AuditiefGeheugen = 16,
           
		[Description("Mondgedrag")]
        Mondgedrag = 32,
           
		[Description("Stoornis in vloeiendheid")]
        StoornisInVloeiendheid = 64,
           
		[Description("Nasaliteit")]
        Nasaliteit = 128,
           
		[Description("Anders")]
        Anders = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 403 
	/// Overleg met (logopedie)403Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OverlegMetLogopedie403multiple
	{   	 

			 

			           
		[Description("Huisarts")]
        Huisarts = 2,
           
		[Description("Specialist")]
        Specialist = 4,
           
		[Description("JGZ arts")]
        JgzArts = 8,
           
		[Description("JGZ verpleegkundige")]
        JgzVerpleegkundige = 16,
           
		[Description("Leerkracht")]
        Leerkracht = 32,
           
		[Description("Intern begeleider")]
        InternBegeleider = 64,
           
		[Description("Behandeld logopedist")]
        BehandeldLogopedist = 128,
           
		[Description("Anders")]
        Anders = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 405 
	/// Observatie vroegtijdig opsporen aangeboren hartafw405Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum ObservatieVroegtijdigOpsporenAangeborenHartafw405multiple
	{   	 

			 

			           
		[Description("Vermoeid")]
        Vermoeid = 2,
           
		[Description("Passief")]
        Passief = 4,
           
		[Description("Ondervoede indruk")]
        OndervoedeIndruk = 8,
           
		[Description("Dysmorfien")]
        Dysmorfien = 16,
           
		[Description("Klamheid/zweterigheid")]
        KlamheidZweterigheid = 32,
           
		[Description("Dikke oogleden")]
        DikkeOogleden = 64,
           
		[Description("Snelle ademhaling")]
        SnelleAdemhaling = 128,
           
		[Description("Kortademigheid")]
        Kortademigheid = 256,
           
		[Description("Intrekkingen borstkas")]
        IntrekkingenBorstkas = 512,
           
		[Description("Ictus (heffing) borstkas")]
        IctusHeffingBorstkas = 1024,
           
		[Description("Asymmtrie borstkas")]
        AsymmtrieBorstkas = 2048,
           
		[Description("Oedeem")]
        Oedeem = 4096,
           
		[Description("Trommelstokvingers")]
        Trommelstokvingers = 8192,
           
		[Description("Horlogeglas nagels")]
        HorlogeglasNagels = 16384,
	} 
	
		/// <summary>
	/// DataValNummer = 408 
	/// Inspanningstolerantie408Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Inspanningstolerantie408multiple
	{   	 

			 

			           
		[Description("Snel vermoeid")]
        SnelVermoeid = 2,
           
		[Description("Transpireren")]
        Transpireren = 4,
           
		[Description("Snelle ademhaling")]
        SnelleAdemhaling = 8,
           
		[Description("Wel honger, fles niet leeg")]
        WelHongerFlesNietLeeg = 16,
           
		[Description("Stopt met drinken aan borst")]
        StoptMetDrinkenAanBorst = 32,
           
		[Description("Blauwe, grauwe kleur")]
        BlauweGrauweKleur = 64,
           
		[Description("Onderbreking bij hurkzit")]
        OnderbrekingBijHurkzit = 128,
           
		[Description("Pijn in benen")]
        PijnInBenen = 256,
           
		[Description("Wegrakingen")]
        Wegrakingen = 512,
           
		[Description("Hartkloppingen")]
        Hartkloppingen = 1024,
           
		[Description("Pijn op borst")]
        PijnOpBorst = 2048,
	} 
	
		/// <summary>
	/// DataValNummer = 415 
	/// Verslavingsrisico415Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Verslavingsrisico415multiple
	{   	 

			 

			           
		[Description("Games")]
        Games = 2,
           
		[Description("Roken")]
        Roken = 4,
           
		[Description("Alcohol")]
        Alcohol = 8,
           
		[Description("Drugs")]
        Drugs = 16,
           
		[Description("Geneesmiddelen")]
        Geneesmiddelen = 32,
           
		[Description("Gokken")]
        Gokken = 64,
           
		[Description("Anders")]
        Anders = 128,
	} 
	
		/// <summary>
	/// DataValNummer = 421 
	/// Omgaan met leeftijdgenoten421Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OmgaanMetLeeftijdgenoten421multiple
	{   	 

			 

			           
		[Description("Legt contact met leeftijdgenoten")]
        LegtContactMetLeeftijdgenoten = 2,
           
		[Description("Speelt met leeftijdgenoten")]
        SpeeltMetLeeftijdgenoten = 4,
           
		[Description("Kan overweg met leeftijdgenoten")]
        KanOverwegMetLeeftijdgenoten = 8,
           
		[Description("Heeft vrienden")]
        HeeftVrienden = 16,
           
		[Description("Wordt gepest")]
        WordtGepest = 32,
           
		[Description("Pest")]
        Pest = 64,
           
		[Description("Schopt, slaat of bijt")]
        SchoptSlaatOfBijt = 128,
           
		[Description("Anders")]
        Anders = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 429 
	/// Bijzonderheden onderste extremiteiten429Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BijzonderhedenOndersteExtremiteiten429multiple
	{   	 

			 

			           
		[Description("O-benen")]
        OBenen = 2,
           
		[Description("X-benen")]
        XBenen = 4,
           
		[Description("Vermoeden van adolescenten patellapijn")]
        VermoedenVanAdolescentenPatellapijn = 8,
           
		[Description("Beenlengte verschil")]
        BeenlengteVerschil = 16,
           
		[Description("Anders")]
        Anders = 32,
	} 
	
		/// <summary>
	/// DataValNummer = 432 
	/// Interventie432Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Interventie432multiple
	{   	 

			 

			           
		[Description("Voorlichting")]
        Voorlichting = 2,
           
		[Description("Advies")]
        Advies = 4,
           
		[Description("Consultatie/inlichtingen vragen")]
        ConsultatieInlichtingenVragen = 8,
           
		[Description("Extra onderzoek/gesprek")]
        ExtraOnderzoekGesprek = 16,
           
		[Description("Melding")]
        Melding = 32,
           
		[Description("Verwijzing")]
        Verwijzing = 64,
           
		[Description("Contactmoment op indicatie")]
        ContactmomentOpIndicatie = 1024,
           
		[Description("Inloopspreekuuur")]
        Inloopspreekuuur = 2048,
           
		[Description("Interventie (plan van aanpak)")]
        InterventiePlanVanAanpak = 4096,
           
		[Description("Opvoedspreekuur")]
        Opvoedspreekuur = 8192,
           
		[Description("Overloopconsult")]
        Overloopconsult = 16384,
           
		[Description("Vangnet spreekuur")]
        VangnetSpreekuur = 32768,
           
		[Description("Anders")]
        Anders = 65536,
	} 
	
		/// <summary>
	/// DataValNummer = 433 
	/// Ingrijpende gebeurtenis 2433Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum IngrijpendeGebeurtenis2433multiple
	{   	 

			 

			           
		[Description("Regelmatig ruzie tussen ouders onderling")]
        RegelmatigRuzieTussenOudersOnderling = 2,
           
		[Description("Seksueel misbruik")]
        SeksueelMisbruik = 4,
           
		[Description("Slachtoffer geweld/criminaliteit")]
        SlachtofferGeweldCriminaliteit = 8,
           
		[Description("Uitbreiding gezin")]
        UitbreidingGezin = 16,
           
		[Description("Uitbreiding gezin met stiefouder-broer /zus")]
        UitbreidingGezinMetStiefouderBroerZus = 32,
           
		[Description("Verhuizing/migratie")]
        VerhuizingMigratie = 64,
           
		[Description("Vlucht")]
        Vlucht = 128,
           
		[Description("Woonproblemen")]
        Woonproblemen = 256,
           
		[Description("Zelf gediscrimineerd worden")]
        ZelfGediscrimineerdWorden = 512,
           
		[Description("Zelf gepest worden")]
        ZelfGepestWorden = 1024,
           
		[Description("Zelf mishandeld door andere volwassene")]
        ZelfMishandeldDoorAndereVolwassene = 2048,
           
		[Description("Zelf mishandeld door ouder")]
        ZelfMishandeldDoorOuder = 4096,
           
		[Description("Zelf problemen met drank/verslaving")]
        ZelfProblemenMetDrankVerslaving = 8192,
           
		[Description("Zelf problemen met geld/inkomen hebben")]
        ZelfProblemenMetGeldInkomenHebben = 16384,
           
		[Description("Zelf problemen op school")]
        ZelfProblemenOpSchool = 32768,
           
		[Description("Zwanger worden")]
        ZwangerWorden = 65536,
           
		[Description("Anders")]
        Anders = 131072,
	} 
	
		/// <summary>
	/// DataValNummer = 437 
	/// Soort voor-buitenschoolse voorziening437Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum SoortVoorBuitenschoolseVoorziening437multiple
	{   	 

			 

			           
		[Description("Kinderdagopvang")]
        Kinderdagopvang = 2,
           
		[Description("Buitenschoolse opvang (incl naschoolse opvang)")]
        BuitenschoolseOpvangInclNaschoolseOpvang = 4,
           
		[Description("Gastouderopvang")]
        Gastouderopvang = 8,
           
		[Description("Ouderparticipatiecrèches")]
        OuderparticipatiecrChes = 16,
           
		[Description("Peuterspeelzaal")]
        Peuterspeelzaal = 32,
           
		[Description("Informeel geregelde gastouder / oppas")]
        InformeelGeregeldeGastouderOppas = 64,
           
		[Description("Kinderopvang Plus")]
        KinderopvangPlus = 128,
           
		[Description("Kinderdagcentrum")]
        Kinderdagcentrum = 256,
           
		[Description("Gespecialiseerde opvang voor kinderen met handicap")]
        GespecialiseerdeOpvangVoorKinderenMetHandicap = 512,
           
		[Description("MKD")]
        Mkd = 1024,
           
		[Description("Anders")]
        Anders = 2048,
	} 
	
		/// <summary>
	/// DataValNummer = 442 
	/// Risico inventarisatie VGV442Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum RisicoInventarisatieVgv442multiple
	{   	 

			 

			           
		[Description("Moeder afkomstig uit risicoland")]
        MoederAfkomstigUitRisicoland = 2,
           
		[Description("Vader afkomstig uit risicoland")]
        VaderAfkomstigUitRisicoland = 4,
           
		[Description("Moeder besneden")]
        MoederBesneden = 8,
           
		[Description("Partner en directe familie positief over VGV")]
        PartnerEnDirecteFamiliePositiefOverVgv = 16,
           
		[Description("Eén of meer zusjes besneden")]
        ENOfMeerZusjesBesneden = 32,
           
		[Description("Gezin regelmatig op (familie)bezoek buitenland")]
        GezinRegelmatigOpFamilieBezoekBuitenland = 64,
           
		[Description("Gezin met veel familie- en/of omgevingsdruk")]
        GezinMetVeelFamilieEnOfOmgevingsdruk = 128,
           
		[Description("Gezin nog niet of slecht geïntegreerd")]
        GezinNogNietOfSlechtGeNtegreerd = 256,
           
		[Description("Anders")]
        Anders = 512,
	} 
	
		/// <summary>
	/// DataValNummer = 444 
	/// Doelgroep VVE444Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum DoelgroepVve444multiple
	{   	 

			 

			           
		[Description("Gesignaleerde (dreigende) taalachterstand")]
        GesignaleerdeDreigendeTaalachterstand = 2,
           
		[Description("Gesign (dreigende) taalachterst niet NL opvoeding")]
        GesignDreigendeTaalachterstNietNlOpvoeding = 4,
           
		[Description("Gesign (dreigende) taalachterst taalarmoede")]
        GesignDreigendeTaalachterstTaalarmoede = 8,
           
		[Description("Leerlinggewicht 0,3")]
        Leerlinggewicht03 = 16,
           
		[Description("Leerlinggewicht 1,2")]
        Leerlinggewicht12 = 32,
           
		[Description("Allochtone afkomst")]
        AllochtoneAfkomst = 64,
           
		[Description("Niet Nederlandstalige opvoeding")]
        NietNederlandstaligeOpvoeding = 128,
           
		[Description("Ouders volgen inburgeringscursus")]
        OudersVolgenInburgeringscursus = 256,
           
		[Description("Ontwikkelingsachterstand soc/emotioneel gebied")]
        OntwikkelingsachterstandSocEmotioneelGebied = 512,
           
		[Description("Ontwikkelingsachterstand op motorisch gebied")]
        OntwikkelingsachterstandOpMotorischGebied = 1024,
           
		[Description("Sociaal-economische status")]
        SociaalEconomischeStatus = 2048,
           
		[Description("Alleenstaand ouderschap")]
        AlleenstaandOuderschap = 4096,
           
		[Description("Opvoedingsspanning")]
        Opvoedingsspanning = 8192,
           
		[Description("Verslaving bij ouders/verzorgers")]
        VerslavingBijOudersVerzorgers = 16384,
           
		[Description("Chronische ziekte van een gezinslid")]
        ChronischeZiekteVanEenGezinslid = 32768,
           
		[Description("Psychische/psychiatrische probl ouders/verzorgers")]
        PsychischePsychiatrischeProblOudersVerzorgers = 65536,
           
		[Description("Tienermoeder")]
        Tienermoeder = 131072,
           
		[Description("Minder verstandelijke vermogens ouders/verzorgers")]
        MinderVerstandelijkeVermogensOudersVerzorgers = 262144,
           
		[Description("Zorgwekkende opvoedsituatie")]
        ZorgwekkendeOpvoedsituatie = 524288,
           
		[Description("Pre-of dysmatuur kind")]
        PreOfDysmatuurKind = 1048576,
           
		[Description("Geadopteerd kind")]
        GeadopteerdKind = 2097152,
           
		[Description("Langdurig werkloos/arbeidsongesch ouders/verzorger")]
        LangdurigWerkloosArbeidsongeschOudersVerzorger = 4194304,
           
		[Description("Pleegkind")]
        Pleegkind = 8388608,
           
		[Description("Overleden gezinslid")]
        OverledenGezinslid = 16777216,
           
		[Description("Financiële problemen")]
        FinanciLeProblemen = 33554432,
           
		[Description("Anders")]
        Anders = 67108864,
	} 
	
		/// <summary>
	/// DataValNummer = 446 
	/// Reden geen VVE446Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum RedenGeenVve446multiple
	{   	 

			 

			           
		[Description("Kind zit nog in toeleidingstraject")]
        KindZitNogInToeleidingstraject = 2,
           
		[Description("Kind bezoekt kinderdagverblijf")]
        KindBezoektKinderdagverblijf = 4,
           
		[Description("Kind bezoekt reguliere peuterspeelzaal")]
        KindBezoektRegulierePeuterspeelzaal = 8,
           
		[Description("Kind gaat naar gastouder")]
        KindGaatNaarGastouder = 16,
           
		[Description("Ouders/verzorgers zijn niet gemotiveerd")]
        OudersVerzorgersZijnNietGemotiveerd = 32,
           
		[Description("Financiële redenen")]
        FinanciLeRedenen = 64,
           
		[Description("VVE-locatie te ver weg")]
        VveLocatieTeVerWeg = 128,
           
		[Description("Anders")]
        Anders = 256,
	} 
	
		/// <summary>
	/// DataValNummer = 447 
	/// Bijzonderheden uiterlijk oor447Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BijzonderhedenUiterlijkOor447multiple
	{   	 

			 

			           
		[Description("Afwijkende vorm kraakbenig deel van het oor")]
        AfwijkendeVormKraakbenigDeelVanHetOor = 2,
           
		[Description("Afwijkende stand")]
        AfwijkendeStand = 4,
           
		[Description("Lage implantatie")]
        LageImplantatie = 8,
           
		[Description("Resten van kieuwboogspleten")]
        RestenVanKieuwboogspleten = 16,
           
		[Description("Bij-oortje")]
        BijOortje = 32,
           
		[Description("Anders")]
        Anders = 64,
	} 
	
		/// <summary>
	/// DataValNummer = 450 
	/// Soort vaccinatie450Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum SoortVaccinatie450multiple
	{   	 

			 

			           
		[Description("DKTP")]
        Dktp = 2,
           
		[Description("DTP")]
        Dtp = 4,
           
		[Description("Hib")]
        Hib = 8,
           
		[Description("BMR")]
        Bmr = 16,
           
		[Description("Hepatitis B")]
        HepatitisB = 32,
           
		[Description("Men C")]
        MenC = 64,
           
		[Description("BCG")]
        Bcg = 128,
           
		[Description("Pneu")]
        Pneu = 256,
           
		[Description("HPV")]
        Hpv = 512,
           
		[Description("DKTP - Hib")]
        DktpHib = 1024,
           
		[Description("DKTP - Hib - Hepatitis B")]
        DktpHibHepatitisB = 2048,
           
		[Description("Anders")]
        Anders = 4096,
	} 
	
		/// <summary>
	/// DataValNummer = 452 
	/// Bron toestemming452Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BronToestemming452multiple
	{   	 

			 

			           
		[Description("Kind")]
        Kind = 2,
           
		[Description("Ouder")]
        Ouder = 4,
           
		[Description("Anders")]
        Anders = 8,
	} 
	
		/// <summary>
	/// DataValNummer = 455 
	/// Prenatale zorg455Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum PrenataleZorg455multiple
	{   	 

			 

			           
		[Description("Huisarts")]
        Huisarts = 2,
           
		[Description("Verloskundige")]
        Verloskundige = 4,
           
		[Description("Gynaecoloog")]
        Gynaecoloog = 8,
           
		[Description("Prenatale begeleiding JGZ individueel")]
        PrenataleBegeleidingJgzIndividueel = 16,
           
		[Description("Prenatale begeleiding JGZ groep")]
        PrenataleBegeleidingJgzGroep = 32,
           
		[Description("VoorZorg")]
        Voorzorg = 64,
           
		[Description("Anders")]
        Anders = 128,
	} 
	
		/// <summary>
	/// DataValNummer = 458 
	/// Omgang broer/zus/leeftijdgenoten458Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OmgangBroerZusLeeftijdgenoten458multiple
	{   	 

			 

			           
		[Description("Legt geen / moeilijk contact met leeftijdgenoten")]
        LegtGeenMoeilijkContactMetLeeftijdgenoten = 2,
           
		[Description("Speelt niet / weinig met leeftijdgenoten")]
        SpeeltNietWeinigMetLeeftijdgenoten = 4,
           
		[Description("Kan niet overweg met leeftijdgenoten")]
        KanNietOverwegMetLeeftijdgenoten = 8,
           
		[Description("Heeft geen / onvoldoende vrienden")]
        HeeftGeenOnvoldoendeVrienden = 16,
           
		[Description("Wordt gepest")]
        WordtGepest = 32,
           
		[Description("Pest")]
        Pest = 64,
           
		[Description("Vecht, schopt, slaat of bijt")]
        VechtSchoptSlaatOfBijt = 128,
           
		[Description("Kan niet alleen zijn")]
        KanNietAlleenZijn = 256,
           
		[Description("Is (extreem) jaloers op brusje")]
        IsExtreemJaloersOpBrusje = 512,
           
		[Description("Maakt veel ruzie met brusje")]
        MaaktVeelRuzieMetBrusje = 1024,
           
		[Description("Heeft moeite met (extreem) gedrag van brusje")]
        HeeftMoeiteMetExtreemGedragVanBrusje = 2048,
           
		[Description("Anders")]
        Anders = 4096,
	} 
	
		/// <summary>
	/// DataValNummer = 459 
	/// Zindelijk459Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Zindelijk459multiple
	{   	 

			 

			           
		[Description("Overdag zindelijk voor urine")]
        OverdagZindelijkVoorUrine = 2,
           
		[Description("'s Nachts zindelijk voor urine")]
        SNachtsZindelijkVoorUrine = 4,
           
		[Description("Opnieuw onzindelijk voor urine")]
        OpnieuwOnzindelijkVoorUrine = 8,
           
		[Description("Zindelijk voor ontlasting")]
        ZindelijkVoorOntlasting = 16,
           
		[Description("Opnieuw onzindelijk voor ontlasting")]
        OpnieuwOnzindelijkVoorOntlasting = 32,
           
		[Description("Anders")]
        Anders = 64,
	} 
	
		/// <summary>
	/// DataValNummer = 460 
	/// Anticonceptie460Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Anticonceptie460multiple
	{   	 

			 

			           
		[Description("Orale anticonceptie")]
        OraleAnticonceptie = 2,
           
		[Description("Condoom")]
        Condoom = 4,
           
		[Description("Vrouwencondoom")]
        Vrouwencondoom = 8,
           
		[Description("Implanon")]
        Implanon = 16,
           
		[Description("IUD")]
        Iud = 32,
           
		[Description("Mirena IUD")]
        MirenaIud = 64,
           
		[Description("Pessarium")]
        Pessarium = 128,
           
		[Description("Prikpil")]
        Prikpil = 256,
           
		[Description("Sterilisatie")]
        Sterilisatie = 512,
           
		[Description("Sterilisatie partner")]
        SterilisatiePartner = 1024,
           
		[Description("Onconventioneel (bv coïtus interruptus)")]
        OnconventioneelBvCoTusInterruptus = 2048,
	} 
	
		/// <summary>
	/// DataValNummer = 485 
	/// Trommelvlies bevindingen485Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum TrommelvliesBevindingen485multiple
	{   	 

			 

			           
		[Description("Bomberend")]
        Bomberend = 2,
           
		[Description("Roodheid")]
        Roodheid = 4,
           
		[Description("Intrekking")]
        Intrekking = 8,
           
		[Description("Perforatie")]
        Perforatie = 16,
           
		[Description("Loopoor")]
        Loopoor = 32,
           
		[Description("Beluchtigingbuisje")]
        Beluchtigingbuisje = 64,
           
		[Description("Glue ear")]
        GlueEar = 128,
           
		[Description("Vocht")]
        Vocht = 256,
           
		[Description("Irritatie")]
        Irritatie = 512,
           
		[Description("Anders")]
        Anders = 1024,
	} 
	
		/// <summary>
	/// DataValNummer = 486 
	/// afwijkingen huid/haar/nagel486Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum AfwijkingenHuidHaarNagel486multiple
	{   	 

			 

			           
		[Description("Hematoom")]
        Hematoom = 2,
           
		[Description("Wond, litteken")]
        WondLitteken = 4,
           
		[Description("Eczeem")]
        Eczeem = 8,
           
		[Description("Naevus")]
        Naevus = 16,
           
		[Description("Café au lait")]
        CafAuLait = 32,
           
		[Description("Vitiligo")]
        Vitiligo = 64,
           
		[Description("Hemangioom")]
        Hemangioom = 128,
           
		[Description("Molluscum contagiosum")]
        MolluscumContagiosum = 256,
           
		[Description("Wrat")]
        Wrat = 512,
           
		[Description("Schimmel")]
        Schimmel = 1024,
           
		[Description("Luieruitslag")]
        Luieruitslag = 2048,
           
		[Description("Hoofdluis")]
        Hoofdluis = 4096,
           
		[Description("Acne")]
        Acne = 8192,
           
		[Description("Ringworm")]
        Ringworm = 16384,
           
		[Description("Berg")]
        Berg = 32768,
           
		[Description("Smetplekken")]
        Smetplekken = 65536,
           
		[Description("Impetigo")]
        Impetigo = 131072,
           
		[Description("Striae")]
        Striae = 262144,
           
		[Description("Mongolenvlek")]
        Mongolenvlek = 524288,
           
		[Description("Ooievaarsbeet")]
        Ooievaarsbeet = 1048576,
           
		[Description("Hirsutisme")]
        Hirsutisme = 2097152,
           
		[Description("Milien")]
        Milien = 4194304,
           
		[Description("Droog")]
        Droog = 8388608,
           
		[Description("Anders")]
        Anders = 16777216,
	} 
	
		/// <summary>
	/// DataValNummer = 488 
	/// Voorlichting en advies488Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum VoorlichtingEnAdvies488multiple
	{   	 

			 

			           
		[Description("Voeding")]
        Voeding = 2,
           
		[Description("Vitamines")]
        Vitamines = 4,
           
		[Description("Verzorging / hoofdluis")]
        VerzorgingHoofdluis = 8,
           
		[Description("Ontlasten / plassen / zindelijkheid")]
        OntlastenPlassenZindelijkheid = 16,
           
		[Description("Mondgezondheid")]
        Mondgezondheid = 32,
           
		[Description("Slapen")]
        Slapen = 64,
           
		[Description("Bewegen / houding")]
        BewegenHouding = 128,
           
		[Description("Veiligheid")]
        Veiligheid = 256,
           
		[Description("Roken in bijzijn van kind")]
        RokenInBijzijnVanKind = 512,
           
		[Description("Ouderschap / opvoeding / dagindeling")]
        OuderschapOpvoedingDagindeling = 1024,
           
		[Description("Speelgoed / vrije tijdsbesteding / vereniging")]
        SpeelgoedVrijeTijdsbestedingVereniging = 2048,
           
		[Description("Ontwikkelingsfasen /  -probl / STO")]
        OntwikkelingsfasenProblSto = 4096,
           
		[Description("Relaties / vrienden / peers / ouders / pesten")]
        RelatiesVriendenPeersOudersPesten = 8192,
           
		[Description("Vaccinaties")]
        Vaccinaties = 16384,
           
		[Description("Klachten (pijn, angst, conc, agressie, moe/mat)")]
        KlachtenPijnAngstConcAgressieMoeMat = 32768,
           
		[Description("Behandelmogelijkheden/gezondheids-/welzijnvoorzien")]
        BehandelmogelijkhedenGezondheidsWelzijnvoorzien = 65536,
           
		[Description("Verslavingsrisico")]
        Verslavingsrisico = 131072,
           
		[Description("Kinderopvang / PSZ / onderwijs")]
        KinderopvangPszOnderwijs = 262144,
           
		[Description("Rijping (seksueel) / relaties")]
        RijpingSeksueelRelaties = 524288,
           
		[Description("SOA-preventie / anticonceptie")]
        SoaPreventieAnticonceptie = 1048576,
           
		[Description("Verblijf in het buitenland")]
        VerblijfInHetBuitenland = 2097152,
           
		[Description("Psychosociaal")]
        Psychosociaal = 4194304,
           
		[Description("Productinformatie GGD")]
        ProductinformatieGgd = 8388608,
           
		[Description("Verwijzers")]
        Verwijzers = 16777216,
           
		[Description("Groepsbijeenkomst")]
        Groepsbijeenkomst = 33554432,
           
		[Description("Opvoedspreekuur")]
        Opvoedspreekuur = 67108864,
           
		[Description("Groei (lengte en gewicht)")]
        GroeiLengteEnGewicht = 134217728,
           
		[Description("Anders")]
        Anders = 268435456,
	} 
	
		/// <summary>
	/// DataValNummer = 490 
	/// Vervolgbeleid490Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Vervolgbeleid490multiple
	{   	 

			 

			           
		[Description("Regulier consult")]
        RegulierConsult = 2,
           
		[Description("Voorlichting / advies")]
        VoorlichtingAdvies = 4,
           
		[Description("Controle")]
        Controle = 8,
           
		[Description("Verwijzing")]
        Verwijzing = 16,
           
		[Description("Begeleiding")]
        Begeleiding = 32,
	} 
	
		/// <summary>
	/// DataValNummer = 493 
	/// Bijzonderheden mondgedrag493Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum BijzonderhedenMondgedrag493multiple
	{   	 

			 

			           
		[Description("Duim- en vingerzuigen")]
        DuimEnVingerzuigen = 2,
           
		[Description("Speengebruik")]
        Speengebruik = 4,
           
		[Description("Habitueel mondademen")]
        HabitueelMondademen = 8,
           
		[Description("Afwijkende tongligging")]
        AfwijkendeTongligging = 16,
           
		[Description("Slikproblemen")]
        Slikproblemen = 32,
           
		[Description("Verstoorde sensomotoriek")]
        VerstoordeSensomotoriek = 64,
           
		[Description("Anders")]
        Anders = 128,
	} 
	
		/// <summary>
	/// DataValNummer = 498 
	/// Opv kenm opvoedsit498Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvKenmOpvoedsit498multiple
	{   	 

			 

			           
		[Description("Financiële problemen")]
        FinanciLeProblemen = 2,
           
		[Description("Geen aansluiting bij ontwikkeling kind")]
        GeenAansluitingBijOntwikkelingKind = 4,
           
		[Description("Geen veerkracht/vermoeidheid ouder/opvoeder")]
        GeenVeerkrachtVermoeidheidOuderOpvoeder = 8,
           
		[Description("Huiselijk geweld")]
        HuiselijkGeweld = 16,
           
		[Description("Ingrijpende gebeurtenissen")]
        IngrijpendeGebeurtenissen = 32,
           
		[Description("Negatieve jeugdervaringen  ouder")]
        NegatieveJeugdervaringenOuder = 64,
           
		[Description("Onveilige woonomgeving/ slechte behuizing")]
        OnveiligeWoonomgevingSlechteBehuizing = 128,
           
		[Description("Opvoedingsonmacht ouders")]
        OpvoedingsonmachtOuders = 256,
           
		[Description("Psychische problemen ouder/ opvoeder")]
        PsychischeProblemenOuderOpvoeder = 512,
           
		[Description("Relatieproblematiek/ gezinsconflicten")]
        RelatieproblematiekGezinsconflicten = 1024,
           
		[Description("Strafrechtelijke achtergrond")]
        StrafrechtelijkeAchtergrond = 2048,
           
		[Description("Verslavingsproblematiek ouders")]
        VerslavingsproblematiekOuders = 4096,
           
		[Description("Weinig steun/ opvoedingsisolement")]
        WeinigSteunOpvoedingsisolement = 8192,
           
		[Description("Weinig tijd/ aandacht voor kinderen")]
        WeinigTijdAandachtVoorKinderen = 16384,
           
		[Description("Werkloosheid")]
        Werkloosheid = 32768,
           
		[Description("Ziekte/ handicap ouder/gezinslid")]
        ZiekteHandicapOuderGezinslid = 65536,
           
		[Description("Anders")]
        Anders = 131072,
	} 
	
		/// <summary>
	/// DataValNummer = 500 
	/// Opv lich ontwikkeling500Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvLichOntwikkeling500multiple
	{   	 

			 

			           
		[Description("Bedplassen")]
        Bedplassen = 2,
           
		[Description("Broekpoepen")]
        Broekpoepen = 4,
           
		[Description("Duimzuigen / speen")]
        DuimzuigenSpeen = 8,
           
		[Description("Eetstoornis")]
        Eetstoornis = 16,
           
		[Description("Lichamelijk klachten / gezondheid")]
        LichamelijkKlachtenGezondheid = 32,
           
		[Description("Lichamelijke achterstand")]
        LichamelijkeAchterstand = 64,
           
		[Description("Lichamelijke groei")]
        LichamelijkeGroei = 128,
           
		[Description("Motorische ontwikkeling")]
        MotorischeOntwikkeling = 256,
           
		[Description("Seksuele ontwikkeling")]
        SeksueleOntwikkeling = 512,
           
		[Description("Snoepen")]
        Snoepen = 1024,
           
		[Description("Tics / hoofdbonken")]
        TicsHoofdbonken = 2048,
           
		[Description("Verstoring dag- nachtritme")]
        VerstoringDagNachtritme = 4096,
           
		[Description("Voeding- / eetproblemen")]
        VoedingEetproblemen = 8192,
           
		[Description("Voedselallergie")]
        Voedselallergie = 16384,
           
		[Description("Ziekenhuisopname")]
        Ziekenhuisopname = 32768,
           
		[Description("Zindelijkheid")]
        Zindelijkheid = 65536,
           
		[Description("Anders")]
        Anders = 131072,
	} 
	
		/// <summary>
	/// DataValNummer = 501 
	/// Opv verstandelijke ontwikkeling501Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvVerstandelijkeOntwikkeling501multiple
	{   	 

			 

			           
		[Description("Cognitieve achterstand")]
        CognitieveAchterstand = 2,
           
		[Description("Concentratieproblemen")]
        Concentratieproblemen = 4,
           
		[Description("Dyslexie")]
        Dyslexie = 8,
           
		[Description("Fantasie")]
        Fantasie = 16,
           
		[Description("Gewetensvorming / morele ontwikkeling")]
        GewetensvormingMoreleOntwikkeling = 32,
           
		[Description("Hoogbegaafd")]
        Hoogbegaafd = 64,
           
		[Description("Intelligentie")]
        Intelligentie = 128,
           
		[Description("Ontwikkelingsstimulering")]
        Ontwikkelingsstimulering = 256,
           
		[Description("Taal- / spraakontwikkeling")]
        TaalSpraakontwikkeling = 512,
           
		[Description("Anders")]
        Anders = 1024,
	} 
	
		/// <summary>
	/// DataValNummer = 502 
	/// Opv soc ontwikkeling502Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvSocOntwikkeling502multiple
	{   	 

			 

			           
		[Description("Contactprobleem / sociale vaardigheden")]
        ContactprobleemSocialeVaardigheden = 2,
           
		[Description("Discriminatie / racisme")]
        DiscriminatieRacisme = 4,
           
		[Description("Enig kind zijn")]
        EnigKindZijn = 8,
           
		[Description("Gepest worden")]
        GepestWorden = 16,
           
		[Description("Huisregels")]
        Huisregels = 32,
           
		[Description("Logeren")]
        Logeren = 64,
           
		[Description("Relatie met familie / grootouder")]
        RelatieMetFamilieGrootouder = 128,
           
		[Description("Rolgedrag jongen / meisje")]
        RolgedragJongenMeisje = 256,
           
		[Description("Ruzie met / tussen kinderen en broers / zussen")]
        RuzieMetTussenKinderenEnBroersZussen = 512,
           
		[Description("Taakjes / helpen in huis")]
        TaakjesHelpenInHuis = 1024,
           
		[Description("Verhuizen")]
        Verhuizen = 2048,
           
		[Description("Vrienden (geen / verkeerde)")]
        VriendenGeenVerkeerde = 4096,
           
		[Description("Woonomgeving / veiligheid")]
        WoonomgevingVeiligheid = 8192,
           
		[Description("Zak- / kostgeld")]
        ZakKostgeld = 16384,
           
		[Description("Zelf pesten")]
        ZelfPesten = 32768,
           
		[Description("Anders")]
        Anders = 65536,
	} 
	
		/// <summary>
	/// DataValNummer = 503 
	/// Opv emotionele ontwikkeling503Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvEmotioneleOntwikkeling503multiple
	{   	 

			 

			           
		[Description("Bang/ angstig")]
        BangAngstig = 2,
           
		[Description("Emotionele achterstand")]
        EmotioneleAchterstand = 4,
           
		[Description("Faalangst")]
        Faalangst = 8,
           
		[Description("Hechtingsroblematiek")]
        Hechtingsroblematiek = 16,
           
		[Description("Hoog sensitief")]
        HoogSensitief = 32,
           
		[Description("Jaloezie/rivaliteit")]
        JaloezieRivaliteit = 64,
           
		[Description("Moeilijk temperament")]
        MoeilijkTemperament = 128,
           
		[Description("Nachtmerries/ dromen")]
        NachtmerriesDromen = 256,
           
		[Description("Negatief zelfbeeld")]
        NegatiefZelfbeeld = 512,
           
		[Description("Niet weerbaar/ onzelfstandig")]
        NietWeerbaarOnzelfstandig = 1024,
           
		[Description("Omgaan met emoties")]
        OmgaanMetEmoties = 2048,
           
		[Description("Onzeker/ verlegen/ weinig zelfvertrouwen")]
        OnzekerVerlegenWeinigZelfvertrouwen = 4096,
           
		[Description("Psychische problemen")]
        PsychischeProblemen = 8192,
           
		[Description("Rouwverwerking")]
        Rouwverwerking = 16384,
           
		[Description("Scheidingsangst/ eenkennig")]
        ScheidingsangstEenkennig = 32768,
           
		[Description("Slaapproblemen")]
        Slaapproblemen = 65536,
           
		[Description("Suïcide")]
        SuCide = 131072,
           
		[Description("Verliefdheid")]
        Verliefdheid = 262144,
           
		[Description("Verwerken echtscheiding")]
        VerwerkenEchtscheiding = 524288,
           
		[Description("Anders")]
        Anders = 1048576,
	} 
	
		/// <summary>
	/// DataValNummer = 504 
	/// Opv spel en vrije tijd504Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvSpelEnVrijeTijd504multiple
	{   	 

			 

			           
		[Description("Buiten spelen/ speelomgeving")]
        BuitenSpelenSpeelomgeving = 2,
           
		[Description("Clubs/ Kindvoorzieningen")]
        ClubsKindvoorzieningen = 4,
           
		[Description("Huisdieren (aanschaf/ verzorging)")]
        HuisdierenAanschafVerzorging = 8,
           
		[Description("Materiële verlangens kind")]
        MateriLeVerlangensKind = 16,
           
		[Description("Regels en grenzen computerspellen/tv/sociale media")]
        RegelsEnGrenzenComputerspellenTvSocialeMedia = 32,
           
		[Description("Speelgoed en veiligheid")]
        SpeelgoedEnVeiligheid = 64,
           
		[Description("Vakantiebesteding/werk")]
        VakantiebestedingWerk = 128,
           
		[Description("Verjaardag/ feesten")]
        VerjaardagFeesten = 256,
           
		[Description("Zich niet kunnen vermaken /kunnen (samen)spelen")]
        ZichNietKunnenVermakenKunnenSamenSpelen = 512,
           
		[Description("Anders")]
        Anders = 1024,
	} 
	
		/// <summary>
	/// DataValNummer = 505 
	/// Opv kindercentrum / (voor) school505Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvKindercentrumVoorSchool505multiple
	{   	 

			 

			           
		[Description("(Slechte) leerprestaties")]
        SlechteLeerprestaties = 2,
           
		[Description("Huiswerk")]
        Huiswerk = 4,
           
		[Description("Keuze kinderopvang")]
        KeuzeKinderopvang = 8,
           
		[Description("Kinderopvang/oppasregeling")]
        KinderopvangOppasregeling = 16,
           
		[Description("Moeite met wennen")]
        MoeiteMetWennen = 32,
           
		[Description("Niet naar school/ speelzaal willen")]
        NietNaarSchoolSpeelzaalWillen = 64,
           
		[Description("Problemen op school/ kindercentrum")]
        ProblemenOpSchoolKindercentrum = 128,
           
		[Description("Relatie beroepskracht-kind")]
        RelatieBeroepskrachtKind = 256,
           
		[Description("Relatie ouder-beroepskracht")]
        RelatieOuderBeroepskracht = 512,
           
		[Description("Schoolkeuze")]
        Schoolkeuze = 1024,
           
		[Description("Spijbelen")]
        Spijbelen = 2048,
           
		[Description("Vakkenpakket/ beroepskeuze")]
        VakkenpakketBeroepskeuze = 4096,
           
		[Description("Verwijzing speciaal onderwijs")]
        VerwijzingSpeciaalOnderwijs = 8192,
           
		[Description("Anders")]
        Anders = 16384,
	} 
	
		/// <summary>
	/// DataValNummer = 506 
	/// Opv opvallend gedrag506Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvOpvallendGedrag506multiple
	{   	 

			 

			           
		[Description("Agressief gedrag")]
        AgressiefGedrag = 2,
           
		[Description("Automutilatie")]
        Automutilatie = 4,
           
		[Description("Claimgedrag, aandacht vragen")]
        ClaimgedragAandachtVragen = 8,
           
		[Description("Criminaliteit")]
        Criminaliteit = 16,
           
		[Description("Driftbuien")]
        Driftbuien = 32,
           
		[Description("Druk/hyperactief/onrustig")]
        DrukHyperactiefOnrustig = 64,
           
		[Description("Dwanghandelingen")]
        Dwanghandelingen = 128,
           
		[Description("Gokken")]
        Gokken = 256,
           
		[Description("In zichzelf gekeerd gedrag")]
        InZichzelfGekeerdGedrag = 512,
           
		[Description("Koppig/ dwars/ opstandig")]
        KoppigDwarsOpstandig = 1024,
           
		[Description("Liegen/ stiekem gedrag")]
        LiegenStiekemGedrag = 2048,
           
		[Description("Niet luisteren/ongehoorzaam")]
        NietLuisterenOngehoorzaam = 4096,
           
		[Description("Overmatig Huilen")]
        OvermatigHuilen = 8192,
           
		[Description("Passief -/ teruggetrokken gedrag")]
        PassiefTeruggetrokkenGedrag = 16384,
           
		[Description("Roken/ alcohol/ drugs")]
        RokenAlcoholDrugs = 32768,
           
		[Description("Stelen")]
        Stelen = 65536,
           
		[Description("Stereotype gedragingen")]
        StereotypeGedragingen = 131072,
           
		[Description("Uitdagen/ brutaal/ grote mond")]
        UitdagenBrutaalGroteMond = 262144,
           
		[Description("Vernielzucht/ vandalisme")]
        VernielzuchtVandalisme = 524288,
           
		[Description("Weglopen")]
        Weglopen = 1048576,
           
		[Description("Anders")]
        Anders = 2097152,
	} 
	
		/// <summary>
	/// DataValNummer = 507 
	/// Opv aanpak opvoeding507Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvAanpakOpvoeding507multiple
	{   	 

			 

			           
		[Description("Adoptieproblematiek")]
        Adoptieproblematiek = 2,
           
		[Description("Bezorgdheid ontwikkeling kind")]
        BezorgdheidOntwikkelingKind = 4,
           
		[Description("Botsing culturen")]
        BotsingCulturen = 8,
           
		[Description("Conflicten stiefgezin")]
        ConflictenStiefgezin = 16,
           
		[Description("Invloed omgeving")]
        InvloedOmgeving = 32,
           
		[Description("Invloed/relatie grootouders")]
        InvloedRelatieGrootouders = 64,
           
		[Description("Lichamelijke verwaarlozing/mishandeling")]
        LichamelijkeVerwaarlozingMishandeling = 128,
           
		[Description("Moeilijke start ouderschap")]
        MoeilijkeStartOuderschap = 256,
           
		[Description("Omgangsregeling/ echtscheiding")]
        OmgangsregelingEchtscheiding = 512,
           
		[Description("Opvoedingsaanpak algemeen")]
        OpvoedingsaanpakAlgemeen = 1024,
           
		[Description("Oudermishandeling")]
        Oudermishandeling = 2048,
           
		[Description("Premature geboorte")]
        PrematureGeboorte = 4096,
           
		[Description("Psychische verwaarlozing/mishandeling")]
        PsychischeVerwaarlozingMishandeling = 8192,
           
		[Description("Puberteitsconflicten")]
        Puberteitsconflicten = 16384,
           
		[Description("Relatie ouder-kind")]
        RelatieOuderKind = 32768,
           
		[Description("Seksueel misbruik")]
        SeksueelMisbruik = 65536,
           
		[Description("Straf geven/ grenzen stellen")]
        StrafGevenGrenzenStellen = 131072,
           
		[Description("Teenage-ouderschap")]
        TeenageOuderschap = 262144,
           
		[Description("Verschil aanpak ouders")]
        VerschilAanpakOuders = 524288,
           
		[Description("Verwachting/ beleving ouderschap")]
        VerwachtingBelevingOuderschap = 1048576,
           
		[Description("Verwennen")]
        Verwennen = 2097152,
           
		[Description("Anders")]
        Anders = 4194304,
	} 
	
		/// <summary>
	/// DataValNummer = 508 
	/// Opv info / overig508Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvInfoOverig508multiple
	{   	 

			 

			           
		[Description("Informatie over inhoudelijk thema")]
        InformatieOverInhoudelijkThema = 2,
           
		[Description("Informatie over voorziening/sociale kaart")]
        InformatieOverVoorzieningSocialeKaart = 4,
           
		[Description("Klacht over hulpverlening/ instelling")]
        KlachtOverHulpverleningInstelling = 8,
           
		[Description("Second opinion")]
        SecondOpinion = 16,
           
		[Description("Vraag m.b.t. sociale kaart")]
        VraagMBTSocialeKaart = 32,
           
		[Description("Anders")]
        Anders = 64,
	} 
	
		/// <summary>
	/// DataValNummer = 511 
	/// Opv Triple P511Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvTripleP511multiple
	{   	 

			 

			           
		[Description("Algemeen")]
        Algemeen = 2,
           
		[Description("Babyfase")]
        Babyfase = 4,
           
		[Description("Peuter")]
        Peuter = 8,
           
		[Description("Kleuter")]
        Kleuter = 16,
           
		[Description("Schoolkind")]
        Schoolkind = 32,
           
		[Description("Pubers")]
        Pubers = 64,
	} 
	
		/// <summary>
	/// DataValNummer = 512 
	/// Opv vervolg consult512Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvVervolgConsult512multiple
	{   	 

			 

			           
		[Description("Afsluiten")]
        Afsluiten = 2,
           
		[Description("Positief opvoeden niveau 2")]
        PositiefOpvoedenNiveau2 = 4,
           
		[Description("Positief opvoeden niveau 3")]
        PositiefOpvoedenNiveau3 = 8,
           
		[Description("Verwijzen")]
        Verwijzen = 16,
	} 
	
		/// <summary>
	/// DataValNummer = 513 
	/// Opv resultaat bij afsluiten513Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvResultaatBijAfsluiten513multiple
	{   	 

			 

			           
		[Description("Andere aanpak uitproberen")]
        AndereAanpakUitproberen = 2,
           
		[Description("Doorverwijzing")]
        Doorverwijzing = 4,
           
		[Description("Probleem beter hanteerbaar")]
        ProbleemBeterHanteerbaar = 8,
           
		[Description("Probleem ongewijzigd")]
        ProbleemOngewijzigd = 16,
           
		[Description("Probleem opgelost")]
        ProbleemOpgelost = 32,
           
		[Description("Anders")]
        Anders = 64,
	} 
	
		/// <summary>
	/// DataValNummer = 514 
	/// Opv verwezen514Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum OpvVerwezen514multiple
	{   	 

			 

			           
		[Description("Cursus positief opvoeden niveau 4")]
        CursusPositiefOpvoedenNiveau4 = 2,
           
		[Description("Eerstelijns-/ basisvoorziening")]
        EerstelijnsBasisvoorziening = 4,
           
		[Description("Groepsaanbod")]
        Groepsaanbod = 8,
           
		[Description("Hulpverlenende instantie/ voorziening")]
        HulpverlenendeInstantieVoorziening = 16,
           
		[Description("Lezing")]
        Lezing = 32,
           
		[Description("Positief opvoeden niveau 4 individueel")]
        PositiefOpvoedenNiveau4Individueel = 64,
           
		[Description("Positief opvoeden niveau 5")]
        PositiefOpvoedenNiveau5 = 128,
           
		[Description("Workshop")]
        Workshop = 256,
           
		[Description("Anders")]
        Anders = 512,
	} 
	
		/// <summary>
	/// DataValNummer = 517 
	/// Geen VVE reden517Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum GeenVveReden517multiple
	{   	 

			 

			           
		[Description("Kind zit nog in toeleidingstraject")]
        KindZitNogInToeleidingstraject = 2,
           
		[Description("Kind bezoekt kinderdagverblijf")]
        KindBezoektKinderdagverblijf = 4,
           
		[Description("Kind bezoekt reguliere peuterspeelzaal")]
        KindBezoektRegulierePeuterspeelzaal = 8,
           
		[Description("Kind gaat naar gastouder")]
        KindGaatNaarGastouder = 16,
           
		[Description("Ouders/verzorgers zijn niet gemotiveerd")]
        OudersVerzorgersZijnNietGemotiveerd = 32,
           
		[Description("Financiële redenen")]
        FinanciLeRedenen = 64,
           
		[Description("VVE-locatie te ver weg")]
        VveLocatieTeVerWeg = 128,
           
		[Description("Wachtlijst")]
        Wachtlijst = 256,
           
		[Description("Anders")]
        Anders = 512,
	} 
	
		/// <summary>
	/// DataValNummer = 518 
	/// Vaccinatiesoort518Multiple 
	/// </summary>
	[Serializable]
	[Flags] 
	public enum Vaccinatiesoort518multiple
	{   	 

			 

			           
		[Description("DKTP-Hib-HepB")]
        DktpHibHepb = 2,
           
		[Description("Pneu")]
        Pneu = 4,
           
		[Description("BMR")]
        Bmr = 8,
           
		[Description("MenC")]
        Menc = 16,
           
		[Description("DKTP")]
        Dktp = 32,
           
		[Description("DTP")]
        Dtp = 64,
           
		[Description("HPV")]
        Hpv = 128,
           
		[Description("DKTP-HepB")]
        DktpHepb = 256,
           
		[Description("HepB 0,5 ml")]
        Hepb05Ml = 512,
           
		[Description("HepB 1,0 ml")]
        Hepb10Ml = 1024,
           
		[Description("Anders")]
        Anders = 2048  

	}


}

