﻿using Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Extensions;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
using System.Diagnostics;
namespace Services
{
	public class HomeLogic
	{
		/// <summary>
		/// This service will run daily to export archives for children in the specified period  
		/// child is 'uitzorg'
		/// child has 'cm70' consult
		/// child has BSN
		/// child has 'toestemming overdracht' bds 1163
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <param name="bsn">specific bsn, will be exported. no checks run...</param>
		/// <param name="directoryManual">specific direcory for export. only in case of exceptions...</param>
		public static void RunDailyService(DateTime startDate, DateTime endDate, long bsn = 0, string directoryManual = "")
		{
			CheckConfig();

			int.TryParse(ConfigurationManager.AppSettings["firstCasNumber"], out int firstCaseNumber);
			int.TryParse(ConfigurationManager.AppSettings["lastCasNumber"], out int lastCaseNumber);
			DateTime.TryParse(ConfigurationManager.AppSettings["birthDateVan"], out DateTime birthDateVan);
			DateTime.TryParse(ConfigurationManager.AppSettings["birthDateTot"], out DateTime birthDateTot);
			var statussen = (ConfigurationManager.AppSettings["status"].Split(',').Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => int.Parse(s)));
			DateTime.TryParse(ConfigurationManager.AppSettings["uitZorgDatumVan"], out DateTime uitZorgDatumVan);
			DateTime.TryParse(ConfigurationManager.AppSettings["uitZorgDatumTot"], out DateTime uitZorgDatumTot);
			DateTime.TryParse(ConfigurationManager.AppSettings["inZorgDatumVan"], out DateTime inZorgDatumVan);
			DateTime.TryParse(ConfigurationManager.AppSettings["inZorgDatumTot"], out DateTime inZorgDatumTot);

			try
			{
				var casNummers = new List<int>();
				using (var entities = new GGDHNoordEntities())
				{
					Logger.WriteLog(LogLevelL4N.INFO, string.Format("Connectie naar db is gemaakt"));
					casNummers = entities.Persoonsgegevens
							.Where(pg =>
                            // pg.CasNummer == 321554 || pg.CasNummer == 113525 || pg.CasNummer == 160502 || pg.CasNummer == 256559 || pg.CasNummer == 263559 || pg.CasNummer == 279744 || pg.CasNummer == 293559 || pg.CasNummer == 305382 || pg.CasNummer == 139949 || pg.CasNummer == 155656 || pg.CasNummer == 251356 || pg.CasNummer == 300281 || pg.CasNummer == 300280 || pg.CasNummer == 284126 || pg.CasNummer == 310655 || pg.CasNummer == 341235 || pg.CasNummer == 312902 || pg.CasNummer == 304633 || pg.CasNummer == 131427 || pg.CasNummer == 190237 || pg.CasNummer == 289438 || pg.CasNummer == 277398 || pg.CasNummer == 95659 || pg.CasNummer == 225999 || pg.CasNummer == 137165 || pg.CasNummer == 122665 || pg.CasNummer == 276866 || pg.CasNummer == 198130 || pg.CasNummer == 337820 || pg.CasNummer == 333449 || pg.CasNummer == 123340 || pg.CasNummer == 140475)
                            // pg.CasNummer == 160347 || pg.CasNummer == 106729 || pg.CasNummer == 221268 || pg.CasNummer == 149148 || pg.CasNummer == 191184 || pg.CasNummer == 234614 || pg.CasNummer == 95490 || pg.CasNummer == 149117 || pg.CasNummer == 332701 || pg.CasNummer == 251957 || pg.CasNummer == 175796 || pg.CasNummer == 321114 || pg.CasNummer == 278289 || pg.CasNummer == 314898 || pg.CasNummer == 298081 || pg.CasNummer == 305519 || pg.CasNummer == 87719 || pg.CasNummer == 96071 || pg.CasNummer == 320184 || pg.CasNummer == 94554 || pg.CasNummer == 124647 || pg.CasNummer == 170165 || pg.CasNummer == 110844 || pg.CasNummer == 110845 || pg.CasNummer == 136544 || pg.CasNummer == 124136 || pg.CasNummer == 236069 || pg.CasNummer == 239882 || pg.CasNummer == 313204 || pg.CasNummer == 323046 || pg.CasNummer == 303228 || pg.CasNummer == 341441)
                            // pg.CasNummer == 87699 || pg.CasNummer == 94554 || pg.CasNummer == 95484 || pg.CasNummer == 96074 || pg.CasNummer == 106757 || pg.CasNummer == 110859 || pg.CasNummer == 110860 || pg.CasNummer == 124183 || pg.CasNummer == 124692 || pg.CasNummer == 136595 || pg.CasNummer == 149191 || pg.CasNummer == 149222 || pg.CasNummer == 160412 || pg.CasNummer == 170227 || pg.CasNummer == 175881 || pg.CasNummer == 191285 || pg.CasNummer == 221336 || pg.CasNummer == 234680 || pg.CasNummer == 236140 || pg.CasNummer == 239968 || pg.CasNummer == 252015 || pg.CasNummer == 278355 || pg.CasNummer == 298134 || pg.CasNummer == 303289 || pg.CasNummer == 305586 || pg.CasNummer == 313273 || pg.CasNummer == 314946 || pg.CasNummer == 320210 || pg.CasNummer == 321144 || pg.CasNummer == 323079 || pg.CasNummer == 332735 || pg.CasNummer == 341455)
                            // pg.CasNummer == 309921)
                            // pg.CasNummer == 170211)
                            //pg.CasNummer == 164) 226922 428012
                            ((lastCaseNumber == 0 && firstCaseNumber == 0) || (pg.CasNummer >= firstCaseNumber && pg.CasNummer <= lastCaseNumber)) &&
                            //((birthDateVan == null || birthDateVan == DateTime.MinValue) || pg.Geboortedatum >= birthDateVan) &&
                            //((birthDateTot == null || birthDateTot == DateTime.MinValue) || pg.Geboortedatum < birthDateTot) &&
                            //((inZorgDatumVan == null || inZorgDatumVan == DateTime.MinValue) || pg.InZorg >= inZorgDatumVan) &&
                            //((inZorgDatumTot == null || inZorgDatumTot == DateTime.MinValue) || pg.InZorg < inZorgDatumTot) &&
                            // pg.UitZorg == null)
                            //((uitZorgDatumVan == null || uitZorgDatumVan == DateTime.MinValue) || pg.UitZorg >= uitZorgDatumVan) &&
                            //((uitZorgDatumTot == null || uitZorgDatumTot == DateTime.MinValue) || pg.UitZorg < uitZorgDatumTot) &&
                            (!statussen.Any() || statussen.Any(s => s == (byte)pg.Status)))
                            .OrderBy(x => x.CasNummer).Select(y => y.CasNummer)
							.ToList();
				}
				//early exit. als er geen geschikte personen zijn
				//als bsn bijvoorbeeld niet bij deze organisatie hoorde
				if (casNummers.Count == 0)
				{
					Logger.WriteLog(LogLevelL4N.INFO, $"LET OP: {casNummers.Count} kinderen worden overgedragen. Geen enkel dossier voldoet aan alle eisen voor overdracht.");
					return;
				}

				Logger.WriteLog(LogLevelL4N.INFO, $"{casNummers.Count} kind(eren) worden geconverteerd naar PDF.");

				var counter = 0;
				var stopWatch = new Stopwatch();
				var ts = stopWatch.Elapsed;
				var elapsedTime = string.Empty;
				stopWatch.Start();

				foreach (var casNummer in casNummers)
				{
					if (counter > 499)
					{
						counter = 0;
						stopWatch.Stop();
						ts = stopWatch.Elapsed;
						elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
														ts.Hours, ts.Minutes, ts.Seconds,
														ts.Milliseconds / 10);

						Console.WriteLine($"Hoeveelheid tijd verstreken voor de laatste 500 dossiers: {elapsedTime}");
						Logger.WriteLog($"Hoeveelheid tijd verstreken voor de laatste 500 dossiers: {elapsedTime}");
						stopWatch.Restart();
					}

					try
					{
						using (var entities = new GGDHNoordEntities())
						{
							var person = entities.Persoonsgegevens
							.Where(pg => pg.CasNummer == casNummer).FirstOrDefault();

							if (person != null)
							{
								string filePath = Path.Combine(ConfigurationManager.AppSettings["outputDirectory"], person.CasNummer.ToString());
								var doc = PDFLogic.CreateDocument(person, filePath);
								doc.Save(person.GetFilename(), person.CasNummer);
								//Logger.LogProcessedCasToIndexFile(person);
							}
						}
					}
					catch (Exception e)
					{
						Logger.WriteLog(LogLevelL4N.ERROR, $"Het converteren van dossier: {casNummer} is mislukt. vanwege: {e.Message} {Environment.NewLine} {e.InnerException} {Environment.NewLine} {e.StackTrace}");
					}
					counter++;
				}  

				stopWatch.Stop();
				ts = stopWatch.Elapsed;
				elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
						ts.Hours, ts.Minutes, ts.Seconds,
						ts.Milliseconds / 10);
				Logger.WriteLog($"Einde: {elapsedTime}");
			}
			catch (Exception e)
			{
				Logger.WriteLog(LogLevelL4N.FATAL, string.Format("Er gaat (waarschijnlijk) iets mis met de connectie naar de database: {0}", e.Message));
				throw new Exception(string.Format("Er gaat iets mis met het connecteren naar de database: {0}", e.Message));
			}
		}

		private static void CheckConfig()
		{
			if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["outputDirectory"]))
			{
				Logger.WriteLog(LogLevelL4N.FATAL, "outputDirectory not defined in app.config - please add. e.g. 'c:\\mydirectory'");
				throw new ConfigurationErrorsException("outputDirectory not defined in app.config - please add. e.g. 'c:\\mydirectory'");
			}
		}
	}
}
