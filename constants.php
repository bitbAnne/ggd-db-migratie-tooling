<?php

define("TABLE_NAME", 0);
define("COLUMN_NAME", 1);

define("GGD_DB_STRUCTURE_CSV", "ggd-db-structure.csv");
define("GGD_DB_TABLES_CSV", "ggd-db-tables-views.csv");

define("GGD_DB_MATRIX_CSV", "ggd-db-matrix.csv");

define("MLC19T",    "MLC19T");
define("MLC19V",    "MLC19V");
define("MLC20T",    "MLC20T");
define("MLC20V",    "MLC20V");
define("HNT",       "HNT");
define("HNV",       "HNV");
define("RUT",       "RUT");
define("RUV",       "RUV");
define("TWT",       "TWT");
define("TWV",       "TWV");
define("HNANOT",    "HNANOT");

define("EML_REGEXP_ALL", "/[^A-Za-z0-9 !#$%&\*\/\-_\+\.?^~{}|]/");
define("TXT_REGEXP_LOWERCASE", "/[a-z]+/");
define("TXT_REGEXP_UPPERCASE", "/[A-Z]+/");
define("TXT_REGEXP_UTF8", "/[äöüÄÖÜß]+/");
define("TXT_REGEXP_DIGITS", "/[0-9]+/");
define("TXT_REGEXP_SPECIALCHARS", "/[!#$%&\*\/\-_\+\.?^~{}|]+/");

define("TXT_LOWERCASE", "lowercase");
define("TXT_UPPERCASE", "uppercase");
define("TXT_UTF8", "utf8");
define("TXT_DIGITS", "digits");
define("TXT_SPECIALCHARS", "specialchars");

define("PHONE_REXEXP", "/(?:\\+)?[0-9]*/");
define("PHONE_REPLACE_SPECIAL_CHARS", array("-", " ", "!", "#", "$", "%", "^", "&", "*", "(", ")", "-", "+", "=", "\\", "]", "[", "}", "{", "'", '"', ";", ":", "?", "/", ">", ".", "<", ",", "`", "~"));
define("PHONE_REPLACE_MOEDER_FULL", array("moeder", "mdr"));
define("PHONE_REPLACE_MOEDER_LTR", array("m"));
define("PHONE_REPLACE_VADER_FULL", array("vader", "vdr"));
define("PHONE_REPLACE_VADER_LTR", array("v", "p"));
define("PHONE_REPLACE_ANDERERELATIE_FULL", array("oma", "opa", "oom", "zus", "con", "pleeg"));
define("PHONE_REPLACE_ANDERERELATIE_LTR", array("n", "w", "o"));

define("PHONE_RELATIE_ANDERS", "Anders");
define("PHONE_RELATIE_MOEDER", "Moeder");
define("PHONE_RELATIE_VADER", "Vader");

define("SHOW_STATUS_MOD", 0.25);

define("PC_PARSE_LEN", 3);
define("PC_PARSE_MAX", 999);

define("WPL_WOONPLAATS", 0);
define("WPL_WOONPLAATSNAAM", 1);
define("WPL_CODENUMMERAANDUID", 2);
define("WPL_CODEVERBLIJFPLAATS", 3);
define("WPL_GEMEENTECODE", 4);

define("WV_BDS_STRAAT", 11);
define("WV_BDS_HUISNUMMER", 12);
define("WV_BDS_POSTCODE", 16);
define("WV_BDS_PLAATS", 10);
define("WV_BDS_TOEVOEGING", 14);


define("WV_FLD_PERVAN", "periodevan");
define("WV_FLD_PERTOT", "periodetot");
define("WV_FLD_GEHEIM", "geheim");

define("FILE_OUTPUT_DIR", "\\temp\\ggd.temp\\dvexport\\output");

define("DIAGDATA_CHARDATA_GREATER_SIZE", 2);

