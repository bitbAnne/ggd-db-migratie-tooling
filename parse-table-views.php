<?php

include_once("constants.php");
include_once("appconfig.php");


$logger->info(print_r($argv, true));

$readdbfile = GGD_DB_STRUCTURE_CSV;

$newStructure = array();

// Open end product csv
$csvreadbase = fopen(GGD_DB_TABLES_CSV, 'r');
if ($csvreadbase !== false) {
    while (($csvline = fgetcsv($csvreadbase, 0, ";"))) {
        $value = array();
        if (count($csvline) > 2) {
            $cnt = 0;
            foreach ($csvline as $element) {
                $cnt++;
                if ($cnt > 2) {
                    $value[$element] = "x"; 
                }
            }
        }
        $newStructure[$csvline[TABLE_NAME]][$csvline[COLUMN_NAME]] = $value;
    }
}

// Open structure db
$csvreaddb = fopen($readdbfile, 'r');

if ($csvreaddb !== false) {
    // Copy original structure into new structure
    // Iterate throug db structure, add to new structure
    while (($csvline = fgetcsv($csvreaddb, 0, ";"))) {
        $table_viewName = $csvline[TABLE_NAME];
        if (!array_key_exists($table_viewName, $newStructure)) {
            $newStructure[$table_viewName] = "";
        }
    }
    
    fclose($csvreadbase);
    
    ksort($newStructure);
    $logger->debug(($newStructure));
    // Iterate through  new structure, saving it as csv
    $csvwritebase = fopen(GGD_DB_TABLES_CSV, 'w');
    foreach ($newStructure as $table_name => $colums) {
        $csvline = array($table_name);
        fputcsv($csvwritebase, $csvline, ";");
    }

}
