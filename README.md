# ggd-db-migratie-tooling

Dit project bevat de tooling nodig voor anaylse, anonimisatie en conversie van de ML CAS database.
De volgende folders zijn van belang:

  - sqlscripts - Hier staan de sql scipts om de ML CAS database op te schonen, te anonimiseren en om specifiek gegevens op te kunnen halen. Bijvoorbeeld sqlscripts/anonimize-Database.sql met instructies om een geanonimiseerde database aan te maken
  - data - Hier staan de bestanden met de uitkomsten van verschillende analyses. Deze kunnen worden gebruikt als referentie bestanden. Bijvoorbeeld data/ggd-matrix.xls een bestand met *alle* velden van alle tabellen en views van alle bekende ML CAS varianten.
  - db-info-results - Hier staan de uitkomsten van het db-info script. Bijvoorbeeld lijsten met niet valide telefoonnummers.
  - Stam-data - Hier staan de lijsten met stamgegevens (uit de stam tabellen) uit ML CAS.
  - GGD-PDF-Export - Hier staan de C# PDF tools waarmee op basis van een ML CAS database PDF bestanden (per dossier) kunnen worden aangemaakt.


### Anonimisering

De volgende stappen moeten worden genomen om een ML CAS database te annimiseren:

- Maak in de MS SQL Sever Management tool (MSSM) een backup (.bak) van de database
- Lees de backup opnieuw in, verander de naam naar een unieke naam en pas  eventueel ook de files locatie voordat dit gestart wordt.
- Bij het uitvoeren van de onderstaande sql scripts moet altijd autocommit aan staan!
- Run het sqlscripts/addIndeces.sql script om indices toe te voegen
- Run het sqlscripts/dropTables-2-anonimize-Database.sql script om tabellen die niet worden gebruikt te verwijderen.
- Verklein de database door in de MSSM tool Tasks - Shrink - Database te kiezen.
- Maak een backup om deze op een later moment opnieuw te kunnen inlezen (voor tijdens ontwikkeling is dit handig)
- Voer de controle scripts sqlscripts/check*.sql uit en pas waar nodig iets aan
- Voer het scripts/anonimize-Database.sql script uit, volg ook de instructies die daar in staan over het uitvoeren van het ano-db.php script
- Controleer de ge-anonimiseerde data
- Maak nogmaals een backup en lees deze in op de gewenste omgeving
- Zorg er voor dat daar de rechten (permissions) voro de database ook goed staan ingesteld.


### PDF Export

...

### Tech

* [MS SQL Server] - MS SQL Server
* [PHP] - PHP
* [MS Visual Studio 2017] - MS Visual Studio 2017.


   [MS SQL Server]: <https://github.com/joemccann/dillinger>
