-- 10min
CREATE INDEX IX_DD_ItemID ON dossierdiagData (ItemID);
CREATE INDEX IX_DD_DiagItemID ON dossierdiagData (DiagItemID);
CREATE INDEX IX_DD_Data ON dossierdiagData (data);
CREATE INDEX IX_DD_CharData ON dossierdiagData (CharData);
CREATE INDEX IX_DD_DataID ON dossierdiagData (DataID);

CREATE INDEX IX_CO_CasW ON CasOms (casWaarde);

CREATE INDEX IX_DI_CasNum ON DossierItems (Casnummer);
CREATE INDEX IX_DI_DossierSoort ON DossierItems (DossierSoort);
CREATE INDEX IX_DI_ItemID ON DossierItems (ItemID);

CREATE INDEX IX_DI_BDS ON DiagItems (BDSnummer);
CREATE INDEX IX_DI_DataValNum ON DiagItems (DataValNummer);
CREATE INDEX IX_DI_DataSrt ON DiagItems (DataSoort);
CREATE INDEX IX_DI_DiagItemID ON DiagItems (DiagItemID);
CREATE INDEX IX_DI_Status ON DiagItems (Status);

CREATE INDEX IX_DV_DataValNum ON DataValues (DataValNummer);
CREATE INDEX IX_DV_DataWaarde ON DataValues (DataWaarde);

CREATE INDEX IX_PG_Status ON persoonsgegevens (Status);

CREATE INDEX IX_anp_casnum ON persoonsgegevens (casnummer);
CREATE INDEX IX_wvb_casnum ON woonverbanden (casnummer);
CREATE INDEX IX_at_casnum ON Activiteiten (casnummer);
CREATE INDEX IX_plan_casnum ON planning (casnummer);
CREATE INDEX IX_afsp_casnum ON afspraken (casnummer);
CREATE INDEX IX_afsph_casnum ON afsprakenhist (casnummer);
CREATE INDEX IX_anp_casnum1 ON ClientRelaties (casnummer1);
CREATE INDEX IX_anp_casnum2 ON ClientRelaties (casnummer2);

-- ML CAS RU
CREATE INDEX IX_bz_casnum ON betrokkenzorgverleners (casnummer);


-- On Archief Database
CREATE INDEX IX_arch_casnum ON Archief (casnummer);