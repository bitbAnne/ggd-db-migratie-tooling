-- Add VIEW to Arcief Table
-- Use correct DB Name, in example below HN_ARCH_ANON_010
DROP VIEW IF EXISTS Archief;
CREATE VIEW Archief AS SELECT * FROM HN_ARCH_ANON_010.dbo.Archief;

-- Schoolhistorie VIERW
DROP VIEW IF EXISTS schoolhistorie;
CREATE VIEW schoolhistorie AS SELECT dosi.CasNummer, Datum, BDSnummer, ddd.[Data], ddd.CharData FROM DossierItems dosi
JOIN DossierDiagData ddd on ddd.ItemID = dosi.ItemID
JOIN DiagItems diag on diag.DiagItemID = ddd.DiagItemID;

-- zorgverantwoordelijken VIEW
DROP VIEW IF EXISTS zorgverantwoordelijken;
CREATE VIEW zorgverantwoordelijken AS SELECT m.extern, pg.casnummer, m.Discipline, d.Omschrijving, tm.positie, pg.bureau, m.MedewerkerID, m.naam FROM Persoonsgegevens pg
LEFT JOIN Bureaus b ON (b.Bureau = pg.Bureau)
LEFT JOIN TeamMedewerkers tm ON (tm.Bureau = b.Bureau)
LEFT JOIN Medewerkers m ON (m.MedewerkerID = tm.MedewerkerID)
LEFT JOIN Disciplines d ON (d.Discipline = m.Discipline)
WHERE (tm.status = 0 AND tm.TotDatum IS NULL)AND (m.Discipline = 1 OR m.Discipline = 7);

-- DossierDiagData
ALTER TABLE DossierDiagData
ADD dl INT;

-- ca 45min
UPDATE DossierDiagData
SET dl = datalength(CharData);

CREATE INDEX IX_DDD_DL ON DossierDiagData (dl);

-- DossierInfoData
ALTER TABLE DossierInfoData
ADD dl INT;

UPDATE DossierInfoData
SET dl  = datalength(info);

UPDATE dossierinfodata
SET Info = ''
where dl >= 62618263

CREATE INDEX IX_DID_DL ON DossierInfoData (dl);

ALTER TABLE DossierInfoData
ADD infotype VARCHAR(5);

UPDATE DossierInfoData
SET infotype = iif(cast(substring(info, 0, 6) as VARCHAR(6)) = '{\rtf', 'RTF', 'TEXT');

CREATE INDEX IX_DID_IT ON DossierInfoData (infotype);


-- DossierInterventieData
ALTER TABLE DossierInterventieData ADD doelsize INT;
ALTER TABLE DossierInterventieData ADD doeltype VARCHAR(5);;

ALTER TABLE DossierInterventieData ADD aanleidingsize INT;
ALTER TABLE DossierInterventieData ADD aanleidingtype VARCHAR(5);;

ALTER TABLE DossierInterventieData ADD evaluatiesize INT;
ALTER TABLE DossierInterventieData ADD evaluatietype VARCHAR(5);;

ALTER TABLE DossierInterventieData ADD analysesize INT;
ALTER TABLE DossierInterventieData ADD analysetype VARCHAR(5);

ALTER TABLE DossierInterventieData ADD totsize INT;
ALTER TABLE DossierInterventieData ADD bin_rtf_type VARCHAR(5);

UPDATE DossierInterventieData
SET doelsize = ISNULL(datalength(doel), 0),
aanleidingsize = ISNULL(datalength(aanleiding), 0),
evaluatiesize = ISNULL(datalength(evaluatie), 0),
analysesize = ISNULL(datalength(analyse), 0);

UPDATE DossierInterventieData
SET totsize = (doelsize + aanleidingsize + evaluatiesize + analysesize);
UPDATE DossierInterventieData
SET totsize = 0 WHERE totsize IS NULL;

-- May not needed anymore?
UPDATE DossierInterventieData
SET DOEL = '',
aanleiding = '',
evaluatie = '',
analyse = ''
WHERE totsize >= 36638272

UPDATE DossierInterventieData
SET doeltype = iif(cast(substring(doel, 0, 6) as VARCHAR(6)) = '{\rtf', 'RTF', 'TEXT'),
aanleidingtype = iif(cast(substring(aanleiding, 0, 6) as VARCHAR(6)) = '{\rtf', 'RTF', 'TEXT'),
evaluatietype = iif(cast(substring(evaluatie, 0, 6) as VARCHAR(6)) = '{\rtf', 'RTF', 'TEXT'),
analysetype = iif(cast(substring(analyse, 0, 6) as VARCHAR(6)) = '{\rtf', 'RTF', 'TEXT');

UPDATE DossierInterventieData
SET bin_rtf_type = concat(iif(doeltype = 'RTF', 'X', '-'), iif(aanleidingtype = 'RTF', 'X', '-'), iif(evaluatietype = 'RTF', 'X', '-'), iif(analysetype = 'RTF', 'X', '-'));


-- Add Persoonsgegevens fields
ALTER TABLE persoonsgegevens ADD IsVluchteling TINYINT NOT NULL DEFAULT 0;
ALTER TABLE persoonsgegevens ADD VaccOnly TINYINT NOT NULL DEFAULT 0;

UPDATE persoonsgegevens SET IsVluchteling = 1
WHERE casnummer IN (
SELECT pg.casnummer FROM woonverbanden wv
LEFT JOIN persoonsgegevens pg ON (pg.casnummer = wv.casnummer)
WHERE (pg.status = 0 OR pg.status = 5)
AND ( (pg.Bureau = 1107 AND (wv.postcode = '1784 NV' AND wv.huisnummer = 20))
OR (pg.Bureau = 1160 AND (wv.postcode = '1784 NV' AND wv.huisnummer = 20))
OR (pg.Bureau = 1160 AND (wv.postcode = '1784 PD' AND wv.huisnummer = 27))
OR (pg.Bureau = 1161 AND pg.schoolnummer = 20400061)
OR (pg.Bureau = 3117 AND (wv.postcode = '1816 MK' AND wv.huisnummer = 1))
OR (pg.Bureau = 4109 AND (wv.postcode = '1701 BP' AND wv.huisnummer = 2)) ))


-- IF Fields NOT exist, ADD them and make them empty
ALTER TABLE persoonsgegevens ADD VerzekeraarID INT NOT NULL DEFAULT 0;
ALTER TABLE persoonsgegevens ADD Polisnummer VARCHAR(20) COLLATE Latin1_General_CI_AS NOT NULL DEFAULT '';

ALTER TABLE Bureaus ADD Regio SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD Brieflocatie VARCHAR(5) NOT NULL DEFAULT '';
ALTER TABLE Bureaus ADD Faxnummer VARCHAR(15) NOT NULL DEFAULT '';
ALTER TABLE Bureaus ADD Blokmagic INT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD Blokdefault SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD Blokrichting SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P1_ZitBloks SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P1_Weken SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P1_MaxBloks SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P2_ZitBloks SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P2_Weken SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P2_MaxBloks SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P3_ZitBloks SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P3_Weken SMALLINT NOT NULL DEFAULT 0;
ALTER TABLE Bureaus ADD P3_MaxBloks SMALLINT NOT NULL DEFAULT 0;

-- Add Woonverbanden fields
ALTER TABLE Woonverbanden ADD Aantal TINYINT NOT NULL DEFAULT 1;
ALTER TABLE Woonverbanden ADD Flags2 INT NOT NULL DEFAULT 0;
ALTER TABLE Woonverbanden ADD Flags3 INT NOT NULL DEFAULT 0;
ALTER TABLE Woonverbanden ADD Flags4 INT NOT NULL DEFAULT 0;
ALTER TABLE Woonverbanden ADD Huisnummer2 VARCHAR(40) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Huisnummer3 VARCHAR(40) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Huisnummer4 VARCHAR(40) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD HuisnummerToev2 VARCHAR(6) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD HuisnummerToev3 VARCHAR(6) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD HuisnummerToev4 VARCHAR(6) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Postcode2 VARCHAR(7) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Postcode3 VARCHAR(7) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Postcode4 VARCHAR(7) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Straatnaam2 VARCHAR(86) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Straatnaam3 VARCHAR(86) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Straatnaam4 VARCHAR(86) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Woonplaats2 VARCHAR(160) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Woonplaats3 VARCHAR(160) NOT NULL DEFAULT '';
ALTER TABLE Woonverbanden ADD Woonplaats4 VARCHAR(160) NOT NULL DEFAULT '';

UPDATE Woonverbanden
SET Aantal = 0
WHERE (woonplaats IS NULL) OR (woonplaats = '')