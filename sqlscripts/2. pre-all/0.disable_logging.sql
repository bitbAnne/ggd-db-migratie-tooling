-- Set logging to lowest level to prevent disk getting too full
ALTER DATABASE [??_ANON_###] SET RECOVERY SIMPLE;
ALTER DATABASE [??_ARCH_ANON_###] SET RECOVERY SIMPLE;