-- Drop tables that do not contain relevant information for migration
-- Based on ggd-db-tables-views-AanvullingOverlegConversie.xls
DROP TABLE ClientTeamHist;
DROP TABLE ClientVerzekeraars;

-- Drop Views that do not contain relevant information for migration
DROP VIEW AfspraakBrieven;
DROP VIEW AantalSlots;
DROP VIEW AantalVrij;
DROP VIEW AfspraakDef;
DROP VIEW AfspraakHistPlan;

-- Drop tables that are empty (HN)
ALTER TABLE DossierInterventieData DROP FK_DossierInterventieData_Interventiegronden;
DROP TABLE Interventiegronden;
DROP TABLE MwActUren;
DROP TABLE DossierVaccinatieData
DROP TABLE DossierVaccinatiePlanData
DROP TABLE DossierMeldData;
DROP TABLE ProductNutrienten;
DROP TABLE ProductOmschrijvingen;
DROP TABLE ClientMenuProducten;
DROP TABLE VoorbeeldMenuProducten;
DROP TABLE ReceptuurProducten;
DROP TABLE OmrekenMeeteenheden;
DROP TABLE Producten;
DROP TABLE BasisMeeteenheden;
DROP TABLE BdsElementen;
DROP TABLE BdsGroepen;
DROP TABLE BdsRubrieken;
DROP TABLE BdsWaarden;
DROP TABLE BdsWaardenDomeinen;
DROP TABLE PortaalRegistraties;
DROP TABLE LiPZexport;
DROP TABLE ClientMenus;
DROP TABLE ClientSchoolHist;
DROP TABLE Contracturen;
DROP TABLE Correspondentie;
DROP TABLE Eetmomenten;
DROP TABLE Factuurnummers;
DROP TABLE DossierOverdracht;
DROP TABLE Hl7Berichten;
DROP TABLE Hulpverleners;
DROP TABLE KostencodeBedragen;
DROP TABLE ZorgCorrectie;
DROP TABLE LiPZrubrieken;
DROP TABLE Nutrienten;
DROP TABLE OverdrachtOrgs;
DROP TABLE Partijnummers;
DROP TABLE Persimport;
DROP TABLE PortaalBerichten;
DROP TABLE Productgroepen;
DROP TABLE RapportHulp;
DROP TABLE RapportHulpSoort;
DROP TABLE ZorgTrajecten;
ALTER TABLE ServiceportaalVerwijzerLink DROP FK_ServiceportaalVerwijzerLink_ServiceportaalUsers;
ALTER TABLE ServiceportaalClientLink DROP FK_ServiceportaalClientLink_ServiceportaalUsers;
ALTER TABLE ServiceportaalMedewerkerLink DROP FK_ServiceportaalMedewerkerLink_ServiceportaalUsers;
ALTER TABLE ServiceportaalSchoolLink DROP FK_ServiceportaalSchoolLink_ServiceportaalUsers;
DROP TABLE ServiceportaalUsers;
DROP TABLE sysdiagrams;
ALTER TABLE Persoonsgegevens DROP FK_Persoonsgegevens_Teamlocaties;
ALTER TABLE Scholen DROP FK_Scholen_Teamlocaties;
DROP TABLE Vaccinatiecodes;
DROP TABLE VanWiechen;
DROP TABLE VektisDeclaraties;
ALTER TABLE Afspraken DROP FK_Afspraken_Verwijzingen;
DROP TABLE Verwijzingen;
DROP TABLE VirPostcodereeksen;
ALTER TABLE VoorbeeldMenuProducten DROP FK_VoorbeeldMenuProducten_VoorbeeldMenus;
DROP TABLE VoorbeeldMenus;
ALTER TABLE AfsprakenHist DROP FK_AfsprakenHist_ZorgExport;
ALTER TABLE Activiteiten DROP FK_Activiteiten_ZorgExport;
ALTER TABLE ZorgCorrectie DROP FK_ZorgCorrectie_ZorgExport1;
ALTER TABLE ZorgCorrectie DROP FK_ZorgCorrectie_ZorgExport2;
DROP TABLE ZorgExport;
DROP TABLE ZoekFilters;