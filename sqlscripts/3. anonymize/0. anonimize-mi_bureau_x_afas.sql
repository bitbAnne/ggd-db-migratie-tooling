DROP TABLE mi_bureau_afas_x_afas;

CREATE TABLE mi_bureau_afas_x_afas ( ID_AFAS BIGINT, Topdesk_code VARCHAR(10) COLLATE Latin1_General_CI_AS NOT NULL );

insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25448, 'ABK-BPK');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25463, 'AKS-MZL');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25454, 'ALK-ARS');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25458, 'ALK-H22');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25457, 'ALK-HAW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25456, 'ALK-MWS');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25477, 'ALK-RBW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25455, 'ALK-VAS');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25464, 'ALK-ZEV');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25446, 'AND-KLG');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25434, 'ANP-DVW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25480, 'BKS-RTH');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25453, 'BRG-BNH');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25465, 'CST-GDW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25466, 'DBU-EML');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25441, 'DHE-BHP');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25467, 'DHE-BRW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25442, 'DHE-TSL');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25468, 'EGM-HNW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25469, 'ENK-WWL');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25470, 'GRF-JPL');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25471, 'HHW-NWL');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25440, 'HHW-PRH');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25472, 'HKS-ODN');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25478, 'HLO-HLW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25436, 'HRN-BWP');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25473, 'HRN-DMT');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25437, 'HRN-GRB');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25435, 'HRN-MSS');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25474, 'HRN-NWS');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25443, 'JUL-MDZ');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25445, 'MDB-SMH');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25444, 'MDM-POS');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25449, 'NND-TMB');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25451, 'OBD-LMS');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25475, 'SCE-HPL');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25476, 'SCH-ZDW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25450, 'SPB-PMS');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25479, 'VHU-TWV');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25438, 'WGN-CFL');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25433, 'WRM-DBK');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25447, 'WRV-OLW');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25452, 'ZSW-BGR');
insert into mi_bureau_afas_x_afas (ID_AFAS, Topdesk_code) values (25439, 'ZWG-AKW');

-- UPDATE AFAS ID's in mi_bureau_x_afas with temp ID's
UPDATE mbxa
SET mbxa.Locaties_Afdeling_ID_AFAS = mbaxa.ID_AFAS
FROM mi_bureau_x_afas mbxa
    INNER JOIN mi_bureau_afas_x_afas mbaxa ON
    (mbxa.Locaties_Topdesk_code = mbaxa.Topdesk_code);
