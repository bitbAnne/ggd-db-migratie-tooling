-- Use original DB

-- Make sure autocommit is on

-- Creating the x-ref table and indices takes a couple of minutes
--
DROP TABLE IF EXISTS an_cross;
CREATE TABLE an_cross (
   rowid INT IDENTITY(1,1),
   casnummer INT,
   casnummer_new INT,
   email VARCHAR(100),
   email2 VARCHAR(100),
   telefoon VARCHAR(100),
   telefoon2 VARCHAR(100),
   telefoon3 VARCHAR(100),
   tel_relatie VARCHAR(100),
   tel_relatie2 VARCHAR(100),
   tel_relatie3 VARCHAR(100),
   email_new VARCHAR(100),
   email2_new VARCHAR(100),
   telefoon_new VARCHAR(100),
   telefoon2_new VARCHAR(100),
   telefoon3_new VARCHAR(100),
   date_offset INT DEFAULT 0,
   anummer FLOAT,
   anummer_new FLOAT,
   aanmelddatum DATETIME,
   datumvertrek DATETIME,
   datumvestiging DATETIME,
   gebouder1 DATETIME,
   gebouder2 DATETIME,
   geboortedatum DATETIME,
   overlijdensdatum DATETIME,
   schoolmutatie DATETIME,
   uitzorg DATETIME,
   aanmelddatum_new DATETIME,
   datumvertrek_new DATETIME,
   datumvestiging_new DATETIME,
   gebouder1_new DATETIME,
   gebouder2_new DATETIME,
   geboortedatum_new DATETIME,
   overlijdensdatum_new DATETIME,
   schoolmutatie_new DATETIME,
   uitzorg_new DATETIME,
   geblandcode SMALLINT,
   geblandouder1code SMALLINT,
   geblandouder2code SMALLINT,
   geblandcode_new SMALLINT,
   geblandouder1code_new SMALLINT,
   geblandouder2code_new SMALLINT,
   nationaliteit SMALLINT,
   nationaliteit_new SMALLINT,
   INDEX IX_anc_casn (casnummer),
   INDEX IX_anc_eml (email),
   INDEX IX_anc_eml2 (email2),
   INDEX IX_anc_tel (telefoon),
   INDEX IX_anc_tel2 (telefoon2),
   INDEX IX_anc_tel3 (telefoon3)
);

DROP TABLE IF EXISTS an_cross_bsn;
CREATE TABLE an_cross_bsn (
   rowid INT IDENTITY(1,1),
   bsn INT,
   bsn_new INT,
   INDEX IX_anc_bsn (bsn),
   INDEX IX_anc_bsn_new (bsn_new)
);


-- Persoonsgegevens
ALTER TABLE persoonsgegevens ADD rowid INT;
CREATE INDEX IX_anp_rowid ON persoonsgegevens (rowid);

-- Woonverbanden
ALTER TABLE woonverbanden ADD rowid INT;
CREATE INDEX IX_wvb_rowid ON woonverbanden (rowid);

-- DossierItems
ALTER TABLE DossierItems ADD rowid INT;
CREATE INDEX IX_di_rowid ON DossierItems (rowid);

-- Activiteiten
ALTER TABLE Activiteiten ADD rowid INT;
CREATE INDEX IX_at_rowid ON Activiteiten (rowid);

-- Planning
ALTER TABLE planning ADD rowid INT;
CREATE INDEX IX_plan_rowid ON planning (rowid);

-- Afspraken
ALTER TABLE afspraken ADD rowid INT;
CREATE INDEX IX_afsp_rowid ON afspraken (rowid);

-- AfsprakenHist
ALTER TABLE afsprakenhist ADD rowid INT;
CREATE INDEX IX_afsph_rowid ON afsprakenhist (rowid);

-- ClientRelaties
ALTER TABLE ClientRelaties ADD rowid1 INT;
ALTER TABLE ClientRelaties ADD rowid2 INT;
CREATE INDEX IX_anp_rowid1 ON ClientRelaties (rowid1);
CREATE INDEX IX_anp_rowid2 ON ClientRelaties (rowid2);

-- BetrokkenZorgverleners
ALTER TABLE betrokkenzorgverleners ADD rowid INT;
CREATE INDEX IX_anb_rowid ON betrokkenzorgverleners (rowid);


-- Use Archief Database!
-- set AUTO COMMIT ON again
ALTER TABLE Archief ADD dl INT;
ALTER TABLE Archief ADD rowid INT;
CREATE INDEX IX_arch_rowid ON Archief (rowid);

UPDATE Archief
SET dl = datalength(data);




-- Disconnect from DB if you only have a single connection license
-- Run ano-x-db.php script
-- Using ORIGINAL DB
-- This takes about 12min
-- 25min for RU

-- If you had to disconnect, reconnect to the ORIGINAL DB and set AUTOCOMMIT on again
-- Run the x-ref id queries below This takes a couple of minutes in total

update p
set p.rowid = ac.rowid
from Persoonsgegevens p
    inner join an_cross ac on
        p.casnummer = ac.casnummer;

update wvb
set wvb.rowid = ac.rowid
from woonverbanden wvb
    inner join an_cross ac on
        wvb.casnummer = ac.casnummer;

update act
set act.rowid = ac.rowid
from Activiteiten act
    inner join an_cross ac on
        act.casnummer = ac.casnummer;

update pl
set pl.rowid = ac.rowid
from planning pl
    inner join an_cross ac on
        pl.casnummer = ac.casnummer;

update a
set a.rowid = ac.rowid
from afspraken a
    inner join an_cross ac on
        a.casnummer = ac.casnummer;

update ah
set ah.rowid = ac.rowid
from afsprakenhist ah
    inner join an_cross ac on
        ah.casnummer = ac.casnummer;

update cr
set
cr.rowid1 = ac.rowid
from ClientRelaties cr
    inner join an_cross ac on
        cr.casnummer1 = ac.casnummer;

update cr
set
cr.rowid2 = ac.rowid
from ClientRelaties cr
    inner join an_cross ac on
        cr.casnummer2 = ac.casnummer;

update di
set di.rowid = ac.rowid
from dossieritems di
    inner join an_cross ac on
        di.casnummer = ac.casnummer;

update bz
set bz.rowid = ac.rowid
from betrokkenzorgverleners bz
    inner join an_cross ac on
        bz.casnummer = ac.casnummer;

-- For HN these fields only contain a couple of values, for RU none.
UPDATE afsprakenhist
SET polisnummer = '',
verzekeraarid = 0;


-- Use name of the database and schema to access Archief tabel in other database
-- HN_ARCH_ORG_007 and dbo in the example below
update arch
set arch.rowid = ac.rowid
from HN_ARCH_ORG_007.dbo.Archief arch
    inner join an_cross ac on
        arch.casnummer = ac.casnummer;

-- Duplicate DB and use copy to maintain the x-reference in the original
-- Check log and remove records that were not updated (???)
-- This takes 10 - 20 minutes


-- Execute dropTables-2-anonimize-Database.sql to remove (currently) unused tables
-- This takes a couple of minutes

-- Add VIEW to Arcief Table to Anonimous version
-- Use correct DB Name, in example below HN_ARCH_ANON_010
DROP VIEW IF EXISTS Archief;
CREATE VIEW Archief AS SELECT * FROM HN_ARCH_ANON_010.dbo.Archief;


-- Disconnect from DB if you only have a single connection license
-- change the appconfig.php setting to the COPY DB!
--- Run ano-db.php script
-- This takes about 4,5hrs  17:09 -  21:30 for HN
-- More than 5.5 hours for RU
-- If you had to disconnect, reconnect to the COPY DB and set AUTOCOMMIT on again

-- Recreate persoonsgegevens table without Casnummer being an identity field
-- as identity fields can ot be changed
CREATE TABLE
    an_persoonsgegevens
    (
        CasNummer INT,
        SaveID INT,
        Achternaam NVARCHAR(200) COLLATE Latin1_General_CI_AI NOT NULL,
        Voorvoegsel NVARCHAR(10) COLLATE Latin1_General_CI_AI,
        Voorletters NVARCHAR(24) COLLATE Latin1_General_CI_AI,
        Roepnaam NVARCHAR(20) COLLATE Latin1_General_CI_AI,
        Geboortedatum DATETIME,
        Geslacht TINYINT NOT NULL,
        Bureau SMALLINT,
        ClientSrt TINYINT NOT NULL,
        GehoorBureau SMALLINT,
        GehoorStat TINYINT NOT NULL,
        Polisnummer VARCHAR(20) COLLATE Latin1_General_CI_AS,
        VerzekeraarID INT,
        Extern FLOAT(53),
        InZorg DATETIME,
        UitZorg DATETIME,
        PersBijz VARCHAR(5) COLLATE Latin1_General_CI_AS,
        PersMemo NVARCHAR(1000) COLLATE Latin1_General_CI_AS,
        Status TINYINT NOT NULL,
        Controle FLOAT(53) NOT NULL,
        PersMods INT NOT NULL,
        PersOrg INT,
        TaalCode TINYINT,
        TIMESTAMP DATETIME NOT NULL,
        UserID CHAR(4) COLLATE Latin1_General_CI_AS NOT NULL,
        HuisartsID INT,
        Aanmelddatum DATETIME,
        Verwijzersoort TINYINT,
        Email VARCHAR(100) COLLATE Latin1_General_CI_AS,
        Telefoon3 VARCHAR(15) COLLATE Latin1_General_CI_AS,
        TelefoonOms3 VARCHAR(20) COLLATE Latin1_General_CI_AS,
        Machtigingsnummer VARCHAR(12) COLLATE Latin1_General_CI_AS,
        Schoolnummer INT,
        KlasGroep VARCHAR(30) COLLATE Latin1_General_CI_AS,
        Schooljaar SMALLINT,
        Dossierlocatie SMALLINT,
        Kostengroep SMALLINT,
        Overlijdensdatum DATETIME,
        Voornamen NVARCHAR(200) COLLATE Latin1_General_CI_AI,
        Verwijsdatum DATETIME,
        VerwijzerID INT,
        Teamlocatie SMALLINT,
        BSN INT,
        BSNouder1 INT,
        BSNouder2 INT,
        NaamOuder1 NVARCHAR(200) COLLATE Latin1_General_CI_AI,
        NaamOuder2 NVARCHAR(200) COLLATE Latin1_General_CI_AI,
        IndicatieGezag TINYINT NOT NULL,
        ConsultSoort TINYINT,
        Leerjaar SMALLINT,
        Geslachtsnaam NVARCHAR(200) COLLATE Latin1_General_CI_AI,
        WidIdMethode SMALLINT DEFAULT 0 NOT NULL,
        WidIdDocType SMALLINT DEFAULT 0 NOT NULL,
        BsnBron SMALLINT DEFAULT 0 NOT NULL,
        WidIdDocNummer VARCHAR(50) COLLATE Latin1_General_CI_AS,
        ANummer FLOAT(53),
        ANummer_s VARCHAR(10),
        GbaVoorvoegsel NVARCHAR(10) COLLATE Latin1_General_CI_AI,
        SchoolMutatie DATETIME,
        DatumVertrek DATETIME,
        DatumVestiging DATETIME,
        GebOuder1 DATETIME,
        GebOuder2 DATETIME,
        IndicatieGeheim TINYINT NOT NULL,
        GbaIndicatieDat DATETIME,
        Email2 VARCHAR(100) COLLATE Latin1_General_CI_AS,
        DossierToegang VARCHAR(250) COLLATE Latin1_General_CI_AS,
        DossierEigenaar INT,
        GebLandcode SMALLINT,
        GebLandOuder1code SMALLINT,
        GebLandOuder2code SMALLINT,
        Nationaliteit SMALLINT,
        GebPlaats NVARCHAR(80) COLLATE Latin1_General_CI_AS,
        Titelcode VARCHAR(2) COLLATE Latin1_General_CI_AS,
        Telefoon VARCHAR(15) COLLATE Latin1_General_CI_AS,
        Telefoon2 VARCHAR(15) COLLATE Latin1_General_CI_AS,
        Tel_relatie VARCHAR(100),
        Tel_relatie2 VARCHAR(100),
        Tel_relatie3 VARCHAR(100),
        Password NVARCHAR(128) COLLATE Latin1_General_CI_AS,
        LastPasswordChangedDate DATETIME,
        Pincode NVARCHAR(128) COLLATE Latin1_General_CI_AS,
        LastPincodeSentDate DATETIME,
        LastLoginDate DATETIME,
        LastLoginAttempt DATETIME,
        FailedLoginCount INT,
        rowid INT,
        IsVluchteling TINYINT NOT NULL DEFAULT 0,
        VaccOnly TINYINT NOT NULL DEFAULT 0,
        INDEX IX_anp_casn (casnummer),
        INDEX IX_anp_rowid (rowid)
    );

INSERT INTO an_persoonsgegevens (CasNummer, SaveID, Achternaam, Voorvoegsel,Voorletters,Roepnaam,Geboortedatum,Geslacht,Bureau,ClientSrt,GehoorBureau,GehoorStat,Polisnummer,VerzekeraarID,Extern,InZorg,UitZorg,PersBijz,PersMemo,Status,Controle,PersMods,PersOrg,TaalCode,TIMESTAMP,UserID,HuisartsID,Aanmelddatum,Verwijzersoort,Email,Telefoon3,TelefoonOms3,Machtigingsnummer,Schoolnummer,KlasGroep,Schooljaar,Dossierlocatie,Kostengroep,Overlijdensdatum,Voornamen,Verwijsdatum,VerwijzerID,Teamlocatie,BSN,BSNouder1,BSNouder2,NaamOuder1,NaamOuder2,IndicatieGezag,ConsultSoort,Leerjaar,Geslachtsnaam,WidIdMethode,WidIdDocType,BsnBron,WidIdDocNummer,ANummer,GbaVoorvoegsel,SchoolMutatie,DatumVertrek,DatumVestiging,GebOuder1,GebOuder2,IndicatieGeheim,GbaIndicatieDat,Email2,DossierToegang,DossierEigenaar,GebLandcode,GebLandOuder1code,GebLandOuder2code,Nationaliteit,GebPlaats,Titelcode,Telefoon,Telefoon2,Password,LastPasswordChangedDate,Pincode,LastPincodeSentDate,LastLoginDate,LastLoginAttempt,FailedLoginCount,rowid, IsVluchteling, VaccOnly)
SELECT CasNummer, SaveID, Achternaam, Voorvoegsel,Voorletters,Roepnaam,Geboortedatum,Geslacht,Bureau,ClientSrt,GehoorBureau,GehoorStat,Polisnummer,VerzekeraarID,Extern,InZorg,UitZorg,PersBijz,PersMemo,Status,Controle,PersMods,PersOrg,TaalCode,TIMESTAMP,UserID,HuisartsID,Aanmelddatum,Verwijzersoort,Email,Telefoon3,TelefoonOms3,Machtigingsnummer,Schoolnummer,KlasGroep,Schooljaar,Dossierlocatie,Kostengroep,Overlijdensdatum,Voornamen,Verwijsdatum,VerwijzerID,Teamlocatie,BSN,BSNouder1,BSNouder2,NaamOuder1,NaamOuder2,IndicatieGezag,ConsultSoort,Leerjaar,Geslachtsnaam,WidIdMethode,WidIdDocType,BsnBron,WidIdDocNummer,ANummer,GbaVoorvoegsel,SchoolMutatie,DatumVertrek,DatumVestiging,GebOuder1,GebOuder2,IndicatieGeheim,GbaIndicatieDat,Email2,DossierToegang,DossierEigenaar,GebLandcode,GebLandOuder1code,GebLandOuder2code,Nationaliteit,GebPlaats,Titelcode,Telefoon,Telefoon2,Password,LastPasswordChangedDate,Pincode,LastPincodeSentDate,LastLoginDate,LastLoginAttempt,FailedLoginCount,rowid,IsVluchteling,VaccOnly FROM persoonsgegevens;

ALTER TABLE Afspraken DROP FK_Afspraken_Persoonsgegevens;
ALTER TABLE AfsprakenHist DROP FK_AfsprakenHist_Persoonsgegevens;
ALTER TABLE Woonverbanden DROP FK_Woonverbanden_Persoonsgegevens;
ALTER TABLE Woonverbanden_hist DROP FK_Woonverbanden_hist_Persoonsgegevens;
ALTER TABLE DossierItems DROP FK_DossierItems_Persoonsgegevens;
ALTER TABLE Planning DROP FK_Planning_Persoonsgegevens;
ALTER TABLE Notities DROP FK_Notities_Persoonsgegevens;
ALTER TABLE Taken DROP FK_Taken_Persoonsgegevens;
ALTER TABLE Activiteiten DROP FK_Activiteiten_Persoonsgegevens;
ALTER TABLE Overdrachten DROP FK_Overdrachten_Persoonsgegevens;

ALTER TABLE ClientRelaties DROP FK_ClientRelaties_Persoonsgegevens1;
ALTER TABLE ClientRelaties DROP FK_ClientRelaties_Persoonsgegevens2;

ALTER TABLE BetrokkenZorgverleners DROP FK_BetrokkenZorgverleners_Persoonsgegevens;
ALTER TABLE ServiceportaalEmailadressen DROP FK_ServiceportaalEmailadressen_Persoonsgegevens;
ALTER TABLE ServiceportaalClientLink DROP FK_ServiceportaalClientLink_Persoonsgegevens;
ALTER TABLE ToekenningsBeschikkingen DROP FK_ToekenningsBeschikkingen_Persoonsgegevens;
ALTER TABLE TijdelijkeDossierToegang DROP FK_TijdelijkeDossierToegang_Persoonsgegevens_1;
ALTER TABLE DossierToegangsLogs DROP FK_DossierToegangsLogs_Persoonsgegevens_1;

DROP TABLE persoonsgegevens;
sp_rename 'an_persoonsgegevens','persoonsgegevens';

update p
set p.bsn = acb.bsn_new
from Persoonsgegevens p
    inner join an_cross_bsn acb on
        p.bsn = acb.bsn;

update p
set p.BSNouder1 = acb.bsn_new
from Persoonsgegevens p
    inner join an_cross_bsn acb on
        p.BSNouder1 = acb.bsn;

update p
set p.BSNouder2 = acb.bsn_new
from Persoonsgegevens p
    inner join an_cross_bsn acb on
        p.BSNouder2 = acb.bsn;

update p
set
p.casnummer = ac.casnummer_new,
p.email     = ac.email_new,
p.email2    = ac.email2_new,
p.telefoon  = ac.telefoon_new,
p.telefoon2 = ac.telefoon2_new,
p.telefoon3 = ac.telefoon3_new,
p.tel_relatie  = ac.tel_relatie,
p.tel_relatie2  = ac.tel_relatie2,
p.tel_relatie3  = ac.tel_relatie3,
p.anummer   = ac.anummer_new,
p.aanmelddatum     = ac.aanmelddatum_new,
p.datumvertrek     = ac.datumvertrek_new,
p.datumvestiging   = ac.datumvestiging_new,
p.gebouder1        = ac.gebouder1_new,
p.gebouder2        = ac.gebouder2_new,
p.geboortedatum    = ac.geboortedatum_new,
p.overlijdensdatum = ac.overlijdensdatum_new,
p.schoolmutatie    = ac.schoolmutatie_new,
p.uitzorg          = ac.uitzorg_new,
p.geblandcode       = ac.geblandcode_new,
p.geblandouder1code = ac.geblandouder1code_new,
p.geblandouder2code = ac.geblandouder2code_new,
p.nationaliteit     = ac.nationaliteit_new
from Persoonsgegevens p
    inner join an_cross ac on
        p.rowid = ac.rowid;

UPDATE persoonsgegevens SET ANummer_S = cast(cast(anummer as decimal(10,0)) as varchar(10));

-- Set random records to VaccOnly
UPDATE persoonsgegevens SET VaccOnly = 1
WHERE CasNummer IN (
SELECT pg.casnummer FROM persoonsgegevens pg
LEFT JOIN scholen s ON (s.schoolnummer = pg.Schoolnummer)
LEFT JOIN Woonverbanden wv ON (wv.Casnummer = pg.CasNummer)
WHERE (NOT s.postcode IS NULL AND NOT wv.postcode IS NULL)
AND (CAST(SUBSTRING(s.postcode, 1,3) AS INTEGER) - CAST(SUBSTRING(wv.postcode, 1,3) AS INTEGER)) < -500);

UPDATE Persoonsgegevens
SET GebPlaats = '',
DossierLocatie = '',
Extern = '',
PersOrg = '',
SaveID = '',
Controle = '',
DossierEigenaar = '',
DossierToegang = '',
FailedLoginCount = '',
GbaIndicatieDat = '',
Kostengroep = '',
LastLoginAttempt = '',
LastLoginDate = '',
LastPasswordChangedDate = '',
LastPincodeSentDate = '',
Machtigingsnummer = '',
Password = '',
Pincode = '',
Polisnummer = '',
TaalCode = '',
Teamlocatie = '',
TelefoonOms3 = '',
Titelcode = '',
UserID = '',
Verwijsdatum = '',
Verwijzersoort = '',
WidIdDocNummer = '',
WidIdDocType = '';

update act
set
act.casnummer = ac.casnummer_new
from Activiteiten act
    inner join an_cross ac on
        act.rowid = ac.rowid;

update wvb
set wvb.casnummer = ac.casnummer_new
from woonverbanden wvb
    inner join an_cross ac on
        wvb.rowid = ac.rowid;

update pl
set
pl.casnummer = ac.casnummer_new
from planning pl
    inner join an_cross ac on
        pl.rowid = ac.rowid;

update a
set
a.casnummer = ac.casnummer_new
from afspraken a
    inner join an_cross ac on
        a.rowid = ac.rowid;

update ah
set
ah.casnummer = ac.casnummer_new
from afsprakenhist ah
    inner join an_cross ac on
        ah.rowid = ac.rowid;

update cr
set
cr.casnummer1 = ac.casnummer_new
from ClientRelaties cr
    inner join an_cross ac on
        cr.rowid1 = ac.rowid;

update cr
set
cr.casnummer2 = ac.casnummer_new
from ClientRelaties cr
    inner join an_cross ac on
        cr.rowid2 = ac.rowid;

update di
set
di.casnummer = ac.casnummer_new
from dossieritems di
    inner join an_cross ac on
        di.rowid = ac.rowid;

update bz
set
bz.casnummer = ac.casnummer_new
from betrokkenzorgverleners bz
    inner join an_cross ac on
        bz.rowid = ac.rowid;


-- Use name of the database and schema to access Archief tabel in other database
-- HN_ARCH_ANON_010 and dbo in the example below
update arch
set
arch.casnummer = ac.casnummer_new
from HN_ARCH_ANON_010.dbo.Archief arch
    inner join an_cross ac on
        arch.rowid = ac.rowid;


UPDATE dossieritems
SET UserId = '';

UPDATE diagitems
SET UserId = '';

UPDATE DiagRubrieken
SET UserId = '';

UPDATE dossierDataVal
SET UserId = '';

UPDATE InterventieItems
SET UserId = '';

ALTER TABLE Woonverbanden DROP FK_Woonverbanden_Wijken;
UPDATE Woonverbanden
SET UserId = '',
Wijknummer = 1;

ALTER TABLE Activiteiten DROP FK_Activiteiten_Medewerkers;
ALTER TABLE Activiteiten DROP FK_Activiteiten_Verzekeraars;
UPDATE Activiteiten
SET UserId = '',
MedewerkerID = '',
Polisnummer = '';

UPDATE Bureaus
SET UserId = '';

UPDATE TeamMedewerkers
SET UserId = '';

UPDATE Menukeuzes
SET UserId = '';

-- Landen - Geen anon nodig

UPDATE Medewerkers
SET UserId = '';

-- Gemeenten - Geen anon nodig

-- Verwijzers
UPDATE Verwijzers
SET Aanhef = '',
UserId = '';

-- Nationaliteiten - Geen anon nodig

-- Dossierlocaties
UPDATE Dossierlocaties
SET UserId = '';

-- Planning
UPDATE Planning
SET UserId = '';

-- Afspraken
UPDATE Afspraken
SET UserId = '';

-- Disciplines
UPDATE Disciplines
SET UserId = '',
TimeStamp = SYSDATETIME();

-- AfsprakenHist
UPDATE AfsprakenHist
SET UserId = '',
TimeStamp = SYSDATETIME();

-- ConsultSoorten
UPDATE ConsultSoorten
SET UserId = '',
TimeStamp = SYSDATETIME();

-- ActAfwijzing
UPDATE ActAfwijzing
SET UserId = '',
TimeStamp = SYSDATETIME();

-- ActiviteitCodes
UPDATE ActiviteitCodes
SET UserId = '',
TimeStamp = SYSDATETIME();

-- ActRapGroepen
UPDATE ActRapGroepen
SET UserId = '',
TimeStamp = SYSDATETIME();

-- VerwijzerSoorten
UPDATE VerwijzerSoorten
SET UserId = '',
TimeStamp = SYSDATETIME();

-- DiagSoorten
UPDATE DiagSoorten
SET UserId = '',
TimeStamp = SYSDATETIME();

-- Contactmomenten
UPDATE Contactmomenten
SET UserId = '',
TimeStamp = SYSDATETIME();

-- Schoolsoorten
UPDATE Schoolsoorten
SET UserId = '',
TimeStamp = SYSDATETIME();

-- TODO: BetrokkenZorgverleners Update?
-- BetrokkenZorgverleners
-- UPDATE
-- SET UserId = '';


-- DossierInfoData
-- No additional Anon needed

=====
-- Clear woon hist table Or recreate it and run db-pre-proc.php preprocWoonverbanden()

=====
-- When succesful DROP an_*tables in copy
-- DROP table an_cross;
-- DROP table an_cross_bsn;

-- TODO: Drop created colums dl and infotype for DossierInfoData
-- ALTER TABLE persoonsgegevens DROP COLUMN rowid;
-- ALTER TABLE woonverbanden DROP COLUMN rowid;
-- ALTER TABLE DossierItems DROP COLUMN rowid;
-- ALTER TABLE planning DROP COLUMN rowid;
-- ALTER TABLE afspraken DROP COLUMN rowid;
-- ALTER TABLE afsprakenhist DROP COLUMN rowid;
-- ALTER TABLE Archief DROP COLUMN rowid