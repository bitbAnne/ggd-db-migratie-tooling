-- Check if BSN field in Persoonsgegevens table is unique
SELECT * FROM (
SELECT BSN, count(bsn) AS cnt FROM Persoonsgegevens 
GROUP BY bsn) cntbsn
WHERE cnt > 1
ORDER BY cnt desc, bsn;