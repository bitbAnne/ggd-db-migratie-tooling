-- Check duplicate filenames in Archief table
SELECT * FROM ( 
SELECT filename, count(filename) AS cnt FROM archief 
GROUP BY filename) cntfilename 
WHERE cnt > 1 
ORDER BY cnt desc;