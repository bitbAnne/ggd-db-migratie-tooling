-- Check landcode refs
SELECT * FROM persoonsgegevens pg
JOIN landen l ON (pg.GebLandcode = l.Landcode)
WHERE NOT pg.GebLandcode IS NULL
AND l.Landcode IS NULL

SELECT * FROM persoonsgegevens pg
JOIN landen l ON (pg.GebLandOuder1code = l.Landcode)
WHERE NOT pg.GebLandOuder1code IS NULL
AND l.Landcode IS NULL

SELECT * FROM persoonsgegevens pg
JOIN landen l ON (pg.GebLandOuder2code = l.Landcode)
WHERE NOT pg.GebLandOuder1code IS NULL
AND l.Landcode IS NULL