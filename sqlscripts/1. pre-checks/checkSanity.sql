-- Check geslacht
SELECT geslacht, count(geslacht) AS cnt FROM dbo.Persoonsgegevens
WHERE geslacht NOT IN (
SELECT casWaarde AS geslacht FROM casoms
WHERE cassoort = 42)
GROUP BY geslacht;

-- Check GbaVoorvoegsel
SELECT GbaVoorvoegsel, count(GbaVoorvoegsel) AS cnt FROM Persoonsgegevens
GROUP BY GbaVoorvoegsel;

-- Check Voorvoegsel
SELECT * FROM (
SELECT Voorvoegsel, count(Voorvoegsel) AS cnt FROM Persoonsgegevens
GROUP BY Voorvoegsel) cntvv
ORDER BY cnt DESC;