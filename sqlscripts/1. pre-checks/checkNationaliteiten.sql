-- Check  nationaliteiten refs
SELECT pg.Nationaliteit FROM Persoonsgegevens pg
JOIN Nationaliteiten n ON (pg.Nationaliteit = n.Nationaliteit)
WHERE NOT pg.Nationaliteit IS NULL
AND n.Nationaliteit IS NULL