-- Check if email field in Persoonsgegevens table is unique
SELECT * FROM (
SELECT email, count(email) AS cnt FROM Persoonsgegevens 
GROUP BY email) cntemail
WHERE cnt > 1
ORDER BY cnt desc, email;