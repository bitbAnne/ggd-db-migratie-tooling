-- Create woonverband history table
DROP TABLE IF EXISTS Woonverbanden_hist;
CREATE TABLE
    Woonverbanden_hist
    (
        WoonverbandHistID INT NOT NULL IDENTITY (100, 1),
        WoonverbandID INT NOT NULL,
        Casnummer INT NOT NULL,
        Straatnaam NVARCHAR(43) COLLATE Latin1_General_CI_AS,
        Huisnummer NVARCHAR(19) COLLATE Latin1_General_CI_AS,
        HuisnummerToev VARCHAR(6) COLLATE Latin1_General_CI_AS,
        Woonplaats NVARCHAR(80) COLLATE Latin1_General_CI_AS,
        Postcode VARCHAR(7) COLLATE Latin1_General_CI_AS,
        PeriodeVan DATETIME,
        PeriodeTot DATETIME,
        IndicatieGeheim TINYINT NOT NULL DEFAULT 0,
        IsWVAdres TINYINT NOT NULL DEFAULT 0,
        CONSTRAINT PK_Woonverbanden_hist PRIMARY KEY (WoonverbandHistID)
    );

CREATE INDEX IX_WVH_Casnum ON Woonverbanden_hist (Casnummer);
CREATE INDEX IX_WVH_IsWVAdres ON Woonverbanden_hist (IsWVAdres);
CREATE INDEX IX_WVH_ID ON Woonverbanden_hist (WoonverbandID);

-- Set begin date for woonverband
UPDATE wv
SET wv.periodevan = pg.inzorg
FROM Woonverbanden wv
    INNER JOIN Persoonsgegevens pg ON
        wv.casnummer = pg.casnummer;

-- Set optional end date for woonverband
UPDATE wv
SET wv.periodetot = IIF(pg.uitzorg IS NULL, IIF(pg.overlijdensdatum IS NULL, pg.datumvertrek, pg.overlijdensdatum), pg.uitzorg)
FROM Woonverbanden wv
    INNER JOIN Persoonsgegevens pg ON
        wv.casnummer = pg.casnummer;

-- Put current address in history table
INSERT INTO Woonverbanden_hist (casnummer, woonverbandid, straatnaam, huisnummer, huisnummertoev, woonplaats, postcode, periodevan, periodetot, indicatiegeheim, iswvadres)
SELECT casnummer, woonverbandid, straatnaam, huisnummer, huisnummertoev, woonplaats, postcode, periodevan, periodetot, iif(flags = 2 OR flags = 3 OR flags = 130 OR flags = 162, 1, 0) as indicatiegeheim, 1 as iswvadres FROM Woonverbanden;