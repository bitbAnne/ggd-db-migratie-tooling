-- Add converted ANummer
ALTER TABLE persoonsgegevens ADD ANummer_s varchar(10);
UPDATE persoonsgegevens SET ANummer_S = cast(cast(anummer as decimal(10,0)) as varchar(10));

-- Check if field exists, if not, alter table
SELECT DISTINCT(COL_LENGTH('persoonsgegevens','tel_relatie3')) FROM persoonsgegevens;
ALTER TABLE persoonsgegevens ADD tel_relatie VARCHAR(100), tel_relatie2 VARCHAR(100), tel_relatie3 VARCHAR(100);