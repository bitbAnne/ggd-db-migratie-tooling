SELECT * FROM 
(
SELECT dosi.datum as gDatum, dd.data as 'Lengte in mm Geboorte' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 112) gglg
FULL JOIN
(
SELECT dosi.datum, dd.data as 'Lengte in mm' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 235) ggl
ON (ggl.datum = gglg.gdatum)
FULL JOIN
(
SELECT dosi.datum as 'mDatum', dd.data as 'Lengte in mm biologische moeder' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 238) gglm
ON (gglm.mdatum = ggl.datum)
FULL JOIN
(
SELECT dosi.datum as 'vDatum', dd.data as 'Lengte in mm biologische vader' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 240) gglv
ON (gglv.vdatum = gglm.mdatum)
ORDER BY Datum, mDatum, vdatum