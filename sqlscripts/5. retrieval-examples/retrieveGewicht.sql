SELECT * FROM 
(
SELECT dosi.datum as gDatum, dd.data as 'Gewicht in gr Geboorte' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 110) gggb
FULL JOIN
(
SELECT dosi.datum, dd.data as 'Gewicht in gram' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 245) ggg
ON (ggg.datum = gggb.gdatum)
ORDER BY datum