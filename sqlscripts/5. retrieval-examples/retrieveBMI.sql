SELECT dosi.datum as Datum, dd.data as 'BMI' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 248
ORDER BY Datum