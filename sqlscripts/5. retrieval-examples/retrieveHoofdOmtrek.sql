SELECT * FROM 
(
SELECT dosi.datum as gDatum, dd.data as 'Hoofdomtrek in mm Geboorte' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 113) gghog
FULL JOIN
(
SELECT dosi.datum, dd.data as 'Hoofdomtrek in mm' FROM dbo.DossierItems dosi
JOIN dossierdiagData dd ON (dd.ItemID = dosi.ItemID)
JOIN DiagItems di ON (di.DiagItemId = dd.DiagItemID)
JOIN casOms dsn ON (dsn.casSoort = 37 and dsn.casWaarde = dosi.DossierSoort) -- Dossiersoort in casOms
WHERE dosi.Casnummer = 269081
AND di.BDSnummer = 252) ggho
ON (ggho.datum = gghog.gdatum)
ORDER BY datum