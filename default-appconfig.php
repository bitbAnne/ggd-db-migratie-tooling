<?php

/*
 * This file is the base configuration file
 * It should be called from all entry points in the application
 *
 * It sets up all default enviroment settings like root directory, web url, database connection etc.
 * author: AJR
 *
 */
unset($APPCFG);
unset($DB);
unset($DBC);
global $APPCFG, $DB, $DBC;
$APPCFG = new stdClass();
$APPCFG->webroot = 'localhost';

// TODO: Make way to determine correct run place
$dirroot = dirname(dirname(__FILE__)) . "\ggd-db-migratie-tooling";
$APPCFG->dirroot = $dirroot;

$APPCFG->debuglevel = E_ALL; // E_ERROR; // 0 for no errors, E_ALL for all errors, E_ERROR
                             
// Turn off all error reporting
error_reporting($APPCFG->debuglevel);

// set time zone
date_default_timezone_set('Europe/Amsterdam');

// MS SQL-databaseserver
define('DB_SERVER', 'localhost');
// define('DB_DATABASENAAM', 'GGDHNoord-20180105-GGD-HN');
// define('DB_ARCH_DATABASENAAM', 'GGDHNoord-20180105-ARCH-GGD-HN');
define('DB_DATABASENAAM', 'HN_ONLY_PERS_ANON');
define('DB_ARCH_DATABASENAAM', 'HN_ONLY_PERS_ANON');
// define('DB_DATABASENAAM', 'GGDTwente-20180105-GGD-TW');
// define('DB_ARCH_DATABASENAAM', 'GGDTwente-20180105-ARCH-GGD-TW');
define('DB_GEBRUIKERSNAAM', '');
define('DB_WACHTWOORD', '');

include_once ($APPCFG->dirroot . '/lib/db.class.php');
$DB = nl\finalist\db\initDB(DB_DATABASENAAM, null, null, DB_SERVER);
$DBC = nl\finalist\db\initDB(DB_ARCH_DATABASENAAM, null, null, DB_SERVER);

include_once ($APPCFG->dirroot . '/lib/locallib.php');

// define webadres
define('WEBROOT', $APPCFG->webroot);

?>