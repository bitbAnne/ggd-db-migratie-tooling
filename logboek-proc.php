<?php

// Parse Notities records to logboek file

include_once("constants.php");
include_once("appconfig.php");

$showProgress = true;
$cmdLine = false;

if (isset($argv)) {
    $cmdLine = true;
    if (count($argv) > 1) {
        if (strtolower($argv[1]) == '--noprogress') {
            $showProgress = false;
        }
    }
}

$time = trackTime();
$maxrecords = -1; // 1000; // TODO remove after test // -1;  


if ($cmdLine) {
    createLogboek($time, $maxrecords);
    trackTime($time);
    
}