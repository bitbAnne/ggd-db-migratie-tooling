SELECT OBJECT_SCHEMA_NAME(T.[object_id],DB_ID()) AS [Schema],  
        T.[name] AS [table_name], AC.[name] AS [column_name],  
    AC.[max_length], 
        AC.[precision], AC.[scale], case when AC.[is_nullable] = 0 then 'n' else 'j' end as 'is nullable'
FROM sys.[tables] AS T  
  INNER JOIN sys.[all_columns] AC ON T.[object_id] = AC.[object_id] 
WHERE T.[is_ms_shipped] = 0 
ORDER BY T.[name], AC.[column_id]