/*
  Auteur Kim Wijdooge voor GGD Hollands Noorden
  Datum 2018-02-02 te Alkmaar

  Conform de huidige stand van de techniek en de kennis die ik bezit over het dossier heb ik de data geanonimiseerd
  ten behoeve van verwerking door een externe partij te weten FINALIST
   
  Het betreft de MLcas database van 2015 van Allegro Solutum. 
  Ik weet niet of het intellectueel eigendom  bij de ggd ligt en of deze database overdraagbaar is zonder het intellectueel eigendomsrecht te schenden
*/


	UPDATE x
	SET x.Locatie = 'Naam1, Naam2, Naam3'
	, Email = 'naam@ggdhn.nl' -- select * 
	FROM [GGDHNoord].[dbo].[Bureaus] x
	where x.locatie not in  ('Niet actief','inactief')

	UPDATE x
	SET AGBcode = '12345678'
	FROM [GGDHNoord].[dbo].[Medewerkers] x
	WHERE AGBcode is not null

	UPDATE x
	SET Email = 'naam@ggdhn.nl'
	FROM [GGDHNoord].[dbo].[Medewerkers] x
	WHERE Email is not null

	UPDATE x
	SET Extern = '123456789'
	FROM [GGDHNoord].[dbo].[Medewerkers] x
	WHERE Extern is not null

	UPDATE x
	SET BriefNaam = REPLACE(BriefNaam,Naam,'Naam')
	FROM [GGDHNoord].[dbo].[Medewerkers] x
	WHERE BriefNaam is not null

	UPDATE x
	SET Naam = 'Naam'
	FROM [GGDHNoord].[dbo].[Medewerkers] x
	WHERE Naam is not null

	update x  
	set BriefNaam =  REPLACE(BriefNaam,achterNaam,'Naam')-- select *
	FROM [GGDHNoord].[dbo].[Medewerkers] x
	WHERE achterNaam is not null
	and BriefNaam like '%'+achterNaam+'%'

	update x  
	set Achternaam =  'Achternaam'-- select *
	FROM [GGDHNoord].[dbo].[Medewerkers] x
	WHERE achterNaam is not null
	
	update x
	set Telefoon = '012-1234567'
	from [dbo].[Persoonsgegevens] x
	where x.Telefoon is not null

	update x
	set Telefoon2 = '012-1234567'
	from [dbo].[Persoonsgegevens] x
	where x.Telefoon2 is not null

	update x
	set x.PersMemo = 'Tekst lengte '+Replace(Cast(Len(x.PersMemo) as char),' ','')+' posties'
	from [dbo].[Persoonsgegevens] x
	where Len(x.PersMemo) > 0

	update x
	set Email = 'naam@email.com'
	from [dbo].[Persoonsgegevens] x
	where Email is not null

	update x
	set BSN = '987654321'
	from [dbo].[Persoonsgegevens] x
	where BSN is not null

	Update x
	set BSNouder1 = '123456789' --'987654321'
	from [dbo].[Persoonsgegevens] x
	where BSNouder1 is not null

	Update x
	set BSNouder2 = '987654321'
	from [dbo].[Persoonsgegevens] x
	where BSNouder2 is not null

	Update x
	SET NaamOuder1 = 'Achternaam, Voornamen'
	from [dbo].[Persoonsgegevens] x
	where NaamOuder1 is not null

	Update x
	SET NaamOuder2 = 'Achternaam, Voornamen'
	from [dbo].[Persoonsgegevens] x
	where NaamOuder2 is not null
	
		Update x
	SET Geslachtsnaam = 'Achternaam'
	from [dbo].[Persoonsgegevens] x
	where Geslachtsnaam is not null
	
	Update x
	SET Achternaam = 'Achternaam'
	from [dbo].[Persoonsgegevens] x
	where Achternaam is not null

	--jongensnaam 'Lieuwe, Mehmet, Carlos'
	--meidennaam 'Sabine, Luna, Deniz'

	Update x
	SET Voornamen = 'Sabine, Luna, Deniz'
	from [dbo].[Persoonsgegevens] x
	where Geslacht = 2

	Update x
	SET Voornamen = 'Lieuwe, Mehmet, Carlos'
	from [dbo].[Persoonsgegevens] x
	where Geslacht = 1

	Update x
	SET Roepnaam = 'Lieuwe'
	from [dbo].[Persoonsgegevens] x
	where Geslacht = 1

	Update x
	SET Roepnaam = 'Deniz'
	from [dbo].[Persoonsgegevens] x
	where Geslacht = 2

	Update x
	SET Email2 = 'naam@gmail.com'
	from [dbo].[Persoonsgegevens] x
	where Email2 is not null

	update x
	SET Straatnaam = 'Straatnaam'
	,	Huisnummer = 8
	FROM  [dbo].[Woonverbanden] x
	WHERE Straatnaam is not NULL

	update x
	SET Straatnaam2 = 'Straatnaam2'
	,	Huisnummer2 = 16
	FROM  [dbo].[Woonverbanden] x
	WHERE Straatnaam2 is not NULL
	
	update x
	SET Straatnaam3 = 'Straatnaam3'
	,	Huisnummer3 = 24
	FROM  [dbo].[Woonverbanden] x
	WHERE Straatnaam3 is not NULL

	update x
	SET Straatnaam4 = 'Straatnaam4'
	,	Huisnummer3 = 32
	FROM  [dbo].[Woonverbanden] x
	WHERE Straatnaam4 is not NULL

	update x
	SET x.NaamOpenbRuimte = 'Straatnaam'
	FROM  [dbo].[Woonverbanden] x
	where x.NaamOpenbRuimte is not null

	drop table #Wpl

	with bron as ( select distinct Woonplaats from [dbo].[Woonverbanden] )
	select Min(t1.WoonverbandID) as WoonverbandId, t.Woonplaats, '       ' as Postcode
	INTO #Wpl
	from Bron t
	inner join  [dbo].[Woonverbanden] t1 on t1.woonplaats = t.Woonplaats
	group by  t.woonplaats

	update x
	set   x.Postcode = Cast(y.Postcode as char)-- select *
	from #Wpl x
	inner join [dbo].[Woonverbanden] y on y.WoonverbandID = x.WoonverbandId
	and y.Postcode is not null

	update x
	set  x.Postcode = y.Postcode 
	from [dbo].[Woonverbanden] x
	inner join #Wpl y on y.Woonplaats = x.Woonplaats

	update x
	set  x.Postcode2 = y.Postcode 
	from [dbo].[Woonverbanden] x
	inner join #Wpl y on y.Woonplaats = x.Woonplaats2

	update x
	set  x.Postcode3 = y.Postcode 
	from [dbo].[Woonverbanden] x
	inner join #Wpl y on y.Woonplaats = x.Woonplaats3

	update x
	set  x.Postcode4 = y.Postcode 
	from [dbo].[Woonverbanden] x
	inner join #Wpl y on y.Woonplaats = x.Woonplaats4

	update [GGDHNoord].[dbo].[CasLog]
	set Tekst = 'OK - info Postcode xxxx xx Naam, BSN  regelnr'
	where left(Tekst,2) = 'Ok'

	update x
	set x.Naam = 'Naam'
	,  x.netnaam = 'DCYB\Naam'
	FROM [GGDHNoord].[dbo].[CasUsers] x

	update x
	set  x.[Vraag] = 'Lengte van de tekst '+CAST(Len(x.[Vraag])as char)+' posities'
	,	x.[Advies] =  'Lengte van de tekst '+CAST(Len(x.[Advies])as char)+' posities'
	, x.[Bijz] =   'Lengte van de tekst '+CAST(Len(x.[Bijz])as char)+' posities'
	FROM [GGDHNoord].[dbo].[Overdrachten] x

	update [GGDHNoord].[dbo].[RepOpmerkingen]
	set [BSN] = '123456789'

	UPDATE [GGDHNoord].[dbo].[Taken]
	SET [Toelichting] =  'Lengte van de tekst '+CAST(Len([Toelichting])as char)+' posities'

	UPDATE x
	SET x.[Straatnaam] = 'Straatnaam'
	,x.[Huisnummer] = '8'
	,x.[Telefoon] = CASE WHEN x.[Telefoon] is not null then  '072-1234567' ELSE NULL END 
	,x.[Faxnummer]  = CASE WHEN x.[Faxnummer] is not null then  '027-7654321' ELSE NULL END 
	,x.[Email] = CASE WHEN x.[Email] is not null then 'naam@hotmail.com' ELSE NULL END 
	,x.[Contact] = CASE WHEN x.[Contact] is not null then 'Contactpersoon' ELSE NULL END 
	,x.[Info] =  'Lengte van de tekst '+CAST(Len(x.[Info])as char)+' posities'
	FROM [GGDHNoord].[dbo].[Scholen] x


	UPDATE  x
	SET x.[Naam] = 'Naam'
	,x.[Straatnaam] = 'Straatnaam'
	,x.[Huisnummer] = 8 
	,x.[Telefoon] = '072-1234567'
	,x.[AGBcode] = CASE WHEN x.AGBCode is not null THEN '01234567' ELSE NULL END
	,x.[Adresnaam] = CASE WHEN LEFT(x.[Adresnaam],1) = 'M' THEN 'Mw. Naam' ELSE 'Dhr. Naam' END 
	FROM [GGDHNoord].[dbo].[Verwijzers] x


	UPDATE [GGDHNoord].[dbo].[DossierDiagData]
	SET [CharData] =  'Lengte van de tekst '+CAST(Len([CharData])as char)+' posities'

	UPDATE x
	SET x.[Info] = 'Lengte van de tekst '+CAST(Len(x.[Info])as char)+' posities'
	FROM [GGDHNoord].[dbo].[DossierInfoData] x



