<?php

include_once("constants.php");

$newStructure = array();

// Open end product csv
$csvreadbase = fopen(GGD_DB_STRUCTURE_CSV, 'r');
if ($csvreadbase !== false) {
    while (($csvline = fgetcsv($csvreadbase, 0, ";"))) {
        $value = createValues();
        $cnt = 1;
        foreach ($csvline as $element) {
            if ($cnt > 2) {
                $value[$element] = "x";
            }
            $cnt++;
        }
        $newStructure[$csvline[TABLE_NAME]][$csvline[COLUMN_NAME]] = $value;
    }
}

// Iterate through  new structure, saving it as csv
$csvwritebase = fopen(GGD_DB_MATRIX_CSV, 'w');
$topline = array("table_name", "column_name", "MLC19T", "MLC19V", "MLC20T", "MLC20V", "HNT", "HNV", "RUT", "RUV", "TWT", "TWV", "HNANOT", "HNANOV");
fputcsv($csvwritebase, $topline, ";");
foreach ($newStructure as $table_name => $colums) {
    foreach ($colums as $colum_name => $value) {
        $csvline = array($table_name, $colum_name);
        foreach  ($value as $key => $keyvalue) {
            $csvline[] = $keyvalue;
        }
        fputcsv($csvwritebase, $csvline, ";");
    }
}

function createValues() {
    $value["MLC19T"] = " ";
    $value["MLC19V"] = " ";
    $value["MLC20T"] = " ";
    $value["MLC20V"] = " ";
    $value["HNT"] = " ";
    $value["HNV"] = " ";
    $value["RUT"] = " ";
    $value["RUV"] = " ";
    $value["TWT"] = " ";
    $value["TWV"] = " ";
    $value["HNANOT"] = " ";
    return $value;
}